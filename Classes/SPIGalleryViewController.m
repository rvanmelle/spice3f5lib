//
//  SPIGalleryViewController.m
//  touchspice
//
//  Created by Shawn Hyam on 12-03-22.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SPIGalleryViewController.h"
#import "SPISchematicViewController.h"
#import "NSManagedObjectContextHelper.h"

@interface SPIGalleryViewController ()

@end

@implementation SPIGalleryViewController

@synthesize document = _document;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (IBAction)viewSchematicAction:(id)sender {
    NSAssert(self.document, nil);
    SPISchematic *schematic = (id)[self.document.context fetchObjectForEntityName:@"SPISchematic" predicate:nil];

    
    SPISchematicViewController *vc = [[SPISchematicViewController alloc] initWithNibName:@"SPISchematicViewController" 
                                                                                  bundle:nil];
    vc.schematic = schematic;
    [self.navigationController pushViewController:vc animated:YES];
}

@end
