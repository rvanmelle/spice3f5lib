//
//  SPINetlistViewController.h
//  touchspice
//
//  Created by Shawn Hyam on 12-03-23.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BWDViewController.h"
#import "SPICircuit.h"

@interface SPINetlistViewController : BWDViewController

@property (strong, nonatomic) IBOutlet UITextView *textView;
@property (strong, nonatomic) SPICircuit *circuit;

@end
