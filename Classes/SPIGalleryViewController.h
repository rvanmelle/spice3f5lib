//
//  SPIGalleryViewController.h
//  touchspice
//
//  Created by Shawn Hyam on 12-03-22.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SPIDocument.h"

@interface SPIGalleryViewController : BWDViewController

@property (nonatomic, strong) SPIDocument *document;

- (IBAction)viewSchematicAction:(id)sender;

@end
