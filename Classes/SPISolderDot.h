//
//  SPISolderDot.h
//  touchspice
//
//  Created by Shawn Hyam on 12-04-26.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "SPIWireConnection.h"

@class SPISchematic;

@interface SPISolderDot : SPIWireConnection

@property (nonatomic, assign) CGPoint position;
@property (nonatomic, retain) SPISchematic *schematic;

@end
