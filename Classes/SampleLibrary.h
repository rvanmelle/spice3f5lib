//
//  SampleLibrary.h
//  touchspice
//
//  Created by Shawn Hyam on 10-12-23.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class SPISchematic;
@class SPIData;

@interface SampleLibrary : NSObject {

}

+ (SPISchematic*)createSchematic:(SPIData*)data;
+ (SPISchematic*)createInContextOld:(NSManagedObjectContext*)ctx;

@end
