//
//  SPIProbeSetViewController.h
//  touchspice
//
//  Created by Shawn Hyam on 10-12-30.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@class SPIProbe;
@class SPIProbeSet;

@protocol SPIProbeSetViewControllerDelegate

- (void)didSelectProbe:(SPIProbe*)probe;

@end



@interface SPIProbeSetViewController : UITableViewController<NSFetchedResultsControllerDelegate> {
	SPIProbeSet *probeSet;
	NSFetchedResultsController *fetchedResultsController;
	id<SPIProbeSetViewControllerDelegate> __unsafe_unretained delegate;
}

@property (nonatomic, retain) SPIProbeSet *probeSet;
@property (nonatomic, retain) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, assign) id<SPIProbeSetViewControllerDelegate> delegate;

@end
