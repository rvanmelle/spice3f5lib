//
//  SPIRun.h
//  touchspice
//
//  Created by Shawn Hyam on 10-12-31.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <CoreData/CoreData.h>

@class SPICircuit;
@class SPIPlot;

@interface SPIRun :  NSManagedObject {
}

@property (nonatomic, retain) NSDate * timestamp;
@property (nonatomic, retain) SPICircuit * circuit;
@property (nonatomic, retain) NSSet* plots;

@end


@interface SPIRun (CoreDataGeneratedAccessors)
- (void)addPlotsObject:(SPIPlot *)value;
- (void)removePlotsObject:(SPIPlot *)value;
- (void)addPlots:(NSSet *)value;
- (void)removePlots:(NSSet *)value;

@end

