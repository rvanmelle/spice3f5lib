//
//  SPIIntParam.m
//  touchspice
//
//  Created by Reid van Melle on 12-04-01.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SPIIntParam.h"


@implementation SPIIntParam

@dynamic defaultValue;
@dynamic maxValue;
@dynamic minValue;
@dynamic validValues;

@end
