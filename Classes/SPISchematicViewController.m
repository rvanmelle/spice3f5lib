//
//  touchspiceViewController.m
//  touchspice
//
//  Created by Shawn Hyam on 10-12-18.
//  Copyright (c) 2010 Brierwood Design Cooperative. All rights reserved.
//

#import "SPISchematicViewController.h"
#import "SPISchematicView.h"
#import <CoreData/CoreData.h>
#import "SampleLibrary.h"
#import "SPIProbeSetViewController.h"
#import "SPIData.h"
#import "SPIVoltageProbe.h"
#import "SPINet.h"
#import "SPISchematic.h"
#import "SPIAnalysis.h"
#import "SPINetlister.h"
#import "SPIWire.h"
#import "SPIProbesViewController.h"
#import "SPIPropertyViewController.h"
#import "SPIPrimitiveGalleryViewController.h"
#import "SPIAnalysisViewController.h"
#import "SPIInstancePosition.h"
#import "SPINetlistViewController.h"
#import "SPIRunsViewController.h"
#import "SPISchematic+Connectivity.h"
#import "NSManagedObject+BWDHelper.h"
#import "NSManagedObjectContextHelper.h"
#import "SPIRoutingGrid.h"
#import "SPISolderDot.h"
#import "SPIPin.h"
#import "SPIPrimitive.h"
#import "NSManagedObject+BWDHelper.h"
#import "NSManagedObjectContextHelper.h"
#import "SPIProbeSet.h"
#import "SPICurrentProbe.h"

#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIGestureRecognizerSubclass.h>

@interface SPITapComponentGestureRecognizer : UITapGestureRecognizer
@property (nonatomic, retain) SPIComponentView *tappedView; 
@end

@implementation SPITapComponentGestureRecognizer

@synthesize tappedView = _tappedView;

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesEnded:touches withEvent:event];
    
    NSLog(@"ended");
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
    
    NSLog(@"began");
    if (self.state == UIGestureRecognizerStatePossible) {
        CGPoint pt = [[touches anyObject] locationInView:self.view];	
        UIView *view = [self.view hitTest:pt withEvent:nil];
        self.tappedView = (SPIComponentView*)view;
        
        if (![view isKindOfClass:[SPIComponentView class]]) {
            self.state = UIGestureRecognizerStateFailed;
            self.tappedView = nil;
        }
    } 

}

@end



@interface SPIDragInstanceGestureRecognizer : UIPanGestureRecognizer
@property (nonatomic, strong) SPIInstanceView *draggingView;
@end

@implementation SPIDragInstanceGestureRecognizer

@synthesize draggingView = _draggingView;

- (void)configureView {
    switch (self.state) {
        case UIGestureRecognizerStateBegan:
        case UIGestureRecognizerStateChanged:
        case UIGestureRecognizerStatePossible:
            _draggingView.highlighted = YES;
            break;
            
        default:
            _draggingView.highlighted = NO;
            break;
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
    
    if (self.state == UIGestureRecognizerStatePossible) {
        CGPoint pt = [[touches anyObject] locationInView:self.view];	
        UIView *view = [self.view hitTest:pt withEvent:nil];
        if ([view isKindOfClass:[SPIInstanceView class]]) {
            self.draggingView = (id)view;
        } else {
            self.state = UIGestureRecognizerStateFailed;
        } 
    }
    [self configureView];
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesCancelled:touches withEvent:event];
    [self configureView];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesEnded:touches withEvent:event];
    [self configureView];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesMoved:touches withEvent:event];
    [self configureView];
}

@end



@interface SPICreateWireGestureRecognizer : UIPanGestureRecognizer 
@property (nonatomic, retain) SPIComponentView *startView, *endView;  // valid views are terminal, solder dot, wire
@property (nonatomic, assign) CGPoint lastCheckpoint;
@end

@implementation SPICreateWireGestureRecognizer

@synthesize startView = _startView;
@synthesize endView = _endView;
@synthesize lastCheckpoint = _lastCheckpoint;

- (BOOL)isValidView:(UIView*)view {
    BOOL rightKind = ([view isKindOfClass:[SPITerminalView class]] || [view isKindOfClass:[SPIWireView class]]);
    
    if ([view isKindOfClass:[SPITerminalView class]]) {
        SPIComponentView *componentView = (SPIComponentView*)view;
        SPINet *net = [componentView.component valueForKey:@"net"];
        if (net) return NO;
    }
    
    // TODO we should probably explicitly tell this method we are searching for a valid endView
    if (rightKind && self.startView) {
        // if the start and endpoints already share the same net, this is invalid
        SPINet *startNet = [self.startView.component valueForKey:@"net"];
        SPIComponentView *endView = (SPIComponentView*)view;
        SPINet *endNet = [endView.component valueForKey:@"net"];
        
        NSLog(@"%@ %@", startNet, endNet);
        
        if (startNet && [startNet.objectID isEqual:endNet.objectID]) {
            return NO;
        }
    }

    return rightKind;
}

- (void)setStartView:(SPIComponentView *)startView {
    _startView.highlighted = NO;
    _startView = startView;
    _startView.highlighted = YES;
}


- (void)setEndView:(SPIComponentView *)endView {
    _endView.highlighted = NO;
    _endView = endView;
    _endView.highlighted = YES;
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesEnded:touches withEvent:event];
    
    if (self.state == UIGestureRecognizerStateEnded) {
        CGPoint pt = [[touches anyObject] locationInView:self.view];	
        UIView *view = [self.view hitTest:pt withEvent:nil];
        if ([self isValidView:view]) {
            self.endView = (id)view;
        } else {
            self.state = UIGestureRecognizerStateFailed;
            self.startView = nil;
            self.endView = nil;
        }
    } else if (self.state == UIGestureRecognizerStateFailed) {
        self.startView = nil;
        self.endView = nil;
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesMoved:touches withEvent:event];
    
    if (self.state == UIGestureRecognizerStateChanged) {
        CGPoint pt = [[touches anyObject] locationInView:self.view];	
        UIView *view = [self.view hitTest:pt withEvent:nil];
        
        if ([self isValidView:view]) {
            self.endView = (id)view;
        } else {
            self.endView = nil;
        }
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
    
    if (self.state == UIGestureRecognizerStatePossible) {
        CGPoint pt = [[touches anyObject] locationInView:self.view];	
        UIView *view = [self.view hitTest:pt withEvent:nil];
        if ([self isValidView:view]) {
            self.startView = (id)view;
        } else {
            self.startView = nil;
            self.endView = nil;
            self.state = UIGestureRecognizerStateFailed;
        } 
    } 
}

@end


@interface SPISchematicViewController ()
@property (nonatomic, retain) UIViewController *analysesViewController;
@end

@implementation SPISchematicViewController {
    UIPopoverController *_popoverController;
    UIView *_pieMenu;
    NSMutableDictionary *_componentViews;
    
    NSManagedObjectContext *_context, *_scratchContext;
}
@synthesize propertiesButton;
@synthesize analysesButton;
@synthesize primitivesButton;
@synthesize netsLabel;
@synthesize configurableButton;

@synthesize schematic, scrollView, schematicView, probeSetButton, netlistButton;
@synthesize analysesViewController = _analysesViewController;
@synthesize selectedViews = _selectedViews;

- (UIViewController*)analysesViewController {
    if (!_analysesViewController) {
        SPIAnalysisViewController *vc = [[SPIAnalysisViewController alloc] initWithNibName:@"SPIAnalysisViewController" bundle:nil];
        vc.circuit = self.schematic;
        vc.contentSizeForViewInPopover = CGSizeMake(320, 320);
        
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
        _analysesViewController = nav;
    }
    return _analysesViewController;
}

#pragma mark - SPIPrimitiveGalleryDelegate

- (void)didSelectPrimitive:(id)primitive {
    NSLog(@"didSelectPrimitive: %@", primitive);
    
    SPIInstance *instance = [SPIInstance instanceFromPrimitive:primitive inCircuit:self.schematic];
    
    SPIInstancePosition *position = [NSEntityDescription insertNewObjectForEntityForName:@"SPIInstancePosition" inManagedObjectContext:self.schematic.managedObjectContext];
    position.transform = CGAffineTransformMakeTranslation(140, 140);
    position.instance = instance;
    position.schematic = self.schematic;
    
    
    // TODO set the instance selected so it will be highlighted in the schematic
    // TODO find an open area to place it in if possible
}

#pragma mark -

- (void)presentInPopover:(UIViewController*)vc fromRect:(CGRect)r {
    if (!_popoverController) {
        _popoverController = [[UIPopoverController alloc] initWithContentViewController:vc];
        _popoverController.passthroughViews = [NSArray arrayWithObjects:self.primitivesButton, self.analysesButton, nil];
    } else {
        _popoverController.contentViewController = vc;
        [_popoverController setPopoverContentSize:vc.contentSizeForViewInPopover animated:YES];
    }
    [_popoverController presentPopoverFromRect:r inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

- (IBAction)selectPlotsAction:(id)sender {
    // TODO there must be a probeSet and run in existence for any data to be displayed here;
    // we could possibly get away without requiring a run
   
    /*
    // choose an analysis and probeset at random
	SPIAnalysis *currentAnalysis = [self.schematic.analyses anyObject];
	SPIProbeSet *probeSet = [currentAnalysis.probeSets anyObject];
    
    // choose a run at random
    SPIRun *run = [self.schematic.runs anyObject];
    */
    
    SPIProbesViewController *vc = [[SPIProbesViewController alloc] initWithNibName:@"SPIPlotsViewController" 
                                                                          bundle:nil];
    vc.circuit = self.schematic;
    
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)selectPropertiesAction:(id)sender {
    // choose an instance at random
    SPIInstance *instance = [self.schematic.instances anyObject];
    
    SPIPropertyViewController *vc = [[SPIPropertyViewController alloc] initWithNibName:@"SPIPropertyViewController" bundle:nil];
    vc.instance = instance;
    [self.navigationController pushViewController:vc animated:YES];
    
}

- (IBAction)selectPrimitivesAction:(UIButton*)sender {
    SPIPrimitiveGalleryViewController *vc = [[SPIPrimitiveGalleryViewController alloc] initWithNibName:@"SPIPrimitiveGalleryViewController" bundle:nil];
    vc.circuit = self.schematic;
    vc.delegate = self;
    
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
    [self presentInPopover:nav fromRect:sender.frame];
/*
    //[self.navigationController pushViewController:vc animated:YES];
    
    [self addChildViewController:vc];
    [self.view addSubview:vc.view];
    CGSize sz = vc.view.frame.size;
    vc.view.frame = CGRectMake(CGRectGetWidth(self.view.bounds)-sz.width, 
                               0, sz.width, sz.height);
    */
}

- (IBAction)selectAnalysesAction:(UIButton*)sender {
    [self presentInPopover:self.analysesViewController fromRect:sender.frame];
}

- (IBAction)selectNetlistAction:(UIButton*)sender {
    SPINetlistViewController *vc = [[SPINetlistViewController alloc] initWithNibName:@"SPINetlistViewController" bundle:nil];
    vc.circuit = self.schematic;
    //[self.navigationController pushViewController:vc animated:YES];
    [self presentInPopover:vc fromRect:sender.frame];
}

- (IBAction)selectRunsAction:(id)sender {
    SPIRunsViewController *vc = [[SPIRunsViewController alloc] initWithNibName:@"SPIRunsViewController" bundle:nil];
    vc.circuit = self.schematic;
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)validateAction:(id)sender {
    NSArray *errors = [self.schematic validateSchematicView];
    NSString *message = [errors componentsJoinedByString:@"\n"];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Errors" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}

- (IBAction)probeSetButtonAction {
#if 0
	// FIXME for now, we're just picking a probeSet at random because there is only one
	
	SPIProbeSetViewController *psvc = [[SPIProbeSetViewController alloc] initWithNibName:@"SPIProbeSetViewController" bundle:nil];
	psvc.contentSizeForViewInPopover = CGSizeMake(320, 320);
	SPIAnalysis *currentAnalysis = [self.schematic.analyses anyObject];
	psvc.probeSet = [currentAnalysis.probeSets anyObject];
	psvc.delegate = self;
	
	UIPopoverController *pc = [[UIPopoverController alloc] initWithContentViewController:psvc];
	[pc presentPopoverFromRect:self.probeSetButton.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
#endif
    
}


/*
// The designated initializer. Override to perform setup that is required before the view is loaded.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/

- (void)scrollViewWillBeginZooming:(UIScrollView *)scrollView withView:(UIView *)view {
    [_pieMenu removeFromSuperview];
    _pieMenu = nil;
}

- (NSSet*)traceWire:(SPIWire*)wire {
    NSMutableSet *result = [NSMutableSet setWithCapacity:64];
    NSMutableSet *untracedWires = [NSMutableSet setWithObject:wire];
    
    while (untracedWires.count > 0) {
        SPIWire *wire = [untracedWires anyObject];
        [untracedWires removeObject:wire];
        [result addObject:wire];
        
        for (SPIWireConnection *conn in wire.connections) {
            NSMutableSet *wiresToAdd = [NSMutableSet setWithSet:conn.wires];
            [wiresToAdd minusSet:result];
            [wiresToAdd minusSet:untracedWires];
            [untracedWires addObjectsFromArray:[wiresToAdd allObjects]];
        }
    }
    
    NSLog(@"result: %@", result);
    return result;
}

- (void)removeWire:(SPIWire*)wire {
    // FIXME this is crap; doesn't account for solder dots, nets disappearing, etc.
    
    SPINet *net = wire.net;
    [self.schematic.managedObjectContext deleteObject:wire];
    [wire setConnections:[NSSet set]];
    
    NSMutableSet *remainingWires = [NSMutableSet setWithSet:net.wires];
    [remainingWires removeObject:wire];
    
    [self.schematic.managedObjectContext deleteObject:net];
    
    SPIRoutingGrid *routingGrid = [[SPIRoutingGrid alloc] initWithSchematic:self.schematic];
    
    while (remainingWires.count > 0) {
        // find the wires that are still grouped together
        NSSet *netWires = [self traceWire:[remainingWires anyObject]];
        [remainingWires minusSet:netWires];
        
        NSMutableArray *terminals = [NSMutableArray arrayWithCapacity:64];
        for (SPIWire *w in netWires) {
            for (SPIWireConnection *conn in w.connections) {
                if ([conn isKindOfClass:[SPITerminal class]]) {
                    [terminals addObject:conn];
                }
            }
        }
        
        NSLog(@"terminals: %@", terminals);

        
        if (terminals.count < 2)
            continue;
        
        // reconnect everything
        SPITerminal *t1 = [terminals objectAtIndex:0];
        SPITerminal *t2 = [terminals objectAtIndex:1];
        [self connectTerminal:t1 toTerminal:t2 onGrid:routingGrid inContext:_context];
        
        for (int i=2; i<terminals.count; i++) {
            SPITerminal *t = [terminals objectAtIndex:i];
            [self connectTerminal:t toNet:t1.net onGrid:routingGrid];
        }

    }
}



- (void)deleteWire:(UIButton*)sender {
    for (SPIComponentView *cv in self.selectedViews) {
        if ([cv isKindOfClass:[SPIWireView class]]) {
            SPIWireView *wv = (SPIWireView*)cv;
            [self removeWire:wv.wire];
        }
    }
    self.selectedViews = nil;
    [_pieMenu removeFromSuperview];
    _pieMenu = nil;
}

- (void)setSelectedViews:(NSSet *)selectedViews {
    // deselect all currently selected views
    for (SPIComponentView *cv in self.selectedViews) {
        cv.selected = NO;
    }
    _selectedViews = selectedViews;
    for (SPIComponentView *cv in self.selectedViews) {
        cv.selected = YES;
        
        
        if ([cv isKindOfClass:[SPIWireView class]]) {
            SPIWireView *wv = (SPIWireView*)cv;
            
            if (_pieMenu) {
                [_pieMenu removeFromSuperview];
            }
            
            
            NSArray *pts = [wv.wire allPoints:10];
            CGPoint pt = [[pts objectAtIndex:pts.count/2] CGPointValue];
            pt = [self.scrollView convertPoint:pt fromView:self.schematicView];
            

            UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            button.frame = CGRectMake(pt.x-50,pt.y-18,100,37);
            [button setTitle:@"Delete" forState:UIControlStateNormal];
            [button addTarget:self action:@selector(deleteWire:) forControlEvents:UIControlEventTouchUpInside];
            button.layer.geometryFlipped = self.scrollView.layer.geometryFlipped;  // if flipped, we must un-flip
            [self.scrollView addSubview:button];
            _pieMenu = button;
            
            
        }
        
    }
    
    
    
    
}

- (void)selectNet:(SPINet*)net {
    NSMutableSet *selection = [NSMutableSet setWithCapacity:16];
    for (SPIWire *wire in net.wires) {
        SPIWireView *wv = [_componentViews objectForKey:wire.objectID];
        [selection addObject:wv];
    }
    self.selectedViews = selection;
}

- (void)didTap:(SPITapComponentGestureRecognizer*)tgr {
#if 0
    NSLog(@"DID TAP!!!");
    if ([tgr.tappedView isKindOfClass:[SPIWireView class]]) {
        SPIWire *wire = ((SPIWireView*)tgr.tappedView).wire;
        [self selectNet:wire.net];
        return;
    }
    
    
    self.selectedView = tgr.tappedView;
    
    SPIAnalysisViewController *vc = [[SPIAnalysisViewController alloc] initWithNibName:@"SPIAnalysisViewController" bundle:nil];
    vc.circuit = self.schematic;
    vc.contentSizeForViewInPopover = CGSizeMake(320, 88);
    
    
    CGRect r = [self.view convertRect:self.selectedView.frame fromView:self.schematicView];
    [self presentInPopover:vc fromRect:r];
    NSLog(@"%@ %@ %@", self.selectedView, NSStringFromCGRect(self.selectedView.frame), NSStringFromCGRect(r));
#endif
    
    if (self.selectedViews && self.selectedViews.count == 1 && [self.selectedViews anyObject] == tgr.tappedView) {
        self.selectedViews = nil;
        return;
    }
    
    if ([tgr.tappedView isKindOfClass:[SPIWireView class]]) {
         //SPIWire *wire = ((SPIWireView*)tgr.tappedView).wire;
        //[self selectNet:wire.net];
        self.selectedViews = [NSSet setWithObject:tgr.tappedView];
        return;
    } else if ([tgr.tappedView isKindOfClass:[SPIInstanceView class]]) {
        self.selectedViews = [NSSet setWithObject:tgr.tappedView];
        return;
    } else if ([tgr.tappedView isKindOfClass:[SPITerminalView class]]) {
        self.selectedViews = [NSSet setWithObject:tgr.tappedView];
        return;
    }
    
    return;
    
    
}

- (CGPoint)gridCoordinateFromPoint:(CGPoint)point {
    int a = (point.x - 0 + 10/2) / 10;
    int b = (point.y - 0 + 10/2) / 10;
    //NSLog(@"%d %d", a, b);
    return CGPointMake(a, b);
}

- (CGPoint)pointFromGridCoordinate:(CGPoint)coord {
    int x = coord.x*10 + 0;
    int y = coord.y*10 + 0;
    return CGPointMake(x, y);
}

- (void)didDragProbe:(UIPanGestureRecognizer*)pgr {
    static SPIComponentView *highlightedView = nil;
    
    CGPoint pt = [pgr locationInView:self.schematicView];;
    UIView *view = [self.schematicView hitTest:pt withEvent:nil];

    
    CGPoint translation = [pgr translationInView:self.view];
    self.configurableButton.layer.affineTransform = CGAffineTransformMakeTranslation(translation.x, translation.y);
    
    if ([view isKindOfClass:[SPIWireView class]] || [view isKindOfClass:[SPITerminalView class]]) {
        highlightedView.highlighted = NO;
        highlightedView = (id)view;
        highlightedView.highlighted = YES;
    }
    
    if (pgr.state == UIGestureRecognizerStateEnded) {
        highlightedView.highlighted = NO;
        self.configurableButton.layer.affineTransform = CGAffineTransformIdentity;
        
        if (highlightedView) {
            // add a probe here!!
            //self.selectedViews = [NSSet setWithObject:highlightedView];
            
            SPIAnalysis *analysis = [NSEntityDescription insertNewObjectForEntityForName:@"SPIAnalysis" inManagedObjectContext:self.schematic.managedObjectContext];
            analysis.circuit = self.schematic;
            
            SPIProbeSet *probeSet = [NSEntityDescription insertNewObjectForEntityForName:@"SPIProbeSet" inManagedObjectContext:self.schematic.managedObjectContext];
            //probeSet.analysis = analysis;
            
            if ([highlightedView isKindOfClass:[SPIWireView class]]) {
                
                SPIVoltageProbe *vp = [NSEntityDescription insertNewObjectForEntityForName:@"SPIVoltageProbe" inManagedObjectContext:self.schematic.managedObjectContext];
                SPIWireView *wv = (id)highlightedView;
                vp.wire = wv.wire;
                vp.net = vp.wire.net;
                vp.probeSet = probeSet;
                vp.analysis = analysis;
            } else {
                SPICurrentProbe *vp = [NSEntityDescription insertNewObjectForEntityForName:@"SPICurrentProbe" inManagedObjectContext:self.schematic.managedObjectContext];
                SPITerminalView *tv = (id)highlightedView;
                vp.terminal = tv.terminal;
                vp.probeSet = probeSet;
                vp.analysis = analysis;
                
            }
        }
    }
}

- (void)rerouteInstance:(NSManagedObjectID*)instanceID withTransform:(CGAffineTransform)transform {
#if 0
    NSManagedObjectContext *context = _context;
#else
    NSManagedObjectContext *context = _scratchContext;
    [_scratchContext reset];
#endif
    
    SPIInstance *instance = (id)[context fetchObjectForEntityName:@"SPIInstance"
                                                        predicate:[NSPredicate predicateWithFormat:@"self == %@", instanceID]];
    instance.position.transform = transform;
    
    SPISchematic *schematic = instance.position.schematic;
    
    // this is a bit drastic, but... kill all the nets and reroute all the connections
    NSMutableSet *nets = [NSMutableSet setWithCapacity:16];
    for (SPITerminal *terminal in instance.terminals) {
        if (terminal.net) {
            [nets addObject:terminal.net];
            [context deleteObject:terminal.net];
        }
    }
    
    for (SPINet *net in nets) {
        // find out what terminals should be reconnected
        NSArray *terminals = [net.terminals allObjects];
        NSArray *wires = [net.wires allObjects];
        
        // kill the old wires
        for (SPIWire *wire in wires) {
            [context deleteObject:wire];
        }
        
        if (terminals.count < 2)
            continue;
        
        SPITerminal *t1 = [terminals objectAtIndex:0];
        SPITerminal *t2 = [terminals objectAtIndex:1];
        
        SPIRoutingGrid *routingGrid = [[SPIRoutingGrid alloc] initWithSchematic:schematic];
        [self connectTerminal:t1 toTerminal:t2 onGrid:routingGrid inContext:context];
        
        for (int i=2; i<terminals.count; i++) {
            SPITerminal *t = [terminals objectAtIndex:i];
            [self connectTerminal:t toNet:t1.net onGrid:routingGrid];
        }
    }
     [context save:nil];
}

- (void)didDragInstance:(SPIDragInstanceGestureRecognizer*)pgr {
    static CGAffineTransform oldTransform;
    static CGPoint lastPt;
    
    CGPoint translation = [pgr translationInView:self.schematicView];
    if (pgr.state == UIGestureRecognizerStateBegan) {
        oldTransform = pgr.draggingView.layer.affineTransform;
        lastPt = translation;
    } else if (pgr.state == UIGestureRecognizerStateChanged) {
    } else if (pgr.state == UIGestureRecognizerStateFailed) {
        translation = CGPointZero;
    }
    
    translation = [self gridCoordinateFromPoint:translation];
    translation = [self pointFromGridCoordinate:translation];
    
    CGAffineTransform t = CGAffineTransformInvert(oldTransform);
    t = CGAffineTransformTranslate(t, -translation.x, -translation.y);
    t = CGAffineTransformInvert(t);
    pgr.draggingView.layer.affineTransform = t;
    
    
    if (pgr.state == UIGestureRecognizerStateEnded) {
        [self rerouteInstance:pgr.draggingView.instance.objectID withTransform:t];
    } else if (fabs(lastPt.x-translation.x) + fabs(lastPt.y-translation.y) >= 10) {
        lastPt = translation;
        [self rerouteInstance:pgr.draggingView.instance.objectID withTransform:t];        
    }
}

- (void)didLongPress:(UILongPressGestureRecognizer*)lpgr {
    CGPoint pt = [lpgr locationInView:self.view];	
    UIView *view = [self.view hitTest:pt withEvent:nil];

    if ([view isKindOfClass:[SPIWireView class]]) {
        SPIWire *wire = ((SPIWireView*)view).wire;
        [self selectNet:wire.net];
    } else {
        self.selectedViews = nil;
    }
    
    if ([view isKindOfClass:[SPIInstanceView class]]) {
        SPIInstanceView *iv = (SPIInstanceView*)view;
        SPIInstance *instance = iv.instance;
        
        // let's try rotating an instance
        CGAffineTransform t = instance.position.transform;
        t = CGAffineTransformRotate(t, 3.1415926/2.0);
        instance.position.transform = t;

        // this is a bit drastic, but... kill all the nets and reroute all the connections
        for (SPITerminal *terminal in instance.terminals) {
            if (terminal.net) {
                // find out what terminals should be reconnected
                NSArray *terminals = [terminal.net.terminals allObjects];
                
                // kill the old one
                [self.schematic.managedObjectContext deleteObject:terminal.net];
                
                if (terminals.count < 2)
                    continue;
                
                // reconnect everything        
                SPIRoutingGrid *routingGrid = [[SPIRoutingGrid alloc] initWithSchematic:self.schematic];
                
                SPITerminal *t1 = [terminals objectAtIndex:0];
                SPITerminal *t2 = [terminals objectAtIndex:1];
                [self connectTerminal:t1 toTerminal:t2 onGrid:routingGrid inContext:_context];
                
                for (int i=2; i<terminals.count; i++) {
                    SPITerminal *t = [terminals objectAtIndex:i];
                    [self connectTerminal:t toNet:t1.net onGrid:routingGrid];
                }
                
            }
        }

    }
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
	CGPoint pt = [gestureRecognizer locationInView:self.schematicView];
    return YES;
}

- (void)connectTerminal:(SPITerminal*)start toTerminal:(SPITerminal*)end onGrid:(SPIRoutingGrid*)grid inContext:(NSManagedObjectContext*)context {
    NSArray *points = [grid findPathFromTerminal:start toTerminal:end];
    if (points.count < 2) return;
    
    // wire between 2 terminals creates a new net
    SPINet *net = [NSEntityDescription insertNewObjectForEntityForName:@"SPINet" inManagedObjectContext:context];
    net.circuit = start.instance.circuit;;

    start.net = net;
    end.net = net;    

    SPIWire *w = [NSEntityDescription insertNewObjectForEntityForName:@"SPIWire" inManagedObjectContext:context];
    w.net = net;
    w.schematic = start.instance.position.schematic;
    w.connections = [NSSet setWithObjects:start, end, nil];
    w.points = points;
}

- (void)connectTerminal:(SPITerminal*)terminal toNet:(SPINet*)net onGrid:(SPIRoutingGrid*)routingGrid {
    NSArray *points = [routingGrid findPathFromTerminal:terminal toNet:net];
    assert(points.count >= 2);
    
    // this terminal is now connected to a net
    terminal.net = net;
    
    // this type of wiring always splits the existing wire in half and creates a solder dot joining the
    // resulting 3 wires
    
    
    // we don't necessarily know which wire on the net we got connected to -- so we have to do some work to find it
    // FIXME this might be an issue if we connect on a solder dot
    SPIWire *originalWire = nil;
    CGPoint intersectionPt = [[points objectAtIndex:points.count-1] CGPointValue];
    for (SPIWire *wire in net.wires) {
        CGPathRef fillPath = [wire fillPath:2.0];
        
        if (CGPathContainsPoint(fillPath, nil, intersectionPt, NO)) {
            originalWire = wire;
            break;
            
        }
    }
    assert(originalWire);
    
    // split the existing wire -- these 2 wires are now each short a connection
    SPIWire *splitWire = [originalWire splitWireAt:intersectionPt];
    
    // create the new wire; after this it will have 1 connection, so it's one short until the solderDot
    SPIWire *newWire = [NSEntityDescription insertNewObjectForEntityForName:@"SPIWire" inManagedObjectContext:terminal.managedObjectContext];
    newWire.net = net;
    newWire.schematic = terminal.instance.position.schematic;
    newWire.points = points;
    [newWire addConnectionsObject:terminal];
    
    // create the solder dot and give all 3 wires their missing connection
    SPISolderDot *dot = [NSEntityDescription insertNewObjectForEntityForName:@"SPISolderDot" inManagedObjectContext:terminal.managedObjectContext];
    dot.wires = [NSSet setWithObjects:originalWire, splitWire, newWire, nil];
    dot.position = intersectionPt;
    dot.schematic = newWire.schematic;    
}

- (void)didCreateWire:(SPICreateWireGestureRecognizer*)pgr {
    static SPIRoutingGrid *routingGrid = nil;
    
	CGPoint pt = [pgr locationInView:self.schematicView];
    UIView *view = [self.schematicView hitTest:pt withEvent:nil];
    NSLog(@"%d  pt: %@ %@ %@", pgr.state, NSStringFromCGPoint(pt), [view class], NSStringFromCGPoint([pgr translationInView:self.schematicView]));
    
    if (pgr.state == UIGestureRecognizerStateBegan) {
        // we are a go
        routingGrid = [[SPIRoutingGrid alloc] initWithSchematic:self.schematic];
        pgr.lastCheckpoint = pt;
        
    } else if (pgr.state == UIGestureRecognizerStateChanged) {
        CGFloat dist = fabs(pt.x - pgr.lastCheckpoint.x) + fabs(pt.y - pgr.lastCheckpoint.y);
        if (dist > 30) {
            pgr.lastCheckpoint = pt;
        }
    } else if (pgr.state == UIGestureRecognizerStateEnded) {
        pgr.lastCheckpoint = pt;
        
        if ([pgr.startView isKindOfClass:[SPITerminalView class]] &&
            [pgr.endView isKindOfClass:[SPITerminalView class]]) {
            
            
            SPITerminalView *start = (id)pgr.startView;
            SPITerminalView *end = (id)pgr.endView;
            
            [self connectTerminal:start.terminal toTerminal:end.terminal onGrid:routingGrid inContext:_context];

        } else if (([pgr.startView isKindOfClass:[SPITerminalView class]] && [pgr.endView isKindOfClass:[SPIWireView class]]) ||
                   ([pgr.startView isKindOfClass:[SPIWireView class]] && [pgr.endView isKindOfClass:[SPITerminalView class]])) {
            
            SPITerminalView *start = (id)pgr.startView;
            SPIWireView *end = (id)pgr.endView;
            
            if ([pgr.startView isKindOfClass:[SPIWireView class]]) {
                start = (id)pgr.endView;
                end = (id)pgr.startView;
            }
            
            
            SPIWire *originalWire = end.wire;
            
            [self connectTerminal:start.terminal toNet:originalWire.net onGrid:routingGrid];
            
        } else if ([pgr.startView isKindOfClass:[SPIWireView class]] &&
                   [pgr.endView isKindOfClass:[SPIWireView class]]) {
            
            
            SPIWireView *startWireView = (id)pgr.startView;
            SPIWireView *endWireView = (id)pgr.endView;
            
            SPIWire *startOriginalWire = startWireView.wire;
            SPIWire *endOriginalWire = endWireView.wire;
            
            NSArray *points = [routingGrid findPathFromWire:startOriginalWire toWire:endOriginalWire];
            assert(points.count >= 2);

            
            // create the new wire; after this it will have no connections, so it will need the two solder dots
            SPIWire *newWire = [NSEntityDescription insertNewObjectForEntityForName:@"SPIWire" inManagedObjectContext:self.schematic.managedObjectContext];
            newWire.net = startOriginalWire.net;
            newWire.schematic = self.schematic;
            newWire.points = points;
            
            
            // split the existing wires
            SPIWire *startSplitWire = [startOriginalWire splitWireAt:[[points objectAtIndex:points.count-1] CGPointValue]];
            SPIWire *endSplitWire = [endOriginalWire splitWireAt:[[points objectAtIndex:0] CGPointValue]];
            
            // create the solder dots and give all wires their missing connections
            SPISolderDot *dot1 = [NSEntityDescription insertNewObjectForEntityForName:@"SPISolderDot" inManagedObjectContext:self.schematic.managedObjectContext];
            dot1.wires = [NSSet setWithObjects:endOriginalWire, endSplitWire, newWire, nil];
            dot1.position = [[points objectAtIndex:0] CGPointValue];
            dot1.schematic = self.schematic;
            
            SPISolderDot *dot2 = [NSEntityDescription insertNewObjectForEntityForName:@"SPISolderDot" inManagedObjectContext:self.schematic.managedObjectContext];
            dot2.wires = [NSSet setWithObjects:startOriginalWire, startSplitWire, newWire, nil];
            dot2.position = [[points objectAtIndex:points.count-1] CGPointValue];
            dot2.schematic = self.schematic;
            
            // we also need to kill a net
            SPINet *oldNet = endOriginalWire.net;
            NSSet *wires = [NSSet setWithSet:oldNet.wires];
            NSSet *terminals = [NSSet setWithSet:oldNet.terminals];
            for (SPIWire *wire in wires) {
                wire.net = startOriginalWire.net;
            }
            for (SPITerminal *terminal in terminals) {
                terminal.net = startOriginalWire.net;
            }
            [self.schematic.managedObjectContext deleteObject:oldNet];
            
            
            NSLog(@"%@ %@", startSplitWire, endSplitWire);
            
            
            
            // TODO this has the effect of eliminating a net -- so that has to be accounted for
            
            
            
            
        }
        
        pgr.startView = nil;
        pgr.endView = nil;
        
        routingGrid = nil;
    } else {
        routingGrid = nil;
    }
}


- (void)addTerminal:(SPITerminal*)terminal toView:(UIView*)view {
    SPITerminalView *tv = [[SPITerminalView alloc] initWithTerminal:terminal];
    [view addSubview:tv];
    
    [_componentViews setObject:tv forKey:terminal.objectID];
}

- (void)addInstance:(SPIInstance*)instance {
    SPIInstanceView *iv = [[SPIInstanceView alloc] initWithInstance:instance];
    [self.schematicView addSubview:iv];
    
    [_componentViews setObject:iv forKey:instance.objectID];
    
    // account for the terminals
    for (SPITerminal *terminal in instance.terminals) {
        [self addTerminal:terminal toView:iv];
    }
}

- (void)addWire:(SPIWire*)wire {
    SPIWireView *wv = [[SPIWireView alloc] initWithWire:wire];
    [self.schematicView addSubview:wv];
    [self.schematicView sendSubviewToBack:wv];
    
    [_componentViews setObject:wv forKey:wire.objectID];
}

- (void)addComponent:(NSManagedObject*)component {
    assert(![component.objectID isTemporaryID]);
    
    if ([component isKindOfClass:[SPIInstance class]]) {
        [self addInstance:(id)component];
    } else if ([component isKindOfClass:[SPIWire class]]) {
        [self addWire:(id)component];
    } 
}




- (void)removeComponent:(NSManagedObject*)component {
    [[_componentViews objectForKey:component.objectID] removeFromSuperview];
    [_componentViews removeObjectForKey:component.objectID];
}


- (void)updateComponent:(NSManagedObject*)component {
    [[_componentViews objectForKey:component.objectID] setNeedsDisplay];
    
    if ([component isKindOfClass:[SPIInstancePosition class]]) {
        SPIInstance *instance = ((SPIInstancePosition*)component).instance;
        [self updateComponent:instance];
    }
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	
    _context = self.schematic.managedObjectContext;
    _scratchContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    _scratchContext.parentContext = _context;
    
    [[NSNotificationCenter defaultCenter] addObserverForName:NSManagedObjectContextObjectsDidChangeNotification
                                                      object:_scratchContext
                                                       queue:[NSOperationQueue mainQueue]
                                                  usingBlock:^(NSNotification *note) {
                                                      NSDictionary *userInfo = note.userInfo;
                                                      NSArray *insertedObjects = [[userInfo objectForKey:NSInsertedObjectsKey] allObjects];
                                                      NSError *error = nil;
                                                      [self.schematic.managedObjectContext obtainPermanentIDsForObjects:insertedObjects error:&error];
                                                  }];
    
    self.navigationItem.title = self.schematic.name;
    
	UIImage *originalImage = [UIImage imageNamed:@"pretty-button.png"];
	UIImage *stretchImage = [originalImage stretchableImageWithLeftCapWidth:9 topCapHeight:9];			
	[probeSetButton setBackgroundImage:stretchImage forState:UIControlStateNormal];
	[probeSetButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	[probeSetButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
	[probeSetButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];			
    
	scrollView.layer.geometryFlipped = YES;
	scrollView.delegate = self;

	schematicView = [[SPISchematicView alloc] initWithFrame:CGRectZero];
    schematicView.schematic = schematic;
	[scrollView addSubview:schematicView];
    
    _componentViews = [NSMutableDictionary dictionaryWithCapacity:128];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:NSManagedObjectContextObjectsDidChangeNotification
                                                      object:self.schematic.managedObjectContext
                                                       queue:[NSOperationQueue mainQueue] 
                                                  usingBlock:^(NSNotification *note) {
                                                      NSDictionary *userInfo = note.userInfo;
                                                      NSLog(@"DID CHANGE");
                                                      
                                                      NSArray *insertedObjects = [[userInfo objectForKey:NSInsertedObjectsKey] allObjects];
                                                      [self.schematic.managedObjectContext obtainPermanentIDsForObjects:insertedObjects error:nil];
                                                         
                                                      for (NSManagedObject *obj in insertedObjects) {
                                                          [self addComponent:obj];
                                                      }
                                                      
                                                      for (NSManagedObject *obj in [userInfo objectForKey:NSDeletedObjectsKey]) {
                                                          [self removeComponent:obj];
                                                      }
                                                      
                                                      for (NSManagedObject *obj in [userInfo objectForKey:NSUpdatedObjectsKey]) {
                                                          [self updateComponent:obj];
                                                      }
                                                      
                                                      self.netsLabel.text = [NSString stringWithFormat:@"%d nets", self.schematic.nets.count];
                                                  }];
    
    for (SPIInstance *instance in self.schematic.instances) {
        [self addInstance:instance];
    }
    for (SPIWire *wire in self.schematic.wires) {
        [self addWire:wire];
    }
    
    
    // TODO remove this observer as well...
    //UITapGestureRecognizer *tgr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTap:)];
    //[schematicView addGestureRecognizer:tgr];
    
    
    SPITapComponentGestureRecognizer *tgr = [[SPITapComponentGestureRecognizer alloc] initWithTarget:self action:@selector(didTap:)];
    [self.schematicView addGestureRecognizer:tgr];
    
    UITapGestureRecognizer *lpgr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didLongPress:)];
    lpgr.numberOfTapsRequired = 2;
    [self.schematicView addGestureRecognizer:lpgr];
    
    UIPanGestureRecognizer *addProbeGR = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(didDragProbe:)];
    [self.configurableButton addGestureRecognizer:addProbeGR];
	
	for (id gr in self.scrollView.gestureRecognizers) {
		if ([gr isKindOfClass:[UIPanGestureRecognizer class]]) {
			((UIPanGestureRecognizer*)gr).minimumNumberOfTouches = 2;
		}
	}
	
    
	SPICreateWireGestureRecognizer *pgr = [[SPICreateWireGestureRecognizer alloc] initWithTarget:self action:@selector(didCreateWire:)];
    //pgr.delegate = self;
    [self.schematicView addGestureRecognizer:pgr];
	
    
    SPIDragInstanceGestureRecognizer *drag = [[SPIDragInstanceGestureRecognizer alloc] initWithTarget:self action:@selector(didDragInstance:)];
    drag.maximumNumberOfTouches = 1;
    [self.schematicView addGestureRecognizer:drag];
	
	
 	NSLog(@"viewDidLoad!!");
}

- (void)viewWillAppear:(BOOL)animated {
	self.schematicView.frame = CGRectMake(0.f, 0.f, 2048.f, 1496.f);
    scrollView.contentSize = CGSizeMake(2048.f, 1496.f);
}



- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [self setConfigurableButton:nil];
    [self setNetsLabel:nil];
    [self setPrimitivesButton:nil];
    [self setAnalysesButton:nil];
    [self setPropertiesButton:nil];
	// Release any retained subviews of the main view.
    self.schematicView = nil;
    self.scrollView = nil;
    self.probeSetButton = nil;
    self.netlistButton = nil;
    dragIcon = nil;
}


#pragma mark -
#pragma mark UIScrollViewDelegate

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
	return self.schematicView;
}

#pragma mark -
#pragma mark SPIProbeSetViewControllerDelegate

- (void)didSelectProbe:(SPIProbe *)probe {
	NSLog(@"selected probe: %@", probe);
	// select this in the schematic view
	if ([probe isKindOfClass:[SPIVoltageProbe class]]) {
		SPIVoltageProbe *vp = (SPIVoltageProbe*)probe;
		[schematicView selectNet:vp.net];
		
		// can I scroll to this net?
		CGRect bb = [vp.net boundingBox];
		NSLog(@"bb: %@", [NSValue valueWithCGRect:bb]);
		
		// adjust for the zoom scale...
		float theScale = scrollView.zoomScale;
		bb.origin.x *= theScale;
		bb.origin.y *= theScale;
		bb.size.width *= theScale;
		bb.size.height *= theScale;	
		
		bb = CGRectInset(bb, -80, -80);
		
		NSLog(@"adjusted bb: %@", [NSValue valueWithCGRect:bb]);
		[self.scrollView scrollRectToVisible:bb animated:YES];
	}
}

- (void)dealloc {
}
@end
