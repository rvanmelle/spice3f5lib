//
//  SPIFloatParam.h
//  touchspice
//
//  Created by Reid van Melle on 12-04-01.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "SPIParameter.h"


@interface SPIFloatParam : SPIParameter

@property (nonatomic, retain) NSNumber * defaultValue;
@property (nonatomic, retain) NSNumber * maxValue;
@property (nonatomic, retain) NSNumber * minValue;

@end
