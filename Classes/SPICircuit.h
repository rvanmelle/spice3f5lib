//
//  SPICircuit.h
//  touchspice
//
//  Created by Shawn Hyam on 10-12-22.
//  Copyright 2010 Brierwood Design Cooperative. All rights reserved.
//

#import <CoreData/CoreData.h>

@class SPIInstance;
@class SPINet;
@class SPIAnalysis;
@class SPIRun;

@interface SPICircuit :  NSManagedObject  
{
}

@property (nonatomic, strong) NSString *name;
@property (nonatomic, retain) NSSet* nets;
@property (nonatomic, retain) NSSet* instances;
@property (nonatomic, retain) NSSet* analyses;
@property (nonatomic, retain) NSSet* runs;


@end


@interface SPICircuit (CoreDataGeneratedAccessors)
- (void)addNetsObject:(SPINet *)value;
- (void)removeNetsObject:(SPINet *)value;
- (void)addNets:(NSSet *)value;
- (void)removeNets:(NSSet *)value;

- (void)addInstancesObject:(SPIInstance *)value;
- (void)removeInstancesObject:(SPIInstance *)value;
- (void)addInstances:(NSSet *)value;
- (void)removeInstances:(NSSet *)value;

- (void)addAnalysesObject:(SPIAnalysis *)value;
- (void)removeAnalysesObject:(SPIAnalysis *)value;
- (void)addAnalyses:(NSSet *)value;
- (void)removeAnalyses:(NSSet *)value;

- (void)addRunsObject:(SPIRun *)value;
- (void)removeRunsObject:(SPIRun *)value;
- (void)addRuns:(NSSet *)value;
- (void)removeRuns:(NSSet *)value;

@end

