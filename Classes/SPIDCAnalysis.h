//
//  SPIDCAnalysis.h
//  touchspice
//
//  Created by Shawn Hyam on 10-12-31.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <CoreData/CoreData.h>
#import "SPIAnalysis.h"


@interface SPIDCAnalysis :  SPIAnalysis  
{
}

@property (nonatomic, retain) NSString * incrValue;
@property (nonatomic, retain) NSString * sourceName;
@property (nonatomic, retain) NSString * startValue;
@property (nonatomic, retain) NSString * stopValue;

@end



