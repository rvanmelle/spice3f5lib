//
//  SPIWire.h
//  touchspice
//
//  Created by Shawn Hyam on 10-12-22.
//  Copyright 2010 Brierwood Design Cooperative. All rights reserved.
//

#import <CoreData/CoreData.h>

@class SPINet;
@class SPISchematic;
@class SPITerminal;
@class SPISolderDot;

@interface SPIWire :  NSManagedObject  

@property (readonly) CGPathRef path;
@property (readonly) CGPathRef hitTestPath;

@property (nonatomic, retain) NSString *SVGPath;
@property (nonatomic, retain) SPISchematic * schematic;
@property (nonatomic, retain) SPINet * net;
@property (nonatomic, retain) NSSet * terminals;
@property (nonatomic, retain) NSSet * solderDots;

- (SPIWire*)splitWireAt:(CGPoint)splitPoint;

@end


@interface SPIWire (CoreDataGeneratedAccessors)

- (void)addTerminalsObject:(SPITerminal *)value;
- (void)removeTerminalsObject:(SPITerminal *)value;
- (void)addTerminals:(NSSet *)value;
- (void)removeTerminals:(NSSet *)value;

- (void)addSolderDotsObject:(SPISolderDot *)value;
- (void)removeSolderDotsObject:(SPISolderDot *)value;
- (void)addSolderDots:(NSSet *)value;
- (void)removeSolderDots:(NSSet *)value;

@end




