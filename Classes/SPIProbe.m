// 
//  SPIProbe.m
//  touchspice
//
//  Created by Shawn Hyam on 10-12-31.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "SPIProbe.h"

// #import "SPIPlot.h"
#import "SPIProbeSet.h"

@implementation SPIProbe 

@dynamic plots;
@dynamic probeSet;
@dynamic name;
@dynamic analysis;

@end
