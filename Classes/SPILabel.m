// 
//  SPILabel.m
//  touchspice
//
//  Created by Shawn Hyam on 11-01-06.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "SPILabel.h"

@interface SPILabel () {
@private
    CGPoint _position;
}
@end

@implementation SPILabel 

@dynamic positionValue;
@dynamic text;
@dynamic primitive;

- (CGPoint)position {
	[self willAccessValueForKey:@"position"];
	_position = [self.positionValue CGPointValue];
	[self didAccessValueForKey:@"position"];
	return _position;
}

- (void)setPosition:(CGPoint)pt {
	[self willChangeValueForKey:@"position"];
	_position = pt;
	[self didChangeValueForKey:@"position"];
	self.positionValue = [NSValue valueWithCGPoint:_position];
}

@end
