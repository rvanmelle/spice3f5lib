// 
//  SPIPrimitive.m
//  touchspice
//
//  Created by Shawn Hyam on 10-12-22.
//  Copyright 2010 Brierwood Design Cooperative. All rights reserved.
//

#import "SPIPrimitive.h"

#import "SPIInstance.h"
#import "SPIPin.h"
#import "NSString+SVG.h"
#import "SPIIntParam.h"
#import "SPIFloatParam.h"
#import "SPIStringParam.h"
#import "SPIDefs.h"

@implementation SPIPrimitive 

//@synthesize instanceRect;
@dynamic name;
@dynamic SVGPath;
@dynamic instanceRectValue;
@dynamic instances;
@dynamic pins;
@dynamic netlistTemplate;
@dynamic ground;
@dynamic parameters;
@dynamic labels;

- (CGPathRef)path {
	// FIXME watch out for memory leaks, cache this value properly (transient?)
	return [self.SVGPath CGPathFromSVG];
}

- (CGRect)instanceRect {
	[self willAccessValueForKey:@"instanceRect"];
	//if (CGRectIsNull(instanceRect)) { instanceRect = [self.instanceRectValue CGRectValue]; }
    instanceRect = [self.instanceRectValue CGRectValue];
	[self didAccessValueForKey:@"instanceRect"];
	
	return instanceRect;
}

- (void)setInstanceRect:(CGRect)r {
	[self willChangeValueForKey:@"instanceRect"];
	instanceRect = CGRectStandardize(r);
	[self didChangeValueForKey:@"instanceRect"];
	
	self.instanceRectValue = [NSValue valueWithCGRect:instanceRect];
}


- (CGSize)size {
	CGRect r = self.instanceRect; //[self.instanceRectValue CGRectValue];
	return CGSizeMake(CGRectGetWidth(r), CGRectGetHeight(r));
}

- (void)drawInContext:(CGContextRef)ctx {
    
	CGContextSetLineWidth(ctx, 1.0);
	CGContextSetStrokeColorWithColor(ctx, [UIColor greenColor].CGColor);
	CGContextAddPath(ctx, self.path);
	CGContextStrokePath(ctx);
    
	CGContextSetFillColorWithColor(ctx, [UIColor redColor].CGColor);
	for (SPIPin *pin in self.pins) {
		CGPoint pt = pin.offset;
		CGContextAddRect(ctx, CGRectMake(pt.x-4, pt.y-4, 8.f, 8.f));
	}
	CGContextFillPath(ctx);	
}

- (UIImage*)imageRepresentation {
    CGRect bb = self.instanceRect;
    for (SPIPin *pin in self.pins) {
		CGPoint pt = pin.offset;        
        bb = CGRectUnion(bb, CGRectMake(pt.x-4, pt.y-4, 8.f, 8.f));
    }
    
    UIGraphicsBeginImageContext(bb.size);
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(ctx, -bb.origin.x, -bb.origin.y);
    
    [self drawInContext:ctx];
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

- (SPIFloatParam*)addFloatParam:(NSString*)name section:(NSString*)section description:(NSString*)description 
                            min:(double_t)min max:(double_t)max initial:(double_t)initial units:(SPIUnitType)units
{
	NSManagedObjectContext *ctx = [self managedObjectContext];
	SPIFloatParam *p = [NSEntityDescription insertNewObjectForEntityForName:@"SPIFloatParam" inManagedObjectContext:ctx];
	p.minValue = [NSNumber numberWithDouble:min];
	p.maxValue = [NSNumber numberWithDouble:max];
	p.defaultValue = [NSNumber numberWithDouble:initial];
	p.units = [NSNumber numberWithInt:units];
	p.sectionName = section;
	p.name = name;
	p.details = description;
	[self addParametersObject:p];
	return p;
}

- (SPIFloatParam*)addFloatParam:(NSString*)name section:(NSString*)section description:(NSString*)description 
                            min:(double_t)min max:(double_t)max initial:(double_t)initial 
                          units:(SPIUnitType)units location:(CGPoint)loc anchor:(SPITextAnchorType)anchor
{
    NSAssert(NO, @"not implemented / supported yet");
	SPIFloatParam *p = [self addFloatParam:name section:section description:description min:min max:max initial:initial units:units];
	//SPILabel *l = [SPILabel createLabelAtLocation:loc anchor:anchor];
	//p.label = l;
	return p;
}

- (SPIIntParam*)addIntParam:(NSString*)name section:(NSString*)section description:(NSString*)description 
                        min:(int)min max:(int)max initial:(int)initial units:(SPIUnitType)units
{
	NSManagedObjectContext *ctx = [self managedObjectContext];
	SPIIntParam *p = [NSEntityDescription insertNewObjectForEntityForName:@"SPIIntParam" inManagedObjectContext:ctx];
	p.minValue = [NSNumber numberWithInt:min];
	p.maxValue = [NSNumber numberWithInt:max];
	p.defaultValue = [NSNumber numberWithInt:initial];
	p.units = [NSNumber numberWithInt:units];
	p.sectionName = section;
	p.name = name;
	p.details = description;
	[self addParametersObject:p];
	return p;
    
}

- (SPIStringParam*)addStringParam:(NSString*)name section:(NSString*)section description:(NSString*)description
                          initial:(NSString*)initial
{
	NSManagedObjectContext *ctx = [self managedObjectContext];
	SPIStringParam *p = [NSEntityDescription insertNewObjectForEntityForName:@"SPIStringParam" inManagedObjectContext:ctx];
	p.defaultValue = initial;
	p.sectionName = section;
	p.units = [NSNumber numberWithInt:kSPIUnitNone];
	p.name = name;
	p.details = description;
	[self addParametersObject:p];
	return p;
}



@end
