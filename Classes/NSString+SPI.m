//
//  NSString+SPI.m
//  ispice
//
//  Created by Reid van Melle on 10-06-05.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "NSString+SPI.h"


@implementation NSString (SPI)

-(const char*)cString
{
	return [self cStringUsingEncoding:NSASCIIStringEncoding];
}

+(NSString*)fromCString:(const char*)str
{
	return [NSString stringWithCString:str encoding:NSASCIIStringEncoding];
}

+(NSNumber*)floatValueFromCString:(const char*)str
{
	NSString *s = [NSString fromCString:str];
	NSScanner *scanner = [NSScanner scannerWithString:s];
	double val;
	if ([scanner scanDouble:&val]) {
		return [NSNumber numberWithDouble:val];
	}
	return nil;
}

@end
