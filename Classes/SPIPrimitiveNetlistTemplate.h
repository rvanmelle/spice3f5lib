//
//  SPIPrimitiveNetlistTemplate.h
//  touchspice
//
//  Created by Shawn Hyam on 10-12-23.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <CoreData/CoreData.h>

@class SPIPrimitive;

@interface SPIPrimitiveNetlistTemplate :  NSManagedObject  
{
}

@property (nonatomic, retain) NSString * templateString;
@property (nonatomic, retain) SPIPrimitive * primitive;

@end



