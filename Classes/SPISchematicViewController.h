//
//  touchspiceViewController.h
//  touchspice
//
//  Created by Shawn Hyam on 10-12-18.
//  Copyright (c) 2010 Brierwood Design Cooperative. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SPIProbeSetViewController.h"
#import "SPIPrimitiveGalleryViewController.h"

@class SPISchematicView;
@class SPISchematic;
@class SPIComponentView;

@interface SPISchematicViewController : BWDViewController<UIScrollViewDelegate, SPIProbeSetViewControllerDelegate, SPIPrimitiveGalleryDelegate, UIGestureRecognizerDelegate> {	
	UIImageView *dragIcon;
}

- (IBAction)probeSetButtonAction;
- (IBAction)netlistButtonAction;
- (IBAction)selectPlotsAction:(id)sender;
- (IBAction)selectPropertiesAction:(id)sender;
- (IBAction)selectPrimitivesAction:(id)sender;
- (IBAction)selectAnalysesAction:(id)sender;
- (IBAction)selectNetlistAction:(id)sender;
- (IBAction)selectRunsAction:(id)sender;
- (IBAction)validateAction:(id)sender;
- (IBAction)configurableAction:(id)sender;

@property (nonatomic, retain) SPISchematic *schematic; 
@property (nonatomic, retain) IBOutlet SPISchematicView *schematicView;
@property (nonatomic, retain) IBOutlet UIScrollView *scrollView;
@property (nonatomic, retain) IBOutlet UIButton *probeSetButton, *netlistButton;

@property (retain, nonatomic) IBOutlet UIButton *propertiesButton;
@property (retain, nonatomic) IBOutlet UIButton *analysesButton;
@property (retain, nonatomic) IBOutlet UIButton *primitivesButton;
@property (retain, nonatomic) IBOutlet UILabel *netsLabel;
@property (retain, nonatomic) IBOutlet UIButton *configurableButton;

//@property (strong, nonatomic) SPIComponentView *selectedView;
@property (strong, nonatomic) NSSet *selectedViews;

@end

