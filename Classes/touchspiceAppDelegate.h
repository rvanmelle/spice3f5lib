//
//  touchspiceAppDelegate.h
//  touchspice
//
//  Created by Shawn Hyam on 10-12-18.
//  Copyright (c) 2010 Brierwood Design Cooperative. All rights reserved.
//


#import <UIKit/UIKit.h>

@interface touchspiceAppDelegate : NSObject <UIApplicationDelegate> {
}

@property (nonatomic, retain) IBOutlet UIWindow *window;

@end

