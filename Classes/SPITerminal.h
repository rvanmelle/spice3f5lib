//
//  SPITerminal.h
//  touchspice
//
//  Created by Shawn Hyam on 12-04-26.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "SPIWireConnection.h"

@class SPIInstance, SPIPin, SPINet;
@class SPICurrentProbe;

@interface SPITerminal : SPIWireConnection

@property (nonatomic, retain) SPIInstance *instance;
@property (nonatomic, retain) SPIPin *pin;
@property (nonatomic, retain) SPINet *net;
@property (nonatomic, retain) NSSet* currentProbes;

// helpers
@property (nonatomic, readonly) SPIWire *wire;
@property (nonatomic, readonly) CGPoint position;
@end


@interface SPITerminal (CoreDataGeneratedAccessors)

- (void)addCurrentProbesObject:(SPICurrentProbe *)value;
- (void)removeCurrentProbesObject:(SPICurrentProbe *)value;
- (void)addCurrentProbes:(NSSet *)value;
- (void)removeCurrentProbes:(NSSet *)value;

@end
