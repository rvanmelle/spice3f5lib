// 
//  SPIProperty.m
//  touchspice
//
//  Created by Shawn Hyam on 10-12-23.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "SPIProperty.h"

#import "SPIInstance.h"
#import "SPIParameter.h"

@implementation SPIProperty 

@dynamic value;
@dynamic instance;
@dynamic parameter;

@end
