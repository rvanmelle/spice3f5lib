
#define kSPIPinRectSize 8.0
#define kSPISolderDotWidth 10.0
#define kSPISolderDotHeight 8.0
#define kSPIPinHandleSize 20.0
#define kSPIGridSize 10.0

/*
   | `AUTUMN ->
        [|
          `HEX 0x5A1F00;
          `HEX 0xD1570D;
          `HEX 0xFDE792;
          `HEX 0x477725;
          `HEX 0xA9CC66;
          
          `HEX 0x468966;
          `HEX 0xFFF0A5;
          `HEX 0xFFB03B;
          `HEX 0xB64926;
          `HEX 0x8E2800;
        |]
    | `TWILIGHT ->
        [| `TWILIGHT_FG0; `TWILIGHT_FG1; `TWILIGHT_FG2; `TWILIGHT_FG3; |] 
    | `BASIC ->
        [| `YELLOW; `MAGENTA; `CYAN; `RED; `GREEN; `BLUE; `WHITE; |]

*/

#define kSPIAutumn1 HEX(0x5A1F00)
#define kSPITwilightBG [UIColor colorWithRed:(20./255.) green:(20./255.) blue:(20./255.) alpha:1.0]
#define kSPITwilightFG0 [UIColor colorWithRed:(143./255.) green:(157./255.) blue:(106./255.) alpha:1.0]
#define kSPITwilightFG1 [UIColor colorWithRed:(155./255.) green:(112./255.) blue:(63./255.) alpha:1.0]
#define kSPITwilightFG2 [UIColor colorWithRed:(249./255.) green:(238./255.) blue:(152./255.) alpha:1.0]
#define kSPITwilightFG3 [UIColor colorWithRed:(207./255.) green:(106./255.) blue:(76./255.) alpha:1.0]


#define kSPIDoneMoveNotification @"SPIDoneMoveNotification"

@class SPIWire;
@class SPIInstance;

@protocol SPIProbeDataSource <NSObject>

- (NSNumber*)getVoltageFor:(SPIWire*)wire;
- (NSNumber*)getCurrentFor:(SPIInstance*)inst branch:(NSString*)branch;

@end

@protocol SPIMovableObjectDelegate <NSObject>

- (void)startMove:(CGPoint)pos;
- (UIImage*)objectImageview;
- (void)completeMove:(CGPoint)pos;

@end

/** This is a generic text editor protocol --- the exit mechanisms are save or cancel */
@protocol SPITextEditorDelegate <NSObject>

- (void)saveAction;
- (void)cancelAction;

@end



typedef enum _SPISchematicEntryMode {
	kSPISchematicEntryModeNone,
	kSPISchematicEntryModeWire
} SPISchematicEntryMode;

typedef enum {
	SPIPrimitiveStyleNormal,
	SPIPrimitiveStyleHighlighted
} SPIPrimitiveStyle;

/** SPISchematicStyle is used to distinguish between the different ways that the schematic
 can be displayed in the library browser.  It is also used to distinguish between regular
 drawing and library browser thumb drawing. */
typedef enum {
	kSPISchematicStyleNormal,
	kSPISchematicStyleHighlighted,
	kSPISchematicStyleThumb
} SPISchematicStyle;

typedef enum _SPISchematicColorType {
	kSPISchematicBGColor,
	kSPISchematicFG1,
	kSPISchematicFG2,
	kSPISchematicFG3,
	kSPISchematicFG4
} SPISchematicColorType;

typedef enum _SPIUnitType {
	kSPIUnitNone,
	kSPIUnitResistance,
	kSPIUnitCapacitance,
	kSPIUnitInductance,
	kSPIUnitVoltage,
	kSPIUnitCurrent,
	kSPIUnitTime,
	kSPIUnitLength,
	kSPIUnitArea
} SPIUnitType;

// Finger size --- use on pie menus and also when creating certain move views etc.
typedef enum _SPIFingerSize {
	kSPIFingerSizeSmall,
	kSPIFingerSizeMedium,
	kSPIFingerSizeLarge
} SPIFingerSize;

typedef enum _SPIParamType {
	kSPIParamTypeDouble,  // 1.0, 2.0
	kSPIParamTypeInteger, // 1,2,3
	kSPIParamTypeText,    // A multi-line, i.e. paragraph of text (netlists etc.)
	kSPIParamTypeString,  // A longish string such as a title or summary (multi-word)
	kSPIParamTypeLabel,   // A short string/label such as "vsrc"
	kSPIParamTypeBoolean, // YES|NO
	kSPIParamTypeEnum,    // A small set of options such as "Option1|Option2|Option3"
	kSPIParamTypeDateTime // An NSDate object
} SPIParamType;

typedef enum {
	kSPIParamPropertiesNone        = 0,        // No special properties
	kSPIParamPropertiesReadonly    = 1 << 0,   // This is readonly -- NO EDIT FIELD
	kSPIParamPropertiesOptional    = 1 << 1,   // This property is optional --- can be empty/nil
	
} SPIParamProperties;

// Themes would be nice --- not really IN USE currently
typedef enum _SPISchematicThemeType {
	kSPISchematicAutumnTheme,
	kSPISchematicTwilightTheme,
	kSPISchematicBasicTheme
} SPISchematicThemeType;

/** These are user-level objects which the user interacts with */
typedef enum _SPISchematicObjectType {
	kSPISchematicObjectTypeInstance,
	kSPISchematicObjectTypeWire,
	kSPISchematicObjectTypeTerminal
} SPISchematicObjectType;

typedef enum _SPISchematicDrawingType {
	kSPISchematicNone = 2000,
	kSPISchematicBackground,
	kSPISchematicBackgroundGrid,
	kSPISchematicWire,
	kSPISchematicPin,
	kSPISchematicNet,
	kSPISchematicInstanceRect,
	kSPISchematicInstanceName,
	kSPISchematicParameter,
	kSPISchematicTerminalName
} SPISchematicDrawingType;

// Text anchors are used to position a piece of text to a particular anchor point
// Used for things like pin names, instance names, etc.
typedef enum _SPITextAnchorType {
	kSPITextAnchorLowerLeft = 3000,
	kSPITextAnchorLowerRight,
	kSPITextAnchorCenterCenter,
	kSPITextAnchorUpperLeft
} SPITextAnchorType;

typedef enum _SPISignalDirectionType {
	kSPISignalDirectionInputOutput = 3100,
	kSPISignalDirectionInput,
	kSPISignalDirectionOutput
} SPISignalDirectionType;

