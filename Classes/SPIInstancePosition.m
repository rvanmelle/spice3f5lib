// 
//  SPIInstancePosition.m
//  touchspice
//
//  Created by Shawn Hyam on 10-12-22.
//  Copyright 2010 Brierwood Design Cooperative. All rights reserved.
//

#import "SPIInstancePosition.h"
#import <UIKit/UIKit.h>

#import "SPIInstance.h"
#import "SPISchematic.h"

@implementation SPIInstancePosition 

@dynamic transformValue;
@dynamic instance;
@dynamic schematic;

- (void)rotateCW {
	CGAffineTransform t = self.transform;
	self.transform = CGAffineTransformRotate(t, M_PI/2);
	//self.transform = t;
}

- (CGAffineTransform)transform {
	[self willAccessValueForKey:@"transform"];
	if (CGAffineTransformIsIdentity(transform)) { transform = [self.transformValue CGAffineTransformValue]; }
	[self didAccessValueForKey:@"transform"];
	
	return [self.transformValue CGAffineTransformValue];
}

- (void)setTransform:(CGAffineTransform)t {
	[self willChangeValueForKey:@"transform"];
	transform = t;
	[self didChangeValueForKey:@"transform"];
	
	self.transformValue = [NSValue valueWithCGAffineTransform:transform];
}

@end
