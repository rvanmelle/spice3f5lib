// 
//  SPIVoltageProbe.m
//  touchspice
//
//  Created by Shawn Hyam on 10-12-31.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "SPIVoltageProbe.h"

#import "SPINet.h"

@implementation SPIVoltageProbe 

@dynamic net;
@dynamic wire;
// FIXME probe naming is a bit messed up
/*
- (NSString*)name {
	NSString *net_name = self.net.name ? self.net.name : @"#net";
	return [NSString stringWithFormat:@"v(%@)", net_name];
}
*/
@end
