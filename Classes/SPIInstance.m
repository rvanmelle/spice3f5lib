// 
//  SPIInstance.m
//  touchspice
//
//  Created by Shawn Hyam on 10-12-22.
//  Copyright 2010 Brierwood Design Cooperative. All rights reserved.
//

#import "SPIInstance.h"

#import "SPICircuit.h"
#import "SPIInstancePosition.h"
#import "SPIPrimitive.h"
#import "SPITerminal.h"

@implementation SPIInstance 

@dynamic name;
@dynamic circuit;
@dynamic terminals;
@dynamic position;
@dynamic primitive;
@dynamic properties;

// TODO validate that # of terminals == # of pins for the primitive

+ (SPIInstance*)instanceFromPrimitive:(SPIPrimitive *)primitive inCircuit:(SPICircuit*)circuit {
    SPIInstance *instance = [NSEntityDescription insertNewObjectForEntityForName:@"SPIInstance" inManagedObjectContext:primitive.managedObjectContext];
    instance.primitive = primitive;
    instance.circuit = circuit;
    
    for (SPIPin *pin in primitive.pins) {
        SPITerminal *terminal = [NSEntityDescription insertNewObjectForEntityForName:@"SPITerminal" inManagedObjectContext:primitive.managedObjectContext];
        terminal.pin = pin;
        terminal.instance = instance;
    }
    return instance;
}

@end
