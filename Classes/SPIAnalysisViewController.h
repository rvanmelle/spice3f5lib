//
//  SPIAnalysisViewController.h
//  touchspice
//
//  Created by Shawn Hyam on 12-03-22.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BWDTableViewController.h"
@class SPICircuit;

@interface SPIAnalysisViewController : BWDTableViewController

@property (nonatomic, strong) SPICircuit *circuit;

@end
