//
//  SPIVoltageProbe.h
//  touchspice
//
//  Created by Shawn Hyam on 10-12-31.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <CoreData/CoreData.h>
#import "SPIProbe.h"

@class SPINet, SPIWire;

@interface SPIVoltageProbe :  SPIProbe  
{
}

@property (nonatomic, retain) SPINet * net;
@property (nonatomic, retain) SPIWire * wire;
//@property (readonly) NSString *name;

@end



