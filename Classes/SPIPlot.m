// 
//  SPIPlot.m
//  touchspice
//
//  Created by Shawn Hyam on 10-12-31.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "SPIPlot.h"

#import "SPIProbe.h"
#import "SPIRun.h"

@implementation SPIPlot 

@dynamic thumbnail;
@dynamic probe;
@dynamic run;

@end
