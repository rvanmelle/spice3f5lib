//
//  SPISolderDot.m
//  touchspice
//
//  Created by Shawn Hyam on 12-04-26.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SPISolderDot.h"
#import "SPISchematic.h"

@interface SPISolderDot ()
@property (nonatomic, retain) NSValue * positionValue;
@end


@implementation SPISolderDot

@dynamic positionValue;
@dynamic position;
@dynamic schematic;

- (CGPoint)position {
	[self willAccessValueForKey:@"position"];
	CGPoint pt = [self.positionValue CGPointValue];
	[self didAccessValueForKey:@"position"];
	return pt;
}

- (void)setPosition:(CGPoint)pt {
	[self willChangeValueForKey:@"position"];
	self.positionValue = [NSValue valueWithCGPoint:pt];	
	[self didChangeValueForKey:@"position"];
}


@end
