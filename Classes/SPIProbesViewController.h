//
//  SPIPlotsViewController.h
//  touchspice
//
//  Created by Shawn Hyam on 12-03-22.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SPICircuit;

@interface SPIProbesViewController : BWDTableViewController

@property (nonatomic, retain) SPICircuit *circuit;

@end
