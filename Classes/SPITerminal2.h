//
//  SPITerminal.h
//  touchspice
//
//  Created by Shawn Hyam on 10-12-22.
//  Copyright 2010 Brierwood Design Cooperative. All rights reserved.
//

#import <CoreData/CoreData.h>

@class SPIInstance;
@class SPINet;
@class SPIPin;
@class SPIWire;

@interface SPITerminal :  NSManagedObject  
{
}

@property (nonatomic, retain) SPIWire * wire;
@property (nonatomic, retain) SPINet * net;
@property (nonatomic, retain) SPIInstance * instance;
@property (nonatomic, retain) SPIPin * pin;

@end

