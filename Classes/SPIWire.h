//
//  SPIWire.h
//  touchspice
//
//  Created by Shawn Hyam on 12-04-26.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class SPINet, SPISchematic, SPIWireConnection;
@class SPIVoltageProbe;

@interface SPIWire : NSManagedObject

@property (nonatomic, retain) NSArray * points;
@property (nonatomic, retain) SPISchematic *schematic;
@property (nonatomic, retain) SPINet *net;
@property (nonatomic, retain) NSSet *connections;
@property (nonatomic, retain) NSSet* voltageProbes;
@end

@interface SPIWire (CoreDataGeneratedAccessors)

- (void)addConnectionsObject:(SPIWireConnection *)value;
- (void)removeConnectionsObject:(SPIWireConnection *)value;
- (void)addConnections:(NSSet *)values;
- (void)removeConnections:(NSSet *)values;

- (void)addVoltageProbesObject:(SPIVoltageProbe *)value;
- (void)removeVoltageProbesObject:(SPIVoltageProbe *)value;
- (void)addVoltageProbes:(NSSet *)value;
- (void)removeVoltageProbes:(NSSet *)value;

@end



@interface SPIWire (Helpers)

@property (nonatomic, readonly) CGPathRef path;
@property (nonatomic) NSString * SVGPath;

- (SPIWire*)splitWireAt:(CGPoint)splitPoint;
- (CGPathRef)fillPath:(CGFloat)padding;
- (NSArray*)allPoints:(CGFloat)stepSize;  // ALL points defining a wire, aligning to the given grid step

@end