// 
//  SPIProbeSet.m
//  touchspice
//
//  Created by Shawn Hyam on 10-12-31.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "SPIProbeSet.h"

#import "SPIAnalysis.h"
#import "SPIProbe.h"

@implementation SPIProbeSet 

@dynamic probes;

@end
