//
//  SPIInstancePosition.h
//  touchspice
//
//  Created by Shawn Hyam on 10-12-22.
//  Copyright 2010 Brierwood Design Cooperative. All rights reserved.
//

#import <CoreData/CoreData.h>
#import <QuartzCore/QuartzCore.h>

@class SPIInstance;
@class SPISchematic;

@interface SPIInstancePosition :  NSManagedObject  
{
	CGAffineTransform transform;
}

- (void)rotateCW;


@property (nonatomic, assign) CGAffineTransform transform;
@property (nonatomic, retain) SPIInstance * instance;
@property (nonatomic, retain) SPISchematic * schematic;

// DON'T USE DIRECTLY
@property (nonatomic, retain) NSValue* transformValue;

@end



