//
//  NSString+SPI.h
//  ispice
//
//  Created by Reid van Melle on 10-06-05.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (SPI)

-(const char*)cString;
+(NSString*)fromCString:(const char*)str;
+(NSNumber*)floatValueFromCString:(const char*)str;

@end
