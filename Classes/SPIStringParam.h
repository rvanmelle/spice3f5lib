//
//  SPIStringParam.h
//  touchspice
//
//  Created by Reid van Melle on 12-04-01.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "SPIParameter.h"


@interface SPIStringParam : SPIParameter

@property (nonatomic, retain) NSString * defaultValue;
@property (nonatomic, retain) id validValues;

@end
