//
//  SPINet.h
//  touchspice
//
//  Created by Shawn Hyam on 10-12-22.
//  Copyright 2010 Brierwood Design Cooperative. All rights reserved.
//

#import <CoreData/CoreData.h>

@class SPICircuit;
@class SPITerminal;
@class SPIWire;
@class SPIVoltageProbe;

@interface SPINet :  NSManagedObject  
{
}

@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSSet* wires;
@property (nonatomic, retain) SPICircuit * circuit;
@property (nonatomic, retain) NSSet* terminals;
@property (nonatomic, retain) NSSet* voltageProbes;

- (BOOL)isGround;

@end

@interface SPINet (Geometry)

- (CGRect)boundingBox;

@end


@interface SPINet (CoreDataGeneratedAccessors)
- (void)addWiresObject:(SPIWire *)value;
- (void)removeWiresObject:(SPIWire *)value;
- (void)addWires:(NSSet *)value;
- (void)removeWires:(NSSet *)value;

- (void)addTerminalsObject:(SPITerminal *)value;
- (void)removeTerminalsObject:(SPITerminal *)value;
- (void)addTerminals:(NSSet *)value;
- (void)removeTerminals:(NSSet *)value;

- (void)addVoltageProbesObject:(SPIVoltageProbe *)value;
- (void)removeVoltageProbesObject:(SPIVoltageProbe *)value;
- (void)addVoltageProbes:(NSSet *)value;
- (void)removeVoltageProbes:(NSSet *)value;

@end

