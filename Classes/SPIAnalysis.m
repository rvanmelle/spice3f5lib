// 
//  SPIAnalysis.m
//  touchspice
//
//  Created by Shawn Hyam on 10-12-31.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "SPIAnalysis.h"

#import "SPICircuit.h"
#import "SPIProbeSet.h"

@implementation SPIAnalysis 

@dynamic name;
@dynamic circuit;
@dynamic probes;

@end
