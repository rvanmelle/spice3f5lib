//
//  SPIWireConnection.h
//  touchspice
//
//  Created by Shawn Hyam on 12-04-26.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class SPIWire;

@protocol SPIWireConnectionProtocol <NSObject>
@property (nonatomic, readonly) CGPoint position;
@end


@interface SPIWireConnection : NSManagedObject<SPIWireConnectionProtocol>

@property (nonatomic, retain) NSSet *wires;
@end

@interface SPIWireConnection (CoreDataGeneratedAccessors)

- (void)addWiresObject:(SPIWire *)value;
- (void)removeWiresObject:(SPIWire *)value;
- (void)addWires:(NSSet *)values;
- (void)removeWires:(NSSet *)values;

@end

