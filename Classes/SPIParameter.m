//
//  SPIParameter.m
//  touchspice
//
//  Created by Reid van Melle on 12-04-01.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SPIParameter.h"
#import "SPIPrimitive.h"
#import "SPIProperty.h"


@implementation SPIParameter

@dynamic name;
@dynamic details;
@dynamic required;
@dynamic sectionName;
@dynamic units;
@dynamic properties;
@dynamic primitive;

@end
