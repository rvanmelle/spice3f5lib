//
//  SPIParameterViewController.h
//  touchspice
//
//  Created by Shawn Hyam on 12-03-22.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SPIInstance.h"

@interface SPIPropertyViewController : BWDTableViewController

@property (nonatomic, strong) SPIInstance *instance;

@end
