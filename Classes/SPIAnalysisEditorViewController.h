//
//  SPIAnalysisEditorViewController.h
//  touchspice
//
//  Created by Shawn Hyam on 12-03-23.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BWDViewController.h"

@class  SPIAnalysis;

@interface SPIAnalysisEditorViewController : BWDViewController

@property (strong, nonatomic) SPIAnalysis *analysis;

@end
