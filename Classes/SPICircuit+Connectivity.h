//
//  SPICircuit+Connectivity.h
//  touchspice
//
//  Created by Shawn Hyam on 12-01-27.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SPICircuit.h"

@interface SPICircuit (Connectivity)

- (void)validateConnectivity;

@end
