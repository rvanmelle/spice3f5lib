// 
//  SPIPin.m
//  touchspice
//
//  Created by Shawn Hyam on 10-12-22.
//  Copyright 2010 Brierwood Design Cooperative. All rights reserved.
//

#import "SPIPin.h"

#import "SPIPrimitive.h"
#import "SPITerminal.h"

@implementation SPIPin 

@dynamic name;
@dynamic offsetValue;
@dynamic terminals;
@dynamic primitive;

- (CGPoint)offset {
	[self willAccessValueForKey:@"offset"];
	offset = [self.offsetValue CGPointValue];  // TODO only calculate when necessary
	[self didAccessValueForKey:@"offset"];
	
	return offset;
}

- (void)setOffset:(CGPoint)pt {
	[self willChangeValueForKey:@"offset"];
	offset = pt;
	[self didChangeValueForKey:@"offset"];
	
	self.offsetValue = [NSValue valueWithCGPoint:offset];
}

@end
