//
//  SPISchematicView.h
//  touchspice
//
//  Created by Shawn Hyam on 10-12-22.
//  Copyright 2010 Brierwood Design Cooperative. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SPISchematic;
@class SPINet;
@class SPIInstance;
@class SPITerminal;
@class SPIWire;

@interface SPIComponentView : UIView
@property (nonatomic, assign) BOOL highlighted;
@property (nonatomic, assign) BOOL selected;
@property (nonatomic, readonly, strong) NSManagedObject *component;
@end

@interface SPIInstanceView : SPIComponentView
@property (nonatomic, readonly) SPIInstance *instance;
- (id)initWithInstance:(SPIInstance*)instance;
@end

@interface SPITerminalView : SPIComponentView
@property (nonatomic, readonly) SPITerminal *terminal;
- (id)initWithTerminal:(SPITerminal*)terminal;
@end

@interface SPIWireView : SPIComponentView
@property (nonatomic, readonly) SPIWire *wire;
- (id)initWithWire:(SPIWire*)wire;
@end



@interface SPISchematicView : UIView {
	SPISchematic *schematic;
	CALayer *selectionLayer;
}

@property (nonatomic, retain) SPISchematic *schematic;
@property (nonatomic, retain) CALayer *selectionLayer;

- (void)selectNet:(SPINet*)net;
- (void)addInstance:(SPIInstance*)instance;

@end
