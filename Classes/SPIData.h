//
//  SPIData.h
//  touchspice
//
//  Created by Shawn Hyam on 10-12-31.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface SPIData : UIManagedDocument {
}


// HACK this is only for making the context available to lua scripts -- there is a better way to do
// this, I just don't know what it is
+ (NSManagedObjectContext*)currentContext;
- (void)libraryInit;
- (void)libraryInitInContext:(NSManagedObjectContext*)context;

@end
