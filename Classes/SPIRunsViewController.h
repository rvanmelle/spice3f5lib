//
//  SPIRunsViewController.h
//  touchspice
//
//  Created by Shawn Hyam on 12-03-23.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BWDTableViewController.h"

@class SPICircuit;

@interface SPIRunsViewController : BWDTableViewController

@property (nonatomic, strong) SPICircuit *circuit;

@end
