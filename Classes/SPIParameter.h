//
//  SPIParameter.h
//  touchspice
//
//  Created by Reid van Melle on 12-04-01.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class SPIPrimitive, SPIProperty;

@interface SPIParameter : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * details;
@property (nonatomic, retain) NSNumber * required;
@property (nonatomic, retain) NSString * sectionName;
@property (nonatomic, retain) NSNumber * units;
@property (nonatomic, retain) NSSet *properties;
@property (nonatomic, retain) SPIPrimitive *primitive;
@end

@interface SPIParameter (CoreDataGeneratedAccessors)

- (void)addPropertiesObject:(SPIProperty *)value;
- (void)removePropertiesObject:(SPIProperty *)value;
- (void)addProperties:(NSSet *)values;
- (void)removeProperties:(NSSet *)values;

@end
