//
//  touchspiceAppDelegate.m
//  touchspice
//
//  Created by Shawn Hyam on 10-12-18.
//  Copyright (c) 2010 Brierwood Design Cooperative. All rights reserved.
//


#import "touchspiceAppDelegate.h"
#import <CoreData/CoreData.h>
#import "SampleLibrary.h"
#import "SPIDocument.h"
#import "NSManagedObjectContextHelper.h"
#import "SPIGalleryViewController.h"
#import "SPIData.h"

@implementation touchspiceAppDelegate


@synthesize window;

- (NSURL*)applicationDocumentsDirectory {
	return [NSURL fileURLWithPath:[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject]];
}



- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    SPIGalleryViewController *vc = [[SPIGalleryViewController alloc] initWithNibName:@"SPIGalleryViewController" bundle:nil];
    vc.document = [[SPIDocument alloc] init];

#if 1
    NSURL *docURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"mylib"];
    SPIData *doc = [[SPIData alloc] initWithFileURL:docURL];
    
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
                             [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
    doc.persistentStoreOptions = options;
    
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:[docURL path]]) {
        [doc openWithCompletionHandler:^(BOOL success){
            if (!success) {
                // Handle the error.
                NSLog(@"Error opening UIManagedDocument");
                return;
            } 
            
            NSLog(@"Opened UIManagedDocument");
            //[doc libraryInitInContext:vc.document.context];
            //NSFetchRequest *req = [NSFetchRequest fetchRequestWithEntityName:@"SPISchematic"];
            //[req setFetchLimit:1];
            // :FIXME:
            //NSArray *schematics = [doc.managedObjectContext executeFetchRequest:req error:nil];
            //NSLog(@"foudn schematics: %d", schematics.count);
            //SPISchematic *schematic = [schematics objectAtIndex:0];
            
            //SPISchematic *schematic = [[doc.managedObjectContext executeFetchRequest:req error:nil] objectAtIndex:0];
            //vc.schematic = schematic;
        }];
    }
    else {
        [doc libraryInitInContext:vc.document.context];
        //SPISchematic *schematic = [SampleLibrary createSchematic:doc];
        //vc.schematic = schematic;
        //	schematicView.schematic = schematic;

        
        /*[doc saveToURL:docURL forSaveOperation:UIDocumentSaveForCreating completionHandler:^(BOOL success){
            if (!success) {
                // Handle the error.
                NSLog(@"Error creating UIManagedDocument");
                return;
            }
            NSLog(@"Created UIManagedDocument");
        }];*/
    }
#else
    
    vc.document = [[SPIDocument alloc] init];
    //[vc.document addSeedData];
#endif
    
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];

    window.rootViewController = nav;
    [window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillTerminate:(UIApplication *)application {

    // Save data if appropriate.
}


@end

