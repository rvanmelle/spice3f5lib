//
//  SPIProperty.h
//  touchspice
//
//  Created by Shawn Hyam on 10-12-23.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <CoreData/CoreData.h>

@class SPIInstance;
@class SPIParameter;

@interface SPIProperty :  NSManagedObject  
{
}

@property (nonatomic, retain) id value;
@property (nonatomic, retain) SPIInstance * instance;
@property (nonatomic, retain) SPIParameter * parameter;

@end



