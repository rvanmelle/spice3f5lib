//
//  NSManagedObjectContext+Extra.m
//  LSATLogic
//
//  Created by Reid van Melle on 09-09-25.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//
// from: http://cocoawithlove.com/2008/03/core-data-one-line-fetch.html

#import "NSManagedObjectContextHelper.h"


@implementation NSManagedObjectContext (BWCHelper)

- (BOOL)saveWithErrorLogging 
{
	NSError *error;
	if (![self save:&error]) {
		NSLog(@"error while saving the test results data: %@ %@ %@", 
			[error localizedDescription], [error localizedFailureReason], [error localizedRecoverySuggestion]);
		NSLog(@"Failed to save to data store: %@", [error localizedDescription]);
		NSArray* detailedErrors = [[error userInfo] objectForKey:NSDetailedErrorsKey];
		if (detailedErrors != nil && [detailedErrors count] > 0) {
			for (NSError* detailedError in detailedErrors) {
				NSLog(@"  DetailedError: %@", [detailedError userInfo]);
			}
		} else {
			NSLog(@"  %@", [error userInfo]);
		}
		return NO;
	}
	return YES;
}

- (NSArray*)fetchObjectsForEntityName:(NSString *)newEntityName 
	sortedBy:(id)stringOrArray ascending:(BOOL)o withPredicate:(id)stringOrPredicate, ...
{
	NSEntityDescription *entity = [NSEntityDescription
			entityForName:newEntityName inManagedObjectContext:self];

	NSFetchRequest *request = [[NSFetchRequest alloc] init];
	[request setEntity:entity];
	
	if (stringOrArray)
	{
		NSArray *sortDescriptors;
		if ([stringOrArray isKindOfClass:[NSString class]]) {
			NSSortDescriptor *desc = [[NSSortDescriptor alloc] initWithKey:stringOrArray ascending:o];
			sortDescriptors = [NSArray arrayWithObjects:desc, nil];
		} else {
			NSAssert2([stringOrArray isKindOfClass:[NSArray class]],
					@"Second parameter passed to %s is of unexpected class %@",
					sel_getName(_cmd), NSStringFromClass([stringOrArray class]));
			sortDescriptors = (NSArray*)stringOrArray;
		}
		[request setSortDescriptors:sortDescriptors];

	}
	
	if (stringOrPredicate)
	{
		NSPredicate *predicate;
		if ([stringOrPredicate isKindOfClass:[NSString class]])
		{
			va_list variadicArguments;
			va_start(variadicArguments, stringOrPredicate);
			predicate = [NSPredicate predicateWithFormat:stringOrPredicate
					arguments:variadicArguments];
			va_end(variadicArguments);
		}
		else
		{
			NSAssert2([stringOrPredicate isKindOfClass:[NSPredicate class]],
					@"Second parameter passed to %s is of unexpected class %@",
					sel_getName(_cmd), NSStringFromClass([stringOrPredicate class]));
			predicate = (NSPredicate *)stringOrPredicate;
		}
		[request setPredicate:predicate];
	}
	 
	NSError *error = nil;
	NSArray *results = [self executeFetchRequest:request error:&error];
	if (error != nil)
	{
		[NSException raise:NSGenericException format:[error description]];
	}
	return results;

}


// Convenience method to fetch the array of objects for a given Entity
// name in the context, optionally limiting by a predicate or by a predicate
// made from a format NSString and variable arguments.
//
- (NSSet *)fetchObjectsForEntityName:(NSString *)newEntityName
    withPredicate:(id)stringOrPredicate, ...
{
	NSEntityDescription *entity = [NSEntityDescription
			entityForName:newEntityName inManagedObjectContext:self];

	NSFetchRequest *request = [[NSFetchRequest alloc] init];
	[request setEntity:entity];
	
	if (stringOrPredicate)
	{
		NSPredicate *predicate;
		if ([stringOrPredicate isKindOfClass:[NSString class]])
		{
			va_list variadicArguments;
			va_start(variadicArguments, stringOrPredicate);
			predicate = [NSPredicate predicateWithFormat:stringOrPredicate
					arguments:variadicArguments];
			va_end(variadicArguments);
		}
		else
		{
			NSAssert2([stringOrPredicate isKindOfClass:[NSPredicate class]],
					@"Second parameter passed to %s is of unexpected class %@",
					sel_getName(_cmd), NSStringFromClass([stringOrPredicate class]));
			predicate = (NSPredicate *)stringOrPredicate;
		}
		[request setPredicate:predicate];
	}
	 
	NSError *error = nil;
	NSArray *results = [self executeFetchRequest:request error:&error];
	if (error != nil)
	{
		[NSException raise:NSGenericException format:[error description]];
	}
	
	return [NSSet setWithArray:results];
}

- (NSManagedObject*) fetchObjectForEntityName:(NSString *)newEntityName
    withPredicate:(id)stringOrPredicate, ...
{
	NSEntityDescription *entity = [NSEntityDescription
			entityForName:newEntityName inManagedObjectContext:self];

	NSFetchRequest *request = [[NSFetchRequest alloc] init];
	[request setEntity:entity];
	[request setFetchLimit:1];
	
	if (stringOrPredicate)
	{
		NSPredicate *predicate;
		if ([stringOrPredicate isKindOfClass:[NSString class]])
		{
			va_list variadicArguments;
			va_start(variadicArguments, stringOrPredicate);
			predicate = [NSPredicate predicateWithFormat:stringOrPredicate
					arguments:variadicArguments];
			va_end(variadicArguments);
		}
		else
		{
			NSAssert2([stringOrPredicate isKindOfClass:[NSPredicate class]],
					@"Second parameter passed to %s is of unexpected class %@",
					sel_getName(_cmd), NSStringFromClass([stringOrPredicate class]));
			predicate = (NSPredicate *)stringOrPredicate;
		}
		[request setPredicate:predicate];
	}
	 
	NSError *error = nil;
	NSArray *results = [self executeFetchRequest:request error:&error];
	if (error != nil)
	{
		[NSException raise:NSGenericException format:[error description]];
	}

	if (results.count > 1) 
	{
		[NSException raise:NSGenericException format:@"Expected a single object to be returned"];
			
	}
	
	if (results.count == 1)
	{
		return [results objectAtIndex:0];
	}
	return nil;
}

// LUA Friendly routines -- no varargs
- (NSManagedObject*) fetchObjectForEntityName:(NSString *)newEntityName
    predicate:(id)stringOrPredicate
{
	return [self fetchObjectForEntityName:newEntityName withPredicate:stringOrPredicate, nil];
}

- (NSManagedObject*) fetchPrimitive:(NSString*)primitiveName
{
    NSManagedObject *obj = [self fetchObjectForEntityName:@"SPIPrimitive" withPredicate:@"name == %@", primitiveName];
    assert(obj != nil);
    return obj;
}

@end
