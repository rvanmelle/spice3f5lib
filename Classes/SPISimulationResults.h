//
//  SPISimulationResults.h
//  touchspice
//
//  Created by Reid van Melle on 11-01-13.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SPISimulation.h"


@interface SPISimulationResults : NSObject {

@private
    NSString *rawFilePath;
    NSString *errFilPath;
    NSString *logFilePath;
    NSString *OPFilePath;
    SPISimulationResultStatus status;
}

@property (retain) NSString *rawFilePath, *errFilePath, *logFilePath, *OPFilePath;
@property SPISimulationResultStatus status;

- (id)initWithStatus:(SPISimulationResultStatus)s;

@end
