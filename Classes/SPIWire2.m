// 
//  SPIWire.m
//  touchspice
//
//  Created by Shawn Hyam on 10-12-22.
//  Copyright 2010 Brierwood Design Cooperative. All rights reserved.
//

#import "SPIWire.h"

#import "SPINet.h"
#import "SPISchematic.h"
#import "SPITerminal.h"
#import "NSString+SVG.h"
#import "SPISolderDot.h"

@implementation SPIWire 

@dynamic SVGPath;
@dynamic schematic;
@dynamic net;
@dynamic terminals;
@dynamic solderDots;

@synthesize path = _path;
@synthesize hitTestPath = _hitTestPath;

- (CGPathRef)path {
	if (_path == nil) {
		_path = [self.SVGPath CGPathFromSVG];
	}
	return _path;
}

- (CGPathRef)hitTestPath {
	if (_hitTestPath == nil) {
		_hitTestPath = [self.SVGPath CGFillPathFromSVG:8];
	}
	return _hitTestPath;
}

- (SPIWire*)splitWireAt:(CGPoint)splitPoint {
    NSArray *points = [self.SVGPath pointsFromSVG];
    assert(points.count >= 2);
    
    NSValue *prev_point = nil;
    
    for (NSValue *point in points) {
        if (prev_point) {
            CGPoint pt1 = [prev_point CGPointValue];
            CGPoint pt2 = [point CGPointValue];
            
            CGRect r = CGRectStandardize(CGRectMake(pt1.x, pt1.y, pt2.x-pt1.x, pt2.y-pt1.y));
            r = CGRectInset(r, -1, -1);
            if (CGRectContainsPoint(r, splitPoint)) {
                NSLog(@"SPLIT AT %@ %@ : %@", point, prev_point, NSStringFromCGPoint(splitPoint));
                
                int idx = [points indexOfObject:point];
                NSArray *w1_points = [points subarrayWithRange:NSMakeRange(0, idx)];
                NSMutableArray *w1 = [NSMutableArray arrayWithArray:w1_points];
                [w1 addObject:[NSValue valueWithCGPoint:splitPoint]];
                
                NSArray *w2_points = [points subarrayWithRange:NSMakeRange(idx, points.count-idx)];
                NSMutableArray *w2 = [NSMutableArray arrayWithArray:w2_points];
                
                //  check the case where pt2 == splitPoint
                if ((pt2.x != splitPoint.x) || (pt2.y != splitPoint.y)) {
                    [w2 insertObject:[NSValue valueWithCGPoint:splitPoint] atIndex:0];
                } 
                
                
                // FIXME need to divvy up the endpoints properly
                
                NSLog(@"%@ %@", w1, w2);
                
                SPIWire *wire = [NSEntityDescription insertNewObjectForEntityForName:@"SPIWire" inManagedObjectContext:self.managedObjectContext];
                wire.net = self.net;
                wire.schematic = self.schematic;
                wire.SVGPath = [NSString SVGFromPoints:w2];
                
                
                self.SVGPath = [NSString SVGFromPoints:w1];
                _path = nil;
                
                /*SPISolderDot *dot = [NSEntityDescription insertNewObjectForEntityForName:@"SPISolderDot" inManagedObjectContext:self.managedObjectContext];
                dot.wires = [NSSet setWithObjects:self, wire, nil];*/
                return wire;
             }
            
        }
        prev_point = point;
    }
    return nil;
}

@end
