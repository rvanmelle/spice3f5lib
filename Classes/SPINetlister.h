//
//  SPINetlister.h
//  touchspice
//
//  Created by Shawn Hyam on 10-12-23.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SPICircuit;

@protocol SPINetlisterDelegate<NSObject>

@end



@interface SPINetlister : NSObject {
	id<SPINetlisterDelegate> delegate;
}

@property (nonatomic, retain) id<SPINetlisterDelegate> delegate;

- (NSString*)netlist:(SPICircuit*)circuit;

@end
