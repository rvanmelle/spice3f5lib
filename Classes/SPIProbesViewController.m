//
//  SPIPlotsViewController.m
//  touchspice
//
//  Created by Shawn Hyam on 12-03-22.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SPIProbesViewController.h"
#import "SPIProbeSet.h"
#import "SPIPlot.h"
#import "SPIProbe.h"
#import "SPICircuit.h"

@interface SPIProbesViewController ()

@end

@implementation SPIProbesViewController

@synthesize circuit = _circuit;

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"ProbesCell";
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    SPIProbe *probe = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.textLabel.text = probe.name;
}

#pragma mark -

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"SPIProbe"];
    request.predicate = [NSPredicate predicateWithFormat:@"probeSet.analysis.circuit == %@", self.circuit];
    request.sortDescriptors = [NSArray arrayWithObjects:
                               [NSSortDescriptor sortDescriptorWithKey:@"probeSet.analysis.name" ascending:YES],
                               [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES],
                               nil];
    
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:request 
                                                                        managedObjectContext:self.circuit.managedObjectContext
                                                                          sectionNameKeyPath:@"probeSet.analysis.name"
                                                                                   cacheName:nil];
    self.fetchedResultsController.delegate = self;
    [self.fetchedResultsController performFetch:nil];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

@end
