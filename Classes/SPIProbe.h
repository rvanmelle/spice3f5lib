//
//  SPIProbe.h
//  touchspice
//
//  Created by Shawn Hyam on 10-12-31.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <CoreData/CoreData.h>

@class SPIPlot;
@class SPIProbeSet;
@class SPIAnalysis;

@interface SPIProbe :  NSManagedObject  
{
}

@property (nonatomic, retain) NSSet* plots;
@property (nonatomic, retain) SPIProbeSet * probeSet;
@property (nonatomic, retain) SPIAnalysis * analysis;
@property (nonatomic, retain) NSString *name;

@end


@interface SPIProbe (CoreDataGeneratedAccessors)
- (void)addPlotsObject:(SPIPlot *)value;
- (void)removePlotsObject:(SPIPlot *)value;
- (void)addPlots:(NSSet *)value;
- (void)removePlots:(NSSet *)value;

@end

