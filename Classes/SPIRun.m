// 
//  SPIRun.m
//  touchspice
//
//  Created by Shawn Hyam on 10-12-31.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "SPIRun.h"

#import "SPICircuit.h"
#import "SPIPlot.h"

@implementation SPIRun 

@dynamic timestamp;
@dynamic circuit;
@dynamic plots;

@end
