// 
//  SPICircuit.m
//  touchspice
//
//  Created by Shawn Hyam on 10-12-22.
//  Copyright 2010 Brierwood Design Cooperative. All rights reserved.
//

#import "SPICircuit.h"

#import "SPIInstance.h"
#import "SPINet.h"

@implementation SPICircuit 

@dynamic name;
@dynamic nets;
@dynamic instances;
@dynamic analyses;
@dynamic runs;


@end
