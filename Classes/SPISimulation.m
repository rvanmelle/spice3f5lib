//
//  SPISimulation.m
//  touchspice
//
//  Created by Reid van Melle on 11-01-13.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "SPISimulation.h"
#import "NSString+SPI.h"
#import "SPISimulationResults.h"

#import "SPISpice3F5.h"

FILE *cp_deck_err;

@interface SPISimulation (Private)

- (NSString*) OPResultsPlistFilePath;
- (void)saveOPInfo:(NSMutableDictionary*)results;
- (NSString*)rawFilePath;
- (NSString*)errFilePath;
- (NSString*)deckErrFilePath;
- (NSString*)logFilePath;
- (SPISimulationResults*)run;
- (NSString*) writeNetlistContents:(NSString*)netlist;

@end


@implementation SPISimulation

@synthesize dirName, fileName;

- (void)saveOPInfo:(NSMutableDictionary*)results
{
	NSString *error;
	NSData *plistData = [NSPropertyListSerialization dataFromPropertyList:results
													format:NSPropertyListXMLFormat_v1_0
													errorDescription:&error];
	if (plistData) {
		[plistData writeToFile:[self OPResultsPlistFilePath] atomically:YES];
	} else {
		NSLog(@"%@", error);
	}

}

- (SPISimulationResults*)run
{
    NSString *netlistPath = [self.dirName stringByAppendingPathComponent:self.fileName];
	FILE *circuit_file;
	int		err;
	static IFfrontEnd nutmeginfo = {
		IFnewUid,
		IFdelUid,
		OUTstopnow,
		seconds,
		OUTerror,
		OUTpBeginPlot,
		OUTpData,
		OUTwBeginPlot,
		OUTwReference,
		OUTwData,
		OUTwEnd,
		OUTendPlot,
		OUTbeginDomain,
		OUTendDomain,
		OUTattributes
	};
	
	freopen([[self logFilePath] cString],"w",stdout);
	// FIXME -- this needs to be re-enabled for proper error handling 
	//fflush(stderr);
	//freopen([[run errFilePath] cString], "w", stderr);
	FILE *fd_err = fopen([[self errFilePath] cString], "w");
    FILE *deck_fd_err = fopen([[self deckErrFilePath] cString], "w");
	//FILE *logFile = fopen([logFilePath cStringUsingEncoding:[NSString defaultCStringEncoding]], "w");
	//int logFileNo = fileno(logFile);
	
	cp_in = stdin;
	cp_out = stdout;
    //cp_out = fd_err;
	cp_err = fd_err;
    cp_deck_err = deck_fd_err;
	
	//cp_out = logFile;
	if (!(circuit_file = fopen([netlistPath cString], "r"))) {
		NSLog(@"circuit file not available");
		assert(false);
	}
	init_time( );

	ARCHme = 0;
	ARCHsize = 1;
	err = SIMinit(&nutmeginfo,&ft_sim);
	if(err != OK) {
		ft_sperror(err,"SIMinit");
		shutdown(EXIT_BAD);  // FIXME: we probably don't want to shutdown here
	}
	cp_program = ft_sim->simulator;

	srandom(getpid());
	
	inp_spsource(circuit_file, FALSE, (char *) NULL);

	err = ft_dorun([[self rawFilePath] cString]);
    SPISimulationResults *result = nil;
	if (err == 0) {
		struct dvec *v;
		char numbuf[BSIZE_SP];
		
		//****************************************************
		// NSSpice reports OP results into a very inconvenient text format
		// By doing this here, we can capture the results directly from the sim engine
		NSMutableDictionary *opResults = [[NSMutableDictionary alloc] initWithCapacity:10];

		spi_plot *p = setcplot("op");
		if (p != NULL) {
			NSLog(@"\t%-30s%15s\n", "Node", "Voltage");
			NSLog(@"\t%-30s%15s\n", "----", "-------");
			NSLog(@"\t----\t-------\n");

			for (v = p->pl_dvecs; v; v = v->v_next) {
		    if (!isreal(v)) {
					fprintf(cp_err, "Internal error: op vector %s not real\n", v->v_name);
					continue;
		    }
		    if (((v->v_type == SV_VOLTAGE)) && (*(v->v_name)!='@')) {
					printnum(numbuf, v->v_realdata[0]);
					NSLog(@"\t%-30s%15s\n", v->v_name, numbuf);
					[opResults setValue:[NSString floatValueFromCString:numbuf] forKey:[NSString fromCString:v->v_name]];
				}
			}
			
			NSLog(@"\n\tSource\tCurrent\n");
			NSLog(@"\t------\t-------\n\n");
			for (v = plot_cur->pl_dvecs; v; v = v->v_next) {
		    if (v->v_type == SV_CURRENT) {
					printnum(numbuf, v->v_realdata[0]);
					NSLog(@"\t%-30s%15s\n", v->v_name, numbuf);
				}
			}
		}
		[self saveOPInfo:opResults];
		//****************************************************
		result = [[SPISimulationResults alloc] initWithStatus:kSPISimulationResultStatusCompleted];
        result.errFilePath = [self errFilePath];
        result.rawFilePath = [self rawFilePath];
        result.logFilePath = [self logFilePath];
        result.OPFilePath = [self OPResultsPlistFilePath];
	} else {
		result = [[SPISimulationResults alloc] initWithStatus:kSPISimulationResultStatusFailed];
	}
    fflush(cp_deck_err);
    fclose(cp_deck_err);
    fflush(cp_err);
    fclose(cp_err);
    return result;
}

- (SPISimulationResults*)runWithNetlist:(NSString*)netlist
{
	self.dirName = NSTemporaryDirectory();
	[self writeNetlistContents:netlist];
	return [self run];
}

- (SPISimulationResults*)runWithNetlist:(NSString*)netlist inDirectory:(NSString*)dir
{
    self.dirName = dir;
	[self writeNetlistContents:netlist];
	return [self run];
}

- (SPISimulationResults*)runWithNetlistFilename:(NSString*)file
{
    self.fileName = [file lastPathComponent];
    self.dirName = [file stringByDeletingLastPathComponent];
    return [self run];
}

-(NSString*) OPResultsPlistFilePath
{
	return [dirName stringByAppendingPathComponent:@"opInfo.plist"];
}

-(void) writeNetlistContents:(NSString*)netlist
{
    assert(self.dirName != nil);
	self.fileName = @"netlist.cir";
	//CFUUIDRef theUUID = CFUUIDCreate(NULL);
	//CFStringRef string = CFUUIDCreateString(NULL, theUUID);
	//CFRelease(theUUID);
	//NSString *fileUID = [(NSString *)string autorelease];
	NSString *abs_filename = [self.dirName stringByAppendingPathComponent:self.fileName];
	// ? perhaps this is better ? from NSFileManager
	// - (BOOL)createFileAtPath:(NSString *)path contents:(NSData *)contents attributes:(NSDictionary *)attributes
	// automatically overwrite a file at the path -- 
	if ([[NSFileManager defaultManager] fileExistsAtPath:abs_filename]) {
		[[NSFileManager defaultManager] removeItemAtPath:abs_filename error:NULL];
	}
	
	[netlist writeToFile:abs_filename atomically:NO encoding:NSASCIIStringEncoding error:NULL];
}

-(NSString*) logFilePath
{
	return [dirName stringByAppendingPathComponent:@"results.log.txt"];
}

-(NSString*) errFilePath
{
	return [dirName stringByAppendingPathComponent:@"results.errors.txt"];
}

-(NSString*) deckErrFilePath
{
	return [dirName stringByAppendingPathComponent:@"deck.errors.txt"];
}

-(NSString*) rawFilePath
{
	return [dirName stringByAppendingPathComponent:@"results.raw"];
}

@end
