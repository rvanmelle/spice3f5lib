// 
//  SPIPrimitiveNetlistTemplate.m
//  touchspice
//
//  Created by Shawn Hyam on 10-12-23.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "SPIPrimitiveNetlistTemplate.h"

#import "SPIPrimitive.h"

@implementation SPIPrimitiveNetlistTemplate 

@dynamic templateString;
@dynamic primitive;

@end
