//
//  SPIData.m
//  touchspice
//
//  Created by Shawn Hyam on 10-12-31.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

// TODO pull in the GTM stuff or similar

#import "SPIData.h"
#import "wax.h"
#import "lauxlib.h" 

@implementation SPIData


static NSManagedObjectContext *ctx = nil;


+ (NSManagedObjectContext*)currentContext {
    return ctx;
}

- (void)libraryInitInContext:(NSManagedObjectContext*)context
{
    ctx = context;
    wax_setup();
	NSLog(@"init -> %d", luaL_dofile(wax_currentLuaState(), WAX_DATA_DIR "/scripts/wax/init.lua"));
	assert(luaL_dofile(wax_currentLuaState(), "data/scripts/TouchSpiceHelpers.lua") == 0);
	assert(luaL_dofile(wax_currentLuaState(), "data/scripts/AnalogLib.lua") == 0);
    assert(luaL_dofile(wax_currentLuaState(), "data/scripts/AnalogLibParams.lua") == 0);
    assert(luaL_dofile(wax_currentLuaState(), "data/scripts/ExampleCircuits.lua") == 0);
}

- (void)libraryInit {
    ctx = self.managedObjectContext;
	wax_setup();
	//NSString *filePath = [[NSBundle mainBundle] pathForResource:@"init" ofType:@"lua"]; 
	NSLog(@"init -> %d", luaL_dofile(wax_currentLuaState(), WAX_DATA_DIR "/scripts/wax/init.lua"));
	assert(luaL_dofile(wax_currentLuaState(), "TouchSpiceHelpers.lua") == 0);
	//NSLog(@"TouchSpiceHelpers -> %d", );
	assert(luaL_dofile(wax_currentLuaState(), "AnalogLib.lua") == 0);
	//NSLog(@"AnalogLib -> %d", );
	
    assert(luaL_dofile(wax_currentLuaState(), "ExampleCircuits.lua") == 0);
    
	//NSLog(@"ExampleCircuits -> %d", );
	
	//NSLog(@"init.lua -> %d", luaL_dofile(wax_currentLuaState(), [filePath cStringUsingEncoding:NSUTF8StringEncoding]));
	//NSLog(@"%d", luaL_dofile(wax_currentLuaState(), "schematic.lua"));
}



@end
