//
//  SPIWire.m
//  touchspice
//
//  Created by Shawn Hyam on 12-04-26.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SPIWire.h"
#import "SPINet.h"
#import "SPISchematic.h"
#import "SPIWireConnection.h"
#import "NSString+SVG.h"
#import "SPIRoutingGrid.h"

@implementation SPIWire

@dynamic points;
@dynamic schematic;
@dynamic net;
@dynamic connections;
@dynamic voltageProbes;

@end



@implementation SPIWire (Helpers)

- (NSString*)SVGPath {
    
    return nil;
}

- (NSArray*)allPoints:(CGFloat)stepSize {
    NSValue *prev_point = nil;
    NSMutableArray *result = [NSMutableArray arrayWithCapacity:64];
    
    for (NSValue *point in self.points) {
        if (prev_point) {
            CGPoint pt1 = [prev_point CGPointValue];
            CGPoint pt2 = [point CGPointValue];
            
            SPIGridCoordinate start = gridCoordinateFromPoint(pt1);
            SPIGridCoordinate stop = gridCoordinateFromPoint(pt2);
            
            for (int ydelta=0; ydelta<=abs(start.y-stop.y); ydelta++) {
                int yd = (start.y<stop.y) ? ydelta : -ydelta;
                for (int xdelta=0; xdelta<=abs(start.x-stop.x); xdelta++) {
                    int xd = (start.x<stop.x) ? xdelta : -xdelta;
                    SPIGridCoordinate coord = { .x = start.x+xd, .y = start.y+yd };
                    CGPoint p = pointFromGridCoordinate(coord);
                    [result addObject:[NSValue valueWithCGPoint:p]];
                }
            }
                
            
        }
        prev_point = point;
    }
    return result;
}

- (void)setSVGPath:(NSString *)SVGPath {
    self.points = [SVGPath pointsFromSVG];
}

- (CGPathRef)path {
    CGMutablePathRef path = CGPathCreateMutable();
    
    BOOL first = YES;
    CGAffineTransform t = CGAffineTransformIdentity;
    for (NSValue *value in self.points) {
        CGPoint point = [value CGPointValue];
        if (first) {
            CGPathMoveToPoint(path, &t, point.x, point.y);
            first = NO;
        } else {
            CGPathAddLineToPoint(path, &t, point.x, point.y);
        }
    }
    return CGPathCreateCopy(path);
    
}

- (CGPathRef)fillPath:(CGFloat)padding {
    CGMutablePathRef path = CGPathCreateMutable();
    
    NSValue *prevValue = nil;
    CGAffineTransform t = CGAffineTransformIdentity;
    for (NSValue *value in self.points) {
        if (prevValue != nil) {
            CGPoint point1 = [prevValue CGPointValue];
            CGPoint point2 = [value CGPointValue];
            CGRect r = CGRectStandardize(CGRectMake(point1.x, point1.y, point2.x-point1.x, point2.y-point1.y));
            CGPathAddRect(path, &t, CGRectInset(r, -padding, -padding));
        }
        prevValue = value;
    }
    return CGPathCreateCopy(path);
}

- (SPIWire*)splitWireAt:(CGPoint)splitPoint {
    NSArray *points = self.points;
    assert(points.count >= 2);
    
    NSValue *prev_point = nil;
    
    for (NSValue *point in points) {
        if (prev_point) {
            CGPoint pt1 = [prev_point CGPointValue];
            CGPoint pt2 = [point CGPointValue];
            
            CGRect r = CGRectStandardize(CGRectMake(pt1.x, pt1.y, pt2.x-pt1.x, pt2.y-pt1.y));
            r = CGRectInset(r, -1, -1);
            if (CGRectContainsPoint(r, splitPoint)) {
                NSLog(@"SPLIT AT %@ %@ : %@", point, prev_point, NSStringFromCGPoint(splitPoint));
                
                int idx = [points indexOfObject:point];
                NSArray *w1_points = [points subarrayWithRange:NSMakeRange(0, idx)];
                NSMutableArray *w1 = [NSMutableArray arrayWithArray:w1_points];
                [w1 addObject:[NSValue valueWithCGPoint:splitPoint]];
                
                NSArray *w2_points = [points subarrayWithRange:NSMakeRange(idx, points.count-idx)];
                NSMutableArray *w2 = [NSMutableArray arrayWithArray:w2_points];
                
                //  check the case where pt2 == splitPoint
                if (!CGPointEqualToPoint(pt2, splitPoint)) {
                    [w2 insertObject:[NSValue valueWithCGPoint:splitPoint] atIndex:0];
                } 
                
                SPIWire *wire = [NSEntityDescription insertNewObjectForEntityForName:@"SPIWire" inManagedObjectContext:self.managedObjectContext];
                wire.net = self.net;
                wire.schematic = self.schematic;
                wire.points = w2;
                
                // FIXME have to divide up the connections properly
                NSValue *firstPoint = [self.points objectAtIndex:0];
                SPIWireConnection * movedConnection = nil;
                NSLog(@"wire has %d connections", self.connections.count);
                
                for (SPIWireConnection *conn in self.connections) {
                    NSLog(@"compare %@ %@", NSStringFromCGPoint(conn.position), firstPoint);
                    if (!CGPointEqualToPoint(conn.position, firstPoint.CGPointValue)) {
                        assert(movedConnection == nil);
                        // the wire is no longer connected to this connection point, must be on the new wire
                         movedConnection = conn;
                    }
                }
                assert(movedConnection);
                [self removeConnectionsObject:movedConnection];
                [wire addConnectionsObject:movedConnection];
                // the movedConnection var is for ensuring exactly one gets moved
                
                
                self.points = w1;
                
                /*SPISolderDot *dot = [NSEntityDescription insertNewObjectForEntityForName:@"SPISolderDot" inManagedObjectContext:self.managedObjectContext];
                 dot.wires = [NSSet setWithObjects:self, wire, nil];*/
                return wire;
            }
            
        }
        prev_point = point;
    }
    return nil;
    
}

@end