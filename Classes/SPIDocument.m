//
//  SPIDocument.m
//  touchspice
//
//  Created by Shawn Hyam on 12-03-22.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SPIDocument.h"
#import "SPISchematic.h"
#import "SPIRun.h"
#import "SPIAnalysis.h"
#import "SPIProbeSet.h"
#import "SPIProbe.h"
#import "SPIPlot.h"
#import "SPINet.h"
#import "SPIVoltageProbe.h"
#import "SPIProperty.h"
#import "SPIParameter.h"
#import "SPIPrimitive.h"
#import "SPIInstance.h"
#import "SPIInstancePosition.h"
#import "SPIPin.h"
#import "SPIPrimitiveNetlistTemplate.h"

@interface SPIDocument () 

@property (strong, atomic) NSTimer *saveTimer;

@end

@implementation SPIDocument

@synthesize context = __context;
@synthesize managedObjectModel = __managedObjectModel;
@synthesize persistentStoreCoordinator = __persistentStoreCoordinator;
@synthesize saveTimer = _saveTimer;

#pragma mark - Application's Documents directory

/**
 Returns the URL to the application's Documents directory.
 */
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}



- (void)addSeedData {
    SPISchematic *schematic = [NSEntityDescription insertNewObjectForEntityForName:@"SPISchematic" inManagedObjectContext:self.context];
    schematic.name = @"Test Schematic";
    
    SPINet *net1 = [NSEntityDescription insertNewObjectForEntityForName:@"SPINet" inManagedObjectContext:self.context];
    net1.name = @"net1";
    net1.circuit = schematic;
    
    SPINet *net2 = [NSEntityDescription insertNewObjectForEntityForName:@"SPINet" inManagedObjectContext:self.context];
    net2.name = @"net2";
    net2.circuit = schematic;
    
    SPIPrimitive *presistor = [NSEntityDescription insertNewObjectForEntityForName:@"SPIPrimitive" inManagedObjectContext:self.context];
    presistor.name = @"presistor";
    presistor.SVGPath = @"M0,-60 0,-48 -10,-45 10,-39 -10,-33 10,-27 -10,-21 10,-15 0,-12 0,0";
    presistor.instanceRect = CGRectMake(-20, -60, 40, 60);  // -20 doesn't seem right...
    
    SPIPrimitiveNetlistTemplate *template = [NSEntityDescription insertNewObjectForEntityForName:@"SPIPrimitiveNetlistTemplate" inManagedObjectContext:self.context];
    template.templateString = @"R<InstId> $PLUS $MINUS 1000";
    template.primitive = presistor;
    
    {
        SPIPin *pin1 = [NSEntityDescription insertNewObjectForEntityForName:@"SPIPin" inManagedObjectContext:self.context];
        pin1.name = @"MINUS";
        pin1.offset = CGPointMake(0, -60);
        pin1.primitive = presistor;
        
        SPIPin *pin2 = [NSEntityDescription insertNewObjectForEntityForName:@"SPIPin" inManagedObjectContext:self.context];
        pin2.name = @"PLUS";
        pin2.offset = CGPointMake(0, 0);
        pin2.primitive = presistor;
        
        // TODO labels
    }
        
    SPIParameter *parameter1 = [NSEntityDescription insertNewObjectForEntityForName:@"SPIParameter" inManagedObjectContext:self.context];
    parameter1.primitive = presistor;
    parameter1.name = @"parameter1";
    
    SPIInstance *instance = [SPIInstance instanceFromPrimitive:presistor inCircuit:schematic];
    
    SPIInstancePosition *position = [NSEntityDescription insertNewObjectForEntityForName:@"SPIInstancePosition" inManagedObjectContext:self.context];
    position.transform = CGAffineTransformMakeTranslation(40, 40);
    position.instance = instance;
    position.schematic = schematic;

    
    SPIProperty *property1 = [NSEntityDescription insertNewObjectForEntityForName:@"SPIProperty" inManagedObjectContext:self.context];
    property1.instance = instance;
    property1.parameter = parameter1;
    property1.value = [NSNumber numberWithInt:99];
    
    SPIAnalysis *analysis = [NSEntityDescription insertNewObjectForEntityForName:@"SPIAnalysis" inManagedObjectContext:self.context];
    analysis.circuit = schematic;
    analysis.name = @"This has a name for sorting purposes only";
    
    SPIProbeSet *probeSet = [NSEntityDescription insertNewObjectForEntityForName:@"SPIProbeSet" inManagedObjectContext:self.context];
    
    SPIVoltageProbe *probe1 = [NSEntityDescription insertNewObjectForEntityForName:@"SPIVoltageProbe" inManagedObjectContext:self.context];
    probe1.probeSet = probeSet;
    probe1.net = net1;
    probe1.name = @"probe1";
    probe1.analysis = analysis;
    
    SPIVoltageProbe *probe2 = [NSEntityDescription insertNewObjectForEntityForName:@"SPIVoltageProbe" inManagedObjectContext:self.context];
    probe2.probeSet = probeSet;
    probe2.net = net2;
    probe2.name = @"probe2";
    probe2.analysis = analysis;
    
    SPIRun *run = [NSEntityDescription insertNewObjectForEntityForName:@"SPIRun" inManagedObjectContext:self.context];
    run.circuit = schematic;

    SPIPlot *plot1 = [NSEntityDescription insertNewObjectForEntityForName:@"SPIPlot" inManagedObjectContext:self.context];
    plot1.probe = probe1;
    plot1.run = run;
    
    SPIPlot *plot2 = [NSEntityDescription insertNewObjectForEntityForName:@"SPIPlot" inManagedObjectContext:self.context];
    plot2.probe = probe2;
    plot2.run = run;
    
    

}

- (void)performSave:(NSTimer*)timer {
    self.saveTimer = nil;
    [self performSelectorOnMainThread:@selector(saveContext) withObject:self waitUntilDone:NO];
}

- (void)objectsDidChange:(NSNotification*)note {
    if (!self.saveTimer) {
        self.saveTimer = [NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(performSave:) userInfo:nil repeats:NO];
    }
}

- (id)init {
    self = [super init];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(objectsDidChange:)
                                                     name:NSManagedObjectContextObjectsDidChangeNotification
                                                   object:self.context];    
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)saveContext
{
    NSLog(@"SAVE");
    NSError *error = nil;
    if ([self.context hasChanges] && ![self.context save:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();        
    }
}

#pragma mark - Core Data stack

/**
 Returns the managed object context for the application.
 If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
 */
- (NSManagedObjectContext *)context
{
    if (!__context) {
        __context = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        __context.persistentStoreCoordinator = self.persistentStoreCoordinator;
    }
    
    return __context;
}

/**
 Returns the managed object model for the application.
 If the model doesn't already exist, it is created from the application's model.
 */
- (NSManagedObjectModel *)managedObjectModel
{
    if (!__managedObjectModel) {
        __managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
        NSAssert(__managedObjectModel, nil);
    }
    
    return __managedObjectModel;
}

/**
 Returns the persistent store coordinator for the application.
 If the coordinator doesn't already exist, it is created and the application's store added to it.
 */
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (!__persistentStoreCoordinator) {
        NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"touchspice.sqlite"];
        [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil];
        
        NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                                 [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption, 
                                 [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, 
                                 nil];
        
        NSError *error = nil;
        __persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
        
        int retries = 0;
        while (![__persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType 
                                                           configuration:nil 
                                                                     URL:storeURL 
                                                                 options:options 
                                                                   error:&error]) {
            
            [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil];
            retries++;
            
            if (retries >= 3) {
                NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
                abort();
            }
        }
    }
    return __persistentStoreCoordinator;
}



@end
