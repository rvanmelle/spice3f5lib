//
//  SPINetlister.m
//  touchspice
//
//  Created by Shawn Hyam on 10-12-23.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "SPINetlister.h"
#import "SPICircuit.h"
#import "SPIInstance.h"
#import "SPINet.h"
#import "SPIPrimitive.h"
#import "SPITerminal.h"
#import "SPIPin.h"
#import "SPIProperty.h"
#import "SPIParameter.h"
#import "SPIDCAnalysis.h"
#import "SPIPrimitiveNetlistTemplate.h"

@implementation SPINetlister

@synthesize delegate;

- (NSString*)netlist:(SPICircuit*)circuit {
	NSMutableArray *lines = [NSMutableArray arrayWithCapacity:64];
	
	// label all nets and instances
	NSLog(@"start labeling");
	
	NSMutableDictionary *netLabels = [NSMutableDictionary dictionaryWithCapacity:circuit.nets.count];
	{
		int i=0;
		for (SPINet *net in circuit.nets) {
			int idx = [net isGround] ? 0 : ++i;  // GND is labeled 0
			[netLabels setObject:[NSString stringWithFormat:@"%d", idx] forKey:net.objectID];
		}
	}
	
	NSMutableDictionary *instLabels = [NSMutableDictionary dictionaryWithCapacity:circuit.instances.count];
	{
		int i=0;
		for (SPIInstance *inst in circuit.instances) {
			[instLabels setObject:[NSString stringWithFormat:@"%d", ++i] forKey:inst.objectID];
		}
	}
	
	NSLog(@"labels done");
	// TODO verify that there is a GND device -- but this should probably be done earlier
	
	
	[lines addObject:@"\n*** INSTANCES"];
	for (SPIInstance *i in circuit.instances) {
        NSLog(@"instance: %@ ", i);
		SPIPrimitive *prim = i.primitive;
		
		if (prim.netlistTemplate == nil) continue;
		
		NSString *r = [prim.netlistTemplate.templateString stringByReplacingOccurrencesOfString:@"<InstId>" withString:[instLabels objectForKey:i.objectID]];
		

		assert(prim.pins.count == i.terminals.count);
		
		//NSLog(@"%@", netLabels);
		for (SPITerminal *t in i.terminals) {
			NSString *termName = [NSString stringWithFormat:@"$%@", t.pin.name];
			if (t.net) {
				r = [r stringByReplacingOccurrencesOfString:termName withString:[netLabels objectForKey:t.net.objectID]];
			}
			/*  FIXME should probably verify all the terminals are connected to nets earlier
			else {
			[nl addError:[NSString stringWithFormat:@"ERROR: %@->%@ is dangling", [self instanceName], termName]];
				r = [r stringByReplacingOccurrencesOfString:termName withString:@"?"];
			}
			 */
		}
		for (SPIProperty *p in i.properties) {
			NSString *paramName = [NSString stringWithFormat:@"#%@#", p.parameter.name];
			r = [r stringByReplacingOccurrencesOfString:paramName withString:[NSString stringWithFormat:@"%@", p.value]];
		}
		[lines addObject:r];
	}
	
	[lines addObject:@"\n*** ANALYSES"];
	[lines addObject:@".OP"];
	for (SPIAnalysis *a in circuit.analyses) {
		//if ([a.enabled boolValue]) {
		if ([a isKindOfClass:[SPIDCAnalysis class]]) {
			SPIDCAnalysis *dca = (SPIDCAnalysis*)a;
			[lines addObject:[NSString stringWithFormat:@".DC %@ %@ %@ %@", dca.sourceName, dca.startValue, dca.stopValue, dca.incrValue]];
		}
		//}
	}
	
	NSString *n = [lines componentsJoinedByString:@"\n"];	
	return n;
}

@end
