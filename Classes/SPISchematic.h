//
//  SPISchematic.h
//  touchspice
//
//  Created by Shawn Hyam on 10-12-22.
//  Copyright 2010 Brierwood Design Cooperative. All rights reserved.
//

#import <CoreData/CoreData.h>
#import "SPICircuit.h"

@class SPIInstancePosition;
@class SPIWire;
@class SPISolderDot;

@interface SPISchematic :  SPICircuit  
{
}

@property (nonatomic, retain) NSSet* wires;
@property (nonatomic, retain) NSSet* solderDots;
@property (nonatomic, retain) NSSet* instancePositions;

@end


@interface SPISchematic (CoreDataGeneratedAccessors)
- (void)addWiresObject:(SPIWire *)value;
- (void)removeWiresObject:(SPIWire *)value;
- (void)addWires:(NSSet *)value;
- (void)removeWires:(NSSet *)value;

- (void)addSolderDotsObject:(SPISolderDot *)value;
- (void)removeSolderDotsObject:(SPISolderDot *)value;
- (void)addSolderDots:(NSSet *)value;
- (void)removeSolderDots:(NSSet *)value;

- (void)addInstancePositionsObject:(SPIInstancePosition *)value;
- (void)removeInstancePositionsObject:(SPIInstancePosition *)value;
- (void)addInstancePositions:(NSSet *)value;
- (void)removeInstancePositions:(NSSet *)value;

@end

