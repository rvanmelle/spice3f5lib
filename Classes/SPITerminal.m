//
//  SPITerminal.m
//  touchspice
//
//  Created by Shawn Hyam on 12-04-26.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SPITerminal.h"
#import "SPIInstance.h"
#import "SPIPin.h"
#import "SPIWire.h"
#import "SPIInstancePosition.h"

@implementation SPITerminal

@dynamic instance;
@dynamic pin;
@dynamic net;
@dynamic currentProbes;

- (SPIWire*)wire {
    return [self.wires anyObject];
}

- (CGPoint)position {
    CGAffineTransform t = self.instance.position.transform;
    return CGPointApplyAffineTransform(self.pin.offset, t);
}

@end
