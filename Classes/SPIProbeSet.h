//
//  SPIProbeSet.h
//  touchspice
//
//  Created by Shawn Hyam on 10-12-31.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <CoreData/CoreData.h>

@class SPIAnalysis;
@class SPIProbe;

@interface SPIProbeSet :  NSManagedObject  
{
}

@property (nonatomic, retain) NSSet* probes;

@end


@interface SPIProbeSet (CoreDataGeneratedAccessors)
- (void)addProbesObject:(SPIProbe *)value;
- (void)removeProbesObject:(SPIProbe *)value;
- (void)addProbes:(NSSet *)value;
- (void)removeProbes:(NSSet *)value;

@end

