// 
//  SPINet.m
//  touchspice
//
//  Created by Shawn Hyam on 10-12-22.
//  Copyright 2010 Brierwood Design Cooperative. All rights reserved.
//

#import "SPINet.h"

#import "SPICircuit.h"
#import "SPITerminal.h"
#import "SPIWire.h"
#import "SPIInstance.h"
#import "SPIPrimitive.h"

@implementation SPINet 

@dynamic name;
@dynamic wires;
@dynamic circuit;
@dynamic terminals;
@dynamic voltageProbes;

- (BOOL)isGround {
	// TODO should probably calculate this aggressively and cache it, because we might want to mark it on the view
	for (SPITerminal *t in self.terminals) {
		if ([t.instance.primitive.ground boolValue]) { return YES; }
	}
	return NO;
}

@end


@implementation SPINet (Geometry)

- (CGRect)boundingBox {
	CGRect r = CGRectNull;
	for (SPIWire *w in self.wires) {
		NSLog(@"wire bb: %@", [NSValue valueWithCGRect:CGPathGetBoundingBox(w.path)]);
		r = CGRectUnion(r, CGPathGetBoundingBox(w.path));
	}
	return r;
}

@end

