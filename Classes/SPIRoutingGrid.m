//
//  SPIRoutingGrid.m
//  touchspice
//
//  Created by Shawn Hyam on 11-10-07.
//  Copyright (c) 2011 Brierwood Design Co-operative. All rights reserved.
//

#import "SPIRoutingGrid.h"
#import "SPINet.h"
#import "SPIInstancePosition.h"
#import "SPIInstance.h"
#import "SPIPrimitive.h"
#import "NSString+SVG.h"
#import "SPIWire.h"
#import "SPITerminal.h"
#import "SPIPin.h"
#import "pqueue.h"

#define YSIZE 256
#define XSIZE 256

SPIGridCoordinate gridCoordinateFromPoint(CGPoint point) {
    CGSize stepSize = CGSizeMake(10, 10);
    int a = (point.x + stepSize.width/2) / stepSize.width;
    int b = (point.y + stepSize.height/2) / stepSize.height;
    SPIGridCoordinate f = { .x = a, .y = b };
    return f;
}

CGPoint pointFromGridCoordinate(SPIGridCoordinate coord) {
    CGSize stepSize = CGSizeMake(10, 10);
    int x = coord.x*stepSize.width ;
    int y = coord.y*stepSize.height;
    return CGPointMake(x, y);
}


typedef struct {
    SPIGridCoordinate point;
    int estimatedCost;
    size_t pos;
} pqueue_node_t;


static int
cmp_pri(double next, double curr)
{
	return (next > curr);
}


static double
get_pri(void *a)
{
	return (double) ((pqueue_node_t *) a)->estimatedCost;
}


static void
set_pri(void *a, double pri)
{
	((pqueue_node_t *) a)->estimatedCost = pri;
}


static size_t
get_pos(void *a)
{
	return ((pqueue_node_t *) a)->pos;
}


static void
set_pos(void *a, size_t pos)
{
	((pqueue_node_t *) a)->pos = pos;
}

@interface SPIRoutingGrid() {
    char _instanceMask[YSIZE][XSIZE];
    char _wireMask[YSIZE][XSIZE];
    char _destinationMask[YSIZE][XSIZE];
    
    //int _movementCost[YSIZE][XSIZE];
    int _distance[YSIZE][XSIZE];
    int _heuristic[YSIZE][XSIZE];
}

@property (strong, nonatomic) SPISchematic *schematic;
@property (assign, nonatomic) CGRect bounds;

@end

@implementation SPIRoutingGrid {
    pqueue_t *_queue;
    
    BOOL _useDistanceHeuristic;
    SPIGridCoordinate _target;
}

@synthesize stepSize = _stepSize;
@synthesize schematic = _schematic;
@synthesize bounds = _bounds;

SPIGridCoordinate move(SPIGridCoordinate elem, int direction) {
    if (direction == 0) elem.x--;
    else if (direction == 1) elem.x++;
    else if (direction == 2) elem.y--;
    else if (direction == 3) elem.y++;
    
    return elem;
}

int check_direction(int distance[YSIZE][XSIZE], int dist, int direction, SPIGridCoordinate elem) {
    elem = move(elem, direction);
    
    if (elem.x<0 || elem.y<0 || elem.x>=XSIZE || elem.y>=YSIZE) return 0;
    
    if (distance[elem.y][elem.x] < 0) return 0;
    if (distance[elem.y][elem.x] < dist) return 1;
    return 0;
}

int get_distance(int distance[YSIZE][XSIZE], int direction, SPIGridCoordinate elem) {
    elem = move(elem, direction);
    if (elem.x<0 || elem.y<0 || elem.x>=XSIZE || elem.y>=YSIZE) return 80000;
    
    return distance[elem.y][elem.x];
}

static const int PoolSize = 1024;
static const int MaxNumPools = 256;

static int numPoolsAllocated = 0;
static int currentIdx = PoolSize;

static pqueue_node_t* pools[MaxNumPools];

void freeCoordinates() {
    for (int i=0; i<numPoolsAllocated; i++) {
        free(pools[i]);
    }
    numPoolsAllocated = 0;
    currentIdx = PoolSize;
}

pqueue_node_t *allocCoordinate() {
    
    if (currentIdx >= PoolSize) {
        currentIdx = 0;
        pqueue_node_t *coords = (pqueue_node_t*)malloc(PoolSize*sizeof(pqueue_node_t));
        pools[numPoolsAllocated] = coords;
        numPoolsAllocated++;
    }
    
    return pools[numPoolsAllocated-1] + currentIdx++;
    
}

NSArray* backtrace(SPIGridCoordinate target, int distance[YSIZE][XSIZE]) {
    int direction = 0;
    SPIGridCoordinate cur = target;
    
    NSMutableArray *path = [NSMutableArray arrayWithCapacity:16];
    //[path appendFormat:@"M%g,%g ", gridSize.width*cur.x, gridSize.height*cur.y];
    
    //[path addObject:[NSValue valueWithCGPoint:CGPointMake(cur.x, cur.y)]];
    
    while (YES) {
        //NSLog(@"(%d, %d) : %d", cur.x, cur.y, distance[cur.y][cur.x]);
        // try the current direction, otherwise try other directions

        int m1 = get_distance(distance, (direction)%4, cur);
        int m2 = get_distance(distance, (direction+1)%4, cur);
        int m3 = get_distance(distance, (direction+2)%4, cur);
        int m4 = get_distance(distance, (direction+3)%4, cur);
        
        //NSLog(@"%d %d %d %d", m1, m2, m3, m4);

        // prefer m1 over m2, m2 over m3, m3 over m4
        BOOL log_point = YES;
        if ((m1<=m2) && (m1<=m3) && (m1<=m4)) {
            // continue in current direction
            log_point = NO;
        } else if ((m2<=m3) && (m2<=m4)) {
            direction = (direction+1)%4;
        } else if (m3<=m4) {
            direction = (direction+2)%4;
        } else {
            direction = (direction+3)%4;
        }
        
        if (log_point || path.count == 0) {
            [path addObject:[NSValue valueWithCGPoint:CGPointMake(cur.x, cur.y)]];
        }
        
        cur = move(cur, direction);
        
        if (distance[cur.y][cur.x] == 0) {
            // since we are finished, we need the last point
            //[path appendFormat:@" %g,%g", gridSize.width*cur.x, gridSize.height*cur.y];
            [path addObject:[NSValue valueWithCGPoint:CGPointMake(cur.x, cur.y)]];
            return path;
        }
    }
    return nil;
}

void add_element(pqueue_t *queue, int x, int y, int distance[YSIZE][XSIZE], 
                 int known, int heuristic) {
    //NSLog(@"add (%d %d) %d %d", x, y, distance[y][x], known);
    if (x<0 || y<0 || x>=XSIZE || y>=YSIZE) return;
    if (distance[y][x] <= known) return;
    
    pqueue_node_t * element = allocCoordinate();
    element->point.x = x;
    element->point.y = y;
	element->estimatedCost = known + heuristic;
    
    pqueue_insert(queue, element);
    
    distance[y][x] = known;
}


- (void)traceWire:(SPIWire*)wire action:(void(^)(int x, int y))action {
    NSArray *points = wire.points;
    assert(points.count >= 2);

    NSValue *prev_point = nil;
    for (NSValue *point in points) {
        if (prev_point) {
            CGPoint pt1 = [prev_point CGPointValue];
            CGPoint pt2 = [point CGPointValue];
            
            SPIGridCoordinate start = [self gridCoordinateFromPoint:pt1];
            SPIGridCoordinate stop = [self gridCoordinateFromPoint:pt2];
            
            int ystart = MIN(start.y, stop.y);
            int ystop = MAX(start.y, stop.y);
            int xstart = MIN(start.x, stop.x);
            int xstop = MAX(start.x, stop.x);
            
            for (int y=ystart; y<=ystop; y++) {
                if ((y<0) || (y>=YSIZE)) continue;
                for (int x=xstart; x<=xstop; x++) {
                    if ((x<0) || (x>=XSIZE)) continue;
                    action(x,y);
                }
            }
        }
        prev_point = point;
    }
}

- (void)traceInstance:(SPIInstance*)instance action:(void(^)(int x, int y))action {
    // block off the area for the instance
    CGAffineTransform t = instance.position.transform;
    CGRect instance_bounds = instance.primitive.instanceRect;
    CGRect r = CGRectApplyAffineTransform(instance_bounds, t);
    
    SPIGridCoordinate start = [self gridCoordinateFromPoint:CGPointMake(CGRectGetMinX(r), 
                                                                        CGRectGetMinY(r))];
    SPIGridCoordinate stop = [self gridCoordinateFromPoint:CGPointMake(CGRectGetMaxX(r), 
                                                                       CGRectGetMaxY(r))];
    
    int ystart = MIN(start.y, stop.y);
    int ystop = MAX(start.y, stop.y);
    int xstart = MIN(start.x, stop.x);
    int xstop = MAX(start.x, stop.x);
    
    for (int y=ystart; y<=ystop; y++) {
        if ((y<0) || (y>=YSIZE)) continue;
        for (int x=xstart; x<=xstop; x++) {
            if ((x<0) || (x>=XSIZE)) continue;
            action(x,y);
        }
    }
}

- (void)traceTerminal:(SPITerminal*)terminal action:(void(^)(int x, int y))action {
    // add the starting terminal as a passable region, plus put it in the queue
    CGAffineTransform t = terminal.instance.position.transform;
    CGPoint pt1 = CGPointApplyAffineTransform(terminal.pin.offset, t);
    
    SPIGridCoordinate e = [self gridCoordinateFromPoint:pt1];  
    action(e.x, e.y);
}

- (void)clearMasks {
    memset(_instanceMask, 0, YSIZE*XSIZE*sizeof(char));
    memset(_wireMask, 0, YSIZE*XSIZE*sizeof(char));
    memset(_destinationMask, 0, YSIZE*XSIZE*sizeof(char));
    
    for (int y=0; y<YSIZE; y++) {
        for (int x=0; x<XSIZE; x++) {
            _distance[y][x] = 0xffffff;
        }
    }
}

- (void)generateMasks {
    [self clearMasks];
    
    // add the wire obstacles
    for (SPIWire *wire in self.schematic.wires) {
        if (wire.isDeleted) continue;
        [self traceWire:wire action:^(int x, int y) {
            _wireMask[y][x] += 1;
        }];
    }
    
    // add the instance obstacles
    for (SPIInstance *instance in self.schematic.instances) {
        if (instance.isDeleted) continue;
        [self traceInstance:instance action:^(int x, int y) {
            _instanceMask[y][x] += 1;
        }];
    }
    
    
    // TODO also add the solder dots as impassable
}


int movementCost(int y, int x, char _instanceMask[YSIZE][XSIZE], char _wireMask[YSIZE][XSIZE], char _destinationMask[YSIZE][XSIZE]) {
    if (_destinationMask[y][x]) return 0;
    return _wireMask[y][x] * 256 + _instanceMask[y][x] * 65536;
    
}

- (NSArray*)search {
    NSDate *d1 = [NSDate date];
#define CACHE_MOVEMENT 1
#if CACHE_MOVEMENT
    // set up the movement cost matrix
    int movementCost[YSIZE][XSIZE];
    memset(movementCost, 0, YSIZE*XSIZE*sizeof(int));
    
    for (int y=0; y<YSIZE; y++) {
        for (int x=0; x<XSIZE; x++) {
            // free to move into a destination coordinate
            if (_destinationMask[y][x]) continue;
            movementCost[y][x] = _wireMask[y][x]*256 + _instanceMask[y][x]*65536;
        }
    }
#endif
    NSDate *d2 = [NSDate date];
    while (1) {
        pqueue_node_t * elem = (pqueue_node_t*)pqueue_pop(_queue);
        if (!elem) break;  // queue is empty
       
        SPIGridCoordinate point = elem->point;
        
        // take all elements from queue
        if (_destinationMask[point.y][point.x]) {
            NSDate *d3 = [NSDate date];
             NSArray *path = backtrace(point, _distance);
            
            NSMutableArray *truePath = [NSMutableArray arrayWithCapacity:path.count];
            for (NSValue *v in path) {
                // the coordinate
                CGPoint pt = [v CGPointValue];
                SPIGridCoordinate coord = { .x = pt.x, .y = pt.y };
                
                // the point
                CGPoint truePoint = [self pointFromGridCoordinate:coord];
                
                // the coordinate and point again
                SPIGridCoordinate coord2 = [self gridCoordinateFromPoint:truePoint];
                
                [truePath addObject:[NSValue valueWithCGPoint:truePoint]];
            }
            
            NSDate *d4 = [NSDate date];
            
            NSTimeInterval ti1 = [d2 timeIntervalSinceDate:d1];
            NSTimeInterval ti2 = [d3 timeIntervalSinceDate:d2];
            NSTimeInterval ti3 = [d4 timeIntervalSinceDate:d3];
            NSTimeInterval ti4 = [d4 timeIntervalSinceDate:d1];
            
            NSLog(@"time intervals: %g %g %g  -->  %g", ti1, ti2, ti3, ti4);
            
            return truePath;
        }

        // increment the distance
        int d = _distance[point.y][point.x] + 1;

        int xdist = point.x - _target.x;
        int ydist = point.y - _target.y;

        if (_useDistanceHeuristic) {
#if CACHE_MOVEMENT
            add_element(_queue, point.x-1, point.y, _distance, d + movementCost[point.y][point.x-1], abs(xdist-1) + abs(ydist));
            add_element(_queue, point.x, point.y-1, _distance, d + movementCost[point.y-1][point.x], abs(xdist) + abs(ydist-1));
            add_element(_queue, point.x, point.y+1, _distance, d + movementCost[point.y+1][point.x], abs(xdist) + abs(ydist+1));
            add_element(_queue, point.x+1, point.y, _distance, d + movementCost[point.y][point.x+1], abs(xdist+1) + abs(ydist));
#else
            add_element(_queue, point.x-1, point.y, _distance, d + movementCost(point.y,point.x-1, _instanceMask, _wireMask, _destinationMask), abs(xdist-1) + abs(ydist));
            add_element(_queue, point.x, point.y-1, _distance, d + movementCost(point.y-1,point.x, _instanceMask, _wireMask, _destinationMask), abs(xdist) + abs(ydist-1));
            add_element(_queue, point.x, point.y+1, _distance, d + movementCost(point.y+1,point.x, _instanceMask, _wireMask, _destinationMask), abs(xdist) + abs(ydist+1));
            add_element(_queue, point.x+1, point.y, _distance, d + movementCost(point.y,point.x+1, _instanceMask, _wireMask, _destinationMask), abs(xdist+1) + abs(ydist));
#endif
        } else {
#if CACHE_MOVEMENT
            add_element(_queue, point.x-1, point.y, _distance, d + movementCost[point.y][point.x-1], 0);
            add_element(_queue, point.x, point.y-1, _distance, d + movementCost[point.y-1][point.x], 0);
            add_element(_queue, point.x, point.y+1, _distance, d + movementCost[point.y+1][point.x], 0);
            add_element(_queue, point.x+1, point.y, _distance, d + movementCost[point.y][point.x+1], 0);
#else
            add_element(_queue, point.x-1, point.y, _distance, d + movementCost(point.y,point.x-1, _instanceMask, _wireMask, _destinationMask), 0);
            add_element(_queue, point.x, point.y-1, _distance, d + movementCost(point.y-1,point.x, _instanceMask, _wireMask, _destinationMask), 0);
            add_element(_queue, point.x, point.y+1, _distance, d + movementCost(point.y+1,point.x, _instanceMask, _wireMask, _destinationMask), 0);
            add_element(_queue, point.x+1, point.y, _distance, d + movementCost(point.y,point.x+1, _instanceMask, _wireMask, _destinationMask), 0);
#endif
        }
    }
    
    NSLog(@"FAIL");
    return nil;
    
}

- (NSArray*)findPathFromTerminal:(SPITerminal *)fromTerm toTerminal:(SPITerminal *)toTerm {
    [self generateMasks];
    
    // add the from terminal to the search queue
    [self traceTerminal:fromTerm action:^(int x, int y) {
        add_element(_queue, x, y, _distance, 0, 0);
    }];
    
    // add the to terminal as a destination
    [self traceTerminal:toTerm action:^(int x, int y) {
        _destinationMask[y][x] = 1;
        
        SPIGridCoordinate t = { .x=x, .y=y };
        _target = t;
    }];
    
    _useDistanceHeuristic = YES;    
    return [self search];
}


- (NSArray*)findPathFromWire:(SPIWire *)fromWire toWire:(SPIWire *)toWire {
    [self generateMasks];
    
    // trace the start wire and initialize the queue
    [self traceWire:fromWire action:^(int x, int y) {
        add_element(_queue, x, y, _distance, 0, 0);
    }];

    // trace the end wire
    [self traceWire:toWire action:^(int x, int y) {
        _destinationMask[y][x] = 1;
    }];
    
    _useDistanceHeuristic = NO;    
    return [self search];
}


- (NSArray*)findPathFromTerminal:(SPITerminal *)fromTerm toNet:(SPINet *)toNet {
    [self generateMasks];

    // trace each wire of the net
    for (SPIWire *wire in toNet.wires) {
        [self traceWire:wire action:^(int x, int y) {
            add_element(_queue, x, y, _distance, 0, 0);
        }];
    }
    
    // add the terminal as a destination
    [self traceTerminal:fromTerm action:^(int x, int y) {
        _destinationMask[y][x] = 1;
        
        SPIGridCoordinate t = { .x=x, .y=y };
        _target = t;
    }];
    
    _useDistanceHeuristic = YES;    
    return [self search];
}

#pragma mark -

- (SPIGridCoordinate)gridCoordinateFromPoint:(CGPoint)point {
    int a = (point.x - self.bounds.origin.x + self.stepSize.width/2) / self.stepSize.width;
    int b = (point.y - self.bounds.origin.y + self.stepSize.height/2) / self.stepSize.height;
    SPIGridCoordinate f = { .x = a, .y = b };
    return f;
}

- (CGPoint)pointFromGridCoordinate:(SPIGridCoordinate)coord {
    int x = coord.x*self.stepSize.width + self.bounds.origin.x;
    int y = coord.y*self.stepSize.height + self.bounds.origin.y;
    return CGPointMake(x, y);
}

- (void)dealloc {
    freeCoordinates();
    free(_queue);
}

- (id)initWithSchematic:(SPISchematic *)schematic {
    self = [super init];
    if (self) {
        self.stepSize = CGSizeMake(10, 10);
        self.schematic = schematic;
        
        CGRect bb = CGRectZero;
        for (SPIInstance *instance in self.schematic.instances) {
            CGAffineTransform t = instance.position.transform;
            CGRect instance_bounds = instance.primitive.instanceRect;
            CGRect r = CGRectApplyAffineTransform(instance_bounds, t);
            bb = CGRectUnion(bb, r);
        }
        
        NSLog(@"bb: %@", NSStringFromCGRect(bb));
        
        self.bounds = CGRectInset(bb, -self.stepSize.width, -self.stepSize.height);
        
        _queue = pqueue_init(65536, cmp_pri, get_pri, set_pri, get_pos, set_pos);
    }
    return self;
}


@end
