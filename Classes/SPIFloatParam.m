//
//  SPIFloatParam.m
//  touchspice
//
//  Created by Reid van Melle on 12-04-01.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SPIFloatParam.h"


@implementation SPIFloatParam

@dynamic defaultValue;
@dynamic maxValue;
@dynamic minValue;

@end
