//
//  SPICurrentProbe.h
//  touchspice
//
//  Created by Shawn Hyam on 12-05-04.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "SPIProbe.h"

@class SPITerminal;

@interface SPICurrentProbe : SPIProbe

@property (nonatomic, retain) SPITerminal *terminal;

@end
