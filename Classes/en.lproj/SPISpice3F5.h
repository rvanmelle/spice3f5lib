#include <stdio.h>
#define _BOOL_H
#include "ngspice.h"
#include "iferrmsg.h"
#include "ftedefs.h"
#include "devdefs.h"
#include "dev.h"
#include "analysis.h"
#include "ivars.h"
#include "getopt.h"
#include "resource.h"
#include "variable.h"
#include "display.h"  /*  added by SDB to pick up Input() fcn  */
#include "signal_handler.h"

#include "numenum.h"

#import "rawfile.h"
#define _BOOL_H
#import "plot.h"


/* static functions */
static int SIMinit(IFfrontEnd *frontEnd, IFsimulator **simulator);
static int shutdown(int exitval);
extern void evalAccLimits(void);
static void app_rl_readlines(void);

/* Global debug flags from CIDER, soon they will become
 * spice variables :)
 */ 
BOOL ONEacDebug   = FALSE;
BOOL ONEdcDebug   = TRUE;
BOOL ONEtranDebug = TRUE;
BOOL ONEjacDebug  = FALSE;
 
BOOL TWOacDebug   = FALSE;
BOOL TWOdcDebug   = TRUE;
BOOL TWOtranDebug = TRUE;
BOOL TWOjacDebug  = FALSE; 

extern IFsimulator SIMinfo;

bool ft_nutmeg = FALSE;
extern struct comm spcp_coms[ ];
struct comm *cp_coms = spcp_coms;
char cp_chars[128];

/* Frontend options */
bool ft_intrpt = FALSE;     /* Set by the (void) signal handlers. TRUE = we've been interrupted. */
bool ft_setflag = FALSE;    /* TRUE = Don't abort simulation after an interrupt. */
char *ft_rawfile = "rawspice.raw";

char *hlp_filelist[] = { "ngspice", 0 };

/* allocate space for global constants in 'CONST.h' */

double CONSTroot2;
double CONSTvt0;
double CONSTKoverQ;
double CONSTe;
IFfrontEnd *SPfrontEnd = NULL;
int DEVmaxnum = 0;

/* Frontend and circuit options */
IFsimulator *ft_sim = NULL;

/* (Virtual) Machine architecture parameters */
int ARCHme;
int ARCHsize;

char *errRtn;
char *errMsg;
char *cp_program;

struct variable *(*if_getparam)( );

/* CIDER Global Variable Declarations */
  
int BandGapNarrowing;
int TempDepMobility, ConcDepMobility, FieldDepMobility, TransDepMobility;
int SurfaceMobility, MatchingMobility, MobDeriv;
int CCScattering;
int Srh, Auger, ConcDepLifetime, AvalancheGen;
int FreezeOut = FALSE;
int OneCarrier;
 
int MaxIterations = 100;
int AcAnalysisMethod = DIRECT;
 
double Temp, RelTemp, Vt;
double RefPsi;/* potential at Infinity */
double EpsNorm, VNorm, NNorm, LNorm, TNorm, JNorm, GNorm, ENorm;

double BMin;                /* lower limit for B(x) */
double BMax;                /* upper limit for B(x) */
double ExpLim;              /* limit for exponential */
double Accuracy;            /* accuracy of the machine */
double Acc, MuLim, MutLim;
 
 /* end cider globals */
 
extern int OUTpBeginPlot(), OUTpData(), OUTwBeginPlot(), OUTwReference();
extern int OUTwData(), OUTwEnd(), OUTendPlot(), OUTbeginDomain();
extern int OUTendDomain(), OUTstopnow(), OUTerror(), OUTattributes();

/* -------------------------------------------------------------------------- */
static int
SIMinit(IFfrontEnd *frontEnd, IFsimulator **simulator)
{
    spice_init_devices();
    SIMinfo.numDevices = DEVmaxnum = num_devices();
    SIMinfo.devices = devices_ptr();
    SIMinfo.numAnalyses = spice_num_analysis();
    SIMinfo.analyses = (IFanalysis **)spice_analysis_ptr(); /* va: we recast, because we use 
                                                             * only the public part 
							     */
							
/* Evaluates limits of machine accuracy for CIDER */
    evalAccLimits();
   						     
    SPfrontEnd = frontEnd;
    *simulator = &SIMinfo;
    CONSTroot2 = sqrt(2.);
    CONSTvt0 = CONSTboltz * (27 /* deg c */ + CONSTCtoK ) / CHARGE;
    CONSTKoverQ = CONSTboltz / CHARGE;
    CONSTe = exp((double)1.0);
    return(OK);
}


/* -------------------------------------------------------------------------- */
/* Shutdown gracefully. */
static int
shutdown(int exitval)
{
	cleanvars();
	exit (exitval);
}

// NEW STUFF
static struct plot *setcplot(char *name)
{
	struct plot *pl;

	for (pl = plot_list; pl; pl = pl->pl_next) {
		if (ciprefix(name, pl->pl_typename)) {
			return pl;
		}
	}
	return NULL;
}
#import "sim.h" // contains definitions for SV_VOLTAGE etc.
// END NEW STUFF

