// 
//  SPITerminal.m
//  touchspice
//
//  Created by Shawn Hyam on 10-12-22.
//  Copyright 2010 Brierwood Design Cooperative. All rights reserved.
//

#import "SPITerminal.h"

#import "SPIInstance.h"
#import "SPINet.h"
#import "SPIPin.h"
#import "SPIWire.h"

@implementation SPITerminal 

@dynamic wire;
@dynamic net;
@dynamic instance;
@dynamic pin;

@end
