//
//  SPISimulation.h
//  touchspice
//
//  Created by Reid van Melle on 11-01-13.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SPISimulationResults;

typedef enum {
	kSPISimulationResultStatusCompleted,
	kSPISimulationResultStatusFailed
} SPISimulationResultStatus;

struct plot;
typedef struct plot spi_plot;

@interface SPISimulation : NSObject {
    
@private
    NSString *fileName;
    NSString *dirName;
}

@property (retain) NSString *fileName, *dirName;

- (SPISimulationResults*)runWithNetlistFilename:(NSString*)file;
- (SPISimulationResults*)runWithNetlist:(NSString*)netlist;
- (SPISimulationResults*)runWithNetlist:(NSString*)netlist inDirectory:(NSString*)dir;


@end
