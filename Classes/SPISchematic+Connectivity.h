//
//  SPISchematic+Connectivity.h
//  touchspice
//
//  Created by Shawn Hyam on 12-01-27.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SPISchematic.h"

@interface SPISchematic (Connectivity)

- (NSArray*)validateSchematicView;
- (void)updateConnectivity;

@end
