//
//  SPIRoutingGrid.h
//  touchspice
//
//  Created by Shawn Hyam on 11-10-07.
//  Copyright (c) 2011 Brierwood Design Co-operative. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SPISchematic.h"
#import "SPITerminal.h"
#import "SPIWire.h"

typedef struct {
    int x;
    int y;
} SPIGridCoordinate;


typedef enum {
    GRID_COST_DESTINATION = 1,
    GRID_COST_PASSABLE = 2,
    GRID_COST_UNDESIRABLE = 100,
    GRID_COST_IMPASSABLE = 10000
} GridCostType;

SPIGridCoordinate gridCoordinateFromPoint(CGPoint point);
CGPoint pointFromGridCoordinate(SPIGridCoordinate coord);

@interface SPIRoutingGrid : NSObject

@property (assign) CGSize stepSize;

- (id)initWithSchematic:(SPISchematic*)schematic;
- (NSArray*)findPathFromTerminal:(SPITerminal*)fromTerm toTerminal:(SPITerminal*)toTerm;
- (NSArray*)findPathFromTerminal:(SPITerminal*)fromTerm toNet:(SPINet*)toNet;
- (NSArray*)findPathFromWire:(SPIWire*)fromWire toWire:(SPIWire*)toWire;

@end
