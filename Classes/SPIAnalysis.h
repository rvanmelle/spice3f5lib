//
//  SPIAnalysis.h
//  touchspice
//
//  Created by Shawn Hyam on 10-12-31.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <CoreData/CoreData.h>

@class SPICircuit;
@class SPIProbe;

@interface SPIAnalysis :  NSManagedObject  
{
}

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) SPICircuit * circuit;
@property (nonatomic, retain) NSSet* probes;

@end


@interface SPIAnalysis (CoreDataGeneratedAccessors)
- (void)addProbesObject:(SPIProbe *)value;
- (void)removeProbesObject:(SPIProbe *)value;
- (void)addProbes:(NSSet *)value;
- (void)removeProbes:(NSSet *)value;

@end

