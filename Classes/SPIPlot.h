//
//  SPIPlot.h
//  touchspice
//
//  Created by Shawn Hyam on 10-12-31.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <CoreData/CoreData.h>

@class SPIProbe;
@class SPIRun;

@interface SPIPlot :  NSManagedObject  
{
}

@property (nonatomic, retain) id thumbnail;
@property (nonatomic, retain) SPIProbe * probe;
@property (nonatomic, retain) SPIRun * run;

@end



