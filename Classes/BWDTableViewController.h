//
//  BWDTableViewController.h
//  touchspice
//
//  Created by Shawn Hyam on 12-03-22.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BWDViewController.h"

@interface BWDTableViewController : BWDViewController<UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong) NSFetchedResultsController *fetchedResultsController;

- (void)configureCell:(UITableViewCell*)cell atIndexPath:(NSIndexPath*)indexPath;


@end
