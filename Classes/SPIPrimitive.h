//
//  SPIPrimitive.h
//  touchspice
//
//  Created by Shawn Hyam on 10-12-22.
//  Copyright 2010 Brierwood Design Cooperative. All rights reserved.
//

#import <CoreData/CoreData.h>
#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>
#import "SPIDefs.h"

@class SPIInstance;
@class SPIPin;
@class SPIPrimitiveNetlistTemplate;
@class SPIParameter;
@class SPILabel;
@class SPIFloatParam;
@class SPIIntParam;
@class SPIStringParam;

@interface SPIPrimitive :  NSManagedObject  
{
	CGRect instanceRect;
}

@property (readonly) CGSize size;
@property (readonly) CGPathRef path;   // TODO support more than one path

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString* SVGPath;
@property (nonatomic, assign) CGRect instanceRect;
@property (nonatomic, retain) NSSet* instances;
@property (nonatomic, retain) NSSet* pins;
@property (nonatomic, retain) SPIPrimitiveNetlistTemplate * netlistTemplate;
@property (nonatomic, retain) NSSet* parameters;
@property (nonatomic, retain) NSNumber * ground;
@property (nonatomic, retain) NSSet* labels;

// DONT CALL DIRECTLY
@property (nonatomic, assign) NSValue* instanceRectValue;

- (UIImage*)imageRepresentation;
- (SPIFloatParam*)addFloatParam:(NSString*)name section:(NSString*)section description:(NSString*)description 
                            min:(double_t)min max:(double_t)max initial:(double_t)initial units:(SPIUnitType)units;
- (SPIFloatParam*)addFloatParam:(NSString*)name section:(NSString*)section description:(NSString*)description 
                            min:(double_t)min max:(double_t)max initial:(double_t)initial 
                          units:(SPIUnitType)units location:(CGPoint)loc anchor:(SPITextAnchorType)anchor;

- (SPIIntParam*)addIntParam:(NSString*)name section:(NSString*)section description:(NSString*)description 
                        min:(int)min max:(int)max initial:(int)initial units:(SPIUnitType)units;
- (SPIStringParam*)addStringParam:(NSString*)name section:(NSString*)section description:(NSString*)description
                          initial:(NSString*)initial;

@end



@interface SPIPrimitive (CoreDataGeneratedAccessors)
- (void)addInstancesObject:(SPIInstance *)value;
- (void)removeInstancesObject:(SPIInstance *)value;
- (void)addInstances:(NSSet *)value;
- (void)removeInstances:(NSSet *)value;

- (void)addPinsObject:(SPIPin *)value;
- (void)removePinsObject:(SPIPin *)value;
- (void)addPins:(NSSet *)value;
- (void)removePins:(NSSet *)value;

- (void)addParametersObject:(SPIParameter *)value;
- (void)removeParametersObject:(SPIParameter *)value;
- (void)addParameters:(NSSet *)value;
- (void)removeParameters:(NSSet *)value;

- (void)addLabelsObject:(SPILabel *)value;
- (void)removeLabelsObject:(SPILabel *)value;
- (void)addLabels:(NSSet *)value;
- (void)removeLabels:(NSSet *)value;

@end

