//
//  SPINetlistViewController.m
//  touchspice
//
//  Created by Shawn Hyam on 12-03-23.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SPINetlistViewController.h"
#import "SPINetlister.h"

@interface SPINetlistViewController ()

@end

@implementation SPINetlistViewController
@synthesize textView;
@synthesize circuit = _circuit;

- (void)configureView {
    SPINetlister *netlister = [[SPINetlister alloc] init];
    NSString *netlist = [netlister netlist:self.circuit];
    self.textView.text = netlist;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self configureView];
}

- (void)viewDidUnload
{
    [self setTextView:nil];
    [super viewDidUnload];
}

@end
