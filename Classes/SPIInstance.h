//
//  SPIInstance.h
//  touchspice
//
//  Created by Shawn Hyam on 10-12-22.
//  Copyright 2010 Brierwood Design Cooperative. All rights reserved.
//

#import <CoreData/CoreData.h>

@class SPICircuit;
@class SPIInstancePosition;
@class SPIPrimitive;
@class SPITerminal;
@class SPIProperty;

@interface SPIInstance :  NSManagedObject  
{
}

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) SPICircuit * circuit;
@property (nonatomic, retain) NSSet* terminals;
@property (nonatomic, retain) SPIInstancePosition * position;
@property (nonatomic, retain) SPIPrimitive * primitive;
@property (nonatomic, retain) NSSet* properties;

+ (SPIInstance*)instanceFromPrimitive:(SPIPrimitive*)primitive inCircuit:(SPICircuit*)circuit;

@end


@interface SPIInstance (CoreDataGeneratedAccessors)
- (void)addTerminalsObject:(SPITerminal *)value;
- (void)removeTerminalsObject:(SPITerminal *)value;
- (void)addTerminals:(NSSet *)value;
- (void)removeTerminals:(NSSet *)value;

- (void)addPropertiesObject:(SPIProperty *)value;
- (void)removePropertiesObject:(SPIProperty *)value;
- (void)addProperties:(NSSet *)value;
- (void)removeProperties:(NSSet *)value;
@end

