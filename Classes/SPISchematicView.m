//
//  SPISchematicView.m
//  touchspice
//
//  Created by Shawn Hyam on 10-12-22.
//  Copyright 2010 Brierwood Design Cooperative. All rights reserved.
//

#import "SPISchematicView.h"
#import "SPIPrimitive.h"
#import "SPISchematic.h"
#import "SPIInstance.h"
#import "SPIPin.h"
#import "SPINet.h"
#import "SPIWire.h"
#import "SPILabel.h"
#import "SPITerminal.h"
#import "SPIInstancePosition.h"
#import "NSString+SVG.h"
#import <QuartzCore/QuartzCore.h>




// TODO don't make this so instance-specific; if we allow a stroke path, a fill path, and a text path, we should be good

typedef enum {
	label_lowerRight = 0,
	label_lowerLeft,
	label_upperLeft,
	label_upperRight
} label_position_t;

@interface PrimitiveLayer : CATiledLayer {
	CGPathRef path;
	UIFont *f1;
	UIFont *f2;
}

@property (nonatomic, retain) SPIPrimitive *primitive;

@end


@implementation PrimitiveLayer

@synthesize primitive = _primitive;

- (id)init {
    self = [super init];
    if (self) {
        self.levelsOfDetail = 4;
        self.levelsOfDetailBias = 3;
    }
    return self;
}

- (CGRect)positionText:(NSString*)txt withFont:(UIFont*)font nearPoint:(CGPoint)pt anchor:(label_position_t)anchor {
	// FIXME examine showText: below and figure out something better here
#if 0
	CGAffineTransform t = instance.position.transform;
	CGAffineTransform tinv = CGAffineTransformInvert(t);
	CGSize sz = CGSizeApplyAffineTransform([txt sizeWithFont:font], tinv);

	// TODO move all this positioning code to the SPIPrimitive
	
	if (t.b == -1 && t.c == 1) { anchor += 1; }
	if (t.a == -1 && t.d == -1) { anchor += 2; }
	if (t.b == 1 && t.c == -1) { anchor += 3; }
	anchor = anchor%4;
	
	CGPoint txt_pt = pt;
	switch (anchor) {
		case label_lowerLeft: break;
		case label_lowerRight: txt_pt.x -= sz.width; break;
		case label_upperLeft: txt_pt.y -= sz.height; break;
		case label_upperRight: txt_pt.x -= sz.width; txt_pt.y -= sz.height; break;
	}
	CGRect r = CGRectStandardize(CGRectMake(txt_pt.x, txt_pt.y, sz.width, sz.height));
	return r;
#endif
}

- (void)showText:(NSString*)txt inContext:(CGContextRef)ctx withFont:(UIFont*)font nearPoint:(CGPoint)pt anchor:(label_position_t)anchor {
#if 0
	CGContextSelectFont(ctx, [font.fontName cStringUsingEncoding:NSASCIIStringEncoding], font.pointSize, kCGEncodingMacRoman);
	CGContextSetFillColorWithColor(ctx, [UIColor whiteColor].CGColor);
	CGContextSetTextDrawingMode(ctx, kCGTextFill);
		
	
	CGAffineTransform t = instance.position.transform;
	CGAffineTransform trot = CGAffineTransformMake(t.a, t.b, t.c, t.d, 0, 0);  // isolate the rotation
	CGAffineTransform trotinv = CGAffineTransformInvert(trot);  // get the reverse rotation
		
	CGSize sz = [txt sizeWithFont:font];
	CGRect r = CGRectMake(0, 0, -sz.width, sz.height);  // FIXME hardcoded lowerRight anchor
	
	// this could probably be fixed up, and be based on the actual affinetransform matrix values, but this
	// basically moves the anchorpoint around after we undo the rotation of the instance
	if (t.a == 1 && t.d == 1) {
		r = CGRectOffset(r, 0, 0);
	} else if (t.b == -1 && t.c == 1) {
		r = CGRectOffset(r, sz.width, 0);
	} else if (t.a == -1 && t.d == -1) {
		r = CGRectOffset(r, sz.width, -sz.height);
	} else if (t.b == 1 && t.c == -1) {
		r = CGRectOffset(r, 0, -sz.height);
	}
	
	r = CGRectStandardize(r);
	
	CGContextSaveGState(ctx);
	CGContextTranslateCTM(ctx, pt.x, pt.y);
	CGContextConcatCTM(ctx, trotinv);
	CGContextShowTextAtPoint(ctx, r.origin.x, r.origin.y, [txt cStringUsingEncoding:NSASCIIStringEncoding], txt.length);
	//CGContextSetStrokeColorWithColor(ctx, [UIColor greenColor].CGColor);
	//CGContextAddRect(ctx, r);
	//CGContextStrokePath(ctx);
	CGContextRestoreGState(ctx);
#endif
}



+(CFTimeInterval)fadeDuration
{
    return 0.0;     // Normally it’s 0.25
}

- (void)setPrimitive:(SPIPrimitive *)primitive {
    _primitive = primitive;

    CGRect bb = self.primitive.instanceRect;
    for (SPIPin *pin in self.primitive.pins) {
        CGPoint pt = pin.offset;        
        bb = CGRectUnion(bb, CGRectMake(pt.x-4, pt.y-4, 8.f, 8.f));
    }
    self.bounds = bb;
        /*
        UIGraphicsBeginImageContext(bb.size);
        CGContextRef ctx = UIGraphicsGetCurrentContext();
        CGContextTranslateCTM(ctx, -bb.origin.x, -bb.origin.y);
        
        [self drawInContext:ctx];
        
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return image;
   */
}

#if 0
- (void)setPath:(CGPathRef)p {
	CGPathRelease(path);
	path=p;
	CGPathRetain(path);
		
	self.levelsOfDetail = 4;
	self.levelsOfDetailBias = 3;

	CGRect ir = instance.primitive.instanceRect;
	CGRect r = CGRectInset(ir, -4, -4);  // leave a little border (this is actually for the terminals, we could be more exact about this)
	
	f1 = [UIFont fontWithName:@"Helvetica" size:8];
	f2 = [UIFont fontWithName:@"Helvetica" size:6];
	// settle on the text elements and where they should go
	if (instance.name) {
		CGRect textrect = [self positionText:instance.name 
									withFont:f1
								   nearPoint:CGPointMake(10,-10)
									  anchor:label_lowerLeft];
		r = CGRectUnion(r, textrect);
	}
	// any primitive labels must be accounted for in the bounding box
	for (SPILabel *l in instance.primitive.labels) {
		r = CGRectUnion(r, [self positionText:l.text withFont:f2 nearPoint:l.position anchor:label_lowerRight]);
	}
		
	r = CGRectInset(r, -1, -1); // for any lines
	
	self.bounds = r;

	CGPoint anchor = CGPointMake(ir.origin.x + ir.size.width/2, ir.origin.y + ir.size.height/2);
	// scale 'anchor' into a (0,1) coordinate space
	self.anchorPoint = CGPointMake((anchor.x - r.origin.x)/r.size.width, (anchor.y - r.origin.y)/r.size.height);	
}

- (CGPathRef)path {
	return path;
}
#endif

- (void)drawInContext:(CGContextRef)ctx {
#if 0
	if (instance.name && (t.a>=2.0)) {
		[self showText:instance.name inContext:ctx withFont:f1 nearPoint:CGPointMake(10,-10) anchor:label_lowerLeft];
	}

	if (t.a >= 3.0) {
		for (SPILabel *l in instance.primitive.labels) {
			[self showText:l.text inContext:ctx withFont:f2 nearPoint:l.position anchor:label_lowerRight];
		}
	}
#endif
	CGContextSetLineWidth(ctx, 1.0);
	CGContextSetStrokeColorWithColor(ctx, [UIColor greenColor].CGColor);
	CGContextAddPath(ctx, self.primitive.path);
	CGContextStrokePath(ctx);

	CGContextSetFillColorWithColor(ctx, [UIColor redColor].CGColor);
	for (SPIPin *pin in self.primitive.pins) {
		CGPoint pt = pin.offset;
		CGContextAddRect(ctx, CGRectMake(pt.x-4, pt.y-4, 8.f, 8.f));
	}
	CGContextFillPath(ctx);	
}

@end

@interface SPIComponentView () 

@property (nonatomic, strong) NSManagedObject *component;
- (void)configureHighlight;

@end

@implementation SPIComponentView
@synthesize selected = _selected;
@synthesize highlighted = _highlighted;
@synthesize component = _component;

- (void)setSelected:(BOOL)selected {
    _selected = selected;
    [self configureHighlight];
}

- (void)setHighlighted:(BOOL)highlighted {
    _highlighted = highlighted;
    [self configureHighlight];
}

@end





@implementation SPIWireView {
    CGPathRef _fillPath;
    UIView *_centerMarkView;
    UIView *_probeView;
}

+ (Class)layerClass {
    return [CAShapeLayer class];
}

- (SPIWire*)wire {
    return (SPIWire*)self.component;
}

- (id)initWithWire:(SPIWire*)wire {
    self = [super initWithFrame:CGRectZero];
    if (self) {
        self.component = wire;
        
        CAShapeLayer *layer = (id)self.layer;
        layer.strokeColor = [UIColor yellowColor].CGColor;
        layer.lineWidth = 1.0;
        layer.fillColor = [UIColor clearColor].CGColor;
        layer.shadowRadius = 0.0;
        layer.shadowOffset = CGSizeMake(0,0);
        layer.shadowColor = [UIColor yellowColor].CGColor;
        layer.shadowOpacity = 1.0;
        
        [self setNeedsDisplay];
    }
    return self;
}

- (void)configureHighlight {
    CAShapeLayer *layer = (id)self.layer;
    if (self.highlighted) {
        layer.lineWidth = 4.0;
        layer.strokeColor = [UIColor redColor].CGColor;
        layer.shadowRadius = 8.0;
    } else if (self.selected) {
        layer.lineWidth = 4.0;
        layer.strokeColor = [UIColor yellowColor].CGColor;
        layer.shadowRadius = 0.0;
    } else {
        layer.lineWidth = 1.0;
        layer.strokeColor = [UIColor yellowColor].CGColor;
        layer.shadowRadius = 0.0;
    }
    
}

- (void)setNeedsDisplay {
    [super setNeedsDisplay];
    
    [_centerMarkView removeFromSuperview];
    [_probeView removeFromSuperview];
    
    CAShapeLayer *layer = (id)self.layer;
    layer.path = self.wire.path;

    _fillPath = [self.wire fillPath:8.0];
    
#if 0
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(-5, -5, 10, 10)];
    v.backgroundColor = [UIColor grayColor];
    [self addSubview:v];
    NSArray *pts = [self.wire allPoints:10];
    CGPoint pt = [[pts objectAtIndex:(pts.count/2)] CGPointValue];
    v.layer.affineTransform = CGAffineTransformMakeTranslation(pt.x, pt.y);
#endif
    
    if (self.wire.voltageProbes.count > 0) {
        UIView *v = [[UIView alloc] initWithFrame:CGRectMake(-8, -8, 16, 16)];
        v.backgroundColor = [UIColor whiteColor];
        v.layer.cornerRadius = 8.0f;
        v.layer.borderWidth = 2.0;
        v.layer.borderColor = [UIColor blackColor].CGColor;
        
        [self addSubview:v];
        NSArray *pts = [self.wire allPoints:10];
        CGPoint pt = [[pts objectAtIndex:(pts.count/2)] CGPointValue];
        v.layer.affineTransform = CGAffineTransformMakeTranslation(pt.x, pt.y);
        _probeView = v;
    }
}


- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event {
    return CGPathContainsPoint(_fillPath, nil, point, NO);
}


@end


@implementation SPITerminalView {
    CAShapeLayer *_highlightLayer;
    UIView *_probeView;
}

+ (Class)layerClass {
    return [CAShapeLayer class];
}

- (SPITerminal*)terminal {
    return (SPITerminal*)self.component;
}

- (void)setNeedsDisplay {
    [_probeView removeFromSuperview];
    
    if (self.terminal.currentProbes.count > 0) {
        UIView *v = [[UIView alloc] initWithFrame:CGRectMake(-8, -8, 16, 16)];
        v.backgroundColor = [UIColor whiteColor];
        v.layer.cornerRadius = 8.0f;
        v.layer.borderWidth = 2.0;
        v.layer.borderColor = [UIColor blackColor].CGColor;
        
        [self addSubview:v];
        _probeView = v;
    }
}

- (id)initWithTerminal:(SPITerminal*)terminal {
    self = [super initWithFrame:CGRectMake(0,0,8,8)];
    if (self) {
        self.component = terminal;
        
        CAShapeLayer *pinLayer = (id)self.layer;
        SPIPin *pin = terminal.pin;
        
        pinLayer.fillColor = [UIColor redColor].CGColor;
        pinLayer.path = [UIBezierPath bezierPathWithRect:CGRectMake(-4, -4, 8, 8)].CGPath;
        pinLayer.affineTransform = CGAffineTransformMakeTranslation(pin.offset.x, pin.offset.y);
        
        _highlightLayer = [[CAShapeLayer alloc] init];
        _highlightLayer.path = [UIBezierPath bezierPathWithArcCenter:CGPointMake(0, 0) 
                                                              radius:20
                                                          startAngle:0 endAngle:3.1415926*2
                                                           clockwise:YES].CGPath;
        _highlightLayer.shadowOpacity = 1.0;
        _highlightLayer.shadowRadius = 0.0;
        _highlightLayer.shadowOffset = CGSizeMake(0, 0);
        _highlightLayer.shadowColor = [UIColor yellowColor].CGColor;
        _highlightLayer.lineWidth = 0.0;
        _highlightLayer.fillColor = nil;
        [self.layer addSublayer:_highlightLayer];        
    }
    return self;    
}

- (void)configureHighlight {
    if (self.highlighted) {
        _highlightLayer.strokeColor = [UIColor redColor].CGColor;
        _highlightLayer.lineWidth = 5.0;
        _highlightLayer.shadowRadius = 5.0;
    } else if (self.selected) {
        _highlightLayer.strokeColor = [UIColor blueColor].CGColor;
        _highlightLayer.lineWidth = 5.0;
        _highlightLayer.shadowRadius = 5.0;
    } else {
        _highlightLayer.lineWidth = 0.0;
        _highlightLayer.shadowRadius = 0.0;
    }
}

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event {
    // NOTE this makes the touching a little more forgiving
    return CGRectContainsPoint(CGRectInset(self.bounds, -8, -8), point);
}

@end


@implementation SPIInstanceView {
}

@synthesize instance = _instance;

+ (Class)layerClass {
    return [CAShapeLayer class];
}

- (SPIInstance*)instance {
    return (SPIInstance*)self.component;
}

- (id)initWithInstance:(SPIInstance *)instance {
    self = [super init];
    if (self) {
        self.component = instance;
        
        CAShapeLayer *il = (id)self.layer;    
        il.strokeColor = [UIColor greenColor].CGColor;
        il.lineWidth = 1.0;
        il.fillColor = nil;
        
        SPIPrimitive *primitive = instance.primitive;
        CGRect ir = primitive.instanceRect;
        
        // FIXME this only works for the resister -- but we have to set the anchor point properly
        // based on the instanceRect of the primitive
        il.anchorPoint = CGPointMake(-ir.origin.x/ir.size.width , -ir.origin.y/ir.size.height);
        [self setNeedsDisplay];
    }
    return self;    
}



- (void)configureHighlight {
    if (self.highlighted) {
        self.layer.borderWidth = 1.0;
        self.layer.borderColor = [UIColor redColor].CGColor;
        self.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.7];
    } else if (self.selected) {
        self.layer.borderWidth = 1.0;
        self.layer.borderColor = [UIColor redColor].CGColor;
        self.backgroundColor = [[UIColor yellowColor] colorWithAlphaComponent:0.7];
    } else {
        self.layer.borderWidth = 0.0;
        self.backgroundColor = [UIColor clearColor];
    }
}

- (void)setNeedsDisplay {
    [super setNeedsDisplay];

    // let's animate this, just for fun
    [UIView animateWithDuration:0.5 animations:^{
        CAShapeLayer *il = (id)self.layer;
        [il setAffineTransform:self.instance.position.transform];
        il.bounds = self.instance.primitive.instanceRect;
        il.path = self.instance.primitive.path;    
    }];
}


- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event {
    // NOTE this makes the touching a little more forgiving
    return CGRectContainsPoint(CGRectInset(self.bounds, -8, -8), point);
}

// hitTest on a given UIView instance is called *before* pointInside -- not sure where subviews fit in though
// I expect it goes view::hitTest, then view::pointInside, then subview::hitTest IFF pointInside was YES
- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    return [super hitTest:point withEvent:event];
}

@end




@implementation SPISchematicView

@synthesize schematic, selectionLayer;

 + (Class)layerClass {
	return [CAShapeLayer class];
}

- (id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
		self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]];
    }
    return self;
}

- (void)selectInstanceLayer:(PrimitiveLayer*)il {
//	il.borderWidth = 3.0;
//	il.backgroundColor = [[UIColor yellowColor] colorWithAlphaComponent:0.5].CGColor;
}

- (void)selectNet:(SPINet*)net {	
	[selectionLayer removeFromSuperlayer];

	// TODO actually create a path for the net out of these subpaths
	CGMutablePathRef netpath = CGPathCreateMutable();
	for (SPIWire *w in net.wires) {
		CGPathAddPath(netpath, NULL, [w path]);
	}
	
	CAShapeLayer *l = [[CAShapeLayer alloc] init];
	[self.layer addSublayer:l];
	l.strokeColor = [UIColor yellowColor].CGColor;
	l.lineWidth = 5.0;
	l.fillColor = nil;		
	l.path = CGPathCreateCopy(netpath);
	
	CABasicAnimation *foo = [CABasicAnimation animationWithKeyPath:@"lineWidth"];
	foo.fromValue = [NSNumber numberWithFloat:1.0];
	foo.toValue = [NSNumber numberWithFloat:5.0];
	[l addAnimation:foo forKey:@"lineWidth"];
	CGPathRelease(netpath);		
	
	self.selectionLayer = l;
}

- (void)didTap:(UITapGestureRecognizer*)tgr {
	NSLog(@"TAP %@", tgr);
    
    // did the user tap an instance?
    NSLog(@"tapped view: %@", tgr.view);
    
	// did the user tap a wire?
	CGPoint pt = [tgr locationInView:self];
	NSLog(@"tap pt: %@ %@", [NSValue valueWithCGPoint:pt], [NSValue valueWithCGPoint:[tgr locationInView:nil]]);
	
	CALayer *l = [self.layer hitTest:[tgr locationInView:self.superview]];
	NSLog(@"hit layer: %@", l);
	l.backgroundColor = [[UIColor yellowColor] colorWithAlphaComponent:0.5].CGColor;
	l.borderWidth = 1.0;
	l.zPosition = -100;

	// did the user tap an instance?
	// TODO
	
	// probably hit a wire then
    /*
	for (SPIWire *w in self.schematic.wires) {
		CGPathRef path = [w hitTestPath];
		if (CGPathContainsPoint(path, NULL, pt, 1)) {
			[self selectNet:w.net];
			return;
		}
	}
     */
}

- (void)setSchematic:(SPISchematic *)s {
	assert(!schematic);
	//self.layer.geometryFlipped = YES;
	
	schematic = s;
	
	NSLog(@"set schematic: %@", s);
	
    /*
	// add layers for the instances
	for (SPIInstance *inst in s.instances) {
        [self addInstance:inst];
	}
     */
	
#if 0
	
	// add shape layers for the nets -- TODO optimize this somehow...
	CGMutablePathRef netpath = CGPathCreateMutable();
	
	// NOTE this is WAY faster when it's done as one giant path; however, this requires much more careful management
	
	
	/*
	for (SPINet *net in s.nets) {
		// this is done net-at-a-time to get proper line joins and (potentially) solder dots
		
		// TODO actually create a path for the net out of these subpaths
		for (SPIWire *w in net.wires) {
			CGPathAddPath(netpath, NULL, [w path]);
		}
	}
     */
	
	UITapGestureRecognizer *tgr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTap:)];
	[self addGestureRecognizer:tgr];
	
    
	CAShapeLayer *l = [[CAShapeLayer alloc] init];
	l.strokeColor = [UIColor yellowColor].CGColor;
	l.lineWidth = 1.0;
	l.fillColor = nil;		
	l.path = CGPathCreateCopy(netpath);
	[self.layer addSublayer:l];
	CGPathRelease(netpath);		

#endif

}




#pragma mark -
#pragma mark UIScrollViewDelegate

- (UIView*)viewForZoomingInScrollView:(UIScrollView *)scrollView {
	return self;
}

- (void)scrollViewWillBeginZooming:(UIScrollView *)scrollView withView:(UIView *)view {
	NSLog(@"scrollViewWillBeginZooming: withView: %@", view);
}

- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(float)scale {
	NSLog(@"scrollViewDidEndZooming: withView: %@ %@ atScale: %5f", view, [NSValue valueWithCGRect:view.layer.frame], scale);
	//self.frame = CGRectOffset(self.frame, 0, -self.frame.origin.y);
	//self.frame = CGRectMake(self.frame.origin.x, -self.frame.origin.y, self.frame.size.width, self.frame.size.height);
}

#pragma mark -
#pragma mark UIGestureRecognizerDelegate

@end
