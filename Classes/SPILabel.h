//
//  SPILabel.h
//  touchspice
//
//  Created by Shawn Hyam on 11-01-06.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <CoreData/CoreData.h>

@class SPIPrimitive;

@interface SPILabel :  NSManagedObject  

@property (nonatomic, assign) CGPoint position;
@property (nonatomic, retain) NSValue * positionValue;

@property (nonatomic, retain) NSString * text;
@property (nonatomic, retain) SPIPrimitive * primitive;


@end



