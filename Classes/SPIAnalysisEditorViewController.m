//
//  SPIAnalysisEditorViewController.m
//  touchspice
//
//  Created by Shawn Hyam on 12-03-23.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SPIAnalysisEditorViewController.h"
#import "SPIAnalysis.h"

@interface SPIAnalysisEditorViewController ()

@end

@implementation SPIAnalysisEditorViewController

@synthesize analysis = _analysis;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

@end
