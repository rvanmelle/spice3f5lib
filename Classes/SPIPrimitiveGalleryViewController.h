//
//  SPIInstanceGalleryViewController.h
//  touchspice
//
//  Created by Shawn Hyam on 12-03-22.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SPIPrimitive;

@protocol SPIPrimitiveGalleryDelegate <NSObject>
- (void)didSelectPrimitive:(SPIPrimitive*)primitive;
@end



@class SPICircuit;
@interface SPIPrimitiveGalleryViewController : BWDTableViewController

@property (nonatomic, strong) SPICircuit *circuit;
@property (nonatomic, weak) id<SPIPrimitiveGalleryDelegate> delegate;

@end
