//
//  SPIPin.h
//  touchspice
//
//  Created by Shawn Hyam on 10-12-22.
//  Copyright 2010 Brierwood Design Cooperative. All rights reserved.
//

#import <CoreData/CoreData.h>

@class SPIPrimitive;
@class SPITerminal;

@interface SPIPin :  NSManagedObject  
{
	CGPoint offset;
}

@property (nonatomic, retain) NSString *name;
@property (nonatomic, assign) CGPoint offset;
@property (nonatomic, retain) NSValue *offsetValue;
@property (nonatomic, retain) NSSet* terminals;
@property (nonatomic, retain) SPIPrimitive * primitive;

@end


@interface SPIPin (CoreDataGeneratedAccessors)
- (void)addTerminalsObject:(SPITerminal *)value;
- (void)removeTerminalsObject:(SPITerminal *)value;
- (void)addTerminals:(NSSet *)value;
- (void)removeTerminals:(NSSet *)value;

@end

