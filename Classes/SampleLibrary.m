//
//  SampleLibrary.m
//  touchspice
//
//  Created by Shawn Hyam on 10-12-23.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "SampleLibrary.h"
#import "SPIPrimitive.h"
#import "SPISchematic.h"
#import "SPIInstance.h"
#import "SPIPrimitiveNetlistTemplate.h"
#import "SPINetlister.h"
#import "SPIPin.h"
#import "SPITerminal.h"
#import "SPINet.h"
#import "SPIWire.h"
#import "SPIDCAnalysis.h"
#import "SPIVoltageProbe.h"
#import "SPIProbeSet.h"
#import "SPIInstancePosition.h"
#import "SPILabel.h"
#import "NSManagedObjectContextHelper.h"

#import "SPIData.h"

@implementation SampleLibrary

+ (SPISchematic*)createSchematic:(SPIData *)data {
	SPISchematic *circuit = (SPISchematic*)[data.managedObjectContext fetchObjectForEntityName:@"SPISchematic" withPredicate:nil];
	return circuit;
}



+ (SPISchematic*)createInContextOld:(NSManagedObjectContext*)ctx {
	SPIData *data = [[SPIData alloc] init];
    [data libraryInit];
	
	//[self libraryInit:ctx];
	//SPIPrimitive *prim = [NSEntityDescription insertNewObjectForEntityForName:@"SPIPrimitive" inManagedObjectContext:ctx];
	// TODO schematic-drawing-related stuff
	SPIPrimitive *prim = (SPIPrimitive*)[ctx fetchObjectForEntityName:@"SPIPrimitive" withPredicate:nil];
	//prim.SVGPath = @"M0,-60 0,-48 -10,-45 10,-39 -10,-33 10,-27 -10,-21 10,-15 0,-12 0,0";
	//CGPoint p1 = CGPointMake(-20.f, -60.f);
	//CGPoint p2 = CGPointMake(20.f, 0.f);
	//prim.instanceRect = CGRectMake(p1.x, p1.y, p2.x-p1.x, p2.y-p1.y);
	
	
	/*SPILabel *l_p = [NSEntityDescription insertNewObjectForEntityForName:@"SPILabel" inManagedObjectContext:ctx];
	l_p.position = CGPointMake(-10,-10);
	l_p.text = @"PLUS";
	l_p.primitive = prim;
	
	SPILabel *l_m = [NSEntityDescription insertNewObjectForEntityForName:@"SPILabel" inManagedObjectContext:ctx];
	l_m.position = CGPointMake(-10,-60);
	l_m.text = @"MINUS";
	l_m.primitive = prim;
	
	
	SPIPrimitiveNetlistTemplate *pnt = [NSEntityDescription insertNewObjectForEntityForName:@"SPIPrimitiveNetlistTemplate" inManagedObjectContext:ctx];
	pnt.primitive = prim;
	pnt.templateString = @"R<InstId> $PLUS $MINUS 1000";*/
	
	SPIPin *pin_p = [NSEntityDescription insertNewObjectForEntityForName:@"SPIPin" inManagedObjectContext:ctx];
	pin_p.name = @"PLUS";
	pin_p.primitive = prim;
	pin_p.offset = CGPointMake(0.f, -60.f);
	
	SPIPin *pin_m = [NSEntityDescription insertNewObjectForEntityForName:@"SPIPin" inManagedObjectContext:ctx];
	pin_m.name = @"MINUS";
	pin_m.primitive = prim;
	pin_m.offset = CGPointMake(0.f, 0.f);
	
	
	SPISchematic *circuit = [NSEntityDescription insertNewObjectForEntityForName:@"SPISchematic" inManagedObjectContext:ctx];

	for (int idx=0, y=40; y<1496; y+=100) { 
		SPITerminal *prev_term = nil;
		for (int x=20; x<2048; x+=100, idx++) {
			SPIInstance *instance = [NSEntityDescription insertNewObjectForEntityForName:@"SPIInstance" inManagedObjectContext:ctx];
			instance.circuit = circuit;
			instance.primitive = prim;
			
			if (idx < 10) { instance.name = [NSString stringWithFormat:@"myName%d", idx]; }
	
			SPIInstancePosition *pos = [NSEntityDescription insertNewObjectForEntityForName:@"SPIInstancePosition" inManagedObjectContext:ctx];
			pos.instance = instance;
			int rot = rand()%4;
			pos.transform = CGAffineTransformRotate(CGAffineTransformMakeTranslation(x, y), (CGFloat)rot*-3.1415926/2.0); // neg rotation is CW
			pos.schematic = circuit;
			//if (idx < 50) { [pos rotateCW]; }
		
			SPITerminal *term_p = [NSEntityDescription insertNewObjectForEntityForName:@"SPITerminal" inManagedObjectContext:ctx];
			term_p.pin = pin_p;
			term_p.instance = instance;
	
			SPITerminal *term_m = [NSEntityDescription insertNewObjectForEntityForName:@"SPITerminal" inManagedObjectContext:ctx];
			term_m.pin = pin_m;
			term_m.instance = instance;

			if (prev_term) {
				SPINet *net = [NSEntityDescription insertNewObjectForEntityForName:@"SPINet" inManagedObjectContext:ctx];
				net.circuit = circuit;
                /*
				term_p.net = net;
				prev_term.net = net;
				*/
                
				if (idx<20) { net.name = [NSString stringWithFormat:@"net.%d", idx]; }
				
				NSString *path = [NSString stringWithFormat:@"M%d,%d %d,%d %d,%d", x, y-30, x-50, y-30, x-100, y-30];

				SPIWire *w = [NSEntityDescription insertNewObjectForEntityForName:@"SPIWire" inManagedObjectContext:ctx];
				w.net = net;
				w.schematic = circuit;
				w.connections = [NSSet setWithObjects:term_m, term_p, nil];
				w.SVGPath = path;
			}
			
			
			prev_term = term_m;
		}
	}
	
	// create an analysis, probeset, and some probes
	SPIDCAnalysis *dca = [NSEntityDescription insertNewObjectForEntityForName:@"SPIDCAnalysis" inManagedObjectContext:ctx];
	dca.circuit = circuit;
	dca.sourceName = @"vin";
	dca.startValue = @"1";
	dca.stopValue = @"10";
	dca.incrValue = @"2";
	
	SPIProbeSet *ps = [NSEntityDescription insertNewObjectForEntityForName:@"SPIProbeSet" inManagedObjectContext:ctx];
	//ps.analysis = dca;
	
	SPIVoltageProbe *vp1 = [NSEntityDescription insertNewObjectForEntityForName:@"SPIVoltageProbe" inManagedObjectContext:ctx];
	vp1.probeSet = ps;
	vp1.net = [circuit.nets anyObject];
    vp1.analysis = dca;

	NSError *error = nil;
	[ctx save:&error];
	
	NSLog(@"error: %@", error);
	assert(error == nil);
	
	//SPINetlister *netlister = [[[SPINetlister alloc] init] autorelease];
	//NSString *n = [netlister netlist:circuit];
	//NSLog(@"netlist: %@", n);
	
	return circuit;
}


@end
