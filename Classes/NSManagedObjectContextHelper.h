//
//  NSManagedObjectContext+Extra.h
//  LSATLogic
//
//  Created by Reid van Melle on 09-09-25.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface NSManagedObjectContext (BWCHelper)

- (BOOL)saveWithErrorLogging;

- (NSArray*)fetchObjectsForEntityName:(NSString *)newEntityName 
	sortedBy:(id)stringOrArray ascending:(BOOL)o withPredicate:(id)stringOrPredicate, ...;
	
- (NSSet *)fetchObjectsForEntityName:(NSString *)newEntityName
    withPredicate:(id)stringOrPredicate, ...;
		
- (NSManagedObject*) fetchObjectForEntityName:(NSString *)newEntityName
    withPredicate:(id)stringOrPredicate, ...;
		
// LUA Friendly routines -- no varargs
- (NSManagedObject*) fetchObjectForEntityName:(NSString *)newEntityName
    predicate:(id)stringOrPredicate;

- (NSManagedObject*) fetchPrimitive:(NSString*)primitiveName;

@end
