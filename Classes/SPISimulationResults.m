//
//  SPISimulationResults.m
//  touchspice
//
//  Created by Reid van Melle on 11-01-13.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "SPISimulationResults.h"


@implementation SPISimulationResults


@synthesize rawFilePath, errFilePath, logFilePath, OPFilePath;
@synthesize status;

- (id)initWithStatus:(SPISimulationResultStatus)s
{
    if (self = [self init]) {
        self.status = s;
    }
    return self;
}

@end
