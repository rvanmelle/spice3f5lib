//
//  NSString+SVG.h
//  touchspice
//
//  Created by Shawn Hyam on 10-12-22.
//  Copyright 2010 Brierwood Design Cooperative. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>

@interface NSString (SVG)

- (CGPathRef)CGPathFromSVG;
- (NSArray*)pointsFromSVG;
+ (NSString*)SVGFromPoints:(NSArray*)points;

@end
