//
//  SPISchematicTest.m
//  touchspice
//
//  Created by Shawn Hyam on 10-12-22.
//  Copyright 2010 Brierwood Design Cooperative. All rights reserved.
//

#import "SPISchematicTest.h"
#import "SPIPrimitive.h"
#import "SPISchematic.h"
#import "SPIInstance.h"

@implementation SPISchematicTest
- (void) testFail {
	STAssertTrue(TRUE, @"Must fail to succeed.");
}

- (void)setUp {
	NSString* path = [[NSBundle bundleWithIdentifier:@"com.brierwooddesign.LogicTests"] 
					  pathForResource:@"touchspice" 
					  ofType:@"mom"];
    NSURL* modelURL = [NSURL URLWithString:path];
    model = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    
	coord = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:model];
    store = [coord addPersistentStoreWithType: NSInMemoryStoreType
                                configuration: nil
                                          URL: nil
                                      options: nil 
                                        error: NULL];
    ctx = [[NSManagedObjectContext alloc] init];
    [ctx setPersistentStoreCoordinator: coord];
	
}

- (void)testPrimitive {
	SPIPrimitive *p = [ctx insertNewEntityWithName:@"SPIPrimitive"];
	
	CGPoint p1 = CGPointMake(-20.f, -60.f);
	CGPoint p2 = CGPointMake(20.f, 0.f);
	CGRect r = CGRectStandardize(CGRectMake(p1.x, p1.y, p2.x-p1.x, p2.y-p1.y));
	
	assert(r.origin.x == -20.f);
	assert(r.origin.y == -60.f);
	assert(r.size.width == 40.f);
	assert(r.size.height == 60.f);
	
	CGSize sz = CGSizeMake(r.size.width, r.size.height);
	p.instanceRect = r;
	STAssertEquals(p.size, sz, @"values should be equal", nil);
	
	// TODO make sure it sets the transient 'path' attribute on SPIPrimitive whenever the string is changed
	NSString *pathstr = @"M0,-60 0,-48 -10,-45 10,-39 -10,-33 10,-27 -10,-21 10,-15 0,-12 0,0";		
	p.SVGPath = pathstr;
}


- (void)testNewCircuit {
	SPISchematic *c = [NSEntityDescription insertNewObjectForEntityForName:@"SPISchematic" inManagedObjectContext:ctx];
	STAssertTrue(c != nil, @"oh oh");
	
	SPIInstance *i = [NSEntityDescription insertNewObjectForEntityForName:@"SPIInstance" inManagedObjectContext:ctx];
	STAssertTrue(i != nil, @"oh oh");
	
	i.circuit = c;
	STAssertTrue([c.instances containsObject:i], @"doesn't contain instance");
}

@end
