//
//  SPISchematic+Connectivity.m
//  touchspice
//
//  Created by Shawn Hyam on 12-01-27.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SPISchematic+Connectivity.h"
#import "SPIInstance.h"
#import "SPIInstancePosition.h"
#import "SPIPrimitive.h"
#import "SPINet.h"
#import "SPISolderDot.h"
#import "SPITerminal.h"
#import "SPIPin.h"
#import "SPIWire.h"
#import "NSString+SVG.h"

@implementation SPISchematic (Connectivity)

- (NSArray*)validateSchematicView {
    NSMutableArray *errors = [NSMutableArray arrayWithCapacity:128];
    
    // every terminal should be connected to a wire
    for (SPIInstance *instance in self.instances) {
        for (SPITerminal *terminal in instance.terminals) {
            if (terminal.wire == nil) {
                // this is not right
                [errors addObject:[NSString stringWithFormat:@"Terminal %@.%@ is not connected.", instance.name, terminal.pin.name]];
            }
        }
    }
    return errors;
    
    
    NSMutableSet *wire_segments = [NSMutableSet setWithCapacity:256];
    
    for (SPIWire *wire in self.wires) {
        NSArray *points = [wire.SVGPath pointsFromSVG];
        
        // [1] make sure the wire is terminated at both ends, with either a solder dot or terminal
        int n = wire.connections.count;
        assert(n == 2);
        
        // create all the straight-line segments of the wire so we can make sure none overlap
        for (int i=1; i<points.count; i++) {
            CGPoint pt0 = [[points objectAtIndex:(i-1)] CGPointValue];
            CGPoint pt1 = [[points objectAtIndex:i] CGPointValue];
            CGRect r = CGRectMake(pt0.x, pt0.y, pt1.x-pt0.x, pt1.y-pt0.y);
            
            [wire_segments addObject:[NSValue valueWithCGRect:r]];
        }
        
        
        // TODO get positions of all solder dots and terminals
        // TODO make sure points[0] == position of solder dot or terminal
        // TODO make sure points[-1] == position of other solder dot or terminal
    }
    
    
    
    
    for (SPISolderDot *solderDot in self.solderDots) {
        // a solder dot is unnecessary unless there are 3 wires connected to it
        int n = solderDot.wires.count;
        assert(n >= 3);
        
        // TODO all wires connected on a solder dot share a net, so make sure that is correct
    }
    
    
    
    
}

// ways circuit connectivity can change...
// 1 add a wire between two terminals (creates a net)
// 2 add a wire between terminal and solder dot (connects terminal to existing net)
// 3 add a wire between two solder dots (combines two existing nets)
// 4 remove a wire that connects two terminals (deletes a net)
// 5 remove a wire between terminal and solder dot (removes terminal from existing net)
// 6 remove a wire between two solder dots (splits single existing net into two)

// related schematic view issues
// connecting a wire to a wire means that we have to split the wire in half at that point, add a solder dot,
//     and then connect to that solder dot
// a solder dot with only 2 wires can (should?) be removed -- which then combines 2 wires into 1
// removing a wire between two solder dots could potentially result in both being removed

- (void)updateConnectivity {
    // scan over the schematic, making any corrections to net/terminal connectivity based on the Wires
}

@end
