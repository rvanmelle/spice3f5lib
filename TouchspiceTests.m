//
//  TouchspiceTests.m
//  touchspice
//
//  Created by Shawn Hyam on 10-12-22.
//  Copyright 2010 Brierwood Design Cooperative. All rights reserved.
//

#import "TouchspiceTests.h"
#import "SPIPrimitive.h"
#import "SPISchematic.h"
#import "SPIInstance.h"
#import "SPIPrimitiveNetlistTemplate.h"
#import "SPINetlister.h"
#import "SPIPin.h"
#import "SPITerminal.h"
#import "SPINet.h"

@implementation TouchspiceTests

- (void)setUp {
	NSString* path = [[NSBundle bundleWithIdentifier:@"com.brierwooddesign.touchspice"] 
					  pathForResource:@"touchspice" 
					  ofType:@"mom"];
    NSURL* modelURL = [NSURL URLWithString:path];
    model = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    
	coord = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:model];
    store = [coord addPersistentStoreWithType: NSInMemoryStoreType
                                configuration: nil
                                          URL: nil
                                      options: nil 
                                        error: NULL];
    ctx = [[NSManagedObjectContext alloc] init];
    [ctx setPersistentStoreCoordinator: coord];
	
}

- (void) testAppDelegate {
    NSLog(@"ok: %@", [[UIApplication sharedApplication] delegate]);
    id yourApplicationDelegate = [[UIApplication sharedApplication] delegate];
    STAssertNotNil(yourApplicationDelegate, @"UIApplication failed to find the AppDelegate");
    
	
	SPIPrimitive *prim = [NSEntityDescription insertNewObjectForEntityForName:@"SPIPrimitive" inManagedObjectContext:ctx];
	// TODO schematic-drawing-related stuff
	prim.SVGPath = @"M0,-60 0,-48 -10,-45 10,-39 -10,-33 10,-27 -10,-21 10,-15 0,-12 0,0";
	CGPoint p1 = CGPointMake(-20.f, -60.f);
	CGPoint p2 = CGPointMake(20.f, 0.f);
	prim.instanceRect = CGRectMake(p1.x, p1.y, p2.x-p1.x, p2.y-p1.y);

	SPIPrimitiveNetlistTemplate *pnt = [NSEntityDescription insertNewObjectForEntityForName:@"SPIPrimitiveNetlistTemplate" inManagedObjectContext:ctx];
	pnt.primitive = prim;
	pnt.templateString = @"R<InstId> $PLUS $MINUS 1000";
	
	SPIPin *pin_p = [NSEntityDescription insertNewObjectForEntityForName:@"SPIPin" inManagedObjectContext:ctx];
	pin_p.name = @"PLUS";
	pin_p.primitive = prim;
	pin_p.offset = CGPointMake(0.f, -60.f);
	
	SPIPin *pin_m = [NSEntityDescription insertNewObjectForEntityForName:@"SPIPin" inManagedObjectContext:ctx];
	pin_m.name = @"MINUS";
	pin_m.primitive = prim;
	pin_m.offset = CGPointMake(0.f, 0.f);

	
	
	NSError *error = nil;
	[ctx save:&error];
	STAssertNil(error, nil);	
	
	SPICircuit *circuit = [NSEntityDescription insertNewObjectForEntityForName:@"SPICircuit" inManagedObjectContext:ctx];
	
	SPIInstance *instance = [NSEntityDescription insertNewObjectForEntityForName:@"SPIInstance" inManagedObjectContext:ctx];
	instance.circuit = circuit;
	instance.primitive = prim;
	
	SPITerminal *term_p = [NSEntityDescription insertNewObjectForEntityForName:@"SPITerminal" inManagedObjectContext:ctx];
	term_p.pin = pin_p;
	term_p.instance = instance;
	
	SPITerminal *term_m = [NSEntityDescription insertNewObjectForEntityForName:@"SPITerminal" inManagedObjectContext:ctx];
	term_m.pin = pin_m;
	term_m.instance = instance;
	
	SPINet *net1 = [NSEntityDescription insertNewObjectForEntityForName:@"SPINet" inManagedObjectContext:ctx];
	net1.circuit = circuit;
	term_p.net = net1;
	
	SPINet *net2 = [NSEntityDescription insertNewObjectForEntityForName:@"SPINet" inManagedObjectContext:ctx];
	net2.circuit = circuit;
	term_m.net = net2;
	
	
	
	
	[ctx save:&error];
	STAssertNil(error, nil);	
	
	
	
	SPINetlister *netlister = [[[SPINetlister alloc] init] autorelease];
	[netlister netlist:circuit];
}

@end
