//
//  main.m
//  touchspice
//
//  Created by Shawn Hyam on 10-12-18.
//  Copyright (c) 2010 Brierwood Design Cooperative. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "touchspiceAppDelegate.h"

int main(int argc, char *argv[])
{
    int retVal = UIApplicationMain(argc, argv, nil, NSStringFromClass([touchspiceAppDelegate class]));
    return retVal;
}

