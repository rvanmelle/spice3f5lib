function BuildPrimitive_cap()
  local prim = Primitive{name='cap', SVGPath='M9.89949 -32.1005 G0,-42 14 0.7853981633974483 2.356194490192345M0,-60 0,-28 M-10,-22 10,-22 M0,-22 0,0 ', instRect={{-20, -60}, {20, 0}}}
  Pin{'MINUS', 0, -60, prim}
  Pin{'PLUS', 0, 0, prim}
  Label{x=-10, y=-10, text='PLUS', primitive=prim}
  Label{x=-10, y=-60, text='MINUS', primitive=prim}
end
BuildPrimitive_cap()
function BuildPrimitive_cap3()
  local prim = Primitive{name='cap3', SVGPath='M9.89949 -32.1005 G0,-42 14 0.7853981633974483 2.356194490192345M0,-60 0,-28 M-10,-22 10,-22 M0,-22 0,0 ', instRect={{-20, -60}, {20, 0}}}
  Pin{'MINUS', 0, -60, prim}
  Pin{'PLUS', 0, 0, prim}
  Pin{'B', 20, -30, prim}
  Label{x=-10, y=-10, text='PLUS', primitive=prim}
  Label{x=-10, y=-60, text='MINUS', primitive=prim}
  Label{x=24, y=-38, text='B', primitive=prim}
end
BuildPrimitive_cap3()
function BuildPrimitive_cap4()
  local prim = Primitive{name='cap4', SVGPath='M9.89949 -32.1005 G0,-42 14 0.7853981633974483 2.356194490192345M0,-60 0,-28 M-10,-22 10,-22 M0,-22 0,0 ', instRect={{-20, -60}, {20, 0}}}
  Pin{'MINUS', 0, -60, prim}
  Pin{'PLUS', 0, 0, prim}
  Pin{'B', 20, -30, prim}
  Pin{'T', -20, -30, prim}
  Label{x=-10, y=-10, text='PLUS', primitive=prim}
  Label{x=-10, y=-60, text='MINUS', primitive=prim}
  Label{x=24, y=-38, text='B', primitive=prim}
  Label{x=-24, y=-38, text='T', primitive=prim}
end
BuildPrimitive_cap4()
function BuildPrimitive_cccs()
  local prim = Primitive{name='cccs', SVGPath='M0,-18 0,0 M0,-60 0,-42 M0,-18 12,-30 0,-42 -12,-30 0,-18 M0,-36 0,-24 M-3,-33 0,-36 3,-33 ', instRect={{-20, -60}, {20, 0}}}
  Pin{'MINUS', 0, -60, prim}
  Pin{'PLUS', 0, 0, prim}
  Label{x=-5, y=-59, text='MINUS', primitive=prim}
  Label{x=-5, y=-10, text='PLUS', primitive=prim}
end
BuildPrimitive_cccs()
function BuildPrimitive_ccvs()
  local prim = Primitive{name='ccvs', SVGPath='M0,-28 0,-22 M-3,-25 3,-25 M0,-18 0,0 M0,-60 0,-42 M-3,-35 3,-35 M0,-18 12,-30 0,-42 -12,-30 0,-18 ', instRect={{-20, -60}, {20, 0}}}
  Pin{'MINUS', 0, -60, prim}
  Pin{'PLUS', 0, 0, prim}
  Label{x=-5, y=-59, text='MINUS', primitive=prim}
  Label{x=-5, y=-10, text='PLUS', primitive=prim}
end
BuildPrimitive_ccvs()
function BuildPrimitive_diode()
  local prim = Primitive{name='diode', SVGPath='M-10,-20 10,-20 0,-35 ZM0,-60 0,-35 M-10,-35 10,-35 M0,-20 0,0 ', instRect={{-20, -60}, {20, 0}}}
  Pin{'MINUS', 0, -60, prim}
  Pin{'PLUS', 0, 0, prim}
  Label{x=-5, y=-10, text='PLUS', primitive=prim}
  Label{x=-5, y=-59, text='MINUS', primitive=prim}
end
BuildPrimitive_diode()
function BuildPrimitive_diode3()
  local prim = Primitive{name='diode3', SVGPath='M-10,-20 10,-20 0,-35 ZM0,-60 0,-35 M-10,-35 10,-35 M0,-20 0,0 ', instRect={{-20, -60}, {20, 0}}}
  Pin{'MINUS', 0, -60, prim}
  Pin{'PLUS', 0, 0, prim}
  Pin{'B', 20, -30, prim}
  Label{x=-5, y=-10, text='PLUS', primitive=prim}
  Label{x=-5, y=-59, text='MINUS', primitive=prim}
  Label{x=24, y=-38, text='B', primitive=prim}
end
BuildPrimitive_diode3()
function BuildPrimitive_diode4()
  local prim = Primitive{name='diode4', SVGPath='M-10,-20 10,-20 0,-35 ZM0,-60 0,-35 M-10,-35 10,-35 M0,-20 0,0 ', instRect={{-20, -60}, {20, 0}}}
  Pin{'MINUS', 0, -60, prim}
  Pin{'PLUS', 0, 0, prim}
  Pin{'B', 20, -30, prim}
  Pin{'T', -20, -30, prim}
  Label{x=-5, y=-10, text='PLUS', primitive=prim}
  Label{x=-5, y=-59, text='MINUS', primitive=prim}
  Label{x=24, y=-38, text='B', primitive=prim}
  Label{x=-24, y=-38, text='T', primitive=prim}
end
BuildPrimitive_diode4()
function BuildPrimitive_fuse()
  local prim = Primitive{name='fuse', SVGPath='M0,0 0,-10 M0,-60 0,-50 M0.0 -10 G0,-20 10 1.5707963267948966 4.71238898038469M0.0 -50 G0,-40 10 4.71238898038469 1.5707963267948966', instRect={{-20, -60}, {20, 0}}}
  Pin{'MINUS', 0, -60, prim}
  Pin{'PLUS', 0, 0, prim}
  Label{x=-10, y=-10, text='PLUS', primitive=prim}
  Label{x=-10, y=-60, text='MINUS', primitive=prim}
end
BuildPrimitive_fuse()
function BuildPrimitive_fuse3()
  local prim = Primitive{name='fuse3', SVGPath='M0,0 0,-10 M0,-60 0,-50 M0.0 -10 G0,-20 10 1.5707963267948966 4.71238898038469M0.0 -50 G0,-40 10 4.71238898038469 1.5707963267948966', instRect={{-20, -60}, {20, 0}}}
  Pin{'MINUS', 0, -60, prim}
  Pin{'PLUS', 0, 0, prim}
  Pin{'B', 20, -30, prim}
  Label{x=-10, y=-10, text='PLUS', primitive=prim}
  Label{x=-10, y=-60, text='MINUS', primitive=prim}
  Label{x=24, y=-38, text='B', primitive=prim}
end
BuildPrimitive_fuse3()
function BuildPrimitive_fuse4()
  local prim = Primitive{name='fuse4', SVGPath='M0,0 0,-10 M0,-60 0,-50 M0.0 -10 G0,-20 10 1.5707963267948966 4.71238898038469M0.0 -50 G0,-40 10 4.71238898038469 1.5707963267948966', instRect={{-20, -60}, {20, 0}}}
  Pin{'MINUS', 0, -60, prim}
  Pin{'PLUS', 0, 0, prim}
  Pin{'B', 20, -30, prim}
  Pin{'T', -20, -30, prim}
  Label{x=-10, y=-10, text='PLUS', primitive=prim}
  Label{x=-10, y=-60, text='MINUS', primitive=prim}
  Label{x=24, y=-38, text='B', primitive=prim}
  Label{x=-24, y=-38, text='T', primitive=prim}
end
BuildPrimitive_fuse4()
function BuildPrimitive_gnd()
  local prim = Primitive{name='gnd', SVGPath='M-10,-30 10,-30 0,-40 ZM0,-30 0,0 ', instRect={{-10, -40}, {10, 0}}}
  Pin{'gnd!', 0, 0, prim}
end
BuildPrimitive_gnd()
function BuildPrimitive_idc()
  local prim = Primitive{name='idc', SVGPath='M10 -30 G0,-30 10 0.0 6.283185307179586M0,-40 0,-60 M0,-20 0,0 M0,-36 0,-24 M-3,-33 0,-36 3,-33 ', instRect={{-20, -60}, {20, 0}}}
  Pin{'MINUS', 0, -60, prim}
  Pin{'PLUS', 0, 0, prim}
  Label{x=-5, y=-59, text='MINUS', primitive=prim}
  Label{x=-5, y=-10, text='PLUS', primitive=prim}
end
BuildPrimitive_idc()
function BuildPrimitive_iexp()
  local prim = Primitive{name='iexp', SVGPath='M-25 -23 G-25,-33 10 1.5707963267948966 3.141592653589793M-25 -22 G-15,-22 10 3.141592653589793 4.71238898038469M10 -30 G0,-30 10 0.0 6.283185307179586M0,-40 0,-60 M0,-20 0,0 M0,-36 0,-24 M-3,-33 0,-36 3,-33 ', instRect={{-20, -60}, {20, 0}}}
  Pin{'MINUS', 0, -60, prim}
  Pin{'PLUS', 0, 0, prim}
  Label{x=-5, y=-59, text='MINUS', primitive=prim}
  Label{x=-5, y=-10, text='PLUS', primitive=prim}
end
BuildPrimitive_iexp()
function BuildPrimitive_ind()
  local prim = Primitive{name='ind', SVGPath='M3 -38 G3,-34 4 4.71238898038469 1.5707963267948966M3 -30 G3,-26 4 4.71238898038469 1.5707963267948966M3 -46 G3,-42 4 4.71238898038469 1.5707963267948966M3 -22 G3,-18 4 4.71238898038469 1.5707963267948966M0,-14 3,-14 M0,-60 0,-46 M0,-22 3,-22 M0,-30 3,-30 M0,-46 3,-46 M0,-38 3,-38 M0,0 0,-14 ', instRect={{-20, -60}, {20, 0}}}
  Pin{'MINUS', 0, -60, prim}
  Pin{'PLUS', 0, 0, prim}
  Label{x=-5, y=-10, text='PLUS', primitive=prim}
  Label{x=-5, y=-59, text='MINUS', primitive=prim}
end
BuildPrimitive_ind()
function BuildPrimitive_ipulse()
  local prim = Primitive{name='ipulse', SVGPath='M10 -30 G0,-30 10 0.0 6.283185307179586M0,-40 0,-60 M0,-20 0,0 M-31,-33 -28,-33 -28,-27 -24,-27 -24,-33 -20,-33 -20,-27 -16,-27 -16,-33 -13,-33 M0,-36 0,-24 M-3,-33 0,-36 3,-33 ', instRect={{-20, -60}, {20, 0}}}
  Pin{'MINUS', 0, -60, prim}
  Pin{'PLUS', 0, 0, prim}
  Label{x=-5, y=-59, text='MINUS', primitive=prim}
  Label{x=-5, y=-10, text='PLUS', primitive=prim}
end
BuildPrimitive_ipulse()
function BuildPrimitive_ipwl()
  local prim = Primitive{name='ipwl', SVGPath='M10 -30 G0,-30 10 0.0 6.283185307179586M0,-40 0,-60 M0,-20 0,0 M-34,-34 -30,-34 -28,-26 -26,-38 -23,-38 -23,-30 -20,-30 -20,-26 -18,-26 -18,-34 -16,-34 -14,-28 M0,-36 0,-24 M-3,-33 0,-36 3,-33 ', instRect={{-20, -60}, {20, 0}}}
  Pin{'MINUS', 0, -60, prim}
  Pin{'PLUS', 0, 0, prim}
  Label{x=-5, y=-59, text='MINUS', primitive=prim}
  Label{x=-5, y=-10, text='PLUS', primitive=prim}
end
BuildPrimitive_ipwl()
function BuildPrimitive_ipwlf()
  local prim = Primitive{name='ipwlf', SVGPath='M10 -30 G0,-30 10 0.0 6.283185307179586M0,-40 0,-60 M0,-20 0,0 M-34,-34 -30,-34 -28,-26 -26,-38 -23,-38 -23,-30 -20,-30 -20,-26 -18,-26 -18,-34 -16,-34 -14,-28 M0,-36 0,-24 M-3,-33 0,-36 3,-33 ', instRect={{-20, -60}, {20, 0}}}
  Pin{'MINUS', 0, -60, prim}
  Pin{'PLUS', 0, 0, prim}
  Label{x=-5, y=-59, text='MINUS', primitive=prim}
  Label{x=-5, y=-10, text='PLUS', primitive=prim}
end
BuildPrimitive_ipwlf()
function BuildPrimitive_isin()
  local prim = Primitive{name='isin', SVGPath='M10 -30 G0,-30 10 0.0 6.283185307179586M-23 -29 G-26,-29 3 0.0 3.141592653589793M-22 -29 G-19,-29 3 3.141592653589793 0.0M0,-40 0,-60 M0,-20 0,0 M0,-36 0,-24 M-3,-33 0,-36 3,-33 ', instRect={{-20, -60}, {20, 0}}}
  Pin{'MINUS', 0, -60, prim}
  Pin{'PLUS', 0, 0, prim}
  Label{x=-5, y=-59, text='MINUS', primitive=prim}
  Label{x=-5, y=-10, text='PLUS', primitive=prim}
end
BuildPrimitive_isin()
function BuildPrimitive_isource()
  local prim = Primitive{name='isource', SVGPath='M10 -30 G0,-30 10 0.0 6.283185307179586M0,-40 0,-60 M0,-20 0,0 M0,-36 0,-24 M-3,-33 0,-36 3,-33 ', instRect={{-20, -60}, {20, 0}}}
  Pin{'MINUS', 0, -60, prim}
  Pin{'PLUS', 0, 0, prim}
  Label{x=-5, y=-59, text='MINUS', primitive=prim}
  Label{x=-5, y=-10, text='PLUS', primitive=prim}
end
BuildPrimitive_isource()
function BuildPrimitive_makeGlobal()
  local prim = Primitive{name='makeGlobal', SVGPath='M-10,30 10,30 0,40 ZM0,0 0,30 M-6,23 6,23 ', instRect={{-10, 0}, {10, 40}}}
  Pin{'net', 0, 0, prim}
end
BuildPrimitive_makeGlobal()
function BuildPrimitive_mind()
  local prim = Primitive{name='mind', SVGPath='M-10,-25 -5,-25 M10,-25 10,-20 M-10,-25 -10,-20 M10,-25 5,-25 M10 -25.4174 G0,-30 11 0.4297000618410039 2.6779459377974995', instRect={{-20, -60}, {20, 0}}}
end
BuildPrimitive_mind()
function BuildPrimitive_nmos()
  local prim = Primitive{name='nmos', SVGPath='M40,-30 40,-15 20,-15 M15,-15 15,15 M15,0 0,0 M30,-20 40,-15 30,-10 M20,15 20,-15 M40,30 40,15 20,15 ', instRect={{0, -30}, {40, 30}}}
  Pin{'D', 40, 30, prim}
  Pin{'G', 0, 0, prim}
  Pin{'S', 40, -30, prim}
  Label{x=35, y=31, text='D', primitive=prim}
  Label{x=10, y=6, text='G', primitive=prim}
  Label{x=35, y=-29, text='S', primitive=prim}
end
BuildPrimitive_nmos()
function BuildPrimitive_nmos4()
  local prim = Primitive{name='nmos4', SVGPath='M40,30 40,15 20,15 M20,15 20,-15 M30,-20 40,-15 30,-10 M15,0 0,0 M15,-15 15,15 M40,0 20,0 M20,-15 40,-15 40,-30 ', instRect={{0, -30}, {40, 30}}}
  Pin{'D', 40, 30, prim}
  Pin{'G', 0, 0, prim}
  Pin{'S', 40, -30, prim}
  Pin{'B', 40, 0, prim}
  Label{x=35, y=31, text='D', primitive=prim}
  Label{x=10, y=6, text='G', primitive=prim}
  Label{x=35, y=-29, text='S', primitive=prim}
  Label{x=35, y=6, text='B', primitive=prim}
end
BuildPrimitive_nmos4()
function BuildPrimitive_nmos4hv()
  local prim = Primitive{name='nmos4hv', SVGPath='M40,30 40,15 20,15 M30,-20 40,-15 30,-10 M15,0 0,0 M15,-15 15,15 M40,0 20,0 M20,-15 40,-15 40,-30 ', instRect={{0, -30}, {40, 30}}}
  Pin{'D', 40, 30, prim}
  Pin{'G', 0, 0, prim}
  Pin{'S', 40, -30, prim}
  Pin{'B', 40, 0, prim}
  Label{x=35, y=20, text='D', primitive=prim}
  Label{x=15, y=6, text='G', primitive=prim}
  Label{x=35, y=-29, text='S', primitive=prim}
  Label{x=20, y=6, text='B', primitive=prim}
end
BuildPrimitive_nmos4hv()
function BuildPrimitive_nmoshv()
  local prim = Primitive{name='nmoshv', SVGPath='M40,-30 40,-15 20,-15 M15,-15 15,15 M15,0 0,0 M30,-20 40,-15 30,-10 M40,30 40,15 20,15 ', instRect={{0, -30}, {40, 30}}}
  Pin{'D', 40, 30, prim}
  Pin{'G', 0, 0, prim}
  Pin{'S', 40, -30, prim}
  Label{x=35, y=20, text='D', primitive=prim}
  Label{x=15, y=6, text='G', primitive=prim}
  Label{x=35, y=-29, text='S', primitive=prim}
end
BuildPrimitive_nmoshv()
function BuildPrimitive_npn()
  local prim = Primitive{name='npn', SVGPath='M35,-5 40,-15 29,-20 M0,0 20,0 M40,-30 40,-15 20,-7 M40,30 40,15 20,7 M20,-15 20,15 ', instRect={{0, -30}, {40, 30}}}
  Pin{'E', 40, -30, prim}
  Pin{'C', 40, 30, prim}
  Pin{'B', 0, 0, prim}
  Label{x=35, y=20, text='C', primitive=prim}
  Label{x=20, y=6, text='B', primitive=prim}
  Label{x=35, y=-28, text='E', primitive=prim}
end
BuildPrimitive_npn()
function BuildPrimitive_npn4()
  local prim = Primitive{name='npn4', SVGPath='M35,-5 40,-15 29,-20 M0,0 20,0 M40,-30 40,-15 20,-7 M40,30 40,15 20,7 M20,-15 20,15 ', instRect={{0, -30}, {40, 30}}}
  Pin{'E', 40, -30, prim}
  Pin{'C', 40, 30, prim}
  Pin{'B', 0, 0, prim}
  Pin{'S', 48, 0, prim}
  Label{x=35, y=20, text='C', primitive=prim}
  Label{x=20, y=6, text='B', primitive=prim}
  Label{x=35, y=-28, text='E', primitive=prim}
  Label{x=44, y=6, text='S', primitive=prim}
end
BuildPrimitive_npn4()
function BuildPrimitive_npn4a()
  local prim = Primitive{name='npn4a', SVGPath='M35,-5 40,-15 29,-20 M0,0 20,0 M40,-30 40,-15 20,-7 M40,30 40,15 20,7 M20,-15 20,15 ', instRect={{0, -30}, {40, 30}}}
  Pin{'E', 40, -30, prim}
  Pin{'C', 40, 30, prim}
  Pin{'B', 0, 0, prim}
  Pin{'S', 48, -8, prim}
  Label{x=35, y=20, text='C', primitive=prim}
  Label{x=20, y=6, text='B', primitive=prim}
  Label{x=35, y=-28, text='E', primitive=prim}
  Label{x=44, y=-2, text='S', primitive=prim}
end
BuildPrimitive_npn4a()
function BuildPrimitive_pcapacitor()
  local prim = Primitive{name='pcapacitor', SVGPath='M9.89949 -32.1005 G0,-42 14 0.7853981633974483 2.356194490192345M0,-60 0,-28 M-10,-22 10,-22 M0,-22 0,0 ', instRect={{-20, -60}, {20, 0}}}
  Pin{'MINUS', 0, -60, prim}
  Pin{'PLUS', 0, 0, prim}
  Label{x=-10, y=-10, text='PLUS', primitive=prim}
  Label{x=-10, y=-60, text='MINUS', primitive=prim}
end
BuildPrimitive_pcapacitor()
function BuildPrimitive_pdc()
  local prim = Primitive{name='pdc', SVGPath='M0,0 0,-12 6,-14 -6,-18 6,-22 -6,-26 6,-30 -6,-34 0,-36 0,-48 M0,-50 0,-56 M-3,-53 3,-53 M-3,-63 3,-63 M0,-80 0,-68 M10 -58 G0,-58 10 0.0 6.283185307179586', instRect={{-20, -80}, {20, 0}}}
  Pin{'MINUS', 0, -80, prim}
  Pin{'PLUS', 0, 0, prim}
  Label{x=-10, y=-10, text='PLUS', primitive=prim}
  Label{x=-10, y=-80, text='MINUS', primitive=prim}
end
BuildPrimitive_pdc()
function BuildPrimitive_pdiode()
  local prim = Primitive{name='pdiode', SVGPath='M-10,-20 10,-20 0,-35 ZM0,-60 0,-35 M-10,-35 10,-35 M0,-20 0,0 ', instRect={{-20, -60}, {20, 0}}}
  Pin{'MINUS', 0, -60, prim}
  Pin{'PLUS', 0, 0, prim}
  Label{x=-5, y=-10, text='PLUS', primitive=prim}
  Label{x=-5, y=-59, text='MINUS', primitive=prim}
end
BuildPrimitive_pdiode()
function BuildPrimitive_pmos()
  local prim = Primitive{name='pmos', SVGPath='M0,0 15,0 M20,-15 40,-15 40,-30 M20,-15 20,15 M20,15 40,15 40,30 M15,-15 15,15 M40,20 30,15 40,10 ', instRect={{0, -30}, {40, 30}}}
  Pin{'D', 40, -30, prim}
  Pin{'G', 0, 0, prim}
  Pin{'S', 40, 30, prim}
  Label{x=35, y=-29, text='D', primitive=prim}
  Label{x=10, y=6, text='G', primitive=prim}
  Label{x=35, y=31, text='S', primitive=prim}
end
BuildPrimitive_pmos()
function BuildPrimitive_pmos4()
  local prim = Primitive{name='pmos4', SVGPath='M15,-15 15,15 M20,-15 40,-15 40,-30 M20,15 40,15 40,30 M0,0 15,0 M37,20 27,15 37,10 M20,0 40,0 M20,-15 20,15 ', instRect={{0, -30}, {40, 30}}}
  Pin{'D', 40, -30, prim}
  Pin{'G', 0, 0, prim}
  Pin{'S', 40, 30, prim}
  Pin{'B', 40, 0, prim}
  Label{x=35, y=-29, text='D', primitive=prim}
  Label{x=10, y=6, text='G', primitive=prim}
  Label{x=35, y=31, text='S', primitive=prim}
  Label{x=35, y=6, text='B', primitive=prim}
end
BuildPrimitive_pmos4()
function BuildPrimitive_pmos4hv()
  local prim = Primitive{name='pmos4hv', SVGPath='M15,-15 15,15 M20,-15 40,-15 40,-30 M20,15 40,15 40,30 M0,0 15,0 M37,20 27,15 37,10 M20,0 40,0 ', instRect={{0, -30}, {40, 30}}}
  Pin{'D', 40, -30, prim}
  Pin{'G', 0, 0, prim}
  Pin{'S', 40, 30, prim}
  Pin{'B', 40, 0, prim}
  Label{x=35, y=-29, text='D', primitive=prim}
  Label{x=15, y=6, text='G', primitive=prim}
  Label{x=35, y=20, text='S', primitive=prim}
  Label{x=20, y=6, text='B', primitive=prim}
end
BuildPrimitive_pmos4hv()
function BuildPrimitive_pmoshv()
  local prim = Primitive{name='pmoshv', SVGPath='M0,0 15,0 M20,-15 40,-15 40,-30 M20,15 40,15 40,30 M15,-15 15,15 M40,20 30,15 40,10 ', instRect={{0, -30}, {40, 30}}}
  Pin{'D', 40, -30, prim}
  Pin{'G', 0, 0, prim}
  Pin{'S', 40, 30, prim}
  Label{x=35, y=-29, text='D', primitive=prim}
  Label{x=15, y=6, text='G', primitive=prim}
  Label{x=35, y=20, text='S', primitive=prim}
end
BuildPrimitive_pmoshv()
function BuildPrimitive_pnp()
  local prim = Primitive{name='pnp', SVGPath='M34,19 31,11 40,8 M0,0 20,0 M40,-30 40,-15 20,-7 M40,30 40,15 20,7 M20,-15 20,15 ', instRect={{0, -30}, {40, 30}}}
  Pin{'E', 40, -30, prim}
  Pin{'C', 40, 30, prim}
  Pin{'B', 0, 0, prim}
  Label{x=35, y=-29, text='C', primitive=prim}
  Label{x=20, y=6, text='B', primitive=prim}
  Label{x=35, y=21, text='E', primitive=prim}
end
BuildPrimitive_pnp()
function BuildPrimitive_pnp4()
  local prim = Primitive{name='pnp4', SVGPath='M34,19 31,11 40,8 M0,0 20,0 M40,-30 40,-15 20,-7 M40,30 40,15 20,7 M20,-15 20,15 ', instRect={{0, -30}, {40, 30}}}
  Pin{'E', 40, -30, prim}
  Pin{'C', 40, 30, prim}
  Pin{'B', 0, 0, prim}
  Pin{'S', 48, 0, prim}
  Label{x=35, y=-29, text='C', primitive=prim}
  Label{x=20, y=6, text='B', primitive=prim}
  Label{x=35, y=21, text='E', primitive=prim}
  Label{x=44, y=6, text='S', primitive=prim}
end
BuildPrimitive_pnp4()
function BuildPrimitive_pnp4a()
  local prim = Primitive{name='pnp4a', SVGPath='M34,19 31,11 40,8 M0,0 20,0 M40,-30 40,-15 20,-7 M40,30 40,15 20,7 M20,-15 20,15 ', instRect={{0, -30}, {40, 30}}}
  Pin{'E', 40, -30, prim}
  Pin{'C', 40, 30, prim}
  Pin{'B', 0, 0, prim}
  Pin{'S', 48, -8, prim}
  Label{x=35, y=-29, text='C', primitive=prim}
  Label{x=20, y=6, text='B', primitive=prim}
  Label{x=35, y=21, text='E', primitive=prim}
  Label{x=44, y=-2, text='S', primitive=prim}
end
BuildPrimitive_pnp4a()
function BuildPrimitive_port()
  local prim = Primitive{name='port', SVGPath='M0,0 0,-12 6,-14 -6,-18 6,-22 -6,-26 6,-30 -6,-34 0,-36 0,-48 M0,-50 0,-56 M-3,-53 3,-53 M-3,-63 3,-63 M0,-80 0,-68 M10 -58 G0,-58 10 0.0 6.283185307179586', instRect={{-20, -80}, {20, 0}}}
  Pin{'MINUS', 0, -80, prim}
  Pin{'PLUS', 0, 0, prim}
  Label{x=-10, y=-10, text='PLUS', primitive=prim}
  Label{x=-10, y=-80, text='MINUS', primitive=prim}
end
BuildPrimitive_port()
function BuildPrimitive_ppulse()
  local prim = Primitive{name='ppulse', SVGPath='M-31,-57 -28,-57 -28,-51 -24,-51 -24,-57 -20,-57 -20,-51 -16,-51 -16,-57 -13,-57 M0,0 0,-12 6,-14 -6,-18 6,-22 -6,-26 6,-30 -6,-34 0,-36 0,-48 M0,-50 0,-56 M-3,-53 3,-53 M-3,-63 3,-63 M0,-80 0,-68 M10 -58 G0,-58 10 0.0 6.283185307179586', instRect={{-20, -80}, {20, 0}}}
  Pin{'MINUS', 0, -80, prim}
  Pin{'PLUS', 0, 0, prim}
  Label{x=-10, y=-10, text='PLUS', primitive=prim}
  Label{x=-10, y=-80, text='MINUS', primitive=prim}
end
BuildPrimitive_ppulse()
function BuildPrimitive_ppwl()
  local prim = Primitive{name='ppwl', SVGPath='M0,0 0,-12 6,-14 -6,-18 6,-22 -6,-26 6,-30 -6,-34 0,-36 0,-48 M0,-50 0,-56 M-3,-53 3,-53 M-3,-63 3,-63 M0,-80 0,-68 M-34,-58 -30,-58 -28,-50 -26,-62 -23,-62 -23,-54 -20,-54 -20,-50 -18,-50 -18,-58 -16,-58 -14,-52 M10 -58 G0,-58 10 0.0 6.283185307179586', instRect={{-20, -80}, {20, 0}}}
  Pin{'MINUS', 0, -80, prim}
  Pin{'PLUS', 0, 0, prim}
  Label{x=-10, y=-10, text='PLUS', primitive=prim}
  Label{x=-10, y=-80, text='MINUS', primitive=prim}
end
BuildPrimitive_ppwl()
function BuildPrimitive_ppwlf()
  local prim = Primitive{name='ppwlf', SVGPath='M0,0 0,-12 6,-14 -6,-18 6,-22 -6,-26 6,-30 -6,-34 0,-36 0,-48 M0,-50 0,-56 M-3,-53 3,-53 M-3,-63 3,-63 M0,-80 0,-68 M-34,-58 -30,-58 -28,-50 -26,-62 -23,-62 -23,-54 -20,-54 -20,-50 -18,-50 -18,-58 -16,-58 -14,-52 M10 -58 G0,-58 10 0.0 6.283185307179586', instRect={{-20, -80}, {20, 0}}}
  Pin{'MINUS', 0, -80, prim}
  Pin{'PLUS', 0, 0, prim}
  Label{x=-10, y=-10, text='PLUS', primitive=prim}
  Label{x=-10, y=-80, text='MINUS', primitive=prim}
end
BuildPrimitive_ppwlf()
function BuildPrimitive_presistor()
  local prim = Primitive{name='presistor', SVGPath='M0,-60 0,-48 -10,-45 10,-39 -10,-33 10,-27 -10,-21 10,-15 0,-12 0,0 ', instRect={{-20, -60}, {20, 0}}}
  Pin{'MINUS', 0, -60, prim}
  Pin{'PLUS', 0, 0, prim}
  Label{x=-10, y=-10, text='PLUS', primitive=prim}
  Label{x=-10, y=-60, text='MINUS', primitive=prim}
end
BuildPrimitive_presistor()
function BuildPrimitive_psin()
  local prim = Primitive{name='psin', SVGPath='M-23 -58 G-26,-58 3 0.0 3.141592653589793M-22 -58 G-19,-58 3 3.141592653589793 0.0M0,0 0,-12 6,-14 -6,-18 6,-22 -6,-26 6,-30 -6,-34 0,-36 0,-48 M0,-50 0,-56 M-3,-53 3,-53 M-3,-63 3,-63 M0,-80 0,-68 M10 -58 G0,-58 10 0.0 6.283185307179586', instRect={{-20, -80}, {20, 0}}}
  Pin{'MINUS', 0, -80, prim}
  Pin{'PLUS', 0, 0, prim}
  Label{x=-10, y=-10, text='PLUS', primitive=prim}
  Label{x=-10, y=-80, text='MINUS', primitive=prim}
end
BuildPrimitive_psin()
function BuildPrimitive_res()
  local prim = Primitive{name='res', SVGPath='M0,-60 0,-48 -10,-45 10,-39 -10,-33 10,-27 -10,-21 10,-15 0,-12 0,0 ', instRect={{-20, -60}, {20, 0}}}
  Pin{'MINUS', 0, -60, prim}
  Pin{'PLUS', 0, 0, prim}
  Label{x=-10, y=-10, text='PLUS', primitive=prim}
  Label{x=-10, y=-60, text='MINUS', primitive=prim}
end
BuildPrimitive_res()
function BuildPrimitive_res3()
  local prim = Primitive{name='res3', SVGPath='M0,-60 0,-48 -10,-45 10,-39 -10,-33 10,-27 -10,-21 10,-15 0,-12 0,0 ', instRect={{-20, -60}, {20, 0}}}
  Pin{'MINUS', 0, -60, prim}
  Pin{'PLUS', 0, 0, prim}
  Pin{'B', 20, -30, prim}
  Label{x=-10, y=-10, text='PLUS', primitive=prim}
  Label{x=-10, y=-60, text='MINUS', primitive=prim}
  Label{x=24, y=-38, text='B', primitive=prim}
end
BuildPrimitive_res3()
function BuildPrimitive_res4()
  local prim = Primitive{name='res4', SVGPath='M0,-60 0,-48 -10,-45 10,-39 -10,-33 10,-27 -10,-21 10,-15 0,-12 0,0 ', instRect={{-20, -60}, {20, 0}}}
  Pin{'MINUS', 0, -60, prim}
  Pin{'PLUS', 0, 0, prim}
  Pin{'B', 20, -30, prim}
  Pin{'T', -20, -30, prim}
  Label{x=-10, y=-10, text='PLUS', primitive=prim}
  Label{x=-10, y=-60, text='MINUS', primitive=prim}
  Label{x=24, y=-38, text='B', primitive=prim}
  Label{x=-24, y=-38, text='T', primitive=prim}
end
BuildPrimitive_res4()
function BuildPrimitive_varactor()
  local prim = Primitive{name='varactor', SVGPath='M9.89949 -32.1005 G0,-42 14 0.7853981633974483 2.356194490192345M0,-60 0,-28 M-10,-22 10,-22 M0,-22 0,0 M-5,-38 5,-18 M5,-21 5,-18 2,-20 ', instRect={{-20, -60}, {20, 0}}}
  Pin{'MINUS', 0, -60, prim}
  Pin{'PLUS', 0, 0, prim}
  Label{x=-10, y=-10, text='PLUS', primitive=prim}
  Label{x=-10, y=-60, text='MINUS', primitive=prim}
end
BuildPrimitive_varactor()
function BuildPrimitive_varactor3()
  local prim = Primitive{name='varactor3', SVGPath='M9.89949 -32.1005 G0,-42 14 0.7853981633974483 2.356194490192345M0,-60 0,-28 M-10,-22 10,-22 M0,-22 0,0 M-5,-38 5,-18 M5,-21 5,-18 2,-20 ', instRect={{-20, -60}, {20, 0}}}
  Pin{'MINUS', 0, -60, prim}
  Pin{'PLUS', 0, 0, prim}
  Pin{'B', 20, -30, prim}
  Label{x=-10, y=-10, text='PLUS', primitive=prim}
  Label{x=-10, y=-60, text='MINUS', primitive=prim}
  Label{x=24, y=-38, text='B', primitive=prim}
end
BuildPrimitive_varactor3()
function BuildPrimitive_varactor4()
  local prim = Primitive{name='varactor4', SVGPath='M9.89949 -32.1005 G0,-42 14 0.7853981633974483 2.356194490192345M0,-60 0,-28 M-10,-22 10,-22 M0,-22 0,0 M-5,-38 5,-18 M5,-21 5,-18 2,-20 ', instRect={{-20, -60}, {20, 0}}}
  Pin{'MINUS', 0, -60, prim}
  Pin{'PLUS', 0, 0, prim}
  Pin{'B', 20, -30, prim}
  Pin{'T', -20, -30, prim}
  Label{x=-10, y=-10, text='PLUS', primitive=prim}
  Label{x=-10, y=-60, text='MINUS', primitive=prim}
  Label{x=24, y=-38, text='B', primitive=prim}
  Label{x=-24, y=-38, text='T', primitive=prim}
end
BuildPrimitive_varactor4()
function BuildPrimitive_vcc()
  local prim = Primitive{name='vcc', SVGPath='M0,0 0,40 M-25,40 25,40 ', instRect={{-25, 0}, {25, 50}}}
  Pin{'vcc!', 0, 0, prim}
end
BuildPrimitive_vcc()
function BuildPrimitive_vccs()
  local prim = Primitive{name='vccs', SVGPath='M0,-18 0,0 M0,-60 0,-42 M0,-18 12,-30 0,-42 -12,-30 0,-18 M0,-36 0,-24 M-3,-33 0,-36 3,-33 M-20,-20 -10,-20 -6,-24 M-20,-40 -10,-40 -6,-36 ', instRect={{-20, -60}, {20, 0}}}
  Pin{'MINUS', 0, -60, prim}
  Pin{'PLUS', 0, 0, prim}
  Pin{'NC+', -20, -20, prim}
  Pin{'NC-', -20, -40, prim}
  Label{x=-5, y=-59, text='MINUS', primitive=prim}
  Label{x=-5, y=-10, text='PLUS', primitive=prim}
end
BuildPrimitive_vccs()
function BuildPrimitive_vcvs()
  local prim = Primitive{name='vcvs', SVGPath='M0,-18 12,-30 0,-42 -12,-30 0,-18 M0,-28 0,-22 M-3,-25 3,-25 M0,-18 0,0 M0,-60 0,-42 M-3,-35 3,-35 M-20,-20 -10,-20 -6,-24 M-20,-40 -10,-40 -6,-36 ', instRect={{-20, -60}, {20, 0}}}
  Pin{'MINUS', 0, -60, prim}
  Pin{'PLUS', 0, 0, prim}
  Pin{'NC+', -20, -20, prim}
  Pin{'NC-', -20, -40, prim}
  Label{x=-5, y=-59, text='MINUS', primitive=prim}
  Label{x=-5, y=-10, text='PLUS', primitive=prim}
end
BuildPrimitive_vcvs()
function BuildPrimitive_vdc()
  local prim = Primitive{name='vdc', SVGPath='M10 -30 G0,-30 10 0.0 6.283185307179586M0,-28 0,-22 M-3,-25 3,-25 M0,-20 0,0 M0,-60 0,-40 M-3,-35 3,-35 ', instRect={{-20, -60}, {20, 0}}}
  Pin{'MINUS', 0, -60, prim}
  Pin{'PLUS', 0, 0, prim}
  Label{x=-10, y=-10, text='PLUS', primitive=prim}
  Label{x=-10, y=-60, text='MINUS', primitive=prim}
end
BuildPrimitive_vdc()
function BuildPrimitive_vdd()
  local prim = Primitive{name='vdd', SVGPath='M0,0 0,40 M-25,40 25,40 ', instRect={{-25, 0}, {25, 50}}}
  Pin{'vdd!', 0, 0, prim}
end
BuildPrimitive_vdd()
function BuildPrimitive_vee()
  local prim = Primitive{name='vee', SVGPath='M-25,-40 25,-40 M0,-40 0,0 ', instRect={{-25, -50}, {25, 0}}}
  Pin{'vee!', 0, 0, prim}
end
BuildPrimitive_vee()
function BuildPrimitive_vexp()
  local prim = Primitive{name='vexp', SVGPath='M-25 -23 G-25,-33 10 1.5707963267948966 3.141592653589793M-25 -22 G-15,-22 10 3.141592653589793 4.71238898038469M0,-28 0,-22 M-3,-25 3,-25 M0,-20 0,0 M0,-60 0,-40 M-3,-35 3,-35 M10 -30 G0,-30 10 0.0 6.283185307179586', instRect={{-20, -60}, {20, 0}}}
  Pin{'MINUS', 0, -60, prim}
  Pin{'PLUS', 0, 0, prim}
  Label{x=-10, y=-10, text='PLUS', primitive=prim}
  Label{x=-10, y=-60, text='MINUS', primitive=prim}
end
BuildPrimitive_vexp()
function BuildPrimitive_vpulse()
  local prim = Primitive{name='vpulse', SVGPath='M0,-28 0,-22 M-3,-35 3,-35 M0,-20 0,0 M0,-60 0,-40 M-3,-25 3,-25 M-31,-33 -28,-33 -28,-27 -24,-27 -24,-33 -20,-33 -20,-27 -16,-27 -16,-33 -13,-33 M10 -30 G0,-30 10 0.0 6.283185307179586', instRect={{-20, -60}, {20, 0}}}
  Pin{'MINUS', 0, -60, prim}
  Pin{'PLUS', 0, 0, prim}
  Label{x=-5, y=-10, text='PLUS', primitive=prim}
  Label{x=-5, y=-59, text='MINUS', primitive=prim}
end
BuildPrimitive_vpulse()
function BuildPrimitive_vpwl()
  local prim = Primitive{name='vpwl', SVGPath='M10 -30 G0,-30 10 0.0 6.283185307179586M0,-28 0,-22 M-3,-35 3,-35 M0,-20 0,0 M0,-60 0,-40 M-3,-25 3,-25 M-34,-34 -30,-34 -28,-26 -26,-38 -23,-38 -23,-30 -20,-30 -20,-26 -18,-26 -18,-34 -16,-34 -14,-28 ', instRect={{-20, -60}, {20, 0}}}
  Pin{'MINUS', 0, -60, prim}
  Pin{'PLUS', 0, 0, prim}
  Label{x=-5, y=-10, text='PLUS', primitive=prim}
  Label{x=-5, y=-59, text='MINUS', primitive=prim}
end
BuildPrimitive_vpwl()
function BuildPrimitive_vpwlf()
  local prim = Primitive{name='vpwlf', SVGPath='M10 -30 G0,-30 10 0.0 6.283185307179586M0,-28 0,-22 M3,-35 -3,-35 M0,-20 0,0 M0,-40 0,-60 M3,-25 -3,-25 M-34,-34 -30,-34 -28,-26 -26,-38 -23,-38 -23,-30 -20,-30 -20,-26 -18,-26 -18,-34 -16,-34 -14,-28 ', instRect={{-20, -60}, {20, 0}}}
  Pin{'MINUS', 0, -60, prim}
  Pin{'PLUS', 0, 0, prim}
  Label{x=-5, y=-59, text='MINUS', primitive=prim}
  Label{x=-5, y=-10, text='PLUS', primitive=prim}
end
BuildPrimitive_vpwlf()
function BuildPrimitive_vsin()
  local prim = Primitive{name='vsin', SVGPath='M-23 -29 G-26,-29 3 0.0 3.141592653589793M-22 -29 G-19,-29 3 3.141592653589793 0.0M10 -30 G0,-30 10 0.0 6.283185307179586M0,-28 0,-22 M-3,-35 3,-35 M0,-20 0,0 M0,-60 0,-40 M-3,-25 3,-25 ', instRect={{-20, -60}, {20, 0}}}
  Pin{'MINUS', 0, -60, prim}
  Pin{'PLUS', 0, 0, prim}
  Label{x=-5, y=-10, text='PLUS', primitive=prim}
  Label{x=-5, y=-59, text='MINUS', primitive=prim}
end
BuildPrimitive_vsin()
function BuildPrimitive_vsource()
  local prim = Primitive{name='vsource', SVGPath='M0,-28 0,-22 M-3,-25 3,-25 M0,-20 0,0 M0,-60 0,-40 M-3,-35 3,-35 M10 -30 G0,-30 10 0.0 6.283185307179586', instRect={{-20, -60}, {20, 0}}}
  Pin{'MINUS', 0, -60, prim}
  Pin{'PLUS', 0, 0, prim}
  Label{x=-10, y=-10, text='PLUS', primitive=prim}
  Label{x=-10, y=-60, text='MINUS', primitive=prim}
end
BuildPrimitive_vsource()
function BuildPrimitive_vss()
  local prim = Primitive{name='vss', SVGPath='M-25,-40 25,-40 M0,-40 0,0 ', instRect={{-25, -50}, {25, 0}}}
  Pin{'vss!', 0, 0, prim}
end
BuildPrimitive_vss()
function BuildPrimitive_xfmr4()
  local prim = Primitive{name='xfmr4', SVGPath='M-15 -38 G-15,-34 4 4.71238898038469 1.5707963267948966M-15 -30 G-15,-26 4 4.71238898038469 1.5707963267948966M-15 -46 G-15,-42 4 4.71238898038469 1.5707963267948966M-15 -22 G-15,-18 4 4.71238898038469 1.5707963267948966M15 -30 G15,-34 4 1.5707963267948966 4.71238898038469M15 -22 G15,-26 4 1.5707963267948966 4.71238898038469M15 -38 G15,-42 4 1.5707963267948966 4.71238898038469M15 -14 G15,-18 4 1.5707963267948966 4.71238898038469M-8 -8 G-10,-8 2 0.0 6.283185307179586M12 -8 G10,-8 2 0.0 6.283185307179586M-18,-14 -15,-14 M-18,-60 -18,-46 M-18,-22 -15,-22 M-18,-30 -15,-30 M-18,-46 -15,-46 M-18,-38 -15,-38 M-18,0 -18,-14 M18,-14 15,-14 M18,-60 18,-46 M18,-22 15,-22 M18,-30 15,-30 M18,-46 15,-46 M18,-38 15,-38 M18,0 18,-14 ', instRect={{-24, -60}, {24, 0}}}
  Pin{'P-', -18, -60, prim}
  Pin{'P+', -18, 0, prim}
  Pin{'S-', 18, -60, prim}
  Pin{'S+', 18, 0, prim}
  Label{x=-23, y=-2, text='P+', primitive=prim}
  Label{x=-23, y=-59, text='P-', primitive=prim}
  Label{x=23, y=-2, text='S+', primitive=prim}
  Label{x=23, y=-59, text='S-', primitive=prim}
end
BuildPrimitive_xfmr4()
function BuildPrimitive_xfmr5p()
  local prim = Primitive{name='xfmr5p', SVGPath='M-15 -38 G-15,-34 4 4.71238898038469 1.5707963267948966M-15 -30 G-15,-26 4 4.71238898038469 1.5707963267948966M-15 -46 G-15,-42 4 4.71238898038469 1.5707963267948966M-15 -22 G-15,-18 4 4.71238898038469 1.5707963267948966M15 -30 G15,-34 4 1.5707963267948966 4.71238898038469M15 -22 G15,-26 4 1.5707963267948966 4.71238898038469M15 -38 G15,-42 4 1.5707963267948966 4.71238898038469M15 -14 G15,-18 4 1.5707963267948966 4.71238898038469M-8 -8 G-10,-8 2 0.0 6.283185307179586M12 -8 G10,-8 2 0.0 6.283185307179586M-18,-14 -15,-14 M-18,-60 -18,-46 M-18,-22 -15,-22 M-18,-30 -15,-30 M-18,-46 -15,-46 M-18,-38 -15,-38 M-18,0 -18,-14 M18,-14 15,-14 M18,-60 18,-46 M18,-22 15,-22 M18,-30 15,-30 M18,-46 15,-46 M18,-38 15,-38 M18,0 18,-14 ', instRect={{-24, -60}, {24, 0}}}
  Pin{'P-', -18, -60, prim}
  Pin{'P+', -18, 0, prim}
  Pin{'S-', 18, -60, prim}
  Pin{'S+', 18, 0, prim}
  Pin{'PB', 0, 12, prim}
  Label{x=-23, y=-2, text='P+', primitive=prim}
  Label{x=5, y=12, text='PB', primitive=prim}
  Label{x=-23, y=-59, text='P-', primitive=prim}
  Label{x=23, y=-2, text='S+', primitive=prim}
  Label{x=23, y=-59, text='S-', primitive=prim}
end
BuildPrimitive_xfmr5p()
function BuildPrimitive_xfmr5s()
  local prim = Primitive{name='xfmr5s', SVGPath='M-15 -38 G-15,-34 4 4.71238898038469 1.5707963267948966M-15 -30 G-15,-26 4 4.71238898038469 1.5707963267948966M-15 -46 G-15,-42 4 4.71238898038469 1.5707963267948966M-15 -22 G-15,-18 4 4.71238898038469 1.5707963267948966M15 -30 G15,-34 4 1.5707963267948966 4.71238898038469M15 -22 G15,-26 4 1.5707963267948966 4.71238898038469M15 -38 G15,-42 4 1.5707963267948966 4.71238898038469M15 -14 G15,-18 4 1.5707963267948966 4.71238898038469M-8 -8 G-10,-8 2 0.0 6.283185307179586M12 -8 G10,-8 2 0.0 6.283185307179586M18,-14 15,-14 M18,-60 18,-46 M18,-22 15,-22 M18,-30 15,-30 M18,-46 15,-46 M18,-38 15,-38 M18,0 18,-14 M-18,-14 -15,-14 M-18,-60 -18,-46 M-18,-22 -15,-22 M-18,-30 -15,-30 M-18,-46 -15,-46 M-18,-38 -15,-38 M-18,0 -18,-14 ', instRect={{-24, -60}, {24, 0}}}
  Pin{'P-', -18, -60, prim}
  Pin{'P+', -18, 0, prim}
  Pin{'S-', 18, -60, prim}
  Pin{'S+', 18, 0, prim}
  Pin{'SB', 0, -72, prim}
  Label{x=-23, y=-2, text='P+', primitive=prim}
  Label{x=5, y=-72, text='SB', primitive=prim}
  Label{x=-23, y=-59, text='P-', primitive=prim}
  Label{x=23, y=-2, text='S+', primitive=prim}
  Label{x=23, y=-59, text='S-', primitive=prim}
end
BuildPrimitive_xfmr5s()
