//
//  main.m
//  CoreSpiceDesktop
//
//  Created by Reid van Melle on 11-06-24.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
