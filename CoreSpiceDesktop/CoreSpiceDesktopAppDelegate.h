//
//  CoreSpiceDesktopAppDelegate.h
//  CoreSpiceDesktop
//
//  Created by Reid van Melle on 11-06-24.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface CoreSpiceDesktopAppDelegate : NSObject <NSApplicationDelegate, NSToolbarDelegate> {
@private
    NSWindow *window;
    NSSearchField *searchFieldOutlet;
    NSPersistentStoreCoordinator *__persistentStoreCoordinator;
    NSManagedObjectModel *__managedObjectModel;
    NSManagedObjectContext *__managedObjectContext;
}

@property (assign) IBOutlet NSWindow *window;
@property (assign) IBOutlet NSSearchField *searchFieldOutlet;

@property (nonatomic, retain, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (nonatomic, retain, readonly) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, retain, readonly) NSManagedObjectContext *managedObjectContext;

- (IBAction)saveAction:sender;

@end
