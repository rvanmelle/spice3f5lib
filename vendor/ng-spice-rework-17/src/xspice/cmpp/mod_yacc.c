/* A Bison parser, made by GNU Bison 1.875d.  */

/* Skeleton parser for Yacc-like parsing with Bison,
   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

/* As a special exception, when this file is copied by Bison into a
   Bison output file, you may use that output file without restriction.
   This special exception was added by the Free Software Foundation
   in version 1.24 of Bison.  */

/* Written by Richard Stallman by simplifying the original so called
   ``semantic'' parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Using locations.  */
#define YYLSP_NEEDED 0



/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     TOK_ARGS = 258,
     TOK_INIT = 259,
     TOK_ANALYSIS = 260,
     TOK_NEW_TIMEPOINT = 261,
     TOK_TIME = 262,
     TOK_RAD_FREQ = 263,
     TOK_TEMPERATURE = 264,
     TOK_T = 265,
     TOK_PARAM = 266,
     TOK_PARAM_SIZE = 267,
     TOK_PARAM_NULL = 268,
     TOK_PORT_SIZE = 269,
     TOK_PORT_NULL = 270,
     TOK_PARTIAL = 271,
     TOK_AC_GAIN = 272,
     TOK_CHANGED = 273,
     TOK_OUTPUT_DELAY = 274,
     TOK_STATIC_VAR = 275,
     TOK_STATIC_VAR_SIZE = 276,
     TOK_INPUT = 277,
     TOK_INPUT_STRENGTH = 278,
     TOK_INPUT_STATE = 279,
     TOK_INPUT_TYPE = 280,
     TOK_OUTPUT = 281,
     TOK_OUTPUT_CHANGED = 282,
     TOK_OUTPUT_STRENGTH = 283,
     TOK_OUTPUT_STATE = 284,
     TOK_OUTPUT_TYPE = 285,
     TOK_COMMA = 286,
     TOK_LPAREN = 287,
     TOK_RPAREN = 288,
     TOK_LBRACKET = 289,
     TOK_RBRACKET = 290,
     TOK_MISC_C = 291,
     TOK_IDENTIFIER = 292,
     TOK_LOAD = 293,
     TOK_TOTAL_LOAD = 294,
     TOK_MESSAGE = 295,
     TOK_CALL_TYPE = 296
   };
#endif
#define TOK_ARGS 258
#define TOK_INIT 259
#define TOK_ANALYSIS 260
#define TOK_NEW_TIMEPOINT 261
#define TOK_TIME 262
#define TOK_RAD_FREQ 263
#define TOK_TEMPERATURE 264
#define TOK_T 265
#define TOK_PARAM 266
#define TOK_PARAM_SIZE 267
#define TOK_PARAM_NULL 268
#define TOK_PORT_SIZE 269
#define TOK_PORT_NULL 270
#define TOK_PARTIAL 271
#define TOK_AC_GAIN 272
#define TOK_CHANGED 273
#define TOK_OUTPUT_DELAY 274
#define TOK_STATIC_VAR 275
#define TOK_STATIC_VAR_SIZE 276
#define TOK_INPUT 277
#define TOK_INPUT_STRENGTH 278
#define TOK_INPUT_STATE 279
#define TOK_INPUT_TYPE 280
#define TOK_OUTPUT 281
#define TOK_OUTPUT_CHANGED 282
#define TOK_OUTPUT_STRENGTH 283
#define TOK_OUTPUT_STATE 284
#define TOK_OUTPUT_TYPE 285
#define TOK_COMMA 286
#define TOK_LPAREN 287
#define TOK_RPAREN 288
#define TOK_LBRACKET 289
#define TOK_RBRACKET 290
#define TOK_MISC_C 291
#define TOK_IDENTIFIER 292
#define TOK_LOAD 293
#define TOK_TOTAL_LOAD 294
#define TOK_MESSAGE 295
#define TOK_CALL_TYPE 296




/* Copy the first part of user declarations.  */
#line 1 "mod_yacc.y"
 /* $Id: mod_yacc.y,v 1.7 2005/05/10 21:50:43 sjborley Exp $ */

/*============================================================================
FILE  mod_yacc.y

MEMBER OF process cmpp

Copyright 1991
Georgia Tech Research Corporation
Atlanta, Georgia 30332
All Rights Reserved

PROJECT A-8503

AUTHORS

    9/12/91  Steve Tynor

MODIFICATIONS

    <date> <person name> <nature of modifications>
  20050420 Steven Borley Renamed strcmpi() to local_strcmpi() to avoid
                         clash with strcmpi() in a windows header file.

SUMMARY

    This file contains a BNF specification of the translation of
    cfunc.mod files to cfunc.c files, together with various support
    functions.

INTERFACES

    mod_yyparse() -    Function 'yyparse()' is generated automatically
                       by UNIX 'yacc' utility and then converted to
                       'mod_yyparse()' by UNIX 'sed' utility under
                       direction of Makefile.

REFERENCED FILES

    mod_lex.l

NON-STANDARD FEATURES

    Names of functions generated by 'yacc' are translated by 'sed'
    under direction of the Makefile to prevent collisions with
    functions generated from ifs_yacc.y.

============================================================================*/


#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include "mod_yacc_y.h"

#define	yymaxdepth mod_yymaxdepth
#define	yyparse	mod_yyparse
#define	yylex	mod_yylex
#define	yyerror	mod_yyerror
#define	yylval	mod_yylval
#define	yychar	mod_yychar
#define	yydebug	mod_yydebug
#define	yypact	mod_yypact
#define	yyr1	mod_yyr1
#define	yyr2	mod_yyr2
#define	yydef	mod_yydef
#define	yychk	mod_yychk
#define	yypgo	mod_yypgo
#define	yyact	mod_yyact
#define	yyexca	mod_yyexca
#define yyerrflag mod_yyerrflag
#define yynerrs	mod_yynerrs
#define	yyps	mod_yyps
#define	yypv	mod_yypv
#define	yys	mod_yys
#define	yy_yys	mod_yyyys
#define	yystate	mod_yystate
#define	yytmp	mod_yytmp
#define	yyv	mod_yyv
#define	yy_yyv	mod_yyyyv
#define	yyval	mod_yyval
#define	yylloc	mod_yylloc
#define yyreds	mod_yyreds
#define yytoks	mod_yytoks
#define yylhs	mod_yyyylhs
#define yylen	mod_yyyylen
#define yydefred mod_yyyydefred
#define yydgoto	mod_yyyydgoto
#define yysindex mod_yyyysindex
#define yyrindex mod_yyyyrindex
#define yygindex mod_yyyygindex
#define yytable	 mod_yyyytable
#define yycheck	 mod_yyyycheck
#define yyname   mod_yyyyname
#define yyrule   mod_yyyyrule

Ifs_Table_t *mod_ifs_table;

extern char *mod_yytext;
extern FILE* mod_yyout;
 extern void mod_yyerror(char*);

#include <string.h>
#include <ctype.h>

int mod_num_errors;

#define BUFFER_SIZE 3000
static char buffer [BUFFER_SIZE];
static int buf_len;
   
typedef enum {CONN, PARAM, STATIC_VAR} Id_Kind_t;

/*--------------------------------------------------------------------------*/
static char *subscript (Sub_Id_t sub_id)
{
   if (sub_id.has_subscript) {
      return sub_id.subscript;
   } else {
      return "0";
   }
}

/*--------------------------------------------------------------------------*/
int local_strcmpi(s, t)
     char *s;
     char *t;
     /* string compare -  case insensitive */
{
   for (; *s && t && tolower(*s) == tolower(*t); s++, t++);
   if (*s && !*t) {
      return 1;
   }
   if (!*s && *t) {
      return -1;
   }
   if (! (*s || *t)) {
      return 0;
   }
   return (tolower(*s) - tolower(*t));
}

/*---------------------------------------------------------------------------*/
static void put_type (FILE *fp, Data_Type_t type)
{
   char ch = ' ';
   
   switch (type) {
   case INTEGER:
      ch = 'i';
      break;
   case REAL:
      ch = 'r';
      break;
   case COMPLEX:
      ch = 'c';
      break;
   case BOOLEAN:
      ch = 'b';
      break;
   case STRING:
      ch = 's';
      break;
   case POINTER:
      ch = 'p';
      break;
   }
   fprintf (fp, ".%cvalue", ch);
}

/*---------------------------------------------------------------------------*/
static void put_conn_type (FILE *fp, Port_Type_t type)
{
   char ch;
   
   switch (type) {
   case USER_DEFINED:
      ch = 'p';
      break;
   case DIGITAL:
      ch = 'p';
      break;
   default:
      ch = 'r';
      break;
   }
   fprintf (fp, ".%cvalue", ch);
}

/*---------------------------------------------------------------------------*/
static void check_dir (int conn_number, Dir_t dir, char *context)
{
   Dir_t conn_dir;
   
   if (conn_number >= 0) {
      /*
       * If negative, this is an invalid port ID and we've already issued
       * an error.
       */
      conn_dir = mod_ifs_table->conn[conn_number].direction;
      if ((conn_dir != dir) && (conn_dir != INOUT)) {
	 char error_str[200];
	 
	 sprintf (error_str,
		  "Direction of port `%s' in %s() is not %s or INOUT",
		  mod_ifs_table->conn[conn_number].name, context,
		  (dir == IN) ? "IN" : "OUT");
	 yyerror (error_str);
	 mod_num_errors++;
      }
   }
}

/*---------------------------------------------------------------------------*/
static void check_subscript (Boolean_t formal, Boolean_t actual,
			     Boolean_t missing_actual_ok,
			     char *context, char *id)
{
   char error_str[200];

   if ((formal && !actual) && !missing_actual_ok) {
      sprintf (error_str,
	       "%s `%s' is an array - subscript required",
	       context, id);
      yyerror (error_str);
      mod_num_errors++;
      return;
   } else if (!formal && actual) {
      sprintf (error_str,
	       "%s `%s' is not an array - subscript prohibited",
	       context, id);
      yyerror (error_str);
      mod_num_errors++;
      return;
   }
}

/*---------------------------------------------------------------------------*/
static int check_id (Sub_Id_t sub_id, Id_Kind_t kind, Boolean_t do_subscript)
{
   int i;
   char error_str[200];
   
   switch (kind) {
   case CONN:
      for (i = 0; i < mod_ifs_table->num_conn; i++) {
	 if (0 == local_strcmpi (sub_id.id, mod_ifs_table->conn[i].name)) {
	    if (do_subscript) {
	       check_subscript (mod_ifs_table->conn[i].is_array,
				sub_id.has_subscript, FALSE, "Port",
				sub_id.id);
	    }
	    return i;
	 }
      }
      break;
   case PARAM:
      for (i = 0; i < mod_ifs_table->num_param; i++) {
      	 if (0 == local_strcmpi (sub_id.id, mod_ifs_table->param[i].name)) {
	    if (do_subscript) {
	       check_subscript (mod_ifs_table->param[i].is_array,
				sub_id.has_subscript, FALSE, "Parameter",
				sub_id.id);
	    }
	    return i;
	 }
      }
      break;
   case STATIC_VAR:
      for (i = 0; i < mod_ifs_table->num_inst_var; i++) {
      	 if (0 == local_strcmpi (sub_id.id, mod_ifs_table->inst_var[i].name)) {
	    if (do_subscript) {
	       check_subscript (mod_ifs_table->inst_var[i].is_array,
				sub_id.has_subscript, TRUE,
				"Static Variable",
				sub_id.id);
	    }
	    return i;
	 }
      }
      break;
   }
   
   sprintf (error_str, "No %s named '%s'",
	    ((kind==CONN)
	     ? "port"
	     : ((kind==PARAM)
		? "parameter"
		:"static variable")),
	    sub_id.id);
   yyerror (error_str);
   mod_num_errors++;
   return -1;
}

/*---------------------------------------------------------------------------*/
static int valid_id (Sub_Id_t sub_id, Id_Kind_t kind)
{
    return check_id (sub_id, kind, FALSE);
}

/*---------------------------------------------------------------------------*/
static int valid_subid (Sub_Id_t sub_id, Id_Kind_t kind)
{
    return check_id (sub_id, kind, TRUE);
}

/*---------------------------------------------------------------------------*/
static void init_buffer ()
{
   buf_len = 0;
   buffer[0] = '\0';
}

/*---------------------------------------------------------------------------*/
static void append (char *str)
{
   int len = strlen (str);
   if (len + buf_len > BUFFER_SIZE) {
      yyerror ("Buffer overflow - try reducing the complexity of CM-macro array subscripts");
      exit (1);
   }
   (void)strcat (buffer,str);
}



/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

#if ! defined (YYSTYPE) && ! defined (YYSTYPE_IS_DECLARED)
#line 328 "mod_yacc.y"
typedef union YYSTYPE {
   char *str;
   Sub_Id_t sub_id;
} YYSTYPE;
/* Line 191 of yacc.c.  */
#line 490 "mod_yacc.c"
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif



/* Copy the second part of user declarations.  */


/* Line 214 of yacc.c.  */
#line 502 "mod_yacc.c"

#if ! defined (yyoverflow) || YYERROR_VERBOSE

# ifndef YYFREE
#  define YYFREE free
# endif
# ifndef YYMALLOC
#  define YYMALLOC malloc
# endif

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   define YYSTACK_ALLOC alloca
#  endif
# else
#  if defined (alloca) || defined (_ALLOCA_H)
#   define YYSTACK_ALLOC alloca
#  else
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning. */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
# else
#  if defined (__STDC__) || defined (__cplusplus)
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   define YYSIZE_T size_t
#  endif
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
# endif
#endif /* ! defined (yyoverflow) || YYERROR_VERBOSE */


#if (! defined (yyoverflow) \
     && (! defined (__cplusplus) \
	 || (defined (YYSTYPE_IS_TRIVIAL) && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  short int yyss;
  YYSTYPE yyvs;
  };

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (short int) + sizeof (YYSTYPE))			\
      + YYSTACK_GAP_MAXIMUM)

/* Copy COUNT objects from FROM to TO.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined (__GNUC__) && 1 < __GNUC__
#   define YYCOPY(To, From, Count) \
      __builtin_memcpy (To, From, (Count) * sizeof (*(From)))
#  else
#   define YYCOPY(To, From, Count)		\
      do					\
	{					\
	  register YYSIZE_T yyi;		\
	  for (yyi = 0; yyi < (Count); yyi++)	\
	    (To)[yyi] = (From)[yyi];		\
	}					\
      while (0)
#  endif
# endif

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack)					\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack, Stack, yysize);				\
	Stack = &yyptr->Stack;						\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (0)

#endif

#if defined (__STDC__) || defined (__cplusplus)
   typedef signed char yysigned_char;
#else
   typedef short int yysigned_char;
#endif

/* YYFINAL -- State number of the termination state. */
#define YYFINAL  2
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   214

/* YYNTOKENS -- Number of terminals. */
#define YYNTOKENS  42
/* YYNNTS -- Number of nonterminals. */
#define YYNNTS  15
/* YYNRULES -- Number of rules. */
#define YYNRULES  59
/* YYNRULES -- Number of states. */
#define YYNSTATES  144

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   296

#define YYTRANSLATE(YYX) 						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const unsigned char yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const unsigned char yyprhs[] =
{
       0,     0,     3,     4,     7,     8,    11,    14,    15,    18,
      19,    22,    24,    26,    28,    29,    34,    35,    40,    42,
      44,    46,    47,    52,    53,    58,    60,    62,    64,    66,
      68,    70,    72,    74,    79,    84,    89,    94,    99,   104,
     111,   118,   123,   128,   133,   138,   143,   148,   153,   158,
     163,   168,   173,   178,   183,   188,   193,   198,   200,   205
};

/* YYRHS -- A `-1'-separated list of the rules' RHS. */
static const yysigned_char yyrhs[] =
{
      43,     0,    -1,    -1,    43,    44,    -1,    -1,    44,    51,
      -1,    44,    54,    -1,    -1,    46,    47,    -1,    -1,    47,
      48,    -1,    37,    -1,    36,    -1,    31,    -1,    -1,    34,
      49,    47,    35,    -1,    -1,    32,    50,    47,    33,    -1,
      37,    -1,    36,    -1,    31,    -1,    -1,    34,    52,    44,
      35,    -1,    -1,    32,    53,    44,    33,    -1,     4,    -1,
       3,    -1,     5,    -1,     6,    -1,    41,    -1,     7,    -1,
       8,    -1,     9,    -1,    10,    32,    45,    33,    -1,    11,
      32,    55,    33,    -1,    12,    32,    56,    33,    -1,    13,
      32,    56,    33,    -1,    14,    32,    56,    33,    -1,    15,
      32,    56,    33,    -1,    16,    32,    55,    31,    55,    33,
      -1,    17,    32,    55,    31,    55,    33,    -1,    20,    32,
      55,    33,    -1,    21,    32,    56,    33,    -1,    19,    32,
      55,    33,    -1,    18,    32,    55,    33,    -1,    22,    32,
      55,    33,    -1,    25,    32,    55,    33,    -1,    30,    32,
      55,    33,    -1,    23,    32,    55,    33,    -1,    24,    32,
      55,    33,    -1,    26,    32,    55,    33,    -1,    28,    32,
      55,    33,    -1,    29,    32,    55,    33,    -1,    27,    32,
      55,    33,    -1,    38,    32,    55,    33,    -1,    39,    32,
      55,    33,    -1,    40,    32,    55,    33,    -1,    56,    -1,
      56,    34,    45,    35,    -1,    37,    -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const unsigned short int yyrline[] =
{
       0,   380,   380,   381,   384,   385,   386,   391,   391,   395,
     396,   399,   400,   401,   403,   402,   407,   406,   412,   413,
     414,   416,   415,   420,   419,   425,   427,   429,   431,   433,
     435,   437,   439,   441,   443,   449,   452,   455,   458,   461,
     469,   478,   490,   494,   500,   506,   514,   520,   526,   535,
     544,   552,   561,   570,   575,   580,   585,   592,   593,   599
};
#endif

#if YYDEBUG || YYERROR_VERBOSE
/* YYTNME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals. */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "TOK_ARGS", "TOK_INIT", "TOK_ANALYSIS",
  "TOK_NEW_TIMEPOINT", "TOK_TIME", "TOK_RAD_FREQ", "TOK_TEMPERATURE",
  "TOK_T", "TOK_PARAM", "TOK_PARAM_SIZE", "TOK_PARAM_NULL",
  "TOK_PORT_SIZE", "TOK_PORT_NULL", "TOK_PARTIAL", "TOK_AC_GAIN",
  "TOK_CHANGED", "TOK_OUTPUT_DELAY", "TOK_STATIC_VAR",
  "TOK_STATIC_VAR_SIZE", "TOK_INPUT", "TOK_INPUT_STRENGTH",
  "TOK_INPUT_STATE", "TOK_INPUT_TYPE", "TOK_OUTPUT", "TOK_OUTPUT_CHANGED",
  "TOK_OUTPUT_STRENGTH", "TOK_OUTPUT_STATE", "TOK_OUTPUT_TYPE",
  "TOK_COMMA", "TOK_LPAREN", "TOK_RPAREN", "TOK_LBRACKET", "TOK_RBRACKET",
  "TOK_MISC_C", "TOK_IDENTIFIER", "TOK_LOAD", "TOK_TOTAL_LOAD",
  "TOK_MESSAGE", "TOK_CALL_TYPE", "$accept", "mod_file", "c_code",
  "buffered_c_code", "@1", "buffered_c_code2", "buffered_c_char", "@2",
  "@3", "c_char", "@4", "@5", "macro", "subscriptable_id", "id", 0
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const unsigned short int yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296
};
# endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const unsigned char yyr1[] =
{
       0,    42,    43,    43,    44,    44,    44,    46,    45,    47,
      47,    48,    48,    48,    49,    48,    50,    48,    51,    51,
      51,    52,    51,    53,    51,    54,    54,    54,    54,    54,
      54,    54,    54,    54,    54,    54,    54,    54,    54,    54,
      54,    54,    54,    54,    54,    54,    54,    54,    54,    54,
      54,    54,    54,    54,    54,    54,    54,    55,    55,    56
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const unsigned char yyr2[] =
{
       0,     2,     0,     2,     0,     2,     2,     0,     2,     0,
       2,     1,     1,     1,     0,     4,     0,     4,     1,     1,
       1,     0,     4,     0,     4,     1,     1,     1,     1,     1,
       1,     1,     1,     4,     4,     4,     4,     4,     4,     6,
       6,     4,     4,     4,     4,     4,     4,     4,     4,     4,
       4,     4,     4,     4,     4,     4,     4,     1,     4,     1
};

/* YYDEFACT[STATE-NAME] -- Default rule to reduce with in state
   STATE-NUM when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const unsigned char yydefact[] =
{
       2,     4,     1,     3,    26,    25,    27,    28,    30,    31,
      32,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    20,    23,    21,    19,    18,     0,     0,     0,
      29,     5,     6,     7,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     4,     4,     0,     0,     0,     0,
       9,    59,     0,    57,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    33,     8,
      34,     7,    35,    36,    37,    38,     0,     0,    44,    43,
      41,    42,    45,    48,    49,    46,    50,    53,    51,    52,
      47,    24,    22,    54,    55,    56,    13,    16,    14,    12,
      11,    10,     0,     0,     0,     9,     9,    58,    39,    40,
       0,     0,    17,    15
};

/* YYDEFGOTO[NTERM-NUM]. */
static const short int yydefgoto[] =
{
      -1,     1,     3,    69,    70,    99,   131,   136,   135,    41,
      65,    64,    42,    72,    73
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -121
static const short int yypact[] =
{
    -121,     5,  -121,   134,  -121,  -121,  -121,  -121,  -121,  -121,
    -121,    -8,     2,     9,    11,    16,    17,    18,    19,    20,
      21,    22,    23,    24,    59,    96,   135,   137,   144,   145,
     146,   147,  -121,  -121,  -121,  -121,  -121,   148,   149,   150,
    -121,  -121,  -121,  -121,   151,   151,   151,   151,   151,   151,
     151,   151,   151,   151,   151,   151,   151,   151,   151,   151,
     151,   151,   151,   151,  -121,  -121,   151,   151,   151,   152,
    -121,  -121,   153,   155,   154,   157,   158,   159,   162,   163,
     164,   165,   166,   167,   168,   169,   170,   171,   172,   173,
     174,   175,   176,    56,    95,   177,   178,   179,  -121,     8,
    -121,  -121,  -121,  -121,  -121,  -121,   151,   151,  -121,  -121,
    -121,  -121,  -121,  -121,  -121,  -121,  -121,  -121,  -121,  -121,
    -121,  -121,  -121,  -121,  -121,  -121,  -121,  -121,  -121,  -121,
    -121,  -121,   160,   180,   181,  -121,  -121,  -121,  -121,  -121,
      -6,     1,  -121,  -121
};

/* YYPGOTO[NTERM-NUM].  */
static const yysigned_char yypgoto[] =
{
    -121,  -121,   -18,    82,  -121,  -120,  -121,  -121,  -121,  -121,
    -121,  -121,  -121,   -49,   -25
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If zero, do what YYDEFACT says.
   If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -1
static const unsigned char yytable[] =
{
      78,    79,    80,    81,    82,     2,    84,    85,    86,    87,
      88,    89,    90,    91,    92,   140,   141,    95,    96,    97,
      74,    75,    76,    77,    43,   126,   127,   142,   128,    83,
     129,   130,   126,   127,    44,   128,   143,   129,   130,   126,
     127,    45,   128,    46,   129,   130,    93,    94,    47,    48,
      49,    50,    51,    52,    53,    54,    55,   133,   134,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,   121,
      34,    56,    35,    36,    37,    38,    39,    40,     4,     5,
       6,     7,     8,     9,    10,    11,    12,    13,    14,    15,
      16,    17,    18,    19,    20,    21,    22,    23,    24,    25,
      26,    27,    28,    29,    30,    31,    32,    33,    57,    34,
     122,    35,    36,    37,    38,    39,    40,     4,     5,     6,
       7,     8,     9,    10,    11,    12,    13,    14,    15,    16,
      17,    18,    19,    20,    21,    22,    23,    24,    25,    26,
      27,    28,    29,    30,    31,    32,    33,    58,    34,    59,
      35,    36,    37,    38,    39,    40,    60,    61,    62,    63,
      66,    67,    68,   132,     0,    98,   100,   102,    71,   101,
     103,   104,   105,   106,   107,   137,     0,   108,   109,   110,
     111,   112,   113,   114,   115,   116,   117,   118,   119,   120,
     123,   124,   125,   138,   139
};

static const short int yycheck[] =
{
      49,    50,    51,    52,    53,     0,    55,    56,    57,    58,
      59,    60,    61,    62,    63,   135,   136,    66,    67,    68,
      45,    46,    47,    48,    32,    31,    32,    33,    34,    54,
      36,    37,    31,    32,    32,    34,    35,    36,    37,    31,
      32,    32,    34,    32,    36,    37,    64,    65,    32,    32,
      32,    32,    32,    32,    32,    32,    32,   106,   107,     3,
       4,     5,     6,     7,     8,     9,    10,    11,    12,    13,
      14,    15,    16,    17,    18,    19,    20,    21,    22,    23,
      24,    25,    26,    27,    28,    29,    30,    31,    32,    33,
      34,    32,    36,    37,    38,    39,    40,    41,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    32,    34,
      35,    36,    37,    38,    39,    40,    41,     3,     4,     5,
       6,     7,     8,     9,    10,    11,    12,    13,    14,    15,
      16,    17,    18,    19,    20,    21,    22,    23,    24,    25,
      26,    27,    28,    29,    30,    31,    32,    32,    34,    32,
      36,    37,    38,    39,    40,    41,    32,    32,    32,    32,
      32,    32,    32,   101,    -1,    33,    33,    33,    37,    34,
      33,    33,    33,    31,    31,    35,    -1,    33,    33,    33,
      33,    33,    33,    33,    33,    33,    33,    33,    33,    33,
      33,    33,    33,    33,    33
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const unsigned char yystos[] =
{
       0,    43,     0,    44,     3,     4,     5,     6,     7,     8,
       9,    10,    11,    12,    13,    14,    15,    16,    17,    18,
      19,    20,    21,    22,    23,    24,    25,    26,    27,    28,
      29,    30,    31,    32,    34,    36,    37,    38,    39,    40,
      41,    51,    54,    32,    32,    32,    32,    32,    32,    32,
      32,    32,    32,    32,    32,    32,    32,    32,    32,    32,
      32,    32,    32,    32,    53,    52,    32,    32,    32,    45,
      46,    37,    55,    56,    56,    56,    56,    56,    55,    55,
      55,    55,    55,    56,    55,    55,    55,    55,    55,    55,
      55,    55,    55,    44,    44,    55,    55,    55,    33,    47,
      33,    34,    33,    33,    33,    33,    31,    31,    33,    33,
      33,    33,    33,    33,    33,    33,    33,    33,    33,    33,
      33,    33,    35,    33,    33,    33,    31,    32,    34,    36,
      37,    48,    45,    55,    55,    50,    49,    35,    33,    33,
      47,    47,    33,    35
};

#if ! defined (YYSIZE_T) && defined (__SIZE_TYPE__)
# define YYSIZE_T __SIZE_TYPE__
#endif
#if ! defined (YYSIZE_T) && defined (size_t)
# define YYSIZE_T size_t
#endif
#if ! defined (YYSIZE_T)
# if defined (__STDC__) || defined (__cplusplus)
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# endif
#endif
#if ! defined (YYSIZE_T)
# define YYSIZE_T unsigned int
#endif

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		(-2)
#define YYEOF		0

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrorlab


/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */

#define YYFAIL		goto yyerrlab

#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)					\
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    {								\
      yychar = (Token);						\
      yylval = (Value);						\
      yytoken = YYTRANSLATE (yychar);				\
      YYPOPSTACK;						\
      goto yybackup;						\
    }								\
  else								\
    { 								\
      yyerror ("syntax error: cannot back up");\
      YYERROR;							\
    }								\
while (0)

#define YYTERROR	1
#define YYERRCODE	256

/* YYLLOC_DEFAULT -- Compute the default location (before the actions
   are run).  */

#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)		\
   ((Current).first_line   = (Rhs)[1].first_line,	\
    (Current).first_column = (Rhs)[1].first_column,	\
    (Current).last_line    = (Rhs)[N].last_line,	\
    (Current).last_column  = (Rhs)[N].last_column)
#endif

/* YYLEX -- calling `yylex' with the right arguments.  */

#ifdef YYLEX_PARAM
# define YYLEX yylex (YYLEX_PARAM)
#else
# define YYLEX yylex ()
#endif

/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (0)

# define YYDSYMPRINT(Args)			\
do {						\
  if (yydebug)					\
    yysymprint Args;				\
} while (0)

# define YYDSYMPRINTF(Title, Token, Value, Location)		\
do {								\
  if (yydebug)							\
    {								\
      YYFPRINTF (stderr, "%s ", Title);				\
      yysymprint (stderr, 					\
                  Token, Value);	\
      YYFPRINTF (stderr, "\n");					\
    }								\
} while (0)

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

#if defined (__STDC__) || defined (__cplusplus)
static void
yy_stack_print (short int *bottom, short int *top)
#else
static void
yy_stack_print (bottom, top)
    short int *bottom;
    short int *top;
#endif
{
  YYFPRINTF (stderr, "Stack now");
  for (/* Nothing. */; bottom <= top; ++bottom)
    YYFPRINTF (stderr, " %d", *bottom);
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)				\
do {								\
  if (yydebug)							\
    yy_stack_print ((Bottom), (Top));				\
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

#if defined (__STDC__) || defined (__cplusplus)
static void
yy_reduce_print (int yyrule)
#else
static void
yy_reduce_print (yyrule)
    int yyrule;
#endif
{
  int yyi;
  unsigned int yylno = yyrline[yyrule];
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %u), ",
             yyrule - 1, yylno);
  /* Print the symbols being reduced, and their result.  */
  for (yyi = yyprhs[yyrule]; 0 <= yyrhs[yyi]; yyi++)
    YYFPRINTF (stderr, "%s ", yytname [yyrhs[yyi]]);
  YYFPRINTF (stderr, "-> %s\n", yytname [yyr1[yyrule]]);
}

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug)				\
    yy_reduce_print (Rule);		\
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YYDSYMPRINT(Args)
# define YYDSYMPRINTF(Title, Token, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   SIZE_MAX < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#if defined (YYMAXDEPTH) && YYMAXDEPTH == 0
# undef YYMAXDEPTH
#endif

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif



#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined (__GLIBC__) && defined (_STRING_H)
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
#   if defined (__STDC__) || defined (__cplusplus)
yystrlen (const char *yystr)
#   else
yystrlen (yystr)
     const char *yystr;
#   endif
{
  register const char *yys = yystr;

  while (*yys++ != '\0')
    continue;

  return yys - yystr - 1;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined (__GLIBC__) && defined (_STRING_H) && defined (_GNU_SOURCE)
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
#   if defined (__STDC__) || defined (__cplusplus)
yystpcpy (char *yydest, const char *yysrc)
#   else
yystpcpy (yydest, yysrc)
     char *yydest;
     const char *yysrc;
#   endif
{
  register char *yyd = yydest;
  register const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

#endif /* !YYERROR_VERBOSE */



#if YYDEBUG
/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

#if defined (__STDC__) || defined (__cplusplus)
static void
yysymprint (FILE *yyoutput, int yytype, YYSTYPE *yyvaluep)
#else
static void
yysymprint (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE *yyvaluep;
#endif
{
  /* Pacify ``unused variable'' warnings.  */
  (void) yyvaluep;

  if (yytype < YYNTOKENS)
    {
      YYFPRINTF (yyoutput, "token %s (", yytname[yytype]);
# ifdef YYPRINT
      YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# endif
    }
  else
    YYFPRINTF (yyoutput, "nterm %s (", yytname[yytype]);

  switch (yytype)
    {
      default:
        break;
    }
  YYFPRINTF (yyoutput, ")");
}

#endif /* ! YYDEBUG */
/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

#if defined (__STDC__) || defined (__cplusplus)
static void
yydestruct (int yytype, YYSTYPE *yyvaluep)
#else
static void
yydestruct (yytype, yyvaluep)
    int yytype;
    YYSTYPE *yyvaluep;
#endif
{
  /* Pacify ``unused variable'' warnings.  */
  (void) yyvaluep;

  switch (yytype)
    {

      default:
        break;
    }
}


/* Prevent warnings from -Wmissing-prototypes.  */

#ifdef YYPARSE_PARAM
# if defined (__STDC__) || defined (__cplusplus)
int yyparse (void *YYPARSE_PARAM);
# else
int yyparse ();
# endif
#else /* ! YYPARSE_PARAM */
#if defined (__STDC__) || defined (__cplusplus)
int yyparse (void);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */



/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;

/* Number of syntax errors so far.  */
int yynerrs;



/*----------.
| yyparse.  |
`----------*/

#ifdef YYPARSE_PARAM
# if defined (__STDC__) || defined (__cplusplus)
int yyparse (void *YYPARSE_PARAM)
# else
int yyparse (YYPARSE_PARAM)
  void *YYPARSE_PARAM;
# endif
#else /* ! YYPARSE_PARAM */
#if defined (__STDC__) || defined (__cplusplus)
int
yyparse (void)
#else
int
yyparse ()

#endif
#endif
{
  
  register int yystate;
  register int yyn;
  int yyresult;
  /* Number of tokens to shift before error messages enabled.  */
  int yyerrstatus;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;

  /* Three stacks and their tools:
     `yyss': related to states,
     `yyvs': related to semantic values,
     `yyls': related to locations.

     Refer to the stacks thru separate pointers, to allow yyoverflow
     to reallocate them elsewhere.  */

  /* The state stack.  */
  short int yyssa[YYINITDEPTH];
  short int *yyss = yyssa;
  register short int *yyssp;

  /* The semantic value stack.  */
  YYSTYPE yyvsa[YYINITDEPTH];
  YYSTYPE *yyvs = yyvsa;
  register YYSTYPE *yyvsp;



#define YYPOPSTACK   (yyvsp--, yyssp--)

  YYSIZE_T yystacksize = YYINITDEPTH;

  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;


  /* When reducing, the number of symbols on the RHS of the reduced
     rule.  */
  int yylen;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY;		/* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */

  yyssp = yyss;
  yyvsp = yyvs;


  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed. so pushing a state here evens the stacks.
     */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack. Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	short int *yyss1 = yyss;


	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if yyoverflow is a macro.  */
	yyoverflow ("parser stack overflow",
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),

		    &yystacksize);

	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyoverflowlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
	goto yyoverflowlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
	yystacksize = YYMAXDEPTH;

      {
	short int *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyoverflowlab;
	YYSTACK_RELOCATE (yyss);
	YYSTACK_RELOCATE (yyvs);

#  undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;


      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

/* Do appropriate processing given the current state.  */
/* Read a lookahead token if we need one and don't already have one.  */
/* yyresume: */

  /* First try to decide what to do without reference to lookahead token.  */

  yyn = yypact[yystate];
  if (yyn == YYPACT_NINF)
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YYDSYMPRINTF ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yyn == 0 || yyn == YYTABLE_NINF)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  /* Shift the lookahead token.  */
  YYDPRINTF ((stderr, "Shifting token %s, ", yytname[yytoken]));

  /* Discard the token being shifted unless it is eof.  */
  if (yychar != YYEOF)
    yychar = YYEMPTY;

  *++yyvsp = yylval;


  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  yystate = yyn;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 7:
#line 391 "mod_yacc.y"
    {init_buffer();}
    break;

  case 8:
#line 392 "mod_yacc.y"
    {yyval.str = strdup (buffer);}
    break;

  case 11:
#line 399 "mod_yacc.y"
    {append (mod_yytext);}
    break;

  case 12:
#line 400 "mod_yacc.y"
    {append (mod_yytext);}
    break;

  case 13:
#line 401 "mod_yacc.y"
    {append (mod_yytext);}
    break;

  case 14:
#line 403 "mod_yacc.y"
    {append("[");}
    break;

  case 15:
#line 405 "mod_yacc.y"
    {append("]");}
    break;

  case 16:
#line 407 "mod_yacc.y"
    {append("(");}
    break;

  case 17:
#line 409 "mod_yacc.y"
    {append(")");}
    break;

  case 18:
#line 412 "mod_yacc.y"
    {fputs (mod_yytext, mod_yyout);}
    break;

  case 19:
#line 413 "mod_yacc.y"
    {fputs (mod_yytext, mod_yyout);}
    break;

  case 20:
#line 414 "mod_yacc.y"
    {fputs (mod_yytext, mod_yyout);}
    break;

  case 21:
#line 416 "mod_yacc.y"
    {putc ('[', mod_yyout);}
    break;

  case 22:
#line 418 "mod_yacc.y"
    {putc (']', mod_yyout);}
    break;

  case 23:
#line 420 "mod_yacc.y"
    {putc ('(', mod_yyout);}
    break;

  case 24:
#line 422 "mod_yacc.y"
    {putc (')', mod_yyout);}
    break;

  case 25:
#line 426 "mod_yacc.y"
    {fprintf (mod_yyout, "private->circuit.init");}
    break;

  case 26:
#line 428 "mod_yacc.y"
    {fprintf (mod_yyout, "Mif_Private_t *private");}
    break;

  case 27:
#line 430 "mod_yacc.y"
    {fprintf (mod_yyout, "private->circuit.anal_type");}
    break;

  case 28:
#line 432 "mod_yacc.y"
    {fprintf (mod_yyout, "private->circuit.anal_init");}
    break;

  case 29:
#line 434 "mod_yacc.y"
    {fprintf (mod_yyout, "private->circuit.call_type");}
    break;

  case 30:
#line 436 "mod_yacc.y"
    {fprintf (mod_yyout, "private->circuit.time");}
    break;

  case 31:
#line 438 "mod_yacc.y"
    {fprintf (mod_yyout, "private->circuit.frequency");}
    break;

  case 32:
#line 440 "mod_yacc.y"
    {fprintf (mod_yyout, "private->circuit.temperature");}
    break;

  case 33:
#line 442 "mod_yacc.y"
    {fprintf (mod_yyout, "private->circuit.t[%s]", yyvsp[-1].str);}
    break;

  case 34:
#line 444 "mod_yacc.y"
    {int i = valid_subid (yyvsp[-1].sub_id, PARAM);
			    fprintf (mod_yyout, "private->param[%d]->element[%s]",
				     i, subscript (yyvsp[-1].sub_id));
			    put_type (mod_yyout, mod_ifs_table->param[i].type);
			   }
    break;

  case 35:
#line 450 "mod_yacc.y"
    {int i = valid_id (yyvsp[-1].sub_id, PARAM);
			    fprintf (mod_yyout, "private->param[%d]->size", i);}
    break;

  case 36:
#line 453 "mod_yacc.y"
    {int i = valid_id (yyvsp[-1].sub_id, PARAM);
			    fprintf (mod_yyout, "private->param[%d]->is_null", i);}
    break;

  case 37:
#line 456 "mod_yacc.y"
    {int i = valid_id (yyvsp[-1].sub_id, CONN);
			    fprintf (mod_yyout, "private->conn[%d]->size", i);}
    break;

  case 38:
#line 459 "mod_yacc.y"
    {int i = valid_id (yyvsp[-1].sub_id, CONN);
			    fprintf (mod_yyout, "private->conn[%d]->is_null", i);}
    break;

  case 39:
#line 463 "mod_yacc.y"
    {int i = valid_subid (yyvsp[-3].sub_id, CONN);
			    int j = valid_subid (yyvsp[-1].sub_id, CONN);
			    check_dir (i, OUT, "PARTIAL");
			    check_dir (j, IN, "PARTIAL");
			    fprintf (mod_yyout, "private->conn[%d]->port[%s]->partial[%d].port[%s]",
				     i, subscript(yyvsp[-3].sub_id), j, subscript(yyvsp[-1].sub_id));}
    break;

  case 40:
#line 471 "mod_yacc.y"
    {int i = valid_subid (yyvsp[-3].sub_id, CONN);
			    int j = valid_subid (yyvsp[-1].sub_id, CONN);
			    check_dir (i, OUT, "AC_GAIN");
			    check_dir (j, IN, "AC_GAIN");
			    fprintf (mod_yyout, 
				     "private->conn[%d]->port[%s]->ac_gain[%d].port[%s]",
				     i, subscript(yyvsp[-3].sub_id), j, subscript(yyvsp[-1].sub_id));}
    break;

  case 41:
#line 479 "mod_yacc.y"
    {int i = valid_subid (yyvsp[-1].sub_id, STATIC_VAR);
			    fprintf (mod_yyout, 
				    "private->inst_var[%d]->element[%s]",
				     i, subscript(yyvsp[-1].sub_id));
			    if (mod_ifs_table->inst_var[i].is_array
				&& !(yyvsp[-1].sub_id.has_subscript)) {
			       /* null - eg. for malloc lvalue */
			    } else {
			       put_type (mod_yyout, 
					mod_ifs_table->inst_var[i].type);
			    } }
    break;

  case 42:
#line 491 "mod_yacc.y"
    {int i = valid_subid (yyvsp[-1].sub_id, STATIC_VAR);
			    fprintf (mod_yyout, "private->inst_var[%d]->size",
				    i, subscript(yyvsp[-1].sub_id));}
    break;

  case 43:
#line 495 "mod_yacc.y"
    {int i = valid_subid (yyvsp[-1].sub_id, CONN);
			    check_dir (i, OUT, "OUTPUT_DELAY");
			    fprintf (mod_yyout, 
				     "private->conn[%d]->port[%s]->delay", i,
				     subscript(yyvsp[-1].sub_id));}
    break;

  case 44:
#line 501 "mod_yacc.y"
    {int i = valid_subid (yyvsp[-1].sub_id, CONN);
			    check_dir (i, OUT, "CHANGED");
			    fprintf (mod_yyout, 
				     "private->conn[%d]->port[%s]->changed", i,
				     subscript(yyvsp[-1].sub_id));}
    break;

  case 45:
#line 507 "mod_yacc.y"
    {int i = valid_subid (yyvsp[-1].sub_id, CONN);
 			    check_dir (i, IN, "INPUT");
			    fprintf (mod_yyout, 
				     "private->conn[%d]->port[%s]->input",
				     i, subscript(yyvsp[-1].sub_id));
			    put_conn_type (mod_yyout, 
			       mod_ifs_table->conn[i].allowed_port_type[0]);}
    break;

  case 46:
#line 515 "mod_yacc.y"
    {int i = valid_subid (yyvsp[-1].sub_id, CONN);
 			    check_dir (i, IN, "INPUT_TYPE");
			    fprintf (mod_yyout, 
				     "private->conn[%d]->port[%s]->type_str",
				     i, subscript(yyvsp[-1].sub_id)); }
    break;

  case 47:
#line 521 "mod_yacc.y"
    {int i = valid_subid (yyvsp[-1].sub_id, CONN);
 			    check_dir (i, OUT, "OUTPUT_TYPE");
			    fprintf (mod_yyout, 
				     "private->conn[%d]->port[%s]->type_str",
				     i, subscript(yyvsp[-1].sub_id)); }
    break;

  case 48:
#line 527 "mod_yacc.y"
    {int i = valid_subid (yyvsp[-1].sub_id, CONN);
 			    check_dir (i, IN, "INPUT_STRENGTH");
			    fprintf (mod_yyout, 
				     "((Digital_t*)(private->conn[%d]->port[%s]->input",
				     i, subscript(yyvsp[-1].sub_id));
			    put_conn_type (mod_yyout, 
			       mod_ifs_table->conn[i].allowed_port_type[0]);
			    fprintf (mod_yyout, "))->strength");}
    break;

  case 49:
#line 536 "mod_yacc.y"
    {int i = valid_subid (yyvsp[-1].sub_id, CONN);
 			    check_dir (i, IN, "INPUT_STATE");
			    fprintf (mod_yyout, 
				     "((Digital_t*)(private->conn[%d]->port[%s]->input", 
				     i, subscript(yyvsp[-1].sub_id));
			    put_conn_type (mod_yyout, 
			       mod_ifs_table->conn[i].allowed_port_type[0]);
			    fprintf (mod_yyout, "))->state");}
    break;

  case 50:
#line 545 "mod_yacc.y"
    {int i = valid_subid (yyvsp[-1].sub_id, CONN);
 			    check_dir (i, OUT, "OUTPUT");
			    fprintf (mod_yyout, 
				     "private->conn[%d]->port[%s]->output", 
				     i, subscript(yyvsp[-1].sub_id));
			    put_conn_type (mod_yyout, 
			       mod_ifs_table->conn[i].allowed_port_type[0]);}
    break;

  case 51:
#line 553 "mod_yacc.y"
    {int i = valid_subid (yyvsp[-1].sub_id, CONN);
 			    check_dir (i, OUT, "OUTPUT_STRENGTH");
			    fprintf (mod_yyout, 
				     "((Digital_t*)(private->conn[%d]->port[%s]->output",
				     i, subscript(yyvsp[-1].sub_id));
			    put_conn_type (mod_yyout, 
			       mod_ifs_table->conn[i].allowed_port_type[0]);
			    fprintf (mod_yyout, "))->strength");}
    break;

  case 52:
#line 562 "mod_yacc.y"
    {int i = valid_subid (yyvsp[-1].sub_id, CONN);
 			    check_dir (i, OUT, "OUTPUT_STATE");
			    fprintf (mod_yyout, 
				     "((Digital_t*)(private->conn[%d]->port[%s]->output",
				     i, subscript(yyvsp[-1].sub_id));
			    put_conn_type (mod_yyout, 
			       mod_ifs_table->conn[i].allowed_port_type[0]);
			    fprintf (mod_yyout, "))->state");}
    break;

  case 53:
#line 571 "mod_yacc.y"
    {int i = valid_subid (yyvsp[-1].sub_id, CONN);
			    fprintf (mod_yyout, 
				     "private->conn[%d]->port[%s]->changed", i,
				     subscript(yyvsp[-1].sub_id));}
    break;

  case 54:
#line 576 "mod_yacc.y"
    {int i = valid_subid (yyvsp[-1].sub_id, CONN);
			    fprintf (mod_yyout, 
				     "private->conn[%d]->port[%s]->load", i,
				     subscript(yyvsp[-1].sub_id));}
    break;

  case 55:
#line 581 "mod_yacc.y"
    {int i = valid_subid (yyvsp[-1].sub_id, CONN);
			    fprintf (mod_yyout, 
				     "private->conn[%d]->port[%s]->total_load", i,
				     subscript(yyvsp[-1].sub_id));}
    break;

  case 56:
#line 586 "mod_yacc.y"
    {int i = valid_subid (yyvsp[-1].sub_id, CONN);
			    fprintf (mod_yyout, 
				     "private->conn[%d]->port[%s]->msg", i,
				     subscript(yyvsp[-1].sub_id));}
    break;

  case 58:
#line 594 "mod_yacc.y"
    {yyval.sub_id = yyvsp[-3].sub_id;
			   yyval.sub_id.has_subscript = TRUE;
			   yyval.sub_id.subscript = yyvsp[-1].str;}
    break;

  case 59:
#line 600 "mod_yacc.y"
    {yyval.sub_id.has_subscript = FALSE;
			      yyval.sub_id.id = strdup (mod_yytext);}
    break;


    }

/* Line 1010 of yacc.c.  */
#line 1868 "mod_yacc.c"

  yyvsp -= yylen;
  yyssp -= yylen;


  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;


  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if YYERROR_VERBOSE
      yyn = yypact[yystate];

      if (YYPACT_NINF < yyn && yyn < YYLAST)
	{
	  YYSIZE_T yysize = 0;
	  int yytype = YYTRANSLATE (yychar);
	  const char* yyprefix;
	  char *yymsg;
	  int yyx;

	  /* Start YYX at -YYN if negative to avoid negative indexes in
	     YYCHECK.  */
	  int yyxbegin = yyn < 0 ? -yyn : 0;

	  /* Stay within bounds of both yycheck and yytname.  */
	  int yychecklim = YYLAST - yyn;
	  int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
	  int yycount = 0;

	  yyprefix = ", expecting ";
	  for (yyx = yyxbegin; yyx < yyxend; ++yyx)
	    if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR)
	      {
		yysize += yystrlen (yyprefix) + yystrlen (yytname [yyx]);
		yycount += 1;
		if (yycount == 5)
		  {
		    yysize = 0;
		    break;
		  }
	      }
	  yysize += (sizeof ("syntax error, unexpected ")
		     + yystrlen (yytname[yytype]));
	  yymsg = (char *) YYSTACK_ALLOC (yysize);
	  if (yymsg != 0)
	    {
	      char *yyp = yystpcpy (yymsg, "syntax error, unexpected ");
	      yyp = yystpcpy (yyp, yytname[yytype]);

	      if (yycount < 5)
		{
		  yyprefix = ", expecting ";
		  for (yyx = yyxbegin; yyx < yyxend; ++yyx)
		    if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR)
		      {
			yyp = yystpcpy (yyp, yyprefix);
			yyp = yystpcpy (yyp, yytname[yyx]);
			yyprefix = " or ";
		      }
		}
	      yyerror (yymsg);
	      YYSTACK_FREE (yymsg);
	    }
	  else
	    yyerror ("syntax error; also virtual memory exhausted");
	}
      else
#endif /* YYERROR_VERBOSE */
	yyerror ("syntax error");
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
	 error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* If at end of input, pop the error token,
	     then the rest of the stack, then return failure.  */
	  if (yychar == YYEOF)
	     for (;;)
	       {
		 YYPOPSTACK;
		 if (yyssp == yyss)
		   YYABORT;
		 YYDSYMPRINTF ("Error: popping", yystos[*yyssp], yyvsp, yylsp);
		 yydestruct (yystos[*yyssp], yyvsp);
	       }
        }
      else
	{
	  YYDSYMPRINTF ("Error: discarding", yytoken, &yylval, &yylloc);
	  yydestruct (yytoken, &yylval);
	  yychar = YYEMPTY;

	}
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

#ifdef __GNUC__
  /* Pacify GCC when the user code never invokes YYERROR and the label
     yyerrorlab therefore never appears in user code.  */
  if (0)
     goto yyerrorlab;
#endif

  yyvsp -= yylen;
  yyssp -= yylen;
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (yyn != YYPACT_NINF)
	{
	  yyn += YYTERROR;
	  if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
	    {
	      yyn = yytable[yyn];
	      if (0 < yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
	YYABORT;

      YYDSYMPRINTF ("Error: popping", yystos[*yyssp], yyvsp, yylsp);
      yydestruct (yystos[yystate], yyvsp);
      YYPOPSTACK;
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  YYDPRINTF ((stderr, "Shifting error token, "));

  *++yyvsp = yylval;


  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#ifndef yyoverflow
/*----------------------------------------------.
| yyoverflowlab -- parser overflow comes here.  |
`----------------------------------------------*/
yyoverflowlab:
  yyerror ("parser stack overflow");
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
  return yyresult;
}


#line 604 "mod_yacc.y"


