local ctx = SPIData:currentContext()

function newEntity(nm)
	return NSEntityDescription:insertNewObjectForEntityForName_inManagedObjectContext(nm, ctx)
end

-- SPI UNITS
kSPIUnitNone = 0
kSPIUnitResistance = 1
kSPIUnitCapacitance = 2
kSPIUnitInductance = 3
kSPIUnitVoltage = 4
kSPIUnitCurrent = 5
kSPIUnitTime = 6
kSPIUnitLength = 7
kSPIUnitArea = 8


-- create a label
function Label(t)
	local label = newEntity("SPILabel")
	label:setPosition(CGPoint(t.x,t.y))
	label:setText(t.text)
	label:setPrimitive(t.primitive)
	return label
end

-- create a netlist template
function NetlistTemplate(template, primitive)
	local tpl = newEntity("SPIPrimitiveNetlistTemplate")
	tpl:setTemplateString(template)
	tpl:setPrimitive(primitive)
	return tpl
end

function PinNew(name, x_offset, y_offset, primitive)
	local pin = newEntity("SPIPin")
	pin:setName(name)
	pin:setOffset(CGPoint(x_offset, y_offset))
	pin:setPrimitive(primitive)
	return pin
end

function Pin(t)
    PinNew(t[1], t[2], t[3], t[4])
	--local pin = newEntity("SPIPin")
	--pin:setName(name)
	--pin:setOffset(CGPoint(x_offset, y_offset))
	--pin:setPrimitive(primitive)
	--return pin
end

function Primitive(t)
	local prim = newEntity("SPIPrimitive")
    prim:setName(t.name)
	prim:setSVGPath(t.SVGPath)
    if t.instRect then
        local p1 = CGPoint(t.instRect[1][1], t.instRect[1][2])
        local p2 = CGPoint(t.instRect[2][1], t.instRect[2][2])
        prim:setInstanceRect(CGRect(p1.x, p1.y, p2.x-p1.x, p2.y-p1.y))
    else
        prim:setInstanceRect(CGRect(t.x, t.y, t.width, t.height))
    end
	return prim
end

function AddFloatParam(t)
    local res = primitiveWithName(t.device)
    res:addFloatParam_section_description_min_max_initial_units(t.name, t.section, t.description, t.min, t.max, t.default, t.units)
end

-- Functions for building schematics
-- create a schematic, store in global variable 'schematic'
function primitiveWithName(name)
    return ctx:fetchPrimitive(name)
	--return ctx:fetchObjectForEntityName_predicate("SPIPrimitive", "name="..name)
end


function CGAffineTransformMakeTranslation(tx,ty)
	return CGAffineTransform(1,0,0,1,tx,ty)
end

function CGAffineTransformMakeScale(sx,sy)
	return CGAffineTransform(sx,0,0,sy,0,0)
end

function CGAffineTransformMakeRotation(angle)
	return CGAffineTransform(math.cos(angle), math.sin(angle), -math.sin(angle), math.cos(angle),0,0)
end

function Schematic(t)
	local schematic = newEntity("SPISchematic")
	-- TODO: add name, summary stuff back in
	-- schematic:setName(t.name)
	-- schematic:setSummary(t.summary)
   schematic.names = {}
   schematic.instanceCount = 0
	--setmetatable(schematic, Oschematic)
	return schematic
end

function connect(inst1, t1_name, inst2, t2_name)
	local net = newEntity("SPINet")
	net:setCircuit(inst1:circuit())
	local t1 = terminalByName(inst1, t1_name)
	local t2 = terminalByName(inst2, t2_name)
	t1:setNet(net)
	t2:setNet(net)
	return net
end

--function Schematic:connect(v)
--end

function Wire(t)
	local w = newEntity("SPIWire")
	w:setNet(t.net)
	w:setSchematic(t.circuit)
	w:setTerminal(t.terminal)
	w:setSVGPath(t.path)
	return w
end

-- create an instance
function Instance(t)
	local inst = newEntity("SPIInstance")
	local primitive = primitiveWithName(t.device)
	inst:setCircuit(t.circuit)
	inst:setPrimitive(primitive)

	-- position
	local pos = newEntity("SPIInstancePosition")
	pos:setInstance(inst)
	pos:setSchematic(t.circuit)
	pos:setTransform(CGAffineTransformMakeTranslation(t.x, t.y))
	
	-- pins
	local pins = primitive:pins():allObjects()
	for idx, pin in ipairs(pins) do
		local term = newEntity("SPITerminal")
		term:setPin(pin)
		term:setInstance(inst)
	end

   --if t.properties then
   --   for k, v in pairs(t.properties) do
	-- 		i:setProperty_toValue(k, v)
   --   end
   --end
	
	if t.name then
      inst.name = t.name
   else
      t.circuit.instanceCount = t.circuit.instanceCount+1
      inst.name = string.format("%s%d", t.device, t.circuit.instanceCount)
      --print(inst.name, t.x, t.y)
   end

   t.circuit.names[inst.name] = i 

   return inst
end

function terminalByName(inst, name)
	local terminals = inst:terminals():allObjects()
	for idx, term in ipairs(terminals) do
		--print(term:pin():name())
		if term:pin():name() == name then
			return term
		end
	end
	return nil
end

function dcAnalysis(t)
	local dc = newEntity("SPIDCAnalysis")
	dc:setCircuit(t.circuit)
	dc:setSourceName(t.source)
	dc:setStartValue(t.start)
	dc:setStopValue(t.stop)
	dc:setIncrValue(t.incr)
	return dc
end

function acAnalysis(variation, start, stop, points)
   local ac = newEntity("SPIACAnalysis")
   ac:setDecadeVariation(variation)
   ac:setStartFreq(start)
   ac:setEndFreq(stop)
   ac:setPoints(points)
   return ac
end

probeSet={}
function probeSet:new(analysis)
	o = {}   -- create object if user does not provide one
   setmetatable(o, self)
   self.__index = self
	self.__ps = newEntity("SPIProbeSet")
	self.__ps:setAnalysis(analysis)
   return o
end

function probeSet:voltageProbe(net)
	local vp = newEntity("SPIVoltageProbe")
	vp:setProbeSet(self.__ps)
	vp:setNet(net)
	return vp
end



