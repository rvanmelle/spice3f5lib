

local idx = 0
local schematic = Schematic{ name="Lots o' Resistors", summary="Stress test this baby"}

for y=140,1496,200 do 
    print(y)
    local prev_inst = nil
	for x=120,1848,200 do
		idx = idx + 1
		local inst = Instance{ circuit=schematic, device="res", x=x, y=y }
		for i=1,math.random(4) do
			inst:position():rotateCW()
		end
		--if prev_inst then
		--	local net = connect(prev_inst, 'MINUS', inst, 'PLUS')
		--	if idx < 20 then
		--		net:setName("net"..idx)
		--	end
		--	local path1 = "M"..x..","..(y-30).." "..(x-50)..","..(y-30)
		--	local path2 = "M"..(x-50)..","..(y-30).." "..(x-100)..","..(y-30)
		--	local w1 = Wire{ net=net, terminal=terminalByName(inst, 'PLUS'), circuit=schematic, path=path1}
		--	local w2 = Wire{ net=net, terminal=terminalByName(prev_inst, 'MINUS'), circuit=schematic, path=path2}
		--end
		--prev_inst = inst
	end

end
print(kSPIUnitTime)
--local dc = dcAnalysis{circuit=schematic, source="vin", start="1", stop="10", incr="2"}
--local ps = probeSet:new(dc)
--ps:voltageProbe(schematic:nets():anyObject())
