#!/opt/local/bin/tclsh

# transforms to XML
source ../external/analogLib.tcl

proc min {args} {
	set x [lindex $args 0]
	foreach arg $args {
		if [expr $arg < $x] {
			set x $arg
		}
	}
	return $x
}

proc max {args} {
	set x [lindex $args 0]
	foreach arg $args {
		if [expr $arg > $x] {
			set x $arg
		}
	}
	return $x
}

proc PinRect {args} {
	# NOT DONE
	global svg
	set name [lindex $args 0]
	set type [lindex $args 1]
	set bool [lindex $args 2]
	set direction [lindex $args 3]
	set x1 [lindex [lindex [lindex $args 4] 0] 0]
	set y1 [lindex [lindex [lindex $args 4] 0] 1]
	set x2 [lindex [lindex [lindex $args 4] 1] 0]
	set y2 [lindex [lindex [lindex $args 4] 1] 1]
	puts "PinRect $name $type $bool $direction $x1,$y1 $x2,$y2"
		
	set w [expr abs($x2 - $x1)]
	set h [expr abs($y2 - $y1)]
	set x1 [expr $x1 < $x2 ? $x1 : $x2]
	set y1 [expr $y1 < $y2 ? $y1 : $y2]
	puts $svg "<rect class=\"pin\" x=\"$x1\" y=\"$y1\" width=\"$w\" height=\"$h\" />"
	# if $args 6 then allowOverride

}

proc InstRect {args} {
	global svg
	set x1 [lindex [lindex $args 0] 0]
	set y1 [lindex [lindex $args 0] 1]
	set x2 [lindex [lindex $args 1] 0]
	set y2 [lindex [lindex $args 1] 1]
	puts "InstRect $x1 $y1 $x2 $y2"
}

proc Rect {args} {
	global svg
	set x1 [lindex [lindex [lindex $args 0] 0] 0]
	set y1 [lindex [lindex [lindex $args 0] 0] 1]
	set x2 [lindex [lindex [lindex $args 0] 1] 0]
	set y2 [lindex [lindex [lindex $args 0] 1] 1]
	
	set w [expr abs($x2 - $x1)]
	set h [expr abs($y2 - $y1)]
	set x1 [expr $x1 < $x2 ? $x1 : $x2]
	set y1 [expr $y1 < $y2 ? $y1 : $y2]
	puts $svg "<rect class=\"pin\" x=\"$x1\" y=\"$y1\" width=\"$w\" height=\"$h\" />"
	puts "Rect $x1 $y1 $x2 $y2"
}

proc Arc {args} {
	global svg
	set x1 [lindex [lindex [lindex $args 0] 0] 0]
	set y1 [lindex [lindex [lindex $args 0] 0] 1]
	set x2 [lindex [lindex [lindex $args 0] 1] 0]
	set y2 [lindex [lindex [lindex $args 0] 1] 1]
	set startAngle [lindex $args 1]
	set stopAngle [lindex $args 2]

	set cx [expr ($x1 + $x2) / 2]
	set cy [expr ($y1 + $y2) / 2]
	set rx [expr abs($x2 - $x1) / 2]
	set ry [expr abs($y2 - $y1) / 2]
	set r $rx
	#if {$rx != $ry} {
	#	error "This is not a circle! $rx != $ry"
	#}

	puts "Arc $x1 $y1 $x2 $y2 $startAngle $stopAngle"
	set pi 3.141562
	set startAngle [expr ($startAngle*$pi)/180.0]
	set stopAngle [expr ($stopAngle*$pi)/180.0]
	
	# That starting point
	if {[expr $startAngle >= 90 && $startAngle <= 270]} {
		set p1x [expr $cx - $r*cos($startAngle)]
	} else {
		set p1x [expr $cx + $r*cos($startAngle)]
	}
	if {[expr $startAngle >= 0 && $startAngle <= 180]} {
		set p1y [expr $cy + $r*sin($startAngle)]
	} else {
		set p1y [expr $cy - $r*sin($startAngle)]
	}
	# The final point
	if {[expr $stopAngle >= 90 && $stopAngle <= 270]} {
		set p2x [expr $cx - $r*cos($stopAngle)]
	} else {
		set p2x [expr $cx + $r*cos($stopAngle)]
	}
	if {[expr $stopAngle >= 0 && $stopAngle <= 180]} {
		set p2y [expr $cy + $r*sin($stopAngle)]
	} else {
		set p2y [expr $cy - $r*sin($stopAngle)]
	}
	
	set p1x [expr abs($p1x) < 0.001 ? 0.0 : [format "%.6g" $p1x]]
	set p1y [expr abs($p1y) < 0.001 ? 0.0 : [format "%.6g" $p1y]]
	set p2x [expr abs($p2x) < 0.001 ? 0.0 : [format "%.6g" $p2x]]
	set p2y [expr abs($p2y) < 0.001 ? 0.0 : [format "%.6g" $p2y]]
	puts $svg "<path class=\"wire\" d=\"M$p1x,$p1y A$rx,$ry 0 0,1 $p2x,$p2y\" />"
	#puts $svg "<path class=\"wire\" d=\"M$x1,$y1 C$x1,[expr $y1+10] $x2,[expr $y2+10] $x2,$y2 \" />"

}

proc Line {args} {
	global svg
	puts -nonewline "Line "
	puts -nonewline $svg "<polyline class=\"wire\" points=\""
	for {set i 0} {$i<[llength $args]} {incr i} {
		set x [lindex [lindex $args $i] 0]
		set y [lindex [lindex $args $i] 1]
		puts -nonewline $svg "$x,$y "
		puts -nonewline "$x $y "		
	}
	#set x [lindex [lindex $args 0] 0]
	#set y [lindex [lindex $args 0] 1]
	#puts -nonewline $svg "$x,$y "
	#puts -nonewline "$x $y "		
	puts $svg "\" />"
	puts ""
}

proc Polygon {args} {
	global svg
	puts -nonewline "Polygon "
	puts -nonewline $svg "<polygon class=\"wire\" points=\""
	for {set i 0} {$i<[llength $args]} {incr i} {
		set x [lindex [lindex $args $i] 0]
		set y [lindex [lindex $args $i] 1]
		puts -nonewline $svg "$x,$y "
		puts -nonewline "$x $y "		
	}
	set x [lindex [lindex $args 0] 0]
	set y [lindex [lindex $args 0] 1]
	puts -nonewline $svg "$x,$y "
	puts -nonewline "$x $y "		
	puts $svg "\" />"
	puts ""
}

proc Ellipse {args} {
	global svg
	set x1 [lindex [lindex [lindex $args 0] 0] 0]
	set y1 [lindex [lindex [lindex $args 0] 0] 1]
	set x2 [lindex [lindex [lindex $args 0] 1] 0]
	set y2 [lindex [lindex [lindex $args 0] 1] 1]
	puts "Ellipse $x1 $y1 $x2 $y2"	
	# TODO: This works for circles but I'm not convinced it works generally
	set cx [expr ($x1 + $x2) / 2]
	set cy [expr ($y1 + $y2) / 2]
	#set height [expr abs($y2 - $y1)]
	#set width [expr abs($x2 - $x1)]
	#set rx [expr sqrt(pow($height,2) + pow($width,2)) / 2]
	#set ry $rx
	set rx [expr abs($x2 - $x1) / 2]
	set ry [expr abs($y2 - $y1) / 2]
	puts $svg "<ellipse class=\"wire\" cx=\"$cx\" cy=\"$cy\" rx=\"$rx\" ry=\"$ry\" />"	
}

proc Path1 {args} {
	global svg
	puts -nonewline "Line "
	set stroke [lindex $args 0]
	puts -nonewline $svg "<polyline stroke-width=\"$stroke\" class=\"wire\" points=\""
	set points [lindex $args 1]
	for {set i 0} {$i<[llength $points]} {incr i} {
		set x [lindex [lindex $args $i] 0]
		set y [lindex [lindex $args $i] 1]
		puts -nonewline $svg "$x,$y "
		puts -nonewline "$x $y "		
	}
	#set x [lindex [lindex $args 0] 0]
	#set y [lindex [lindex $args 0] 1]
	#puts -nonewline $svg "$x,$y "
	#puts -nonewline "$x $y "		
	puts $svg "\" />"
	puts ""
}

proc Path1 {args} {
	# NOT DONE
	puts "Path1: UNHANDLED"
}

proc eval_text {class args} {
	# NOT DONE: This does not handle the upper/lower/center thing right
	set args [lindex $args 0]
	puts "eval_text $class, $args, [lindex $args 0]"
	global svg
	set cdsTerm [lindex $args 0]
	#regexp {cdsTerm\("(.*)"\)} $cdsTerm m0 m1
	#set name $m1
	set x1 [lindex [lindex $args 1] 0]
	set y1 [lindex [lindex $args 1] 1]
	set anchor [lindex $args 2]
	variable anchor_val
	variable dy ""
	if {$anchor == "lowerLeft"} {
		set anchor_val "start"
	} elseif {$anchor == "lowerRight"} {
		set anchor_val "end"
	} elseif {$anchor == "centerCenter"} {
		set anchor_val "middle"
	} elseif {$anchor == "upperLeft"} {
		set anchor_val "start"
		set dy "dy=\"8+\""
	} {
		error "unrecognized alignment: $anchor"
	}
	puts $svg "<text class=\"$class\" $dy text-anchor=\"$anchor_val\" x=\"$x1\" y=\"$y1\" >\
		$cdsTerm</text>"
	puts "EvalTextTerm $cdsTerm $x1 $y1"	
}

proc EvalTextTerm {args} {
	eval_text "terminalName" $args
}

proc EvalTextParam {args} {
	eval_text "param" $args
}

proc EvalTextName {args} {
	eval_text "instName" $args
}

proc Prop {type name value} {
	puts "Prop $type $name $value"
}


set symbols [slDefineanalogLibSymbols]

set svg [open "output.svg" "w"]
set html [open "output.html" "w"]
puts $svg "<defs>"
set use_block ""
set index 0
set row 6
set width 150
set height 80
foreach sym $symbols {
	set x [expr (($index % $row) + 0.5) * $width]
	set y [expr (($index / $row) + 1) * $height]

	set name [lindex $sym 0]
	set stop_list [lindex $sym 1]
	set type [lindex $sym 2]
	puts $svg "<g id=\"$name\" x=\"$x\" y=\"$y\">"
	append use_block "<use x=\"$x\" y=\"$y\" xlink:href=\""
	append use_block #
	append use_block "$name\" />\n"
	puts "$name $stop_list $type"
	for {set i 3} {$i<[llength $sym]} {incr i} {
		set shape [lindex $sym $i]	
		puts "EVAL $shape"
		eval $shape
	}
	puts $svg "</g>\n"
	incr index
}
puts $svg "</defs>\n"

puts $svg $use_block
puts $svg "</svg>"
puts $svg "</svg>"
close $svg
close $html

exec cat ../../svg/partial_canvas.svg > ../../svg/canvas.svg
exec cat output.svg >> ../../svg/canvas.svg