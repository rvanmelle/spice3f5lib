{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_cap name:@"cap" rect:CGRectMake(-20, -60, 40, 60)];
  [p addPin:@"MINUS" global:false location:CGPointMake(0, -60) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"PLUS" global:false location:CGPointMake(0, 0) direction:kSPISignalDirectionInputOutput];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_cap3 name:@"cap3" rect:CGRectMake(-20, -60, 40, 60)];
  [p addPin:@"MINUS" global:false location:CGPointMake(0, -60) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"PLUS" global:false location:CGPointMake(0, 0) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"B" global:false location:CGPointMake(20, -30) direction:kSPISignalDirectionInputOutput];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_cap4 name:@"cap4" rect:CGRectMake(-20, -60, 40, 60)];
  [p addPin:@"MINUS" global:false location:CGPointMake(0, -60) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"PLUS" global:false location:CGPointMake(0, 0) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"B" global:false location:CGPointMake(20, -30) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"T" global:false location:CGPointMake(-20, -30) direction:kSPISignalDirectionInputOutput];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_cccs name:@"cccs" rect:CGRectMake(-20, -60, 40, 60)];
  [p addPin:@"MINUS" global:false location:CGPointMake(0, -60) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"PLUS" global:false location:CGPointMake(0, 0) direction:kSPISignalDirectionInputOutput];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_ccvs name:@"ccvs" rect:CGRectMake(-20, -60, 40, 60)];
  [p addPin:@"MINUS" global:false location:CGPointMake(0, -60) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"PLUS" global:false location:CGPointMake(0, 0) direction:kSPISignalDirectionInputOutput];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_diode name:@"diode" rect:CGRectMake(-20, -60, 40, 60)];
  [p addPin:@"MINUS" global:false location:CGPointMake(0, -60) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"PLUS" global:false location:CGPointMake(0, 0) direction:kSPISignalDirectionInputOutput];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_diode3 name:@"diode3" rect:CGRectMake(-20, -60, 40, 60)];
  [p addPin:@"MINUS" global:false location:CGPointMake(0, -60) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"PLUS" global:false location:CGPointMake(0, 0) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"B" global:false location:CGPointMake(20, -30) direction:kSPISignalDirectionInputOutput];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_diode4 name:@"diode4" rect:CGRectMake(-20, -60, 40, 60)];
  [p addPin:@"MINUS" global:false location:CGPointMake(0, -60) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"PLUS" global:false location:CGPointMake(0, 0) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"B" global:false location:CGPointMake(20, -30) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"T" global:false location:CGPointMake(-20, -30) direction:kSPISignalDirectionInputOutput];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_fuse name:@"fuse" rect:CGRectMake(-20, -60, 40, 60)];
  [p addPin:@"MINUS" global:false location:CGPointMake(0, -60) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"PLUS" global:false location:CGPointMake(0, 0) direction:kSPISignalDirectionInputOutput];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_fuse3 name:@"fuse3" rect:CGRectMake(-20, -60, 40, 60)];
  [p addPin:@"MINUS" global:false location:CGPointMake(0, -60) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"PLUS" global:false location:CGPointMake(0, 0) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"B" global:false location:CGPointMake(20, -30) direction:kSPISignalDirectionInputOutput];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_fuse4 name:@"fuse4" rect:CGRectMake(-20, -60, 40, 60)];
  [p addPin:@"MINUS" global:false location:CGPointMake(0, -60) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"PLUS" global:false location:CGPointMake(0, 0) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"B" global:false location:CGPointMake(20, -30) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"T" global:false location:CGPointMake(-20, -30) direction:kSPISignalDirectionInputOutput];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_gnd name:@"gnd" rect:CGRectMake(-10, -40, 20, 40)];
  [p addPin:@"gnd!" global:true location:CGPointMake(0, 0) direction:kSPISignalDirectionInputOutput];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_idc name:@"idc" rect:CGRectMake(-20, -60, 40, 60)];
  [p addPin:@"MINUS" global:false location:CGPointMake(0, -60) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"PLUS" global:false location:CGPointMake(0, 0) direction:kSPISignalDirectionInputOutput];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_iexp name:@"iexp" rect:CGRectMake(-20, -60, 40, 60)];
  [p addPin:@"MINUS" global:false location:CGPointMake(0, -60) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"PLUS" global:false location:CGPointMake(0, 0) direction:kSPISignalDirectionInputOutput];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_ind name:@"ind" rect:CGRectMake(-20, -60, 40, 60)];
  [p addPin:@"MINUS" global:false location:CGPointMake(0, -60) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"PLUS" global:false location:CGPointMake(0, 0) direction:kSPISignalDirectionInputOutput];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_ipulse name:@"ipulse" rect:CGRectMake(-20, -60, 40, 60)];
  [p addPin:@"MINUS" global:false location:CGPointMake(0, -60) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"PLUS" global:false location:CGPointMake(0, 0) direction:kSPISignalDirectionInputOutput];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_ipwl name:@"ipwl" rect:CGRectMake(-20, -60, 40, 60)];
  [p addPin:@"MINUS" global:false location:CGPointMake(0, -60) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"PLUS" global:false location:CGPointMake(0, 0) direction:kSPISignalDirectionInputOutput];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_ipwlf name:@"ipwlf" rect:CGRectMake(-20, -60, 40, 60)];
  [p addPin:@"MINUS" global:false location:CGPointMake(0, -60) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"PLUS" global:false location:CGPointMake(0, 0) direction:kSPISignalDirectionInputOutput];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_isin name:@"isin" rect:CGRectMake(-20, -60, 40, 60)];
  [p addPin:@"MINUS" global:false location:CGPointMake(0, -60) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"PLUS" global:false location:CGPointMake(0, 0) direction:kSPISignalDirectionInputOutput];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_isource name:@"isource" rect:CGRectMake(-20, -60, 40, 60)];
  [p addPin:@"MINUS" global:false location:CGPointMake(0, -60) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"PLUS" global:false location:CGPointMake(0, 0) direction:kSPISignalDirectionInputOutput];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_makeGlobal name:@"makeGlobal" rect:CGRectMake(-10, 0, 20, 40)];
  [p addPin:@"net" global:true location:CGPointMake(0, 0) direction:kSPISignalDirectionInputOutput];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_mind name:@"mind" rect:CGRectMake(-20, -60, 40, 60)];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_nmos name:@"nmos" rect:CGRectMake(0, -30, 40, 60)];
  [p addPin:@"D" global:false location:CGPointMake(40, 30) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"G" global:false location:CGPointMake(0, 0) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"S" global:false location:CGPointMake(40, -30) direction:kSPISignalDirectionInputOutput];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_nmos4 name:@"nmos4" rect:CGRectMake(0, -30, 40, 60)];
  [p addPin:@"D" global:false location:CGPointMake(40, 30) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"G" global:false location:CGPointMake(0, 0) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"S" global:false location:CGPointMake(40, -30) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"B" global:false location:CGPointMake(40, 0) direction:kSPISignalDirectionInputOutput];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_nmos4hv name:@"nmos4hv" rect:CGRectMake(0, -30, 40, 60)];
  [p addPin:@"D" global:false location:CGPointMake(40, 30) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"G" global:false location:CGPointMake(0, 0) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"S" global:false location:CGPointMake(40, -30) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"B" global:false location:CGPointMake(40, 0) direction:kSPISignalDirectionInputOutput];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_nmoshv name:@"nmoshv" rect:CGRectMake(0, -30, 40, 60)];
  [p addPin:@"D" global:false location:CGPointMake(40, 30) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"G" global:false location:CGPointMake(0, 0) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"S" global:false location:CGPointMake(40, -30) direction:kSPISignalDirectionInputOutput];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_npn name:@"npn" rect:CGRectMake(0, -30, 40, 60)];
  [p addPin:@"E" global:false location:CGPointMake(40, -30) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"C" global:false location:CGPointMake(40, 30) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"B" global:false location:CGPointMake(0, 0) direction:kSPISignalDirectionInputOutput];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_npn4 name:@"npn4" rect:CGRectMake(0, -30, 40, 60)];
  [p addPin:@"E" global:false location:CGPointMake(40, -30) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"C" global:false location:CGPointMake(40, 30) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"B" global:false location:CGPointMake(0, 0) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"S" global:false location:CGPointMake(48, 0) direction:kSPISignalDirectionInputOutput];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_npn4a name:@"npn4a" rect:CGRectMake(0, -30, 40, 60)];
  [p addPin:@"E" global:false location:CGPointMake(40, -30) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"C" global:false location:CGPointMake(40, 30) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"B" global:false location:CGPointMake(0, 0) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"S" global:false location:CGPointMake(48, -8) direction:kSPISignalDirectionInputOutput];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_pcapacitor name:@"pcapacitor" rect:CGRectMake(-20, -60, 40, 60)];
  [p addPin:@"MINUS" global:false location:CGPointMake(0, -60) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"PLUS" global:false location:CGPointMake(0, 0) direction:kSPISignalDirectionInputOutput];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_pdc name:@"pdc" rect:CGRectMake(-20, -80, 40, 80)];
  [p addPin:@"MINUS" global:false location:CGPointMake(0, -80) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"PLUS" global:false location:CGPointMake(0, 0) direction:kSPISignalDirectionInputOutput];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_pdiode name:@"pdiode" rect:CGRectMake(-20, -60, 40, 60)];
  [p addPin:@"MINUS" global:false location:CGPointMake(0, -60) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"PLUS" global:false location:CGPointMake(0, 0) direction:kSPISignalDirectionInputOutput];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_pmos name:@"pmos" rect:CGRectMake(0, -30, 40, 60)];
  [p addPin:@"D" global:false location:CGPointMake(40, -30) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"G" global:false location:CGPointMake(0, 0) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"S" global:false location:CGPointMake(40, 30) direction:kSPISignalDirectionInputOutput];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_pmos4 name:@"pmos4" rect:CGRectMake(0, -30, 40, 60)];
  [p addPin:@"D" global:false location:CGPointMake(40, -30) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"G" global:false location:CGPointMake(0, 0) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"S" global:false location:CGPointMake(40, 30) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"B" global:false location:CGPointMake(40, 0) direction:kSPISignalDirectionInputOutput];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_pmos4hv name:@"pmos4hv" rect:CGRectMake(0, -30, 40, 60)];
  [p addPin:@"D" global:false location:CGPointMake(40, -30) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"G" global:false location:CGPointMake(0, 0) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"S" global:false location:CGPointMake(40, 30) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"B" global:false location:CGPointMake(40, 0) direction:kSPISignalDirectionInputOutput];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_pmoshv name:@"pmoshv" rect:CGRectMake(0, -30, 40, 60)];
  [p addPin:@"D" global:false location:CGPointMake(40, -30) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"G" global:false location:CGPointMake(0, 0) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"S" global:false location:CGPointMake(40, 30) direction:kSPISignalDirectionInputOutput];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_pnp name:@"pnp" rect:CGRectMake(0, -30, 40, 60)];
  [p addPin:@"E" global:false location:CGPointMake(40, -30) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"C" global:false location:CGPointMake(40, 30) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"B" global:false location:CGPointMake(0, 0) direction:kSPISignalDirectionInputOutput];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_pnp4 name:@"pnp4" rect:CGRectMake(0, -30, 40, 60)];
  [p addPin:@"E" global:false location:CGPointMake(40, -30) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"C" global:false location:CGPointMake(40, 30) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"B" global:false location:CGPointMake(0, 0) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"S" global:false location:CGPointMake(48, 0) direction:kSPISignalDirectionInputOutput];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_pnp4a name:@"pnp4a" rect:CGRectMake(0, -30, 40, 60)];
  [p addPin:@"E" global:false location:CGPointMake(40, -30) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"C" global:false location:CGPointMake(40, 30) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"B" global:false location:CGPointMake(0, 0) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"S" global:false location:CGPointMake(48, -8) direction:kSPISignalDirectionInputOutput];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_port name:@"port" rect:CGRectMake(-20, -80, 40, 80)];
  [p addPin:@"MINUS" global:false location:CGPointMake(0, -80) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"PLUS" global:false location:CGPointMake(0, 0) direction:kSPISignalDirectionInputOutput];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_ppulse name:@"ppulse" rect:CGRectMake(-20, -80, 40, 80)];
  [p addPin:@"MINUS" global:false location:CGPointMake(0, -80) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"PLUS" global:false location:CGPointMake(0, 0) direction:kSPISignalDirectionInputOutput];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_ppwl name:@"ppwl" rect:CGRectMake(-20, -80, 40, 80)];
  [p addPin:@"MINUS" global:false location:CGPointMake(0, -80) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"PLUS" global:false location:CGPointMake(0, 0) direction:kSPISignalDirectionInputOutput];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_ppwlf name:@"ppwlf" rect:CGRectMake(-20, -80, 40, 80)];
  [p addPin:@"MINUS" global:false location:CGPointMake(0, -80) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"PLUS" global:false location:CGPointMake(0, 0) direction:kSPISignalDirectionInputOutput];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_presistor name:@"presistor" rect:CGRectMake(-20, -60, 40, 60)];
  [p addPin:@"MINUS" global:false location:CGPointMake(0, -60) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"PLUS" global:false location:CGPointMake(0, 0) direction:kSPISignalDirectionInputOutput];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_psin name:@"psin" rect:CGRectMake(-20, -80, 40, 80)];
  [p addPin:@"MINUS" global:false location:CGPointMake(0, -80) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"PLUS" global:false location:CGPointMake(0, 0) direction:kSPISignalDirectionInputOutput];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_res name:@"res" rect:CGRectMake(-20, -60, 40, 60)];
  [p addPin:@"MINUS" global:false location:CGPointMake(0, -60) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"PLUS" global:false location:CGPointMake(0, 0) direction:kSPISignalDirectionInputOutput];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_res3 name:@"res3" rect:CGRectMake(-20, -60, 40, 60)];
  [p addPin:@"MINUS" global:false location:CGPointMake(0, -60) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"PLUS" global:false location:CGPointMake(0, 0) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"B" global:false location:CGPointMake(20, -30) direction:kSPISignalDirectionInputOutput];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_res4 name:@"res4" rect:CGRectMake(-20, -60, 40, 60)];
  [p addPin:@"MINUS" global:false location:CGPointMake(0, -60) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"PLUS" global:false location:CGPointMake(0, 0) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"B" global:false location:CGPointMake(20, -30) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"T" global:false location:CGPointMake(-20, -30) direction:kSPISignalDirectionInputOutput];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_varactor name:@"varactor" rect:CGRectMake(-20, -60, 40, 60)];
  [p addPin:@"MINUS" global:false location:CGPointMake(0, -60) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"PLUS" global:false location:CGPointMake(0, 0) direction:kSPISignalDirectionInputOutput];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_varactor3 name:@"varactor3" rect:CGRectMake(-20, -60, 40, 60)];
  [p addPin:@"MINUS" global:false location:CGPointMake(0, -60) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"PLUS" global:false location:CGPointMake(0, 0) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"B" global:false location:CGPointMake(20, -30) direction:kSPISignalDirectionInputOutput];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_varactor4 name:@"varactor4" rect:CGRectMake(-20, -60, 40, 60)];
  [p addPin:@"MINUS" global:false location:CGPointMake(0, -60) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"PLUS" global:false location:CGPointMake(0, 0) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"B" global:false location:CGPointMake(20, -30) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"T" global:false location:CGPointMake(-20, -30) direction:kSPISignalDirectionInputOutput];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_vcc name:@"vcc" rect:CGRectMake(-25, 0, 50, 50)];
  [p addPin:@"vcc!" global:true location:CGPointMake(0, 0) direction:kSPISignalDirectionInputOutput];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_vccs name:@"vccs" rect:CGRectMake(-20, -60, 40, 60)];
  [p addPin:@"MINUS" global:false location:CGPointMake(0, -60) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"PLUS" global:false location:CGPointMake(0, 0) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"NC+" global:false location:CGPointMake(-20, -20) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"NC-" global:false location:CGPointMake(-20, -40) direction:kSPISignalDirectionInputOutput];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_vcvs name:@"vcvs" rect:CGRectMake(-20, -60, 40, 60)];
  [p addPin:@"MINUS" global:false location:CGPointMake(0, -60) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"PLUS" global:false location:CGPointMake(0, 0) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"NC+" global:false location:CGPointMake(-20, -20) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"NC-" global:false location:CGPointMake(-20, -40) direction:kSPISignalDirectionInputOutput];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_vdc name:@"vdc" rect:CGRectMake(-20, -60, 40, 60)];
  [p addPin:@"MINUS" global:false location:CGPointMake(0, -60) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"PLUS" global:false location:CGPointMake(0, 0) direction:kSPISignalDirectionInputOutput];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_vdd name:@"vdd" rect:CGRectMake(-25, 0, 50, 50)];
  [p addPin:@"vdd!" global:true location:CGPointMake(0, 0) direction:kSPISignalDirectionInputOutput];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_vee name:@"vee" rect:CGRectMake(-25, -50, 50, 50)];
  [p addPin:@"vee!" global:true location:CGPointMake(0, 0) direction:kSPISignalDirectionInputOutput];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_vexp name:@"vexp" rect:CGRectMake(-20, -60, 40, 60)];
  [p addPin:@"MINUS" global:false location:CGPointMake(0, -60) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"PLUS" global:false location:CGPointMake(0, 0) direction:kSPISignalDirectionInputOutput];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_vpulse name:@"vpulse" rect:CGRectMake(-20, -60, 40, 60)];
  [p addPin:@"MINUS" global:false location:CGPointMake(0, -60) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"PLUS" global:false location:CGPointMake(0, 0) direction:kSPISignalDirectionInputOutput];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_vpwl name:@"vpwl" rect:CGRectMake(-20, -60, 40, 60)];
  [p addPin:@"MINUS" global:false location:CGPointMake(0, -60) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"PLUS" global:false location:CGPointMake(0, 0) direction:kSPISignalDirectionInputOutput];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_vpwlf name:@"vpwlf" rect:CGRectMake(-20, -60, 40, 60)];
  [p addPin:@"MINUS" global:false location:CGPointMake(0, -60) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"PLUS" global:false location:CGPointMake(0, 0) direction:kSPISignalDirectionInputOutput];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_vsin name:@"vsin" rect:CGRectMake(-20, -60, 40, 60)];
  [p addPin:@"MINUS" global:false location:CGPointMake(0, -60) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"PLUS" global:false location:CGPointMake(0, 0) direction:kSPISignalDirectionInputOutput];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_vsource name:@"vsource" rect:CGRectMake(-20, -60, 40, 60)];
  [p addPin:@"MINUS" global:false location:CGPointMake(0, -60) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"PLUS" global:false location:CGPointMake(0, 0) direction:kSPISignalDirectionInputOutput];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_vss name:@"vss" rect:CGRectMake(-25, -50, 50, 50)];
  [p addPin:@"vss!" global:true location:CGPointMake(0, 0) direction:kSPISignalDirectionInputOutput];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_xfmr4 name:@"xfmr4" rect:CGRectMake(-24, -60, 48, 60)];
  [p addPin:@"P-" global:false location:CGPointMake(-18, -60) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"P+" global:false location:CGPointMake(-18, 0) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"S-" global:false location:CGPointMake(18, -60) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"S+" global:false location:CGPointMake(18, 0) direction:kSPISignalDirectionInputOutput];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_xfmr5p name:@"xfmr5p" rect:CGRectMake(-24, -60, 48, 60)];
  [p addPin:@"P-" global:false location:CGPointMake(-18, -60) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"P+" global:false location:CGPointMake(-18, 0) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"S-" global:false location:CGPointMake(18, -60) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"S+" global:false location:CGPointMake(18, 0) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"PB" global:false location:CGPointMake(0, 12) direction:kSPISignalDirectionInputOutput];
}
{
  SPIPrimitive *p = [SPIPrimitive addPrimitive:kSPIPrimitiveDeviceType_xfmr5s name:@"xfmr5s" rect:CGRectMake(-24, -60, 48, 60)];
  [p addPin:@"P-" global:false location:CGPointMake(-18, -60) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"P+" global:false location:CGPointMake(-18, 0) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"S-" global:false location:CGPointMake(18, -60) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"S+" global:false location:CGPointMake(18, 0) direction:kSPISignalDirectionInputOutput];
  [p addPin:@"SB" global:false location:CGPointMake(0, -72) direction:kSPISignalDirectionInputOutput];
}
