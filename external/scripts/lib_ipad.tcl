#!/opt/local/bin/tclsh

# transforms to XML
source ../external/analogLib.tcl

proc min {args} {
	set x [lindex $args 0]
	foreach arg $args {
		if [expr $arg < $x] {
			set x $arg
		}
	}
	return $x
}

proc max {args} {
	set x [lindex $args 0]
	foreach arg $args {
		if [expr $arg > $x] {
			set x $arg
		}
	}
	return $x
}

proc PinRect {args} {
	# NOT DONE
	global draw
	global pins
	set name [lindex $args 0]
	set type [lindex $args 1]
	set isGlobal [lindex $args 2]
	set direction [lindex $args 3]
	set x1 [lindex [lindex [lindex $args 4] 0] 0]
	set y1 [lindex [lindex [lindex $args 4] 0] 1]
	set x2 [lindex [lindex [lindex $args 4] 1] 0]
	set y2 [lindex [lindex [lindex $args 4] 1] 1]
	puts "PinRect $name $type $isGlobal $direction $x1,$y1 $x2,$y2"
		
	set w [expr abs($x2 - $x1)]
	set h [expr abs($y2 - $y1)]
	set x1 [expr $x1 < $x2 ? $x1 : $x2]
	set y1 [expr $y1 < $y2 ? $y1 : $y2]
	#puts $draw "  \[self configureContext:ctx for:kSPISchematicPin\];"
	#puts $draw "  CGContextStrokePath(ctx);"
	#puts $draw "  CGContextFillRect(ctx, CGRectMake($x1, $y1, $w, $h));"
	
	set cx [expr ($x2 + $x1) / 2]
	set cy [expr ($y2 + $y1) / 2]
	set pinLocation "CGPointMake($cx, $cy)"
	set pinRect "CGRectMake($x1, $y1, $w, $h)"
	lappend pins [list @\"$name\" $isGlobal $pinLocation "kSPISignalDirectionInputOutput" $pinRect]
	# if $args 6 then allowOverride

}

proc InstRect {args} {
	global draw
	global instRect
	set pts [lindex $args 0]
	set x1 [lindex [lindex $pts 0] 0]
	set y1 [lindex [lindex $pts 0] 1]
	set x2 [lindex [lindex $pts 1] 0]
	set y2 [lindex [lindex $pts 1] 1]
	puts "InstRect '$x1' '$y1' '$x2' '$y2'"
	set w [expr abs($x2 - $x1)]
	set h [expr abs($y2 - $y1)]
	puts "InstRect $x1 $y1 $x2 $y2"
	set instRect "CGRectMake($x1, $y1, $w, $h)"
}

proc Rect {args} {
	global draw
	global paths
	set x1 [lindex [lindex [lindex $args 0] 0] 0]
	set y1 [lindex [lindex [lindex $args 0] 0] 1]
	set x2 [lindex [lindex [lindex $args 0] 1] 0]
	set y2 [lindex [lindex [lindex $args 0] 1] 1]
	
	set w [expr abs($x2 - $x1)]
	set h [expr abs($y2 - $y1)]
	set x1 [expr $x1 < $x2 ? $x1 : $x2]
	set y1 [expr $y1 < $y2 ? $y1 : $y2]
	puts $draw "  \[self configureContext:ctx for:kSPISchematicPin\];"
	puts $draw "  CGContextAddRect(ctx, CGRectMake($x1, $y1, $w, $h));"
	puts $draw "  CGContextStrokePath(ctx);"
	puts $paths "  CGPathAddRect(path1, NULL, CGRectMake($x1, $y1, $w, $h));"
}

proc Arc {args} {
	global draw
	global paths
	set x1 [lindex [lindex [lindex $args 0] 0] 0]
	set y1 [lindex [lindex [lindex $args 0] 0] 1]
	set x2 [lindex [lindex [lindex $args 0] 1] 0]
	set y2 [lindex [lindex [lindex $args 0] 1] 1]
	set startAngle [lindex $args 1]
	set stopAngle [lindex $args 2]

	set cx [expr ($x1 + $x2) / 2]
	set cy [expr ($y1 + $y2) / 2]
	set rx [expr abs($x2 - $x1) / 2]
	set ry [expr abs($y2 - $y1) / 2]
	set r $rx
	#if {$rx != $ry} {
	#	error "This is not a circle! $rx != $ry"
	#}

	puts "Arc $x1 $y1 $x2 $y2 $startAngle $stopAngle"
	set pi 3.1415926535897932384626433832795
	set startAngle [expr ($startAngle*$pi)/180.0]
	set stopAngle [expr ($stopAngle*$pi)/180.0]
	
	# That starting point
	if {[expr $startAngle >= 90 && $startAngle <= 270]} {
		set p1x [expr $cx - $r*cos($startAngle)]
	} else {
		set p1x [expr $cx + $r*cos($startAngle)]
	}
	if {[expr $startAngle >= 0 && $startAngle <= 180]} {
		set p1y [expr $cy + $r*sin($startAngle)]
	} else {
		set p1y [expr $cy - $r*sin($startAngle)]
	}
	# The final point
	if {[expr $stopAngle >= 90 && $stopAngle <= 270]} {
		set p2x [expr $cx - $r*cos($stopAngle)]
	} else {
		set p2x [expr $cx + $r*cos($stopAngle)]
	}
	if {[expr $stopAngle >= 0 && $stopAngle <= 180]} {
		set p2y [expr $cy + $r*sin($stopAngle)]
	} else {
		set p2y [expr $cy - $r*sin($stopAngle)]
	}
	
	set p1x [expr abs($p1x) < 0.001 ? 0.0 : [format "%.6g" $p1x]]
	set p1y [expr abs($p1y) < 0.001 ? 0.0 : [format "%.6g" $p1y]]
	set p2x [expr abs($p2x) < 0.001 ? 0.0 : [format "%.6g" $p2x]]
	set p2y [expr abs($p2y) < 0.001 ? 0.0 : [format "%.6g" $p2y]]
	
	puts $draw "  \[self configureContext:ctx for:kSPISchematicWire\];"
	puts $draw "  CGContextAddArc(ctx, $cx, $cy, $r, $startAngle, $stopAngle, 0);"
	puts $draw "  CGContextStrokePath(ctx);"
	#puts $paths "  CGPathCloseSubpath(path1);"
	puts $paths "  CGPathMoveToPoint(path1, NULL, $p1x, $p1y);"
	puts $paths "  CGPathAddArc(path1, NULL, $cx, $cy, $r, $startAngle, $stopAngle, 0);"
	#puts $paths "  CGPathCloseSubpath(path1);"
	#puts $draw "<path class=\"wire\" d=\"M$p1x,$p1y A$rx,$ry 0 0,1 $p2x,$p2y\" />"
	#puts $draw "<path class=\"wire\" d=\"M$x1,$y1 C$x1,[expr $y1+10] $x2,[expr $y2+10] $x2,$y2 \" />"

}

proc Line {args} {
	global draw
	global paths
	set x1 [lindex [lindex $args 0] 0]
	set y1 [lindex [lindex $args 0] 1]
	# WIRE
	puts $draw "  \[self configureContext:ctx for:kSPISchematicWire\];"
	puts $draw "  CGContextMoveToPoint(ctx, $x1, $y1);"
	puts $paths "  CGPathMoveToPoint(path1, NULL, $x1, $y1);"
	for {set i 1} {$i<[llength $args]} {incr i} {
		set x [lindex [lindex $args $i] 0]
		set y [lindex [lindex $args $i] 1]
		puts $draw "  CGContextAddLineToPoint(ctx, $x, $y);"
		puts $paths "  CGPathAddLineToPoint(path1, NULL, $x, $y);"
	}
	puts $draw "  CGContextStrokePath(ctx);"
}

proc Polygon {args} {
	global draw
	global paths
	set x1 [lindex [lindex $args 0] 0]
	set y1 [lindex [lindex $args 0] 1]
	# WIRE
	puts $draw "  \[self configureContext:ctx for:kSPISchematicWire\];"
	puts $draw "  CGContextMoveToPoint(ctx, $x1, $y1);"
	puts $paths "  CGPathMoveToPoint(path1, NULL, $x1, $y1);"	
	for {set i 1} {$i<[llength $args]} {incr i} {
		set x [lindex [lindex $args $i] 0]
		set y [lindex [lindex $args $i] 1]
		puts $draw "  CGContextAddLineToPoint(ctx, $x, $y);"
		puts $paths "  CGPathAddLineToPoint(path1, NULL, $x, $y);"
	}
	puts $draw "  CGContextClosePath(ctx);"
	puts $draw "  CGContextStrokePath(ctx);"
	puts $paths "  CGPathAddLineToPoint(path1, NULL, $x1, $y1);"
	#puts $paths "  CGPathCloseSubpath(path1);"
}

proc Ellipse {args} {
	global draw
	global paths
	set x1 [lindex [lindex [lindex $args 0] 0] 0]
	set y1 [lindex [lindex [lindex $args 0] 0] 1]
	set x2 [lindex [lindex [lindex $args 0] 1] 0]
	set y2 [lindex [lindex [lindex $args 0] 1] 1]
	set width [expr abs($x2 - $x1)]
	set height [expr abs($y2 - $y1)]
	puts "Ellipse $x1 $y1 $x2 $y2"	
	# TODO: This works for circles but I'm not convinced it works generally
	set cx [expr ($x1 + $x2) / 2]
	set cy [expr ($y1 + $y2) / 2]
	#set height [expr abs($y2 - $y1)]
	#set width [expr abs($x2 - $x1)]
	#set rx [expr sqrt(pow($height,2) + pow($width,2)) / 2]
	#set ry $rx
	set rx [expr abs($x2 - $x1) / 2]
	set ry [expr abs($y2 - $y1) / 2]
	puts $draw "  \[self configureContext:ctx for:kSPISchematicWire\];"
	puts $draw "  CGContextAddEllipseInRect(ctx, CGRectMake($x1, $y1, $width, $height));"
	puts $draw "  CGContextStrokePath(ctx);"
	puts $paths "  CGPathAddEllipseInRect(path1, NULL, CGRectMake($x1, $y1, $width, $height));"
}

proc Path1 {args} {
	global draw
	global paths
	puts -nonewline "Line "
	set stroke [lindex $args 0]
	puts $draw "  \[self configureContext:ctx for:kSPISchematicWire\];"
	set points [lindex $args 1]
	set x1 [lindex [lindex $args 0] 0]
	set y1 [lindex [lindex $args 0] 1]
	puts $draw "  CGContextMoveToPoint(ctx, $x1, $y1);"
	puts $paths "  CGPathMoveToPoint(path1, NULL, $x1, $y1);"
	
	for {set i 1} {$i<[llength $points]} {incr i} {
		set x [lindex [lindex $args $i] 0]
		set y [lindex [lindex $args $i] 1]
		puts $draw "  CGContextAddLineToPoint(ctx, $x, $y);"
		puts $paths "  CGPathAddLineToPoint(path1, NULL, $x, $y);"
	}
	puts $draw "  CGContextClosePath(ctx);"
	puts $draw "  CGContextStrokePath(ctx);"
	puts $paths "  CGPathAddLineToPoint(path1, NULL, $x1, $y1);"
	#puts $paths "  CGPathCloseSubpath(path1);"
}

proc Path1 {args} {
	# NOT DONE
	puts "Path1: UNHANDLED"
}

proc eval_text {class args} {
	# NOT DONE: This does not handle the upper/lower/center thing right
	set args [lindex $args 0]
	puts "eval_text $class, $args, [lindex $args 0]"
	global draw
	set cdsTerm [lindex $args 0]
	#regexp {cdsTerm\("(.*)"\)} $cdsTerm m0 m1
	#set name $m1
	set x1 [lindex [lindex $args 1] 0]
	set y1 [lindex [lindex $args 1] 1]
	set anchor [lindex $args 2]
	variable anchor_val
	variable dy ""
	if {$anchor == "lowerLeft"} {
		set anchor_val "start"
	} elseif {$anchor == "lowerRight"} {
		set anchor_val "end"
	} elseif {$anchor == "centerCenter"} {
		set anchor_val "middle"
	} elseif {$anchor == "upperLeft"} {
		set anchor_val "start"
		set dy "dy=\"8+\""
	} {
		error "unrecognized alignment: $anchor"
	}
	#puts $draw "  CGContextShowText(ctx, \"$cdsTerm\", )"
	#puts $draw "<text class=\"$class\" $dy text-anchor=\"$anchor_val\" x=\"$x1\" y=\"$y1\" >\
		$cdsTerm</text>"
	puts "EvalTextTerm $cdsTerm $x1 $y1"	
}

proc EvalTextTerm {args} {
	global pin_labels
	#set args [lindex $args 0]
	puts "EvalTextTerm $args, [lindex $args 0]"
	set cdsTerm [lindex $args 0]
	variable matchresult
	variable term_name
	if {[regexp -nocase {cdsTerm\("(.*)"\)} $cdsTerm matchresult term_name]} {
	  puts $matchresult
	} else {
	  error "my regex could not match the subject string"
	}
	set x1 [lindex [lindex $args 1] 0]
	set y1 [lindex [lindex $args 1] 1]
	set anchor [lindex $args 2]
	variable anchor_val
	if {$anchor == "lowerLeft"} {
		set anchor_val "kSPITextAnchorLowerLeft"
	} elseif {$anchor == "lowerRight"} {
		set anchor_val "kSPITextAnchorLowerRight"
	} elseif {$anchor == "centerCenter"} {
		set anchor_val "kSPITextAnchorCenterCenter"
	} elseif {$anchor == "upperLeft"} {
		set anchor_val "kSPITextAnchorUpperLeft"
	} {
		error "unrecognized alignment: $anchor"
	}
	set labelLocation "CGPointMake($x1, $y1)"
	lappend pin_labels [list @\"$term_name\" $labelLocation $anchor_val]
}

proc EvalTextParam {args} {
	eval_text "param" $args
	global param_labels
	#set args [lindex $args 0]
	set cdsTerm [lindex $args 0]
	variable matchresult
	variable term_name
	if {[regexp -nocase {cdsParam\((.*)\)} $cdsTerm matchresult term_name]} {
	  puts $matchresult
	} else {
	  error "my regex could not match the subject string"
	}
	set x1 [lindex [lindex $args 1] 0]
	set y1 [lindex [lindex $args 1] 1]
	set anchor [lindex $args 2]
	variable anchor_val
	if {$anchor == "lowerLeft"} {
		set anchor_val "kSPITextAnchorLowerLeft"
	} elseif {$anchor == "lowerRight"} {
		set anchor_val "kSPITextAnchorLowerRight"
	} elseif {$anchor == "centerCenter"} {
		set anchor_val "kSPITextAnchorCenterCenter"
	} elseif {$anchor == "upperLeft"} {
		set anchor_val "kSPITextAnchorUpperLeft"
	} {
		error "unrecognized alignment: $anchor"
	}
	set labelLocation "CGPointMake($x1, $y1)"
	lappend param_labels [list $labelLocation $anchor_val]
}

proc EvalTextName {args} {
	eval_text "instName" $args
	global instName
	#set args [lindex $args 0]
	set cdsTerm [lindex $args 0]
	variable matchresult
	variable term_name
	if {[regexp -nocase {cdsName\((.*)\)} $cdsTerm matchresult term_name]} {
	  puts $matchresult
	} else {
	  error "my regex could not match the subject string"
	}
	set x1 [lindex [lindex $args 1] 0]
	set y1 [lindex [lindex $args 1] 1]
	set anchor [lindex $args 2]
	variable anchor_val
	if {$anchor == "lowerLeft"} {
		set anchor_val "kSPITextAnchorLowerLeft"
	} elseif {$anchor == "lowerRight"} {
		set anchor_val "kSPITextAnchorLowerRight"
	} elseif {$anchor == "centerCenter"} {
		set anchor_val "kSPITextAnchorCenterCenter"
	} elseif {$anchor == "upperLeft"} {
		set anchor_val "kSPITextAnchorUpperLeft"
	} {
		error "unrecognized alignment: $anchor"
	}
	set labelLocation "CGPointMake($x1, $y1)"
	lappend instName $labelLocation
	lappend instName $anchor_val
}

proc Prop {type name value} {
	puts "Prop $type $name $value"
}


set symbols [slDefineanalogLibSymbols]

set paths [open "/Users/rvanmelle/brierwood/ispice/generated/build_primitives.h" "w"]
set draw [open "/Users/rvanmelle/brierwood/ispice/generated/draw_primitives.h" "w"]
set defs [open "/Users/rvanmelle/brierwood/ispice/generated/primitive_defs.h" "w"]
set soup [open "/Users/rvanmelle/brierwood/ispice/generated/schematic_soup.h" "w"]
set lib [open "/Users/rvanmelle/brierwood/ispice/generated/build_primitive_lib.h" "w"]
set lookup [open "/Users/rvanmelle/brierwood/ispice/generated/primitive_device_lookup.h" "w"]
set instRect ""
set pins ""
set pin_labels ""
set instName ""
set param_labels ""

set use_block ""
set index 0
set row 8
set width 120
set height 100
foreach sym $symbols {
	set x [expr (($index % $row) + 0.5) * $width]
	set y [expr (($index / $row) + 1) * $height]
	set pins [list]
	set pin_labels [list]
	set param_labels [list]
	set instName [list]

	set name [lindex $sym 0]
	set stop_list [lindex $sym 1]
	set type [lindex $sym 2]
	set device_type "kSPIPrimitiveDeviceType_$name"
	puts $lookup "\[self addDeviceMapping:\"$name\" to:$device_type\];"
	puts $paths "{"
	puts $paths "  CGMutablePathRef path1 = CGPathCreateMutable();"
	puts $paths "  CGMutablePathRef path2 = CGPathCreateMutable();"
	puts $defs "$device_type,"
	puts $soup "\[schematic addInstance:$device_type x:$x y:$y\];"
	#puts $lib "\[SPIPrimitive addPrimitive:$device_type name:@\"$name\"\];"
	puts $draw "case $device_type:"


	puts "$name $stop_list $type"
	for {set i 3} {$i<[llength $sym]} {incr i} {
		set shape [lindex $sym $i]	
		puts "EVAL $shape"
		eval $shape
	}
	puts $draw "  break;"
	puts $lib "{"
	puts $lib "  SPIPrimitive *p = \[SPIPrimitive addPrimitive:$device_type name:@\"$name\" rect:$instRect\];"
	if [llength $instName] {
		set loc [lindex $instName 0]
		set anchor [lindex $instName 1]
		puts $lib "  \[p addInstanceLabel:$loc anchor:$anchor\];"
	} 
	foreach pin $pins {
		set name [lindex $pin 0]
		set isGlobal [lindex $pin 1]
		set pinLocation [lindex $pin 2]
		set direction [lindex $pin 3]
		set pinRect [lindex $pin 4]
		puts $lib "  \[p addPin:$name global:$isGlobal location:$pinLocation direction:$direction\];"
		puts $paths "  CGPathAddRect(path2, NULL, $pinRect);"
	}
	# lappend pin_labels [list @\"$term_name\" $labelLocation $anchor_val]
	foreach label $pin_labels {
		set name [lindex $label 0]
		set loc [lindex $label 1]
		set anchor [lindex $label 2]
		puts $lib "  \[p addLabelToPin:$name text:$name anchor:$anchor location:$loc\];"
	}
	#foreach label $param_labels {
	#	set loc [lindex $label 0]
	#	set anchor [lindex $label 1]
	#	puts $lib "  \[p addParamLabelAtLocation:$loc anchor:$anchor\];"		
	#}

	puts $lib "}"
	puts $paths "  primitivePaths\[$device_type\]\[0\] = CGPathCreateCopy(path1);"
	puts $paths "  primitivePaths\[$device_type\]\[1\] = CGPathCreateCopy(path2);"
	puts $paths "  CGPathRelease(path1);"
	puts $paths "  CGPathRelease(path2);"
	puts $paths "}"
	incr index
}

close $draw
close $soup
close $defs
close $lib
close $lookup

#exec cat ../../svg/partial_canvas.svg > ../../svg/canvas.svg
#exec cat output.svg >> ../../svg/canvas.svg