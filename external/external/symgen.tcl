#
# Procs to generate symbol
#
set ns [oa::CdbaNS]
oa::pushNameSpace $ns

set slGlobalScaleValue 1.0
set slScaleValue [expr {$slGlobalScaleValue/[oa::TechGetDefaultDBUPerUU [oa::ViewTypeGet schematicSymbol]]}]
set slScaleValueXX 1.0
array set lay [list \
   device   231 \
   pin      229 \
   text     230 \
   instance 236 \
   annotate 237 \
   border   232 \
   wire     228 \
]
array set purp [list \
   drw   -1 \
   dr1   241 \
   dr7   247 \
   dr8   248 \
   lbl   237 \
]

proc slCreateSheets {} {
   set sizes { { "" 1 } }
   set pageSizes {
   { ASheet    11       8.5      }  
   { ABook     8.5      11       }  
   { Legal     14       8.5      }  
   { BSheet    17       11       }
   { CSheet    22       17       }
   { DSheet    35       22       }
   { ESheet    44       34       }
   { FSheet    68       44       }
   { GSheet    88       68       }
   { A0Sheet   46.75    33       }
   { A1Sheet   33       23.375   }
   { A2Sheet   23.375   16.5     }
   { A3Sheet   16.5     11.75    }
   { A4Sheet   11.75    8.25     }
   { A5Sheet   8.25     5.875    }
   { A4Book    8.25     11.75    }
   }

   proc slListToArrayXX { slList } {
      set slArray [oa::PointArray]
      foreach point $slList {       
         oa::append $slArray [list [slSVXX [lindex $point 0]] [slSVXX [lindex $point 1]]]
      }
      return $slArray
   }
   proc slSVXX { value } {
      global slScaleValueXX
      return [expr $slScaleValueXX*$value]
   }
   proc slPVXX { point } {
      return [list [slSVXX [lindex $point 0]] [slSVXX [lindex $point 1]]]
   }
   proc slBVXX { box } {
      return [list [slPVXX [lindex $box 0]] [slPVXX [lindex $box 1]]]
   }

   proc slCreateCellName { pageName size } {
       return ${pageName}${size}
   }
   proc slCreateBorder { cellName width height spacing } {
      set viewName symbol
      set design [oa::DesignOpen sheets $cellName $viewName [oa::ViewTypeGet schematicSymbol] w]
      set block [oa::BlockCreate $design]
      set innerUpperX [expr $width-$spacing]
      set innerUpperY [expr $height-$spacing]
      set titleWidth 5.875
      set titleHeight 1.375
      oa::RectCreate $block 232 -1 [slBVXX [list [oa::Point 0 0] [oa::Point $width $height] ] ]
      oa::RectCreate $block 232 -1 [slBVXX [list [oa::Point $spacing $spacing] [oa::Point $innerUpperX $innerUpperY] ] ]
      slCreateXAxis $block 0 0 $width $spacing
      slCreateXAxis $block 0 $innerUpperY $width $spacing
      slCreateYAxis $block 0 0 $height $spacing
      slCreateYAxis $block $innerUpperX 0 $height $spacing
      set titleX [expr $innerUpperX-$titleWidth]
      set titleY [expr $spacing+$titleHeight]
      oa::RectCreate $block 236 -1 [slBVXX [list [oa::Point $titleX $spacing] [oa::Point $innerUpperX $titleY]]]
      set tf [oa::Transform $titleX $spacing R0]
      oa::ScalarInstCreate $block sheets Title symbol $tf
      oa::StringPropCreate $design nlAction ignore
      oa::StringPropCreate $design schType border
      oa::BooleanPropCreate $design lvsIgnore true
      oa::save $design
      oa::close $design		
   }
   proc slCreateXAxis { block x y width spacing } {
      set threshold 4
      set div 2
      while { [expr $width/$div] > $threshold } {
         incr div 2
      }
      set size [expr double($width)/double($div)]
      set y2 [expr $y+$spacing]
      for { set tick 1 } { $tick<$div } { incr tick } {
         set tickMark [expr $x+($tick*$size)]
         oa::LineCreate $block 232 -1 [slListToArrayXX [list [ list $tickMark $y] [list $tickMark $y2]] ]
      }
      set y3 [expr $y+(double($spacing)/2.0)]
      set delta [expr (double($size)/2.0)]
      for { set label 1; set tickMark [expr $x+$delta]} { $label <= $div } \
         { incr label; set tickMark [expr $tickMark+$size] } {
         oa::TextCreate $block 230 -1 $label [slPVXX [list $tickMark $y3]] centerCenter R0 stick 0.1 0 1 1		
      }
   }
   proc slCreateYAxis { block x y height spacing } {
      set letter " ABCDEFGHIJKLMNOPQRSTUVWXYZ"
      set threshold 4
      set div 2
      while { [expr $height/$div] > $threshold } {
         incr div 2
      }
      set size [expr double($height)/double($div)]
      set x2 [expr $x+$spacing]
      for { set tick 1 } { $tick<$div } { incr tick } {
         # ?? add arrow to center of axis, then break up the 2 halfs equally  
         set tickMark [expr $y+($tick*$size)]
         oa::LineCreate $block 232 -1 [slListToArrayXX [list [ list $x $tickMark] [list $x2 $tickMark] ] ]
      }
      set x3 [expr $x+(double($spacing)/2.0)]
      set delta [expr (double($size)/2.0)]
      for { set label 1; set tickMark [expr $y+$delta]} { $label <= $div } \
         { incr label; set tickMark [expr $tickMark+$size] } {
         oa::TextCreate $block 230 -1 $label [slPVXX [list $x3 $tickMark]] centerCenter R0 stick 0.1 0 1 1		
      }
   }
   foreach page $pageSizes {
      foreach size $sizes {
         set pageName [lindex $page 0][lindex $size 0]
         set cellName [slCreateCellName $pageName [lindex $size 0]]
         set w [expr [lindex $page 1]*[lindex $size 1]]
         set h [expr [lindex $page 2]*[lindex $size 1]]
         slCreateBorder $cellName $w $h 0.2
      }
   }
}

proc slGenAllSymbols { libPath } {

   proc slListToArray { slList } {
      set slArray [oa::PointArray]
      foreach point $slList {       
      oa::append $slArray [list [slSV [lindex $point 0]] [slSV [lindex $point 1]]]
      }
      return $slArray
   }

   proc slSV { value } {
      global slScaleValue
      return [expr ($slScaleValue*$value)]
   }
   proc slPV { point } {
      return [list [slSV [lindex $point 0]] [slSV [lindex $point 1]] ]
   }
   proc slBV { box } {
      return [list [slPV [lindex $box 0]] [slPV [lindex $box 1]]]
   }

   proc slCreateLib { name path } {
      if { ![oa::LibExists $path] } {
         set dmLib [dm::createLib $name -path $path]
         set lib [oa::LibFind $name]
         if { [oa::TechExists $lib] } {
            set tech [oa::TechOpen $lib]
         } else {
         set tech [db::importTechFile tech_file.tf -lib $dmLib]
         }
         oa::save $tech
         oa::close $tech
      } else {
         set lib [oa::LibFind $name]
      }
   }

   proc slGenSymbols { libName libPath } {
      global ns
      global lay purp

      set lib [slCreateLib $libName $libPath/$libName]

      source ${libName}.tcl

      foreach obj [slDefine${libName}Symbols] {
         set cellName [lindex $obj 0]
         foreach viewName [lindex $obj 1] {
            set design [oa::DesignOpen $libName $cellName $viewName [oa::ViewTypeGet [lindex $obj 2]] a]
            if { [oa::getTopBlock $design]!="" } {
               set block [oa::getTopBlock $design]
            } else {
               set block [oa::BlockCreate $design]
            }
            set pi 3.1415926535897932384626433832795
            foreach shape [lrange $obj 3 end] {     

          switch [lindex $shape 0] {
             Arc {
                array set d [list ellipseBBox [slBV [lindex $shape 1]] \
                   startAngle [expr {[expr {[lindex $shape 2] / 180.0}] * $pi}] \
                   stopAngle [expr {[expr {[lindex $shape 3] / 180.0}] * $pi}] ]
                oa::ArcCreate $block $lay(device) $purp(drw) $d(ellipseBBox) \
                $d(startAngle) $d(stopAngle)
             }
             Ellipse {
                set bBox [slBV [lindex $shape 1]]
                oa::EllipseCreate $block $lay(device) $purp(drw) $bBox
             }
             PinPoly {
                array set d [list name [lindex $shape 1] sigType [lindex $shape 2] \
                   isGlobal [lindex $shape 3] direction [lindex $shape 4] \
                   points [slListToArray [lrange $shape 5 end]] ]
                set pname [oa::Name $ns $d(name)]
                set pnet [oa::NetCreate $block $pname [oa::SigType $d(sigType)] $d(isGlobal)]
                set pterm [oa::TermCreate $pnet $pname [oa::TermType $d(direction)]]
                set ppin [oa::PinCreate $pterm]
                set pshape [oa::PolygonCreate $block $lay(pin) $purp(drw) $d(points)]
                oa::addToPin $pshape $ppin
                if { $d(isGlobal) } {
                   oa::BooleanPropCreate $pterm isGlobal true
                }
             }
             PinRect {
                array set d [list name [lindex $shape 1] sigType [lindex $shape 2] \
                   isGlobal [lindex $shape 3] direction [lindex $shape 4] \
                   bBox [slBV [lindex $shape 5]] allowOverride [lindex $shape 6] ]
                set pname [oa::Name $ns $d(name)]
                set pnet [oa::NetCreate $block $pname [oa::SigType $d(sigType)] $d(isGlobal)]
                set pterm [oa::TermCreate $pnet $pname [oa::TermType $d(direction)]]
                set ppin [oa::PinCreate $pterm]
                set pshape [oa::RectCreate $block $lay(pin) $purp(drw) $d(bBox)]
                oa::addToPin $pshape $ppin
                if { $d(isGlobal) } {
                   oa::BooleanPropCreate $pterm isGlobal true
                }
                if { $d(allowOverride) != "" } {
                   oa::BooleanPropCreate $pterm allowOverride true
                }
             }
             PinInst {
                array set d [list name [lindex $shape 1] sigType [lindex $shape 2] \
                   isGlobal [lindex $shape 3] direction [lindex $shape 4] \
                   lib [lindex $shape 5] cell [lindex $shape 6] view [lindex $shape 7] \
                   x [slSV [lindex $shape 8]] y [slSV [lindex $shape 9]] orient [lindex $shape 10] \
                   allowOverride [lindex $shape 11] ]
                set pname [oa::Name $ns $d(name)]
                set pnet [oa::NetCreate $block $pname [oa::SigType $d(sigType)] $d(isGlobal)]
                set pterm [oa::TermCreate $pnet $pname [oa::TermType $d(direction)]]
                set ppin [oa::PinCreate $pterm]
                set tf [oa::Transform $d(x) $d(y) $d(orient) ]
                set pshape [oa::ScalarInstCreate $block $d(lib) $d(cell) $d(view) $tf]
                oa::addToPin $pshape $ppin
                if { $d(isGlobal) } {
                   oa::BooleanPropCreate $pterm isGlobal true
                }
                if { $d(allowOverride) != "" } {
                   oa::BooleanPropCreate $pterm allowOverride true
                }
             }            
             Polygon {
                set points [slListToArray [lrange $shape 1 end]]
                oa::PolygonCreate $block $lay(device) $purp(drw) $points
             }
             Line {
                set points [slListToArray [lrange $shape 1 end]]
                oa::LineCreate $block $lay(device) $purp(drw) $points
             }
             Wire {
                set points [slListToArray [lrange $shape 1 end]]
                oa::LineCreate $block $lay(wire) $purp(drw) $points
             }
             WireFat {
                array set d [list width [slSV [lindex $shape 1]] points [slListToArray [lindex $shape 2]] \
                   style [lindex $shape 3] beginExt [slSV [lindex $shape 4]] endExt [slSV [lindex $shape 5]] ]
                oa::PathCreate $block $lay(wire) $purp(drw) $d(width) $d(points) \
                $d(style) $d(beginExt) $d(endExt)
             }
             LineBorder {
                set points [slListToArray [lrange $shape 1 end]]
                oa::LineCreate $block $lay(border) $purp(drw) $points
             }
             Path {
                array set d [list width [slSV [lindex $shape 1]] points [slListToArray [lindex $shape 2]] \
                   style [lindex $shape 3] beginExt [slSV [lindex $shape 4]] endExt [slSV [lindex $shape 5]] ]
                oa::PathCreate $block $lay(device) $purp(drw) $d(width) $d(points) \
                $d(style) $d(beginExt) $d(endExt)
             }
             Path1 {
                array set d [list width [slSV [lindex $shape 1]] points [slListToArray [lindex $shape 2]] \
                   style [lindex $shape 3] beginExt [slSV [lindex $shape 4]] endExt [slSV [lindex $shape 5]] ]
                oa::PathCreate $block $lay(device) $purp(dr1) $d(width) $d(points) \
                $d(style) $d(beginExt) $d(endExt)
             }
             Rect {
                set bBox [slBV [lindex $shape 1]]
                oa::RectCreate $block $lay(device) $purp(drw) $bBox
             }	    
             RectBorder {
                set bBox [slBV [lindex $shape 1]]
                oa::RectCreate $block $lay(border) $purp(drw) $bBox
             }	    
             Text {
                array set d [list text [lindex $shape 1] origin [slPV [lindex $shape 2]] \
                   alignment [lindex $shape 3] orient [lindex $shape 4] font [lindex $shape 5] \
                   height [slSV [lindex $shape 6]] overbar [lindex $shape 7] \
                   visible [lindex $shape 8] drafting [lindex $shape 9]]
                oa::TextCreate $block $lay(text) $purp(drw) $d(text) $d(origin)\
                $d(alignment) $d(orient) $d(font) $d(height) $d(overbar) \
                $d(visible) $d(drafting)					
             }
             InstRect {
                set bBox [slBV [lindex $shape 1]]
                oa::RectCreate $block $lay(instance) $purp(drw) $bBox
             }
             InstPoly {
                set points [slListToArray [lrange $shape 1 end]]
                oa::PolygonCreate $block $lay(instance) $purp(drw) $points
             }
             TermAttrDisplay {
                array set d [list term [oa::Name $ns [lindex $shape 1]] \
                   attribute [lindex $shape 2] origin [slPV [lindex $shape 3]] \
                   alignment [lindex $shape 4] orient [lindex $shape 5] \
                   font [lindex $shape 6] height [slSV [lindex $shape 7]] \
                   format [lindex $shape 8] overbar [lindex $shape 9] \
                   visible [lindex $shape 10] drafting [lindex $shape 11] ]
                oa::AttrDisplayCreate [oa::TermFind $block $d(term)] \
                [oa::AttrType $d(attribute)] $lay(pin) $purp(lbl) \
                $d(origin) $d(alignment) $d(orient) $d(font) $d(height) \
                $d(format) $d(overbar) $d(visible) $d(drafting)
             }
             EvalTextTerm {
                array set d [list text [lindex $shape 1] origin [slPV [lindex $shape 2]] \
                   alignment [lindex $shape 3] orient [lindex $shape 4] \
                   font [lindex $shape 5] height [slSV [lindex $shape 6]] \
                   linkName [lindex $shape 7] overbar [lindex $shape 8] \
                   visible [lindex $shape 9] drafting [lindex $shape 10]]
                oa::EvalTextCreate $block $lay(annotate) $purp(dr8) $d(text) \
                $d(origin) $d(alignment) $d(orient) $d(font) $d(height) \
                $d(linkName) $d(overbar) $d(visible) $d(drafting)
             }
             EvalTextParam {
                array set d [list text [lindex $shape 1] origin [slPV [lindex $shape 2]] \
                   alignment [lindex $shape 3] orient [lindex $shape 4] \
                   font [lindex $shape 5] height [slSV [lindex $shape 6]] \
                   linkName [lindex $shape 7] overbar [lindex $shape 8] \
                   visible [lindex $shape 9] drafting [lindex $shape 10]]
                oa::EvalTextCreate $block $lay(annotate) $purp(drw) $d(text) \
                $d(origin) $d(alignment) $d(orient) $d(font) $d(height) \
                $d(linkName) $d(overbar) $d(visible) $d(drafting)
             }
             EvalTextName {
                array set d [list text [lindex $shape 1] origin [slPV [lindex $shape 2]] \
                   alignment [lindex $shape 3] orient [lindex $shape 4] \
                   font [lindex $shape 5] height [slSV [lindex $shape 6]] \
                   linkName [lindex $shape 7] overbar [lindex $shape 8] \
                   visible [lindex $shape 9] drafting [lindex $shape 10]]
                oa::EvalTextCreate $block $lay(annotate) $purp(dr7) $d(text) \
                $d(origin) $d(alignment) $d(orient) $d(font) $d(height) \
                $d(linkName) $d(overbar) $d(visible) $d(drafting)
             }
             EvalTextAttr {
                array set d [list text [lindex $shape 1] origin [slPV [lindex $shape 2]] \
                   alignment [lindex $shape 3] orient [lindex $shape 4] \
                   font [lindex $shape 5] height [slSV [lindex $shape 6]] \
                   linkName [lindex $shape 7] overbar [lindex $shape 8] \
                   visible [lindex $shape 9] drafting [lindex $shape 10]]
                oa::EvalTextCreate $block $lay(annotate) $purp(lbl) $d(text) \
                $d(origin) $d(alignment) $d(orient) $d(font) $d(height) \
                $d(linkName) $d(overbar) $d(visible) $d(drafting)
             }
             Prop {
                array set d [list type [lindex $shape 1] name [lindex $shape 2] \
                   value [lindex $shape 3] ]
                oa::$d(type)PropCreate $design $d(name) $d(value)
             }
             Instance {
                array set d [list lib [lindex $shape 1] cell [lindex $shape 2] view [lindex $shape 3] \
                   x [slSV [lindex $shape 4]] y [slSV [lindex $shape 5]] rotation [lindex $shape 6] ]
                set tf [oa::Transform $d(x) $d(y) $d(rotation)]
                oa::ScalarInstCreate $block $d(lib) $d(cell) $d(view) $tf
             }
             default {
                puts "Wrong Shape!!!"
             }
          }
       }
       oa::save $design
       oa::close $design		
     }
    }
   }

   proc slGenerateSchematicWithAllSymbols { libName x1 y1 xstep ystep} {
    set lib [oa::LibFind $libName]
    oa::getAccess $lib write 1
    set cvs [oa::getCellViews $lib]
    set design [oa::DesignOpen $libName allSymbols schematic [oa::ViewTypeGet schematic] w]
    if { [oa::getTopBlock $design]!="" } {
       set block [oa::getTopBlock $design]
    } else {
       set block [oa::BlockCreate $design]
    }
    set block [oa::getTopBlock $design]
    set x2 $x1
    set y2 $y1
    while {[set cv [oa::getNext $cvs] ] != ""} {
       set view [oa::getView $cv]
       set viewName [oa::getName $view]
       set type [oa::getName [oa::getViewType $view]]
       set cellName [oa::getName [oa::getCell $cv]]
       if {$type == "schematicSymbol"} {
          oa::TextCreate $block 230 -1 "$cellName/$viewName" [slPV [ list $x2 [expr ($y2+45) ] ] ] centerCenter R0 stick [slSV 8] 0 1 1
          set tf [oa::Transform [slSV $x2] [slSV $y2] R0 ]
          oa::ScalarInstCreate $block $libName $cellName $viewName $tf
          if { $x2 == [expr {$xstep*10+$x1}]} {
             set x2 $x1
             set y2 [expr ($y2+$ystep)]
          } else {
             set x2 [expr ($x2+$xstep)]
          }
       }
    }
   oa::save $design
   oa::close $design	
   }

   puts "Generating basic..."
   slGenSymbols basic $libPath
   source basic_params.tcl
   puts "Generating analogLib..."
   slGenSymbols analogLib $libPath
   source analogLib_params.tcl
   puts "Generating sheets..."
   slGenSymbols sheets $libPath
   #source sheets_params.tcl
   slCreateSheets
   puts "Generating sample..."
   slGenSymbols sample $libPath
#   puts "Generating AllSchematics..."
#   slGenerateSchematicWithAllSymbols analogLib 60 100 160 160
#   slGenerateSchematicWithAllSymbols basic 60 100 160 160
#   slGenerateSchematicWithAllSymbols sheets 20 20 15000 15000
#   slGenerateSchematicWithAllSymbols sample 60 40 300 300
   puts "Done.."
}
