#DisplayResourceTcl
set display [dr::createDisplay display  -maxColors 256 -maxStipples 256 -maxLineStyles 256]

# ------  display  information for the  display  device 'display'. ------

# ------ Color Definitions ------

dr::createColor white -display $display -red 255 -green 255 -blue 255
dr::createColor yellow -display $display -red 255 -green 255 -blue 0
dr::createColor silver -display $display -red 217 -green 230 -blue 255
dr::createColor cream -display $display -red 255 -green 255 -blue 204
dr::createColor pink  -display $display -red 255 -green 191 -blue 242
dr::createColor lime -display $display -red 0 -green 255 -blue 0
dr::createColor tan -display $display -red 255 -green 230 -blue 191
dr::createColor cyan -display $display -red 0 -green 255 -blue 255
dr::createColor cadetBlue -display $display -red 57 -green 191 -blue 255
dr::createColor orange -display $display -red 255 -green 128 -blue 0
dr::createColor red -display $display -red 255 -green 0 -blue 0
dr::createColor purple -display $display -red 153 -green 0 -blue 230
dr::createColor green -display $display -red 0 -green 204 -blue 102
dr::createColor brown -display $display -red 191 -green 64 -blue 38
dr::createColor blue -display $display -red 0 -green 0 -blue 255
dr::createColor slate -display $display -red 140 -green 140 -blue 166
dr::createColor gold -display $display -red 217 -green 204 -blue 0
dr::createColor maroon -display $display -red 230 -green 31 -blue 13
dr::createColor magenta -display $display -red 255 -green 0 -blue 255
dr::createColor voilet -display $display -red 94 -green 0 -blue 230
dr::createColor forest -display $display -red 38 -green 140 -blue 107
dr::createColor chocolate -display $display -red 128 -green 38 -blue 38
dr::createColor navy -display $display -red 51 -green 51 -blue 153
dr::createColor black -display $display -red 0 -green 0 -blue 0
dr::createColor gray -display $display -red 204 -green 204 -blue 217
dr::createColor winColor1 -display $display -red 166 -green 166 -blue 166
dr::createColor winColor2 -display $display -red 115 -green 115 -blue 115
dr::createColor winColor3 -display $display -red 189 -green 204 -blue 204
dr::createColor winColor4 -display $display -red 204 -green 204 -blue 204
dr::createColor winColor5 -display $display -red 199 -green 199 -blue 199
dr::createColor blinkRed -display $display -red 255 -green 0 -blue 0 -blink true
dr::createColor blinkYellow -display $display -red 255 -green 255 -blue 0 -blink true
dr::createColor blinkWhite -display $display -red 255 -green 255 -blue 255 -blink true
dr::createColor TrueColorBright -display $display -red 166 -green 176 -blue 186
dr::createColor TrueColorGhost -display $display -red 160 -green 200 -blue 240

# end of color definitions

# ------ Stipple Definitions ------

dr::createStipple blank -display $display -pattern [list \
                              0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 \
                              0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 \
                              0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 \
                              0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 \
                              0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 \
                              0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 \
                              0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 \
                              0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 \
                              0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 \
                              0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 \
                              0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 \
                              0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 \
                              0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 \
                              0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 \
                              0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 \
                              0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ]
dr::createStipple solid -display $display -pattern [list \
                              1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 \
                              1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 \
                              1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 \
                              1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 \
                              1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 \
                              1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 \
                              1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 \
                              1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 \
                              1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 \
                              1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 \
                              1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 \
                              1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 \
                              1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 \
                              1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 \
                              1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 \
                              1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 ]
dr::createStipple dots -display $display -pattern [list \
                              0 1 0 0 0 1 0 0 0 1 0 0 0 1 0 0 \
                              0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 \
                              0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 \
                              0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 \
                              0 1 0 0 0 1 0 0 0 1 0 0 0 1 0 0 \
                              0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 \
                              0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 \
                              0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 \
                              0 1 0 0 0 1 0 0 0 1 0 0 0 1 0 0 \
                              0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 \
                              0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 \
                              0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 \
                              0 1 0 0 0 1 0 0 0 1 0 0 0 1 0 0 \
                              0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 \
                              0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 \
                              0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ]
dr::createStipple dots1 -display $display -pattern [list \
                              0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 \
                              0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 0 \
                              0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 \
                              1 0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 \
                              0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 \
                              0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 0 \
                              0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 \
                              1 0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 \
                              0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 \
                              0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 0 \
                              0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 \
                              1 0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 \
                              0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 \
                              0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 0 \
                              0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 \
                              1 0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 ]
dr::createStipple hLine -display $display -pattern [list \
                              0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 \
                              0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 \
                              0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 \
                              1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 \
                              0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 \
                              0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 \
                              0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 \
                              1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 \
                              0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 \
                              0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 \
                              0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 \
                              1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 \
                              0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 \
                              0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 \
                              0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 \
                              1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 ]
dr::createStipple vLine -display $display -pattern [list \
                              0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 0 \
                              0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 0 \
                              0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 0 \
                              0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 0 \
                              0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 0 \
                              0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 0 \
                              0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 0 \
                              0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 0 \
                              0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 0 \
                              0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 0 \
                              0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 0 \
                              0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 0 \
                              0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 0 \
                              0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 0 \
                              0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 0 \
                              0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 0 ]
dr::createStipple cross -display $display -pattern [list \
                              1 0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 \
                              0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 \
                              0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 0 \
                              0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 \
                              1 0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 \
                              0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 \
		              0 1 0 1 0 1 1 0 0 1 0 1 0 1 0 1 \
                              0 0 1 0 0 0 0 1 0 0 1 0 0 0 1 0 \
                              1 0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 \
                              0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 \
                              0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 0 \
                              0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 \
                              1 0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 \
                              0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 \
                              0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 0 \
                              0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 ]
dr::createStipple grid -display $display -pattern [list \
                              0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 \
                              0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 \
                              0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 \
                              1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 \
                              0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 \
                              0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 \
                              0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 \
                              1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 \
                              0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 \
                              0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 \
                              0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 \
                              1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 \
                              0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 \
                              0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 \
                              0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 \
                              1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 ]
dr::createStipple slash -display $display -pattern [list \
                              0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 \
                              0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 0 \
                              0 1 0 0 0 1 0 0 0 1 0 0 0 1 0 0 \
                              1 0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 \
                              0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 \
                              0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 0 \
                              0 1 0 0 0 1 0 0 0 1 0 0 0 1 0 0 \
                              1 0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 \
                              0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 \
                              0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 0 \
                              0 1 0 0 0 1 0 0 0 1 0 0 0 1 0 0 \
                              1 0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 \
                              0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 \
                              0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 0 \
                              0 1 0 0 0 1 0 0 0 1 0 0 0 1 0 0 \
                              1 0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 ]
dr::createStipple backSlash -display $display -pattern [list \
                              1 0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 \
                              0 1 0 0 0 1 0 0 0 1 0 0 0 1 0 0 \
                              0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 0 \
                              0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 \
                              1 0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 \
                              0 1 0 0 0 1 0 0 0 1 0 0 0 1 0 0 \
                              0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 0 \
                              0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 \
                              1 0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 \
                              0 1 0 0 0 1 0 0 0 1 0 0 0 1 0 0 \
                              0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 0 \
                              0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 \
                              1 0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 \
                              0 1 0 0 0 1 0 0 0 1 0 0 0 1 0 0 \
                              0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 0 \
                              0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 ]
dr::createStipple hZigZag -display $display -pattern [list \
                              1 1 0 0 0 0 0 0 1 1 0 0 0 0 0 0 \
                              0 0 1 0 0 0 0 0 0 0 1 0 0 0 0 0 \
                              0 0 0 1 1 0 0 0 0 0 0 1 1 0 0 0 \
                              0 0 0 0 0 1 0 0 0 0 0 0 0 1 0 0 \
                              0 0 0 0 0 0 1 1 0 0 0 0 0 0 1 1 \
                              1 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 \
                              0 1 1 0 0 0 0 0 0 1 1 0 0 0 0 0 \
                              0 0 0 1 0 0 0 0 0 0 0 1 0 0 0 0 \
                              0 0 0 0 1 1 0 0 0 0 0 0 1 1 0 0 \
                              0 0 0 0 0 0 1 0 0 0 0 0 0 0 1 0 \
                              1 0 0 0 0 0 0 1 1 0 0 0 0 0 0 1 \
                              0 1 0 0 0 0 0 0 0 1 0 0 0 0 0 0 \
                              0 0 1 1 0 0 0 0 0 0 1 1 0 0 0 0 \
                              0 0 0 0 1 0 0 0 0 0 0 0 1 0 0 0 \
                              0 0 0 0 0 1 1 0 0 0 0 0 0 1 1 0 \
                              0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 1 ]
dr::createStipple vZigZag -display $display -pattern [list \
                              1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 0 \
                              1 0 0 0 0 0 1 0 0 0 0 1 0 0 0 0 \
                              0 1 0 0 0 0 1 0 0 0 0 0 1 0 0 0 \
                              0 0 1 0 0 0 0 1 0 0 0 0 1 0 0 0 \
                              0 0 1 0 0 0 0 0 1 0 0 0 0 1 0 0 \
                              0 0 0 1 0 0 0 0 1 0 0 0 0 0 1 0 \
                              0 0 0 0 1 0 0 0 0 1 0 0 0 0 1 0 \
                              0 0 0 0 1 0 0 0 0 0 1 0 0 0 0 1 \
                              1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 0 \
                              1 0 0 0 0 0 1 0 0 0 0 1 0 0 0 0 \
                              0 1 0 0 0 0 1 0 0 0 0 0 1 0 0 0 \
                              0 0 1 0 0 0 0 1 0 0 0 0 1 0 0 0 \
                              0 0 1 0 0 0 0 0 1 0 0 0 0 1 0 0 \
                              0 0 0 1 0 0 0 0 1 0 0 0 0 0 1 0 \
                              0 0 0 0 1 0 0 0 0 1 0 0 0 0 1 0 \
                              0 0 0 0 1 0 0 0 0 0 1 0 0 0 0 1 ]
dr::createStipple hCurb -display $display -pattern [list \
                              0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 \
                              0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 \
                              0 0 0 1 1 1 1 1 0 0 0 1 1 1 1 1 \
                              0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 \
                              0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 \
                              1 1 1 1 0 0 0 1 1 1 1 1 0 0 0 1 \
                              0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 \
                              0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 \
                              0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 \
                              0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 \
                              0 0 0 1 1 1 1 1 0 0 0 1 1 1 1 1 \
                              0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 \
                              0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 \
                              1 1 1 1 0 0 0 1 1 1 1 1 0 0 0 1 \
                              0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 \
                              0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ]
dr::createStipple vCurb -display $display -pattern [list \
                              0 0 0 0 0 1 0 0 0 0 0 0 0 1 0 0 \
                              0 0 0 0 0 1 0 0 0 0 0 0 0 1 0 0 \
                              0 0 0 0 0 1 0 0 0 0 0 0 0 1 0 0 \
                              0 0 1 1 1 1 0 0 0 0 1 1 1 1 0 0 \
                              0 0 1 0 0 0 0 0 0 0 1 0 0 0 0 0 \
                              0 0 1 0 0 0 0 0 0 0 1 0 0 0 0 0 \
                              0 0 1 0 0 0 0 0 0 0 1 0 0 0 0 0 \
                              0 0 1 1 1 1 0 0 0 0 1 1 1 1 0 0 \
                              0 0 0 0 0 1 0 0 0 0 0 0 0 1 0 0 \
                              0 0 0 0 0 1 0 0 0 0 0 0 0 1 0 0 \
                              0 0 0 0 0 1 0 0 0 0 0 0 0 1 0 0 \
                              0 0 1 1 1 1 0 0 0 0 1 1 1 1 0 0 \
                              0 0 1 0 0 0 0 0 0 0 1 0 0 0 0 0 \
                              0 0 1 0 0 0 0 0 0 0 1 0 0 0 0 0 \
                              0 0 1 0 0 0 0 0 0 0 1 0 0 0 0 0 \
                              0 0 1 1 1 1 0 0 0 0 1 1 1 1 0 0 ]
dr::createStipple brick -display $display -pattern [list \
                              1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 \
                              0 0 1 0 0 0 0 0 0 0 1 0 0 0 0 0 \
                              0 0 1 0 0 0 0 0 0 0 1 0 0 0 0 0 \
                              0 0 1 0 0 0 0 0 0 0 1 0 0 0 0 0 \
                              1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 \
                              0 0 0 0 0 0 1 0 0 0 0 0 0 0 1 0 \
                              0 0 0 0 0 0 1 0 0 0 0 0 0 0 1 0 \
                              0 0 0 0 0 0 1 0 0 0 0 0 0 0 1 0 \
                              1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 \
                              0 0 1 0 0 0 0 0 0 0 1 0 0 0 0 0 \
                              0 0 1 0 0 0 0 0 0 0 1 0 0 0 0 0 \
                              0 0 1 0 0 0 0 0 0 0 1 0 0 0 0 0 \
                              1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 \
                              0 0 0 0 0 0 1 0 0 0 0 0 0 0 1 0 \
                              0 0 0 0 0 0 1 0 0 0 0 0 0 0 1 0 \
                              0 0 0 0 0 0 1 0 0 0 0 0 0 0 1 0 ]
dr::createStipple dagger -display $display -pattern [list \
                              0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 \
                              0 0 1 0 0 0 0 0 0 0 1 0 0 0 0 0 \
                              0 0 1 0 0 0 0 0 0 0 1 0 0 0 0 0 \
                              0 0 1 0 0 0 0 0 0 0 1 0 0 0 0 0 \
                              1 1 1 1 1 0 0 0 1 1 1 1 1 0 0 0 \
                              0 0 1 0 0 0 0 0 0 0 1 0 0 0 0 0 \
                              0 0 1 0 0 0 0 0 0 0 1 0 0 0 0 0 \
                              0 0 1 0 0 0 0 0 0 0 1 0 0 0 0 0 \
                              0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 \
                              0 0 0 0 0 1 0 0 0 0 0 0 0 1 0 0 \
                              0 0 0 0 0 1 0 0 0 0 0 0 0 1 0 0 \
                              0 0 0 0 0 1 0 0 0 0 0 0 0 1 0 0 \
                              0 0 0 1 1 1 1 1 0 0 0 1 1 1 1 1 \
                              0 0 0 0 0 1 0 0 0 0 0 0 0 1 0 0 \
                              0 0 0 0 0 1 0 0 0 0 0 0 0 1 0 0 \
                              0 0 0 0 0 1 0 0 0 0 0 0 0 1 0 0 ]
dr::createStipple triangle -display $display -pattern [list \
                              0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 \
                              0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 \
                              0 0 0 1 0 1 0 0 0 0 0 0 0 0 0 0 \
                              0 0 1 0 0 0 1 0 0 0 0 0 0 0 0 0 \
                              0 1 0 0 0 0 0 1 0 0 0 0 0 0 0 0 \
                              1 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 \
                              0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 \
                              0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 \
                              0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 \
                              0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 \
                              0 0 0 0 0 0 0 0 0 0 1 0 1 0 0 0 \
                              0 0 0 0 0 0 0 0 0 1 0 0 0 1 0 0 \
                              0 0 0 0 0 0 0 0 1 0 0 0 0 0 1 0 \
                              0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 1 \
                              0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 \
                              0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ]
dr::createStipple X -display $display -pattern [list \
                              1 0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 \
                              0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 \
                              0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 0 \
                              0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 \
                              1 0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 \
                              0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 \
                              0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 0 \
                              0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 \
                              1 0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 \
                              0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 \
                              0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 0 \
                              0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 \
                              1 0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 \
                              0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 \
                              0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 0 \
                              0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 ]

# end of stipple definitions

# ------ LineStyle Definitions ------

dr::createLineStyle solid -display $display -width 1 -pattern [list 1 1 1 ]
dr::createLineStyle dashed -display $display -width 1 -pattern [list 1 1 1 1 0 0 ]
dr::createLineStyle dots -display $display -width 1 -pattern [list 1 0 0 ]
dr::createLineStyle dashDot -display $display -width 1 -pattern [list 1 1 1 0 0 1 0 0 0 ]
dr::createLineStyle shortDash -display $display -width 1 -pattern [list 1 1 0 0 ]
dr::createLineStyle doubleDash -display $display -width 1 -pattern [list 1 1 1 1 0 0 1 1 0 0 ]
dr::createLineStyle hidden -display $display -width 1 -pattern [list 1 0 0 0 ]
dr::createLineStyle thickLine -display $display -width 3 -pattern [list 1 1 1 ]

# end of line style definitions

# ------ Packet Definitions ------

#dr::createPacket defaultPacket -display $display -stipple blank \
#                 -lineStyle solid -fill white -outline yellow
dr::createPacket defaultPacket -display $display -stipple blank \
                 -lineStyle solid -fill white -outline red
dr::createPacket background -display $display -stipple blank \
                 -lineStyle solid -fill black -outline white
dr::createPacket grid -display $display -stipple blank \
                 -lineStyle solid -fill slate -outline slate
dr::createPacket grid1 -display $display -stipple blank \
                 -lineStyle solid -fill white -outline white
dr::createPacket axis_white -display $display -stipple blank \
                 -lineStyle solid -fill white -outline white 
dr::createPacket instance -display $display -stipple blank \
                 -lineStyle solid -fill red -outline red 
dr::createPacket Lbl -display $display -stipple blank \
                 -lineStyle solid -fill gold -outline gold 
dr::createPacket prBoundary -display $display -stipple blank \
                 -lineStyle solid -fill purple -outline purple
dr::createPacket prBoundaryBnd -display $display -stipple blank \
                 -lineStyle solid -fill cyan -outline cyan
dr::createPacket prBoundaryLbl -display $display -stipple blank \
                 -lineStyle solid -fill purple -outline purple
dr::createPacket align -display $display -stipple blank \
                 -lineStyle solid -fill tan -outline tan 
dr::createPacket hardFence -display $display -stipple blank \
                 -lineStyle solid -fill red -outline red 
dr::createPacket softFence -display $display -stipple blank \
                 -lineStyle solid -fill yellow -outline yellow
dr::createPacket text -display $display -stipple blank \
                 -lineStyle solid -fill white -outline white
dr::createPacket text1 -display $display -stipple blank \
                 -lineStyle dashed -fill white -outline white 
dr::createPacket text2 -display $display -stipple blank \
                 -lineStyle solid -fill white -outline white 
dr::createPacket border -display $display -stipple blank \
                 -lineStyle solid -fill tan -outline tan 
dr::createPacket device -display $display -stipple blank \
                 -lineStyle solid -fill green -outline green
dr::createPacket green -display $display -stipple blank \
                 -lineStyle solid -fill green -outline green
dr::createPacket device1 -display $display -stipple solid \
                 -lineStyle solid -fill green -outline green
dr::createPacket device2 -display $display -stipple blank \
                 -lineStyle dashed -fill green -outline green
dr::createPacket wire -display $display -stipple solid \
                 -lineStyle solid -fill cadetBlue -outline cadetBlue
dr::createPacket wireLbl -display $display -stipple solid \
                 -lineStyle solid -fill cadetBlue -outline cadetBlue
dr::createPacket wireFlt -display $display -stipple blank \
                 -lineStyle dashed -fill red -outline red
dr::createPacket deviceAnt -display $display -stipple blank \
                 -lineStyle solid -fill yellow -outline yellow
dr::createPacket pinLbl -display $display -stipple blank \
                 -lineStyle solid -fill red -outline red
dr::createPacket pin -display $display -stipple solid \
                 -lineStyle solid -fill red -outline red
dr::createPacket pinAnt -display $display -stipple blank \
                 -lineStyle solid -fill red -outline red
dr::createPacket annotate -display $display -stipple blank \
                 -lineStyle solid -fill orange -outline orange
dr::createPacket annotate1 -display $display -stipple blank \
                 -lineStyle solid -fill pink -outline pink
dr::createPacket annotate2 -display $display -stipple blank \
                 -lineStyle solid -fill lime -outline lime
dr::createPacket annotate3 -display $display -stipple blank \
                 -lineStyle solid -fill cyan -outline cyan
dr::createPacket annotate4 -display $display -stipple blank \
                 -lineStyle solid -fill yellow -outline yellow
dr::createPacket annotate5 -display $display -stipple blank \
                 -lineStyle solid -fill white -outline white
dr::createPacket annotate6 -display $display -stipple blank \
                 -lineStyle solid -fill silver -outline silver
dr::createPacket annotate7 -display $display -stipple blank \
                 -lineStyle solid -fill red -outline red
dr::createPacket annotate8 -display $display -stipple blank \
                 -lineStyle solid -fill tan -outline tan
dr::createPacket annotate9 -display $display -stipple blank \
                 -lineStyle solid -fill green -outline green
dr::createPacket edgeLayer -display $display -stipple blank \
                 -lineStyle solid -fill winColor5 -outline winColor5
dr::createPacket edgeLayerPin -display $display -stipple blank \
                 -lineStyle solid -fill yellow -outline yellow
dr::createPacket snap -display $display -stipple blank \
                 -lineStyle solid -fill yellow -outline yellow
dr::createPacket stretch -display $display -stipple blank \
                 -lineStyle solid -fill yellow -outline yellow
dr::createPacket y0 -display $display -stipple blank \
                 -lineStyle solid -fill yellow -outline yellow
dr::createPacket y1 -display $display -stipple blank \
                 -lineStyle solid -fill red -outline red
dr::createPacket y2 -display $display -stipple blank \
                 -lineStyle solid -fill green -outline green
dr::createPacket y3 -display $display -stipple blank \
                 -lineStyle solid -fill magenta -outline magenta
dr::createPacket y4 -display $display -stipple blank \
                 -lineStyle solid -fill cyan -outline cyan
dr::createPacket y5 -display $display -stipple blank \
                 -lineStyle solid -fill purple -outline purple
dr::createPacket y6 -display $display -stipple blank \
                 -lineStyle solid -fill orange -outline orange
dr::createPacket y7 -display $display -stipple blank \
                 -lineStyle solid -fill gold -outline gold
dr::createPacket y8 -display $display -stipple blank \
                 -lineStyle solid -fill blue -outline blue
dr::createPacket y0Flt -display $display -stipple blank \
                 -lineStyle solid -fill yellow -outline yellow
dr::createPacket y1Flt -display $display -stipple blank \
                 -lineStyle solid -fill red -outline red
dr::createPacket y2Flt -display $display -stipple blank \
                 -lineStyle solid -fill green -outline green
dr::createPacket y3Flt -display $display -stipple blank \
                 -lineStyle solid -fill magenta -outline magenta
dr::createPacket y4Flt -display $display -stipple blank \
                 -lineStyle solid -fill cyan -outline cyan
dr::createPacket y5Flt -display $display -stipple blank \
                 -lineStyle solid -fill purple -outline purple
dr::createPacket y6Flt -display $display -stipple blank \
                 -lineStyle solid -fill orange -outline orange
dr::createPacket y7Flt -display $display -stipple blank \
                 -lineStyle solid -fill gold -outline gold
dr::createPacket y8Flt -display $display -stipple blank \
                 -lineStyle solid -fill blue -outline blue
dr::createPacket y9Flt -display $display -stipple blank \
                 -lineStyle solid -fill silver -outline silver
dr::createPacket hilite -display $display -stipple blank \
                 -lineStyle solid -fill white -outline white
dr::createPacket hilite1 -display $display -stipple blank \
                 -lineStyle solid -fill magenta -outline magenta
dr::createPacket hilite2 -display $display -stipple blank \
                 -lineStyle solid -fill tan -outline tan
dr::createPacket hilite3 -display $display -stipple blank \
                 -lineStyle solid -fill cyan -outline cyan
dr::createPacket hilite4 -display $display -stipple blank \
                 -lineStyle solid -fill orange -outline orange
dr::createPacket hilite5 -display $display -stipple blank \
                 -lineStyle solid -fill lime -outline lime
dr::createPacket hilite6 -display $display -stipple blank \
                 -lineStyle solid -fill orange -outline orange
dr::createPacket hilite7 -display $display -stipple blank \
                 -lineStyle solid -fill cream -outline cream
dr::createPacket hilite8 -display $display -stipple blank \
                 -lineStyle solid -fill magenta -outline magenta
dr::createPacket hilite9 -display $display -stipple blank \
                 -lineStyle solid -fill pink -outline pink
dr::createPacket hilite6 -display $display -stipple blank \
                 -lineStyle solid -fill orange -outline orange
dr::createPacket select -display $display -stipple blank \
                 -lineStyle solid -fill tan -outline tan
dr::createPacket drive -display $display -stipple blank \
                 -lineStyle solid -fill blue -outline blue
dr::createPacket hiz -display $display -stipple blank \
                 -lineStyle solid -fill orange -outline orange
dr::createPacket resist -display $display -stipple blank \
                 -lineStyle solid -fill cyan -outline cyan
dr::createPacket spike -display $display -stipple blank \
                 -lineStyle solid -fill purple -outline purple
dr::createPacket supply -display $display -stipple blank \
                 -lineStyle solid -fill lime -outline lime
dr::createPacket unknown -display $display -stipple blank \
                 -lineStyle solid -fill yellow -outline yellow
dr::createPacket designFlow -display $display -stipple dots \
                 -lineStyle solid -fill blue -outline blue
dr::createPacket designFlow1 -display $display -stipple dots \
                 -lineStyle solid -fill purple -outline purple
dr::createPacket designFlow2 -display $display -stipple dots \
                 -lineStyle solid -fill magenta -outline magenta
dr::createPacket designFlow3 -display $display -stipple dots \
                 -lineStyle solid -fill cyan -outline cyan
dr::createPacket designFlow4 -display $display -stipple dots \
                 -lineStyle solid -fill forest -outline forest
dr::createPacket designFlow5 -display $display -stipple dots \
                 -lineStyle solid -fill green -outline green
dr::createPacket designFlow6 -display $display -stipple dots \
                 -lineStyle solid -fill orange -outline orange
dr::createPacket designFlow7 -display $display -stipple dots \
                 -lineStyle solid -fill yellow -outline yellow
dr::createPacket designFlow8 -display $display -stipple dots \
                 -lineStyle solid -fill brown -outline brown
dr::createPacket designFlow9 -display $display -stipple dots \
                 -lineStyle solid -fill red -outline red
dr::createPacket changedLayerTl0 -display $display -stipple blank \
                 -lineStyle solid -fill red -outline red
dr::createPacket changedLayerTl1 -display $display -stipple blank \
                 -lineStyle solid -fill yellow -outline yellow
dr::createPacket markerAno -display $display -stipple X \
                 -lineStyle solid -fill blue -outline blue
dr::createPacket markerInf -display $display -stipple X \
                 -lineStyle solid -fill green -outline green
dr::createPacket markerAck -display $display -stipple X \
                 -lineStyle solid -fill yellow -outline yellow
dr::createPacket markerWarn -display $display -stipple X \
                 -lineStyle solid -fill blinkYellow -outline blinkYellow
dr::createPacket markerSer -display $display -stipple X \
                 -lineStyle solid -fill orange -outline orange
dr::createPacket markerErr -display $display -stipple X \
                 -lineStyle solid -fill blinkWhite -outline blinkWhite
dr::createPacket markerScr -display $display -stipple X \
                 -lineStyle solid -fill red -outline red
dr::createPacket markerCrt -display $display -stipple X \
                 -lineStyle solid -fill red -outline red
dr::createPacket markerFat -display $display -stipple X \
                 -lineStyle solid -fill blinkRed -outline blinkRed
dr::createPacket Row -display $display -stipple blank \
                 -lineStyle solid -fill cyan -outline cyan
dr::createPacket RowBnd -display $display -stipple blank \
                 -lineStyle solid -fill cyan -outline cyan
dr::createPacket RowLbl -display $display -stipple blank \
                 -lineStyle solid -fill cyan -outline cyan
dr::createPacket Group -display $display -stipple dots \
                 -lineStyle solid -fill green -outline green
dr::createPacket GroupLbl -display $display -stipple blank \
                 -lineStyle solid -fill green -outline green
dr::createPacket GroupBnd -display $display -stipple blank \
                 -lineStyle solid -fill green -outline green
dr::createPacket Cannotoccupy -display $display -stipple X \
                 -lineStyle solid -fill red -outline red
dr::createPacket CannotoccupyBnd -display $display -stipple blank \
                 -lineStyle solid -fill red -outline red
dr::createPacket Canplace -display $display -stipple blank \
                 -lineStyle solid -fill cyan -outline cyan
dr::createPacket Unrouted -display $display -stipple blank \
                 -lineStyle dashed -fill winColor5 -outline winColor5
dr::createPacket UnroutedTrk -display $display -stipple blank \
                 -lineStyle dashed -fill yellow -outline yellow
dr::createPacket Unrouted1 -display $display -stipple blank \
                 -lineStyle dashed -fill brown -outline brown
dr::createPacket Unrouted2 -display $display -stipple blank \
                 -lineStyle dashed -fill red -outline red
dr::createPacket Unrouted3 -display $display -stipple blank \
                 -lineStyle dashed -fill pink -outline pink
dr::createPacket Unrouted4 -display $display -stipple blank \
                 -lineStyle dashed -fill orange -outline orange
dr::createPacket Unrouted5 -display $display -stipple blank \
                 -lineStyle dashed -fill green -outline green
dr::createPacket Unrouted6 -display $display -stipple blank \
                 -lineStyle dashed -fill blue -outline blue
dr::createPacket Unrouted7 -display $display -stipple blank \
                 -lineStyle dashed -fill purple -outline purple
dr::createPacket Unrouted8 -display $display -stipple blank \
                 -lineStyle dashed -fill gold -outline gold
dr::createPacket Unrouted9 -display $display -stipple blank \
                 -lineStyle dashed -fill silver -outline silver
dr::createPacket stroke_white -display $display -stipple blank \
                  -lineStyle solid -fill white -outline white
dr::createPacket select1_yellow -display $display -stipple blank \
                  -lineStyle solid -fill yellow -outline yellow
dr::createPacket select2_red -display $display -stipple blank \
                  -lineStyle solid -fill red -outline red 
dr::createPacket select3_blue -display $display -stipple blank \
                  -lineStyle solid -fill blue -outline blue 
dr::createPacket highlight1_yellow -display $display -stipple blank \
                  -lineStyle solid -fill yellow -outline yellow
dr::createPacket highlight2_red -display $display -stipple blank \
                  -lineStyle solid -fill red -outline red 
dr::createPacket highlight3_green -display $display -stipple blank \
                  -lineStyle solid -fill green -outline green 
dr::createPacket highlight4_blue -display $display -stipple blank \
                  -lineStyle solid -fill blue -outline blue 
dr::createPacket highlight5_purple -display $display -stipple blank \
                  -lineStyle solid -fill purple -outline purple 
dr::createPacket highlight6_cyan -display $display -stipple blank \
                  -lineStyle solid -fill cyan -outline cyan 
dr::createPacket highlight7_white -display $display -stipple blank \
                  -lineStyle solid -fill white -outline white 
dr::createPacket highlight8_gold -display $display -stipple blank \
                  -lineStyle solid -fill gold -outline gold 
dr::createPacket highlight9_maroon -display $display -stipple blank \
                  -lineStyle solid -fill maroon -outline maroon 
dr::createPacket highlight10_gray -display $display -stipple blank \
                  -lineStyle solid -fill gray -outline gray 
dr::createPacket ruler_white -display $display -stipple blank \
                  -lineStyle solid -fill white -outline white 
dr::createPacket active_TrueColorBright -display $display -stipple blank \
                  -lineStyle solid -fill TrueColorBright -outline TrueColorBright 
dr::createPacket runtime_TrueColorGhost -display $display -stipple blank \
                  -lineStyle solid -fill TrueColorGhost -outline TrueColorGhost 
dr::createPacket axis_white -display $display -stipple blank \
                  -lineStyle solid -fill white -outline white
dr::createPacket cursor_white -display $display -stipple blank \
                  -lineStyle solid -fill white -outline white                  
dr::createPacket major_grid -display $display -stipple blank \
                 -lineStyle shortDash -fill slate -outline slate
dr::createPacket minor_grid -display $display -stipple blank \
                 -lineStyle dots -fill white -outline white
dr::createPacket yellow_white -display $display -stipple blank \
                 -lineStyle solid -fill yellow -outline white
dr::createPacket transparent_white -display $display -stipple blank \
                 -lineStyle solid -fill yellow -outline white -fillStyle outline
dr::createPacket deviceLbl -display $display -stipple blank \
                 -lineStyle solid -fill white -outline white
dr::createPacket pinUnc -display $display -stipple cross \
                 -lineStyle solid -fill red -outline red

# end of packet definitions

# ------ PacketAlias Definitions ------
	
dr::createPacketAlias stroke -packet [lindex [dr::getPackets stroke_white -display $display] 0]
dr::createPacketAlias select1 -packet [lindex [dr::getPackets select1_yellow -display $display] 0]
dr::createPacketAlias select2 -packet [lindex [dr::getPackets select2_red -display $display] 0]
dr::createPacketAlias select3 -packet [lindex [dr::getPackets select3_blue -display $display] 0]
dr::createPacketAlias highlight1 -packet [lindex [dr::getPackets highlight1_yellow -display $display] 0]
dr::createPacketAlias highlight2 -packet [lindex [dr::getPackets highlight2_red -display $display] 0]
dr::createPacketAlias highlight3 -packet [lindex [dr::getPackets highlight3_green -display $display] 0]
dr::createPacketAlias highlight4 -packet [lindex [dr::getPackets highlight4_blue -display $display] 0]
dr::createPacketAlias highlight5 -packet [lindex [dr::getPackets highlight5_purple -display $display] 0]
dr::createPacketAlias highlight6 -packet [lindex [dr::getPackets highlight6_cyan -display $display] 0]
dr::createPacketAlias highlight7 -packet [lindex [dr::getPackets highlight7_white -display $display] 0]
dr::createPacketAlias highlight8 -packet [lindex [dr::getPackets highlight8_gold -display $display] 0]
dr::createPacketAlias highlight9 -packet [lindex [dr::getPackets highlight9_maroon -display $display] 0]
dr::createPacketAlias highlight10 -packet [lindex [dr::getPackets highlight10_gray -display $display] 0]
dr::createPacketAlias coordinate_mark -packet [lindex [dr::getPackets yellow_white -display $display] 0]
dr::createPacketAlias ghost_mark -packet [lindex [dr::getPackets transparent_white -display $display] 0]
dr::createPacketAlias ruler -packet [lindex [dr::getPackets ruler_white -display $display] 0]
dr::createPacketAlias active -packet [lindex [dr::getPackets active_TrueColorBright -display $display] 0]
dr::createPacketAlias runtime -packet [lindex [dr::getPackets runtime_TrueColorGhost -display $display] 0]
dr::createPacketAlias axis -packet [lindex [dr::getPackets axis_white -display $display] 0]
dr::createPacketAlias cursor -packet [lindex [dr::getPackets cursor_white -display $display] 0]
dr::createPacketAlias display_major_grid -packet [lindex [dr::getPackets major_grid -display $display] 0]
dr::createPacketAlias display_minor_grid -packet [lindex [dr::getPackets minor_grid -display $display] 0]
dr::createPacketAlias redsolid_S -packet [lindex [dr::getPackets pin -display $display] 0]

# end of PacketAlias definitions

