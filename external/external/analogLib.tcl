#
# Procedures for returning the { symbol hspice } data
#
#
#Layer numbers
# device    231
# pin       229
# text      230
# instance  236
# annotate  237
# border    232
# wire      228
#
#Purposes  
# drw       -1
# dr1       241
# dr7       247
# dr8       248
# lbl       237

proc slDefineanalogLibSymbols { } {
return {
{ cap { symbol hspice } schematicSymbol
   { PinRect MINUS signal false inputOutput { { -4 -64 } { 4 -56 } } }
   { PinRect PLUS signal false inputOutput { { -4 -4 } { 4 4 } } }
   { InstRect { { -20 -60 } { 20 0 } } }
   { Arc { { -14 -56 } { 14 -28} } 45 135 }
   { Line { 0 -60 } { 0 -28 } }
   { Line { -10 -22 } { 10 -22 } }
   { Line { 0 -22 } { 0 0 } }
   { EvalTextTerm cdsTerm("PLUS") {-10 -10} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("MINUS") {-10 -60} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(1) {15 -25} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(2) {15 -40} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(3) {15 -55} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {10 -10} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { Prop String instNamePrefix C }
}
{ cap3 { symbol hspice } schematicSymbol
   { PinRect MINUS signal false inputOutput { { -4 -64 } { 4 -56 } } }
   { PinRect PLUS signal false inputOutput { { -4 -4 } { 4 4 } } }
   { PinRect B signal false inputOutput { { 16 -34 } { 24 -26 } } }
   { InstRect { { -20 -60 } { 20 0 } } }
   { Arc { { -14 -56 } { 14 -28} } 45 135 }
   { Line { 0 -60 } { 0 -28 } }
   { Line { -10 -22 } { 10 -22 } }
   { Line { 0 -22 } { 0 0 } }
   { EvalTextTerm cdsTerm("PLUS") {-10 -10} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("MINUS") {-10 -60} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("B") {24 -38} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(1) {15 -25} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(2) {15 -40} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(3) {15 -55} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {10 -10} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { Prop String instNamePrefix C }
}
{ cap4 { symbol hspice } schematicSymbol
   { PinRect MINUS signal false inputOutput { { -4 -64 } { 4 -56 } } }
   { PinRect PLUS signal false inputOutput { { -4 -4 } { 4 4 } } }
   { PinRect B signal false inputOutput { { 16 -34 } { 24 -26 } } }
   { PinRect T signal false inputOutput { { -24 -34 } { -16 -26 } } }
   { InstRect { { -20 -60 } { 20 0 } } }
   { Arc { { -14 -56 } { 14 -28} } 45 135 }
   { Line { 0 -60 } { 0 -28 } }
   { Line { -10 -22 } { 10 -22 } }
   { Line { 0 -22 } { 0 0 } }
   { EvalTextTerm cdsTerm("PLUS") {-10 -10} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("MINUS") {-10 -60} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("B") {24 -38} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("T") {-24 -38} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(1) {15 -25} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(2) {15 -40} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(3) {15 -55} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {10 -10} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { Prop String instNamePrefix C }
}
{ cccs { symbol hspice } schematicSymbol
   { PinRect MINUS signal false inputOutput { { -4 -64 } { 4 -56 } } }
   { PinRect PLUS signal false inputOutput { { -4 -4 } { 4 4 } } }
   { InstRect { {-20 -60} {20 0} } }
   { Line {0 -18} {0 0} }
   { Line {0 -60} {0 -42} }
   { Line { 0 -18 } { 12 -30 } { 0 -42 } { -12 -30 } {0 -18 } }
   { Line { 0 -36 } { 0 -24 } }
   { Line { -3 -33 } { 0 -36 } { 3 -33 } }
   { EvalTextTerm cdsTerm("MINUS") { -5 -59 } lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("PLUS") { -5 -10 } lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() { 10 -10 } lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(3) { 15 -55 } lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(1) { 15 -25 } lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(2) { 15 -40 } lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { Prop String instNamePrefix F }
   { Prop Boolean lvsIgnore true }
}
{ ccvs { symbol hspice } schematicSymbol
   { PinRect MINUS signal false inputOutput { { -4 -64 } { 4 -56 } } }
   { PinRect PLUS signal false inputOutput { { -4 -4 } { 4 4 } } }
   { InstRect { {-20 -60} {20 0} } }
   { Line {0 -28} {0 -22} }
   { Line {-3 -25} {3 -25} }
   { Line {0 -18} {0 0} }
   { Line {0 -60} {0 -42} }
   { Line {-3 -35} {3 -35} }
   { Line { 0 -18 } { 12 -30 } { 0 -42 } { -12 -30 } {0 -18 } }
   { EvalTextTerm cdsTerm("MINUS") { -5 -59 } lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("PLUS") { -5 -10 } lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() { 10 -10 } lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(3) { 15 -55 } lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(1) { 15 -25 } lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(2) { 15 -40 } lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { Prop String instNamePrefix H }
   { Prop Boolean lvsIgnore true }
}
{ diode { symbol hspice } schematicSymbol
   { PinRect MINUS signal false inputOutput { { -4 -64 } { 4 -56 } } }
   { PinRect PLUS signal false inputOutput { { -4 -4 } { 4 4 } } }
   { InstRect { { -20 -60 } { 20 0 } } }
   { Polygon {-10 -20} {10 -20} {0 -35 } }
   { Line { 0 -60 } { 0 -35 } }
   { Line { -10 -35} { 10 -35 } }
   { Line { 0 -20 } { 0 0 } }
   { EvalTextTerm cdsTerm("PLUS") {-5 -10} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("MINUS") {-5 -59} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(1) {15 -25} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(2) {15 -40} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(3) {15 -55} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {10 -10} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { Prop String instNamePrefix D }
}
{ diode3 { symbol hspice } schematicSymbol
   { PinRect MINUS signal false inputOutput { { -4 -64 } { 4 -56 } } }
   { PinRect PLUS signal false inputOutput { { -4 -4 } { 4 4 } } }
   { PinRect B signal false inputOutput { { 16 -34 } { 24 -26 } } }
   { InstRect { { -20 -60 } { 20 0 } } }
   { Polygon {-10 -20} {10 -20} {0 -35 } }
   { Line { 0 -60 } { 0 -35 } }
   { Line { -10 -35} { 10 -35 } }
   { Line { 0 -20 } { 0 0 } }
   { EvalTextTerm cdsTerm("PLUS") {-5 -10} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("MINUS") {-5 -59} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("B") {24 -38} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(1) {15 -25} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(2) {15 -40} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(3) {15 -55} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {10 -10} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { Prop String instNamePrefix D }
}
{ diode4 { symbol hspice } schematicSymbol
   { PinRect MINUS signal false inputOutput { { -4 -64 } { 4 -56 } } }
   { PinRect PLUS signal false inputOutput { { -4 -4 } { 4 4 } } }
   { PinRect B signal false inputOutput { { 16 -34 } { 24 -26 } } }
   { PinRect T signal false inputOutput { { -24 -34 } { -16 -26 } } }
   { InstRect { { -20 -60 } { 20 0 } } }
   { Polygon {-10 -20} {10 -20} {0 -35 } }
   { Line { 0 -60 } { 0 -35 } }
   { Line { -10 -35} { 10 -35 } }
   { Line { 0 -20 } { 0 0 } }
   { EvalTextTerm cdsTerm("PLUS") {-5 -10} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("MINUS") {-5 -59} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("B") {24 -38} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("T") {-24 -38} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(1) {15 -25} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(2) {15 -40} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(3) {15 -55} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {10 -10} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { Prop String instNamePrefix D }
}
{ fuse { symbol hspice } schematicSymbol
   { PinRect MINUS signal false inputOutput { { -4 -64 } { 4 -56 } } }
   { PinRect PLUS signal false inputOutput { { -4 -4 } { 4 4 } } }
   { InstRect { { -20 -60 } { 20 0 } } }	
   { Line { 0 0 } { 0 -10 } }
   { Line { 0 -60 } { 0 -50 } }
   { Arc { { -10 -30 } { 10 -10 } } 90 270 }
   { Arc { { -10 -50 } { 10 -30 } } 270 90 }
   { EvalTextTerm cdsTerm("PLUS") {-10 -10} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("MINUS") {-10 -60} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(1) {15 -25} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(2) {15 -40} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(3) {15 -55} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {15 -10} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { Prop String instNamePrefix V }
}
{ fuse3 { symbol hspice } schematicSymbol
   { PinRect MINUS signal false inputOutput { { -4 -64 } { 4 -56 } } }
   { PinRect PLUS signal false inputOutput { { -4 -4 } { 4 4 } } }
   { PinRect B signal false inputOutput { { 16 -34 } { 24 -26 } } }
   { InstRect { { -20 -60 } { 20 0 } } }	
   { Line { 0 0 } { 0 -10 } }
   { Line { 0 -60 } { 0 -50 } }
   { Arc { { -10 -30 } { 10 -10 } } 90 270 }
   { Arc { { -10 -50 } { 10 -30 } } 270 90 }
   { EvalTextTerm cdsTerm("PLUS") {-10 -10} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("MINUS") {-10 -60} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("B") {24 -38} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(1) {15 -25} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(2) {15 -40} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(3) {15 -55} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {15 -10} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { Prop String instNamePrefix V }
}
{ fuse4 { symbol hspice } schematicSymbol
   { PinRect MINUS signal false inputOutput { { -4 -64 } { 4 -56 } } }
   { PinRect PLUS signal false inputOutput { { -4 -4 } { 4 4 } } }
   { PinRect B signal false inputOutput { { 16 -34 } { 24 -26 } } }
   { PinRect T signal false inputOutput { { -24 -34 } { -16 -26 } } }
   { InstRect { { -20 -60 } { 20 0 } } }	
   { Line { 0 0 } { 0 -10 } }
   { Line { 0 -60 } { 0 -50 } }
   { Arc { { -10 -30 } { 10 -10 } } 90 270 }
   { Arc { { -10 -50 } { 10 -30 } } 270 90 }
   { EvalTextTerm cdsTerm("PLUS") {-10 -10} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("MINUS") {-10 -60} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("B") {24 -38} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("T") {-24 -38} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(1) {15 -25} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(2) {15 -40} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(3) {15 -55} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {15 -10} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { Prop String instNamePrefix V }
}
{ gnd { symbol hspice } schematicSymbol
   { PinRect gnd! signal true inputOutput { { -4 -4 } { 4 4 } } }
   { InstRect { {-10 -40} {10 0} } }
   { Polygon {-10 -30} {10 -30} {0 -40} }
   { Line {0 -30} {0 0} }
   { Prop String nlAction ignore }
   { Prop Boolean lvsIgnore true }
}
{ idc { symbol hspice } schematicSymbol
   { PinRect MINUS signal false inputOutput { { -4 -64 } { 4 -56 } } }
   { PinRect PLUS signal false inputOutput { { -4 -4 } { 4 4 } } }
   { InstRect { {-20 -60} {20 0} } }
   { Ellipse { {-10 -40} {10 -20} }}
   { Line { 0 -40 } { 0 -60 } }
   { Line { 0 -20 } { 0 0 } }
   { Line { 0 -36 } { 0 -24 } }
   { Line { -3 -33 } { 0 -36 } { 3 -33 } }
   { EvalTextTerm cdsTerm("MINUS") { -5 -59 } lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("PLUS") { -5 -10 } lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() { 10 -10 } lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(2) { 15 -40 } lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(1) { 15 -25 } lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(3) { 15 -55 } lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { Prop String instNamePrefix I }
   { Prop Boolean lvsIgnore true }
}
{ iexp { symbol hspice } schematicSymbol
   { PinRect MINUS signal false inputOutput { { -4 -64 } { 4 -56 } } }
   { PinRect PLUS signal false inputOutput { { -4 -4 } { 4 4 } } }
   { InstRect { {-20 -60} {20 0} } }
   { Arc  { {-35 -44} {-15 -22}} 90 180 }
   { Arc  { {-25 -33} {-5 -11} } 180 270 }
   { Ellipse { {-10 -40} {10 -20} }}
   { Line { 0 -40 } { 0 -60 } }
   { Line { 0 -20 } { 0 0 } }
   { Line { 0 -36 } { 0 -24 } }
   { Line { -3 -33 } { 0 -36 } { 3 -33 } }
   { EvalTextTerm cdsTerm("MINUS") { -5 -59 } lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("PLUS") { -5 -10 } lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() { 10 -10 } lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(2) { 15 -40 } lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(1) { 15 -25 } lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(3) { 15 -55 } lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { Prop String instNamePrefix I }
   { Prop Boolean lvsIgnore true }
}
{ ind { symbol hspice } schematicSymbol
   { PinRect MINUS signal false inputOutput { { -4 -64 } { 4 -56 } } }
   { PinRect PLUS signal false inputOutput { { -4 -4 } { 4 4 } } }
   { InstRect { {-20 -60} {20 0} } }
   { Arc { { -1 -38 } { 7 -30 } } 270 90 }
   { Arc { { -1 -30 } { 7 -22 } } 270 90 }
   { Arc { { -1 -46 } { 7 -38 } } 270 90 }
   { Arc { { -1 -22 } { 7 -14 } } 270 90 }
   { Line { 0 -14 } { 3 -14 } }
   { Line { 0 -60 } { 0 -46 } }
   { Line { 0 -22 } { 3 -22 } }
   { Line { 0 -30 } { 3 -30 } }
   { Line { 0 -46 } { 3 -46 } }
   { Line { 0 -38 } { 3 -38 } }
   { Line { 0 0 } { 0 -14 } }
   { EvalTextTerm cdsTerm("PLUS") {-5 -10} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("MINUS") {-5 -59} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(1) {15 -25} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(2) {15 -40} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(3) {15 -55} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {15 -10} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { Prop String instNamePrefix L }
}
{ ipulse { symbol hspice } schematicSymbol
   { PinRect MINUS signal false inputOutput { { -4 -64 } { 4 -56 } } }
   { PinRect PLUS signal false inputOutput { { -4 -4 } { 4 4 } } }
   { Ellipse { {-10 -40} {10 -20} } }
   { InstRect { {-20 -60} {20 0} } }
   { Line { 0 -40 } { 0 -60 }  }
   { Line { 0 -20 } { 0 0 } }
   { Line { -31 -33 } { -28 -33 } { -28 -27 } { -24 -27 } { -24 -33 } { -20 -33 } { -20 -27 } { -16 -27 } { -16 -33 } { -13 -33 } }
   { Line { 0 -36 } { 0 -24 } }
   { Line { -3 -33 } { 0 -36 } { 3 -33 } }
   { EvalTextTerm cdsTerm("MINUS") { -5 -59 } lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("PLUS") { -5 -10 } lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() { 10 -10 } lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(1) { 15 -25 } lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(2) { 15 -40 } lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(3) { 15 -55 } lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { Prop String instNamePrefix I }
   { Prop Boolean lvsIgnore true }
}
{ ipwl { symbol hspice } schematicSymbol
   { PinRect MINUS signal false inputOutput { { -4 -64 } { 4 -56 } } }
   { PinRect PLUS signal false inputOutput { { -4 -4 } { 4 4 } } }
   { InstRect { {-20 -60} {20 0} } }
   { Ellipse { {-10 -40} {10 -20} } }
   { Line { 0 -40 } { 0 -60 } }
   { Line { 0 -20 } { 0 0 } }
   { Line  {-34 -34} {-30 -34} {-28 -26} {-26 -38} {-23 -38} {-23 -30} {-20 -30} {-20 -26} {-18 -26} {-18 -34} {-16 -34} {-14 -28} }
   { Line { 0 -36 } { 0 -24 } }
   { Line { -3 -33 } { 0 -36 } { 3 -33 } }
   { EvalTextTerm cdsTerm("MINUS") { -5 -59 } lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("PLUS") { -5 -10 } lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() { 10 -10 } lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(1) { 15 -25 } lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(2) { 15 -40 } lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(3) { 15 -55 } lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { Prop String instNamePrefix I }
   { Prop Boolean lvsIgnore true }
}
{ ipwlf { symbol hspice } schematicSymbol
   { PinRect MINUS signal false inputOutput { { -4 -64 } { 4 -56 } } }
   { PinRect PLUS signal false inputOutput { { -4 -4 } { 4 4 } } }
   { InstRect { {-20 -60} {20 0} } }
   { Ellipse { {-10 -40} {10 -20} } }
   { Line { 0 -40 } { 0 -60 } }
   { Line { 0 -20 } { 0 0 } }
   { Line  {-34 -34} {-30 -34} {-28 -26} {-26 -38} {-23 -38} {-23 -30} {-20 -30} {-20 -26} {-18 -26} {-18 -34} {-16 -34} {-14 -28} }
   { Line { 0 -36 } { 0 -24 } }
   { Line { -3 -33 } { 0 -36 } { 3 -33 } }
   { EvalTextTerm cdsTerm("MINUS") { -5 -59 } lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("PLUS") { -5 -10 } lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() { 10 -10 } lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(1) { 15 -25 } lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(2) { 15 -40 } lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(3) { 15 -55 } lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { Prop String instNamePrefix I }
   { Prop Boolean lvsIgnore true }
}
{ isin { symbol hspice } schematicSymbol
   { PinRect MINUS signal false inputOutput { { -4 -64 } { 4 -56 } } }
   { PinRect PLUS signal false inputOutput { { -4 -4 } { 4 4 } } }
   { InstRect { {-20 -60} {20 0} } }
   { Ellipse { {-10 -40} {10 -20} } }
   { Arc  { {-29 -33} {-22 -25}} 0 180 }
   { Arc  { {-22 -33} {-15 -25} } 180 0 }
   { Line { 0 -40 } { 0 -60 } }
   { Line { 0 -20 } { 0 0 } }
   { Line { 0 -36 } { 0 -24 } }
   { Line { -3 -33 } { 0 -36 } { 3 -33 } }
   { EvalTextTerm cdsTerm("MINUS") { -5 -59 } lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("PLUS") { -5 -10 } lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() { 10 -10 } lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(1) { 15 -25 } lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(2) { 15 -40 } lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(3) { 15 -55 } lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { Prop String instNamePrefix I }
   { Prop Boolean lvsIgnore true }
}
{ isource { symbol hspice } schematicSymbol
   { PinRect MINUS signal false inputOutput { { -4 -64 } { 4 -56 } } }
   { PinRect PLUS signal false inputOutput { { -4 -4 } { 4 4 } } }
   { InstRect { {-20 -60} {20 0} } }
   { Ellipse { {-10 -40} {10 -20} } }
   { Line { 0 -40 } { 0 -60 } }
   { Line { 0 -20 } { 0 0 } }
   { Line { 0 -36 } { 0 -24 } }
   { Line { -3 -33 } { 0 -36 } { 3 -33 } }
   { EvalTextTerm cdsTerm("MINUS") { -5 -59 } lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("PLUS") { -5 -10 } lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() { 10 -10 } lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(2) { 15 -40 } lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(1) { 15 -25 } lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(3) { 15 -55 } lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { Prop String instNamePrefix I }
   { Prop Boolean lvsIgnore true }
}
{ makeGlobal { symbol hspice } schematicSymbol
   { PinRect net signal true inputOutput { { -4 -4 } { 4 4 } } allowOverride }
   { InstRect { {-10 0} {10 40} } }
   { Polygon {-10 30} {10 30} {0 40} }
   { Line {0 0} {0 30} }
   { Line {-6 23} {6 23} }
   { Prop String nlAction ignore }
   { Prop Boolean lvsIgnore true }
}
{ mind { symbol hspice } schematicSymbol
   { Line { -10 -25 } { -5 -25 } }
   { Line { 10 -25 } { 10 -20 } }
   { Line { -10 -25 } { -10 -20 } }
   { Line { 10 -25 } { 5 -25 } }
   { Arc { {-11 -41} {11 -19} } 24.62 153.435 }
   { InstRect { {-20 -60} {20 0} } }
   { EvalTextName cdsName() { 0 -10 } centerCenter R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(1) { -5 0 } centerCenter R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(2) { -5 10 } centerCenter R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(3) { -5 20 } centerCenter R0 stick 8 cdsSkillEvalText 0 1 1 }
   { Prop String instNamePrefix K }
}
{ nmos { symbol hspice } schematicSymbol
   { PinRect D signal false inputOutput { {36 26} {44 34} } }
   { PinRect G signal false inputOutput { {-4 -4} {4 4} } }
   { PinRect S signal false inputOutput { {36 -34} {44 -26} } }
   { InstRect { {0 -30} {40 30} } }
   { Line { 40 -30 } { 40 -15 } { 20 -15 } }
   { Line { 15 -15 } { 15 15 } }
   { Line { 15 0 } { 0 0 } }
   { Line { 30 -20 } { 40 -15 } { 30 -10 } }
   { Line { 20 15 } { 20 -15 } }
   { Line { 40 30 } { 40 15 } { 20 15 } }
   { EvalTextTerm cdsTerm("D") {35 31} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("G") {10 6} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("S") {35 -29} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(1) {45 5} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(2) {45 -10} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(3) {45 -25} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {45 20} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { Prop String instNamePrefix M }
}
{ nmos4 { symbol hspice } schematicSymbol
   { PinRect D signal false inputOutput { {36 26} {44 34} } }
   { PinRect G signal false inputOutput { {-4 -4} {4 4} } }
   { PinRect S signal false inputOutput { {36 -34} {44 -26} } }
   { PinRect B signal false inputOutput { { 36 -4 } { 44 4 } } }
   { InstRect { {0 -30} {40 30} } }
   { Line { 40 30 } { 40 15 } { 20 15 } }
   { Line { 20 15 } { 20 -15 } }
   { Line { 30 -20 } { 40 -15 } { 30 -10 } }
   { Line { 15 0 } { 0 0 } }
   { Line { 15 -15 } { 15 15 } }
   { Line { 40 0 } { 20 0 } }
   { Line { 20 -15} {40 -15} {40 -30} }
   { EvalTextTerm cdsTerm("D") {35 31} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("G") {10 6} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("S") {35 -29} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("B") {35 6} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(1) {45 5} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(2) {45 -10} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(3) {45 -25} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {45 20} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { Prop String instNamePrefix M }
}
{ nmos4hv { symbol hspice } schematicSymbol
   { PinRect D signal false inputOutput { {36 26} {44 34} } }
   { PinRect G signal false inputOutput { {-4 -4} {4 4} } }
   { PinRect S signal false inputOutput { {36 -34} {44 -26} } }
   { PinRect B signal false inputOutput { { 36 -4 } { 44 4 } } }
   { InstRect { {0 -30} {40 30} } }
   { Line { 40 30 } { 40 15 } { 20 15 } }
   { Line { 30 -20 } { 40 -15 } { 30 -10 } }
   { Line { 15 0 } { 0 0 } }
   { Line { 15 -15 } { 15 15 } }
   { Line { 40 0 } { 20 0 } }
   { Line { 20 -15} {40 -15} {40 -30} }
   { Path1 6 { {23 15} {23 -15} } truncate 0 0 }
   { EvalTextTerm cdsTerm("D") {35 20} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("G") {15 6} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("S") {35 -29} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("B") {20 6} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(1) {45 5} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(2) {45 -10} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(3) {45 -25} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {45 20} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { Prop String instNamePrefix M }
}
{ nmoshv { symbol hspice } schematicSymbol
   { PinRect D signal false inputOutput { {36 26} {44 34} } }
   { PinRect G signal false inputOutput { {-4 -4} {4 4} } }
   { PinRect S signal false inputOutput { {36 -34} {44 -26} } }
   { InstRect { {0 -30} {40 30} } }
   { Line { 40 -30 } { 40 -15 } { 20 -15 } }
   { Line { 15 -15 } { 15 15 } }
   { Line { 15 0 } { 0 0 } }
   { Line { 30 -20 } { 40 -15 } { 30 -10 } }
   { Line { 40 30 } { 40 15 } { 20 15 } }
   { Path1 6 { {23 15} {23 -15} } truncate 0 0 }
   { EvalTextTerm cdsTerm("D") {35 20} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("G") {15 6} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("S") {35 -29} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(1) {45 5} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(2) {45 -10} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(3) {45 -25} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {45 20} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { Prop String instNamePrefix M }
}
{ npn { symbol hspice } schematicSymbol
   { PinRect E signal false inputOutput { {36 -34} {44 -26} } }
   { PinRect C signal false inputOutput { {36 26} {44 34} } }
   { PinRect B signal false inputOutput { {-4 -4} {4 4} } }
   { InstRect { {0 -30} {40 30} } }
   { Line {35 -5} {40 -15} {29 -20} }
   { Line {0 0} {20 0} }
   { Line {40 -30} {40 -15} {20 -7} } 
   { Line {40 30} {40 15} {20 7} } 
   { Line {20 -15} {20 15} } 
   { EvalTextTerm cdsTerm("C") {35 20} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("B") {20 6} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("E") {35 -28} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(1) {45 5} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(2) {45 -10} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(3) {45 -25} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {45 20} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { Prop String instNamePrefix Q }
}
{ npn4 { symbol hspice } schematicSymbol
   { PinRect E signal false inputOutput { {36 -34} {44 -26} } }
   { PinRect C signal false inputOutput { {36 26} {44 34} } }
   { PinRect B signal false inputOutput { {-4 -4} {4 4} } }
	{ PinRect S signal false inputOutput { {44 -4} {52 4} } }
   { InstRect { {0 -30} {40 30} } }
   { Line {35 -5} {40 -15} {29 -20} }
   { Line {0 0} {20 0} }
   { Line {40 -30} {40 -15} {20 -7} } 
   { Line {40 30} {40 15} {20 7} } 
   { Line {20 -15} {20 15} } 
   { EvalTextTerm cdsTerm("C") {35 20} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("B") {20 6} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("E") {35 -28} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
	{ EvalTextTerm cdsTerm("S") {44 6} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(1) {45 5} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(2) {45 -10} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(3) {45 -25} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {45 20} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { Prop String instNamePrefix Q }
}
{ npn4a { symbol hspice } schematicSymbol
   { PinRect E signal false inputOutput { {36 -34} {44 -26} } }
   { PinRect C signal false inputOutput { {36 26} {44 34} } }
   { PinRect B signal false inputOutput { {-4 -4} {4 4} } }
	{ PinRect S signal false inputOutput { {44 -12} {52 -4} } }
   { InstRect { {0 -30} {40 30} } }
   { Line {35 -5} {40 -15} {29 -20} }
   { Line {0 0} {20 0} }
   { Line {40 -30} {40 -15} {20 -7} } 
   { Line {40 30} {40 15} {20 7} } 
   { Line {20 -15} {20 15} } 
   { EvalTextTerm cdsTerm("C") {35 20} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("B") {20 6} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("E") {35 -28} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
	{ EvalTextTerm cdsTerm("S") {44 -2} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(1) {45 5} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(2) {45 -10} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(3) {45 -25} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {45 20} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { Prop String instNamePrefix Q }
}
{ pcapacitor { symbol hspice } schematicSymbol
   { PinRect MINUS signal false inputOutput { { -4 -64 } { 4 -56 } } }
   { PinRect PLUS signal false inputOutput { { -4 -4 } { 4 4 } } }
   { InstRect { { -20 -60 } { 20 0 } } }
   { Arc { { -14 -56 } { 14 -28} } 45 135 }
   { Line { 0 -60 } { 0 -28 } }
   { Line { -10 -22 } { 10 -22 } }
   { Line { 0 -22 } { 0 0 } }
   { EvalTextTerm cdsTerm("PLUS") {-10 -10} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("MINUS") {-10 -60} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(1) {15 -25} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(2) {15 -40} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(3) {15 -55} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {10 -10} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { Prop String instNamePrefix C }
}
{ pdc { symbol hspice } schematicSymbol
   { PinRect MINUS signal false inputOutput { { -4 -84 } { 4 -76 } } }
   { PinRect PLUS signal false inputOutput { { -4 -4 } { 4 4 } } }
   { InstRect { {-20 -80} {20 0} } }
   { Line {0 0} {0 -12} {6 -14} {-6 -18} {6 -22} {-6 -26} {6 -30} {-6 -34} {0 -36} {0 -48}}
   { Line {0 -50} {0 -56} }
   { Line {-3 -53} {3 -53} }
   { Line {-3 -63} {3 -63} }
   { Line {0 -80} {0 -68} }
   { Ellipse { {-10 -68} {10 -48} } }
   { EvalTextTerm cdsTerm("PLUS") {-10 -10} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("MINUS") {-10 -80} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(1) {15 -35} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(2) {15 -50} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(3) {15 -65} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {5 -10} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { Prop String instNamePrefix P }
   { Prop Boolean lvsIgnore true }
}
{ pdiode { symbol hspice } schematicSymbol
   { PinRect MINUS signal false inputOutput { { -4 -64 } { 4 -56 } } }
   { PinRect PLUS signal false inputOutput { { -4 -4 } { 4 4 } } }
   { InstRect { { -20 -60 } { 20 0 } } }
   { Polygon {-10 -20} {10 -20} {0 -35 } }
   { Line { 0 -60 } { 0 -35 } }
   { Line { -10 -35} { 10 -35 } }
   { Line { 0 -20 } { 0 0 } }
   { EvalTextTerm cdsTerm("PLUS") {-5 -10} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("MINUS") {-5 -59} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(1) {15 -25} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(2) {15 -40} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(3) {15 -55} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {10 -10} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { Prop String instNamePrefix D }
}
{ pmos { symbol hspice } schematicSymbol
   { PinRect D signal false inputOutput { {36 -34} {44 -26} } }
   { PinRect G signal false inputOutput { {-4 -4} {4 4} } }
   { PinRect S signal false inputOutput { {36 26} {44 34} } }
   { InstRect {{0 -30} {40 30}}}
   { Line {0 0} {15 0} }
   { Line {20 -15} {40 -15} {40 -30} }
   { Line {20 -15} {20 15} }
   { Line {20 15} {40 15} {40 30} }
   { Line {15 -15} {15 15} }
   { Line {40 20} {30 15} {40 10}}
   { EvalTextTerm cdsTerm("D") {35 -29} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("G") {10 6} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("S") {35 31} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(1) {45 5} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(2) {45 -10} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(3) {45 -25} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {45 20} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { Prop String instNamePrefix M }
}
{ pmos4 { symbol hspice } schematicSymbol
   { PinRect D signal false inputOutput { {36 -34} {44 -26} } }
   { PinRect G signal false inputOutput { {-4 -4} {4 4} } }
   { PinRect S signal false inputOutput { {36 26} {44 34} } }
   { PinRect B signal false inputOutput { { 36 -4 } { 44 4 } } }
   { InstRect {{0 -30} {40 30} } }
   { Line {15 -15} {15 15}}
   { Line {20 -15} {40 -15} {40 -30} }
   { Line {20 15} {40 15} {40 30} }
   { Line {0 0} {15 0} }
   { Line {37 20} {27 15} {37 10} }
   { Line {20 0} {40 0} }
   { Line {20 -15} {20 15} }
   { EvalTextTerm cdsTerm("D") {35 -29} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("G") {10 6} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("S") {35 31} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("B") {35 6} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(1) {45 5} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(2) {45 -10} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(3) {45 -25} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {45 20} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { Prop String instNamePrefix M }
}
{ pmos4hv { symbol hspice } schematicSymbol
   { PinRect D signal false inputOutput { {36 -34} {44 -26} } }
   { PinRect G signal false inputOutput { {-4 -4} {4 4} } }
   { PinRect S signal false inputOutput { {36 26} {44 34} } }
   { PinRect B signal false inputOutput { { 36 -4 } { 44 4 } } }
   { InstRect {{0 -30} {40 30} } }
   { Line {15 -15} {15 15}}
   { Line {20 -15} {40 -15} {40 -30} }
   { Line {20 15} {40 15} {40 30} }
   { Line {0 0} {15 0} }
   { Line {37 20} {27 15} {37 10} }
   { Line {20 0} {40 0} }
   { Path1 6 { {23 15} {23 -15} } truncate 0 0 }
   { EvalTextTerm cdsTerm("D") {35 -29} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("G") {15 6} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("S") {35 20} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("B") {20 6} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(1) {45 5} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(2) {45 -10} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(3) {45 -25} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {45 20} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { Prop String instNamePrefix M }
}
{ pmoshv { symbol hspice } schematicSymbol
   { PinRect D signal false inputOutput { {36 -34} {44 -26} } }
   { PinRect G signal false inputOutput { {-4 -4} {4 4} } }
   { PinRect S signal false inputOutput { {36 26} {44 34} } }
   { InstRect {{0 -30} {40 30}}}
   { Line {0 0} {15 0} }
   { Line {20 -15} {40 -15} {40 -30} }
   { Line {20 15} {40 15} {40 30} }
   { Line {15 -15} {15 15} }
   { Line {40 20} {30 15} {40 10}}
   { Path1 6 { {23 15} {23 -15} } truncate 0 0 }
   { EvalTextTerm cdsTerm("D") {35 -29} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("G") {15 6} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("S") {35 20} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(1) {45 5} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(2) {45 -10} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(3) {45 -25} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {45 20} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { Prop String instNamePrefix M }
}
{ pnp { symbol hspice } schematicSymbol
   { PinRect E signal false inputOutput { {36 -34} {44 -26} } }
   { PinRect C signal false inputOutput { {36 26} {44 34} } }
   { PinRect B signal false inputOutput { {-4 -4} {4 4} } }
   { InstRect { {0 -30} {40 30} } }
   { Line {34 19} {31 11} {40 8} }
   { Line {0 0} {20 0} }
   { Line {40 -30} {40 -15} {20 -7} } 
   { Line {40 30} {40 15} {20 7} } 
   { Line {20 -15} {20 15} } 
   { EvalTextTerm cdsTerm("C") {35 -29} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("B") {20 6} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("E") {35 21} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(1) {45 5} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(2) {45 -10} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(3) {45 -25} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {45 20} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { Prop String instNamePrefix Q }
}
{ pnp4 { symbol hspice } schematicSymbol
   { PinRect E signal false inputOutput { {36 -34} {44 -26} } }
   { PinRect C signal false inputOutput { {36 26} {44 34} } }
   { PinRect B signal false inputOutput { {-4 -4} {4 4} } }
	{ PinRect S signal false inputOutput { {44 -4} {52 4} } }
   { InstRect { {0 -30} {40 30} } }
   { Line {34 19} {31 11} {40 8} }
   { Line {0 0} {20 0} }
   { Line {40 -30} {40 -15} {20 -7} } 
   { Line {40 30} {40 15} {20 7} } 
   { Line {20 -15} {20 15} } 
   { EvalTextTerm cdsTerm("C") {35 -29} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("B") {20 6} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("E") {35 21} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
	{ EvalTextTerm cdsTerm("S") {44 6} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(1) {45 5} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(2) {45 -10} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(3) {45 -25} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {45 20} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { Prop String instNamePrefix Q }
}
{ pnp4a { symbol hspice } schematicSymbol
   { PinRect E signal false inputOutput { {36 -34} {44 -26} } }
   { PinRect C signal false inputOutput { {36 26} {44 34} } }
   { PinRect B signal false inputOutput { {-4 -4} {4 4} } }
	{ PinRect S signal false inputOutput { {44 -12} {52 -4} } }
   { InstRect { {0 -30} {40 30} } }
   { Line {34 19} {31 11} {40 8} }
   { Line {0 0} {20 0} }
   { Line {40 -30} {40 -15} {20 -7} } 
   { Line {40 30} {40 15} {20 7} } 
   { Line {20 -15} {20 15} } 
   { EvalTextTerm cdsTerm("C") {35 -29} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("B") {20 6} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("E") {35 21} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
	{ EvalTextTerm cdsTerm("S") {44 -2} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(1) {45 5} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(2) {45 -10} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(3) {45 -25} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {45 20} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { Prop String instNamePrefix Q }
}
{ port { symbol hspice } schematicSymbol
   { PinRect MINUS signal false inputOutput { { -4 -84 } { 4 -76 } } }
   { PinRect PLUS signal false inputOutput { { -4 -4 } { 4 4 } } }
   { InstRect { {-20 -80} {20 0} } }
   { Line {0 0} {0 -12} {6 -14} {-6 -18} {6 -22} {-6 -26} {6 -30} {-6 -34} {0 -36} {0 -48}}
   { Line {0 -50} {0 -56} }
   { Line {-3 -53} {3 -53} }
   { Line {-3 -63} {3 -63} }
   { Line {0 -80} {0 -68} }
   { Ellipse { {-10 -68} {10 -48} } }
   { EvalTextTerm cdsTerm("PLUS") {-10 -10} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("MINUS") {-10 -80} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(1) {15 -35} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(2) {15 -50} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(3) {15 -65} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {5 -10} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { Prop String instNamePrefix P }
   { Prop Boolean lvsIgnore true }
}
{ ppulse { symbol hspice } schematicSymbol
   { PinRect MINUS signal false inputOutput { { -4 -84 } { 4 -76 } } }
   { PinRect PLUS signal false inputOutput { { -4 -4 } { 4 4 } } }
   { InstRect { {-20 -80} {20 0} } }
   { Line {-31 -57} {-28 -57} {-28 -51} {-24 -51} {-24 -57} {-20 -57} {-20 -51} {-16 -51} {-16 -57} {-13 -57} }
   { Line {0 0} {0 -12} {6 -14} {-6 -18} {6 -22} {-6 -26} {6 -30} {-6 -34} {0 -36} {0 -48}}
   { Line {0 -50} {0 -56} }
   { Line {-3 -53} {3 -53} }
   { Line {-3 -63} {3 -63} }
   { Line {0 -80} {0 -68} }
   { Ellipse { {-10 -68} {10 -48} } }
   { EvalTextTerm cdsTerm("PLUS") {-10 -10} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("MINUS") {-10 -80} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(1) {15 -35} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(2) {15 -50} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(3) {15 -65} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {5 -10} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { Prop String instNamePrefix P }
   { Prop Boolean lvsIgnore true }
}
{ ppwl { symbol hspice } schematicSymbol
   { PinRect MINUS signal false inputOutput { { -4 -84 } { 4 -76 } } }
   { PinRect PLUS signal false inputOutput { { -4 -4 } { 4 4 } } }
   { InstRect { {-20 -80} {20 0} } }
   { Line {0 0} {0 -12} {6 -14} {-6 -18} {6 -22} {-6 -26} {6 -30} {-6 -34} {0 -36} {0 -48}}
   { Line {0 -50} {0 -56} }
   { Line {-3 -53} {3 -53} }
   { Line {-3 -63} {3 -63} }
   { Line {0 -80} {0 -68} }
   { Line  {-34 -58} {-30 -58} {-28 -50} {-26 -62} {-23 -62} {-23 -54} {-20 -54} {-20 -50} {-18 -50} {-18 -58} {-16 -58} {-14 -52} }
   { Ellipse { {-10 -68} {10 -48} } }
   { EvalTextTerm cdsTerm("PLUS") {-10 -10} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("MINUS") {-10 -80} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(1) {15 -35} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(2) {15 -50} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(3) {15 -65} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {5 -10} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { Prop String instNamePrefix P }
   { Prop Boolean lvsIgnore true }
}
{ ppwlf { symbol hspice } schematicSymbol
   { PinRect MINUS signal false inputOutput { { -4 -84 } { 4 -76 } } }
   { PinRect PLUS signal false inputOutput { { -4 -4 } { 4 4 } } }
   { InstRect { {-20 -80} {20 0} } }
   { Line {0 0} {0 -12} {6 -14} {-6 -18} {6 -22} {-6 -26} {6 -30} {-6 -34} {0 -36} {0 -48}}
   { Line {0 -50} {0 -56} }
   { Line {-3 -53} {3 -53} }
   { Line {-3 -63} {3 -63} }
   { Line {0 -80} {0 -68} }
   { Line  {-34 -58} {-30 -58} {-28 -50} {-26 -62} {-23 -62} {-23 -54} {-20 -54} {-20 -50} {-18 -50} {-18 -58} {-16 -58} {-14 -52} }
   { Ellipse { {-10 -68} {10 -48} } }
   { EvalTextTerm cdsTerm("PLUS") {-10 -10} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("MINUS") {-10 -80} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(1) {15 -35} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(2) {15 -50} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(3) {15 -65} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {5 -10} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { Prop String instNamePrefix P }
   { Prop Boolean lvsIgnore true }
}
{ presistor { symbol hspice } schematicSymbol
   { PinRect MINUS signal false inputOutput { { -4 -64 } { 4 -56 } } }
   { PinRect PLUS signal false inputOutput { { -4 -4 } { 4 4 } } }
   { InstRect { { -20 -60 } { 20 0 } } }	
   { Line { 0 -60 } { 0 -48 } { -10 -45 } { 10 -39 } { -10 -33 } { 10 -27 } { -10 -21 } { 10 -15 } { 0 -12 } { 0 0 } }
   { EvalTextTerm cdsTerm("PLUS") {-10 -10} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("MINUS") {-10 -60} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(1) {15 -25} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(2) {15 -40} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(3) {15 -55} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {15 -10} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { Prop String instNamePrefix R }
}
{ psin { symbol hspice } schematicSymbol
   { PinRect MINUS signal false inputOutput { { -4 -84 } { 4 -76 } } }
   { PinRect PLUS signal false inputOutput { { -4 -4 } { 4 4 } } }
   { InstRect { {-20 -80} {20 0} } }
   { Arc  { {-29 -62} {-22 -54}} 0 180 }
   { Arc  { {-22 -62} {-15 -54} } 180 0 }
   { Line {0 0} {0 -12} {6 -14} {-6 -18} {6 -22} {-6 -26} {6 -30} {-6 -34} {0 -36} {0 -48}}
   { Line {0 -50} {0 -56} }
   { Line {-3 -53} {3 -53} }
   { Line {-3 -63} {3 -63} }
   { Line {0 -80} {0 -68} }
   { Ellipse { {-10 -68} {10 -48} } }
   { EvalTextTerm cdsTerm("PLUS") {-10 -10} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("MINUS") {-10 -80} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(1) {15 -35} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(2) {15 -50} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(3) {15 -65} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {5 -10} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { Prop String instNamePrefix P }
   { Prop Boolean lvsIgnore true }
}
{ res { symbol hspice } schematicSymbol
   { PinRect MINUS signal false inputOutput { { -4 -64 } { 4 -56 } } }
   { PinRect PLUS signal false inputOutput { { -4 -4 } { 4 4 } } }
   { InstRect { { -20 -60 } { 20 0 } } }	
   { Line { 0 -60 } { 0 -48 } { -10 -45 } { 10 -39 } { -10 -33 } { 10 -27 } { -10 -21 } { 10 -15 } { 0 -12 } { 0 0 } }
   { EvalTextTerm cdsTerm("PLUS") {-10 -10} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("MINUS") {-10 -60} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(1) {15 -25} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(2) {15 -40} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(3) {15 -55} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {15 -10} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { Prop String instNamePrefix R }
}
{ res3 { symbol hspice } schematicSymbol
   { PinRect MINUS signal false inputOutput { { -4 -64 } { 4 -56 } } }
   { PinRect PLUS signal false inputOutput { { -4 -4 } { 4 4 } } }
   { PinRect B signal false inputOutput { { 16 -34 } { 24 -26 } } }
   { InstRect { { -20 -60 } { 20 0 } } }	
   { Line { 0 -60 } { 0 -48 } { -10 -45 } { 10 -39 } { -10 -33 } { 10 -27 } { -10 -21 } { 10 -15 } { 0 -12 } { 0 0 } }
   { EvalTextTerm cdsTerm("PLUS") {-10 -10} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("MINUS") {-10 -60} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("B") {24 -38} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(1) {15 -25} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(2) {15 -40} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(3) {15 -55} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {15 -10} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { Prop String instNamePrefix R }
}
{ res4 { symbol hspice } schematicSymbol
   { PinRect MINUS signal false inputOutput { { -4 -64 } { 4 -56 } } }
   { PinRect PLUS signal false inputOutput { { -4 -4 } { 4 4 } } }
   { PinRect B signal false inputOutput { { 16 -34 } { 24 -26 } } }
   { PinRect T signal false inputOutput { { -24 -34 } { -16 -26 } } }
   { InstRect { { -20 -60 } { 20 0 } } }	
   { Line { 0 -60 } { 0 -48 } { -10 -45 } { 10 -39 } { -10 -33 } { 10 -27 } { -10 -21 } { 10 -15 } { 0 -12 } { 0 0 } }
   { EvalTextTerm cdsTerm("PLUS") {-10 -10} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("MINUS") {-10 -60} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("B") {24 -38} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("T") {-24 -38} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(1) {15 -25} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(2) {15 -40} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(3) {15 -55} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {15 -10} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { Prop String instNamePrefix R }
}
{ varactor { symbol hspice } schematicSymbol
   { PinRect MINUS signal false inputOutput { { -4 -64 } { 4 -56 } } }
   { PinRect PLUS signal false inputOutput { { -4 -4 } { 4 4 } } }
   { InstRect { { -20 -60 } { 20 0 } } }
   { Arc { { -14 -56 } { 14 -28} } 45 135 }
   { Line { 0 -60 } { 0 -28 } }
   { Line { -10 -22 } { 10 -22 } }
   { Line { 0 -22 } { 0 0 } }
   { Line { -5 -38} { 5 -18 } }
   { Line { 5 -21} { 5 -18 } { 2 -20 } }
   { EvalTextTerm cdsTerm("PLUS") {-10 -10} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("MINUS") {-10 -60} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(1) {15 -25} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(2) {15 -40} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(3) {15 -55} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {10 -10} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { Prop String instNamePrefix C }
}
{ varactor3 { symbol hspice } schematicSymbol
   { PinRect MINUS signal false inputOutput { { -4 -64 } { 4 -56 } } }
   { PinRect PLUS signal false inputOutput { { -4 -4 } { 4 4 } } }
   { PinRect B signal false inputOutput { { 16 -34 } { 24 -26 } } }
   { InstRect { { -20 -60 } { 20 0 } } }
   { Arc { { -14 -56 } { 14 -28} } 45 135 }
   { Line { 0 -60 } { 0 -28 } }
   { Line { -10 -22 } { 10 -22 } }
   { Line { 0 -22 } { 0 0 } }
   { Line { -5 -38} { 5 -18 } }
   { Line { 5 -21} { 5 -18 } { 2 -20 } }
   { EvalTextTerm cdsTerm("PLUS") {-10 -10} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("MINUS") {-10 -60} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("B") {24 -38} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(1) {15 -25} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(2) {15 -40} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(3) {15 -55} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {10 -10} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { Prop String instNamePrefix C }
}
{ varactor4 { symbol hspice } schematicSymbol
   { PinRect MINUS signal false inputOutput { { -4 -64 } { 4 -56 } } }
   { PinRect PLUS signal false inputOutput { { -4 -4 } { 4 4 } } }
   { PinRect B signal false inputOutput { { 16 -34 } { 24 -26 } } }
   { PinRect T signal false inputOutput { { -24 -34 } { -16 -26 } } }
   { InstRect { { -20 -60 } { 20 0 } } }
   { Arc { { -14 -56 } { 14 -28} } 45 135 }
   { Line { 0 -60 } { 0 -28 } }
   { Line { -10 -22 } { 10 -22 } }
   { Line { 0 -22 } { 0 0 } }
   { Line { -5 -38} { 5 -18 } }
   { Line { 5 -21} { 5 -18 } { 2 -20 } }
   { EvalTextTerm cdsTerm("PLUS") {-10 -10} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("MINUS") {-10 -60} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("B") {24 -38} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("T") {-24 -38} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(1) {15 -25} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(2) {15 -40} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(3) {15 -55} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {10 -10} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { Prop String instNamePrefix C }
}
{ vcc { symbol hspice } schematicSymbol
   { PinRect vcc! signal true inputOutput { { -4 -4 } { 4 4 } } }
   { InstRect { {-25 0} {25 50} } }
   { Line {0 0} {0 40} }
   { Line  {-25 40} {25 40} }
   { Prop String nlAction ignore }
   { Prop Boolean lvsIgnore true }
   { Prop Boolean ignore true }
}
{ vccs { symbol hspice } schematicSymbol
   { PinRect MINUS signal false inputOutput { { -4 -64 } { 4 -56 } } }
   { PinRect PLUS signal false inputOutput { { -4 -4 } { 4 4 } } }
   { PinRect NC+ signal false inputOutput { { -24 -24 } { -16 -16 } } }
   { PinRect NC- signal false inputOutput { { -24 -44 } { -16 -36 } } }
   { InstRect { {-20 -60} {20 0} } }
   { Line {0 -18} {0 0} }
   { Line {0 -60} {0 -42} }
   { Line { 0 -18 } { 12 -30 } { 0 -42 } { -12 -30 } {0 -18 } }
   { Line { 0 -36 } { 0 -24 } }
   { Line { -3 -33 } { 0 -36 } { 3 -33 } }
   { Line { -20 -20 } { -10 -20 } { -6 -24 } }
   { Line { -20 -40 } { -10 -40 } { -6 -36 } }
   { EvalTextTerm cdsTerm("MINUS") { -5 -59 } lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("PLUS") { -5 -10 } lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() { 10 -10 } lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(3) { 15 -55 } lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(1) { 15 -25 } lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(2) { 15 -40 } lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { Prop String instNamePrefix G }
   { Prop Boolean lvsIgnore true }
}
{ vcvs { symbol hspice } schematicSymbol
   { PinRect MINUS signal false inputOutput { { -4 -64 } { 4 -56 } } }
   { PinRect PLUS signal false inputOutput { { -4 -4 } { 4 4 } } }
   { PinRect NC+ signal false inputOutput { { -24 -24 } { -16 -16 } } }
   { PinRect NC- signal false inputOutput { { -24 -44 } { -16 -36 } } }
   { InstRect { {-20 -60} {20 0} } }
   { Line { 0 -18 } { 12 -30 } { 0 -42 } { -12 -30 } {0 -18 } }
   { Line {0 -28} {0 -22} }
   { Line {-3 -25} {3 -25} }
   { Line {0 -18} {0 0} }
   { Line {0 -60} {0 -42} }
   { Line {-3 -35} {3 -35} }
   { Line { -20 -20 } { -10 -20 } { -6 -24 } }
   { Line { -20 -40 } { -10 -40 } { -6 -36 } }
   { EvalTextTerm cdsTerm("MINUS") { -5 -59 } lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("PLUS") { -5 -10 } lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() { 10 -10 } lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(3) { 15 -55 } lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(1) { 15 -25 } lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(2) { 15 -40 } lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { Prop String instNamePrefix E }
   { Prop Boolean lvsIgnore true }
}
{ vdc { symbol hspice } schematicSymbol
   { PinRect MINUS signal false inputOutput { { -4 -64 } { 4 -56 } } }
   { PinRect PLUS signal false inputOutput { { -4 -4 } { 4 4 } } }
   { InstRect { {-20 -60} {20 0} } }
   { Ellipse { {-10 -40} {10 -20} } }
   { Line {0 -28} {0 -22} }
   { Line {-3 -25} {3 -25} }
   { Line {0 -20} {0 0} }
   { Line {0 -60} {0 -40} }
   { Line {-3 -35} {3 -35} }
   { EvalTextTerm cdsTerm("PLUS") {-10 -10} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("MINUS") {-10 -60} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(1) {15 -25} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(2) {15 -40} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(3) {15 -55} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {5 -10} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { Prop String instNamePrefix V }
   { Prop Boolean lvsIgnore true }
}
{ vdd { symbol hspice } schematicSymbol
   { PinRect vdd! signal true inputOutput { { -4 -4 } { 4 4 } } }
   { InstRect { {-25 0} {25 50} } }
   { Line {0 0} {0 40} }
   { Line  {-25 40} {25 40} }
   { Prop String nlAction ignore }
   { Prop Boolean lvsIgnore true }
   { Prop Boolean ignore true }
}
{ vee { symbol hspice } schematicSymbol
   { PinRect vee! signal true inputOutput { { -4 -4 } { 4 4 } } }
   { InstRect { {-25 -50} {25 0} } }
   { Line {-25 -40} {25 -40} }
   { Line {0 -40} {0 0} }
   { Prop String nlAction ignore }
   { Prop Boolean lvsIgnore true }
   { Prop Boolean ignore true }
}
{ vexp { symbol hspice } schematicSymbol
   { PinRect MINUS signal false inputOutput { { -4 -64 } { 4 -56 } } }
   { PinRect PLUS signal false inputOutput { { -4 -4 } { 4 4 } } }
   { InstRect { {-20 -60} {20 0} } }
   { Arc  { {-35 -44} {-15 -22}} 90 180 }
   { Arc  { {-25 -33} {-5 -11} } 180 270 }
   { Line {0 -28} {0 -22} }
   { Line {-3 -25} {3 -25} }
   { Line {0 -20} {0 0} }
   { Line {0 -60} {0 -40} }
   { Line {-3 -35} {3 -35} }
   { Ellipse { {-10 -40} {10 -20} } }
   { EvalTextTerm cdsTerm("PLUS") {-10 -10} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("MINUS") {-10 -60} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(1) {15 -25} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(2) {15 -40} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(3) {15 -55} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {5 -10} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { Prop String instNamePrefix V }
   { Prop Boolean lvsIgnore true }
}
{ vpulse { symbol hspice } schematicSymbol
   { PinRect MINUS signal false inputOutput { { -4 -64 } { 4 -56 } } }
   { PinRect PLUS signal false inputOutput { { -4 -4 } { 4 4 } } }
   { InstRect { {-20 -60} {20 0} } }
   { Line {0 -28} {0 -22} }
   { Line {-3 -35} {3 -35} }
   { Line {0 -20} {0 0} }
   { Line {0 -60} {0 -40} }
   { Line {-3 -25} {3 -25} }
   { Line {-31 -33} {-28 -33} {-28 -27} {-24 -27} {-24 -33} {-20 -33} {-20 -27} {-16 -27} {-16 -33} {-13 -33} }
   { Ellipse { {-10 -40} {10 -20} }}
   { EvalTextTerm cdsTerm("PLUS") {-5 -10} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("MINUS") {-5 -59} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(1) {15 -25} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(2) {15 -40} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(3) {15 -55} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {10 -10} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { Prop String instNamePrefix V }
   { Prop Boolean lvsIgnore true }
}
{ vpwl { symbol hspice } schematicSymbol
   { PinRect MINUS signal false inputOutput { { -4 -64 } { 4 -56 } } }
   { PinRect PLUS signal false inputOutput { { -4 -4 } { 4 4 } } }
   { InstRect { {-20 -60} {20 0} } }
   { Ellipse { {-10 -40} {10 -20} }}
   { Line {0 -28} {0 -22} }
   { Line {-3 -35} {3 -35} }
   { Line {0 -20} {0 0} }
   { Line {0 -60} {0 -40} }
   { Line {-3 -25} {3 -25} }
   { Line  {-34 -34} {-30 -34} {-28 -26} {-26 -38} {-23 -38} {-23 -30} {-20 -30} {-20 -26} {-18 -26} {-18 -34} {-16 -34} {-14 -28} }
   { EvalTextTerm cdsTerm("PLUS") {-5 -10} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("MINUS") {-5 -59} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(1) {15 -25} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(2) {15 -40} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(3) {15 -55} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {10 -10} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { Prop String instNamePrefix V }
   { Prop Boolean lvsIgnore true }
}
{ vpwlf { symbol hspice } schematicSymbol
   { PinRect MINUS signal false inputOutput { { -4 -64 } { 4 -56 } } }
   { PinRect PLUS signal false inputOutput { { -4 -4 } { 4 4 } } }
   { InstRect { {-20 -60} {20 0} } }
   { Ellipse { {-10 -40} {10 -20} } }
   { Line { 0 -28 } { 0 -22 } }
   { Line { 3 -35 } { -3 -35 } }
   { Line { 0 -20 } { 0 0 } }
   { Line { 0 -40 } { 0 -60 } }
   { Line { 3 -25 } { -3 -25 } }
   { Line  {-34 -34} {-30 -34} {-28 -26} {-26 -38} {-23 -38} {-23 -30} {-20 -30} {-20 -26} {-18 -26} {-18 -34} {-16 -34} {-14 -28} }
   { EvalTextTerm cdsTerm("MINUS") { -5 -59 } lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("PLUS") { -5 -10 } lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(1) { 15 -25 } lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(2) { 15 -40 } lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(3) { 15 -55 } lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() { 10 -10 } lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { Prop String instNamePrefix V }
   { Prop Boolean lvsIgnore true }
} 
{ vsin { symbol hspice } schematicSymbol
   { PinRect MINUS signal false inputOutput { { -4 -64 } { 4 -56 } } }
   { PinRect PLUS signal false inputOutput { { -4 -4 } { 4 4 } } }
   { InstRect { {-20 -60} {20 0} } }
   { Arc  { {-29 -33} {-22 -25}} 0 180 }
   { Arc  { {-22 -33} {-15 -25} } 180 0 }
   { Ellipse { {-10 -40} {10 -20} }}
   { Line {0 -28} {0 -22} }
   { Line {-3 -35} {3 -35} }
   { Line {0 -20} {0 0} }
   { Line {0 -60} {0 -40} }
   { Line {-3 -25} {3 -25} }
   { EvalTextTerm cdsTerm("PLUS") {-5 -10} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("MINUS") {-5 -59} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(1) {15 -25} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(2) {15 -40} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(3) {15 -55} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {10 -10} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { Prop String instNamePrefix V }
   { Prop Boolean lvsIgnore true }
}
{ vsource { symbol hspice } schematicSymbol
   { PinRect MINUS signal false inputOutput { { -4 -64 } { 4 -56 } } }
   { PinRect PLUS signal false inputOutput { { -4 -4 } { 4 4 } } }
   { InstRect { {-20 -60} {20 0} } }
   { Line {0 -28} {0 -22} }
   { Line {-3 -25} {3 -25} }
   { Line {0 -20} {0 0} }
   { Line {0 -60} {0 -40} }
   { Line {-3 -35} {3 -35} }
   { Ellipse { {-10 -40} {10 -20} } }
   { EvalTextTerm cdsTerm("PLUS") {-10 -10} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("MINUS") {-10 -60} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(1) {15 -25} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(2) {15 -40} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(3) {15 -55} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {5 -10} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { Prop String instNamePrefix V }
   { Prop Boolean lvsIgnore true }
}
{ vss { symbol hspice } schematicSymbol
   { PinRect vss! signal true inputOutput { { -4 -4 } { 4 4 } } }
   { InstRect { {-25 -50} {25 0} } }
   { Line {-25 -40} {25 -40} }
   { Line {0 -40} {0 0} }
   { Prop String nlAction ignore }
   { Prop Boolean lvsIgnore true }
   { Prop Boolean ignore true }
}
{ xfmr4 { symbol } schematicSymbol
   { PinRect P- signal false inputOutput { { -22 -64 } { -14 -56 } } }
   { PinRect P+ signal false inputOutput { { -22 -4 } { -14 4 } } }
   { PinRect S- signal false inputOutput { { 14 -64 } { 22 -56 } } }
   { PinRect S+ signal false inputOutput { { 14 -4 } { 22 4 } } }
   { InstRect { {-24 -60} {24 0} } }
   { Arc { { -19 -38 } { -11 -30 } } 270 90 }
   { Arc { { -19 -30 } { -11 -22 } } 270 90 }
   { Arc { { -19 -46 } { -11 -38 } } 270 90 }
   { Arc { { -19 -22 } { -11 -14 } } 270 90 }
   { Arc { { 11 -38 } { 19 -30 } } 90 270 }
   { Arc { { 11 -30 } { 19 -22 } } 90 270 }
   { Arc { { 11 -46 } { 19 -38 } } 90 270 }
   { Arc { { 11 -22 } { 19 -14 } } 90 270 }
   { Ellipse { {-12 -10} {-8 -6} }}
   { Ellipse { {8 -10} {12 -6} }}
   { Line { -18 -14 } { -15 -14 } }
   { Line { -18 -60 } { -18 -46 } }
   { Line { -18 -22 } { -15 -22 } }
   { Line { -18 -30 } { -15 -30 } }
   { Line { -18 -46 } { -15 -46 } }
   { Line { -18 -38 } { -15 -38 } }
   { Line { -18 0 } { -18 -14 } }
   { Line { 18 -14 } { 15 -14 } }
   { Line { 18 -60 } { 18 -46 } }
   { Line { 18 -22 } { 15 -22 } }
   { Line { 18 -30 } { 15 -30 } }
   { Line { 18 -46 } { 15 -46 } }
   { Line { 18 -38 } { 15 -38 } }
   { Line { 18 0 } { 18 -14 } }
   { Rect { { -1 -24 } { 1 -22 } } }
   { Rect { { -1 -38 } { 1 -36 } } }
   { EvalTextTerm cdsTerm("P+") {-23 -2} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("P-") {-23 -59} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("S+") {23 -2} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("S-") {23 -59} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(1) {24 -25} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(2) {24 -35} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(3) {24 -45} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {23 -10} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { Prop String instNamePrefix E }
}
{ xfmr5p { symbol } schematicSymbol
   { InstRect { {-24 -60} {24 0} } }
   { PinRect P- signal false inputOutput { { -22 -64 } { -14 -56 } } }
   { PinRect P+ signal false inputOutput { { -22 -4 } { -14 4 } } }
   { PinRect S- signal false inputOutput { { 14 -64 } { 22 -56 } } }
   { PinRect S+ signal false inputOutput { { 14 -4 } { 22 4 } } }
   { PinRect PB signal false inputOutput { { -4 8 } { 4 16 } } }
   { Arc { { -19 -38 } { -11 -30 } } 270 90 }
   { Arc { { -19 -30 } { -11 -22 } } 270 90 }
   { Arc { { -19 -46 } { -11 -38 } } 270 90 }
   { Arc { { -19 -22 } { -11 -14 } } 270 90 }
   { Arc { { 11 -38 } { 19 -30 } } 90 270 }
   { Arc { { 11 -30 } { 19 -22 } } 90 270 }
   { Arc { { 11 -46 } { 19 -38 } } 90 270 }
   { Arc { { 11 -22 } { 19 -14 } } 90 270 }
   { Ellipse { {-12 -10} {-8 -6} }}
   { Ellipse { {8 -10} {12 -6} }}
   { Line { -18 -14 } { -15 -14 } }
   { Line { -18 -60 } { -18 -46 } }
   { Line { -18 -22 } { -15 -22 } }
   { Line { -18 -30 } { -15 -30 } }
   { Line { -18 -46 } { -15 -46 } }
   { Line { -18 -38 } { -15 -38 } }
   { Line { -18 0 } { -18 -14 } }
   { Line { 18 -14 } { 15 -14 } }
   { Line { 18 -60 } { 18 -46 } }
   { Line { 18 -22 } { 15 -22 } }
   { Line { 18 -30 } { 15 -30 } }
   { Line { 18 -46 } { 15 -46 } }
   { Line { 18 -38 } { 15 -38 } }
   { Line { 18 0 } { 18 -14 } }
   { Rect { { -1 -24 } { 1 -22 } } }
   { Rect { { -1 -38 } { 1 -36 } } }
   { EvalTextTerm cdsTerm("P+") {-23 -2} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("PB") {5 12} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("P-") {-23 -59} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("S+") {23 -2} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("S-") {23 -59} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(1) {24 -25} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(2) {24 -35} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(3) {24 -45} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {23 -10} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { Prop String instNamePrefix E }
}
{ xfmr5s { symbol } schematicSymbol
   { InstRect { {-24 -60} {24 0} } }
   { PinRect P- signal false inputOutput { { -22 -64 } { -14 -56 } } }
   { PinRect P+ signal false inputOutput { { -22 -4 } { -14 4 } } }
   { PinRect S- signal false inputOutput { { 14 -64 } { 22 -56 } } }
   { PinRect S+ signal false inputOutput { { 14 -4 } { 22 4 } } }
   { PinRect SB signal false inputOutput { { -4 -76 } { 4 -68 } } }
   { Arc { { -19 -38 } { -11 -30 } } 270 90 }
   { Arc { { -19 -30 } { -11 -22 } } 270 90 }
   { Arc { { -19 -46 } { -11 -38 } } 270 90 }
   { Arc { { -19 -22 } { -11 -14 } } 270 90 }
   { Arc { { 11 -38 } { 19 -30 } } 90 270 }
   { Arc { { 11 -30 } { 19 -22 } } 90 270 }
   { Arc { { 11 -46 } { 19 -38 } } 90 270 }
   { Arc { { 11 -22 } { 19 -14 } } 90 270 }
   { Ellipse { {-12 -10} {-8 -6} }}
   { Ellipse { {8 -10} {12 -6} }}
   { Line { 18 -14 } { 15 -14 } }
   { Line { 18 -60 } { 18 -46 } }
   { Line { 18 -22 } { 15 -22 } }
   { Line { 18 -30 } { 15 -30 } }
   { Line { 18 -46 } { 15 -46 } }
   { Line { 18 -38 } { 15 -38 } }
   { Line { 18 0 } { 18 -14 } }
   { Line { -18 -14 } { -15 -14 } }
   { Line { -18 -60 } { -18 -46 } }
   { Line { -18 -22 } { -15 -22 } }
   { Line { -18 -30 } { -15 -30 } }
   { Line { -18 -46 } { -15 -46 } }
   { Line { -18 -38 } { -15 -38 } }
   { Line { -18 0 } { -18 -14 } }
   { Rect { { -1 -24 } { 1 -22 } } }
   { Rect { { -1 -38 } { 1 -36 } } }
   { EvalTextTerm cdsTerm("P+") {-23 -2} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("SB") {5 -72} upperLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("P-") {-23 -59} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("S+") {23 -2} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("S-") {23 -59} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(1) {24 -25} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(2) {24 -35} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(3) {24 -45} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {23 -10} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { Prop String instNamePrefix E }
}

}
}
