#
# CDF Parameter generation for basic
# 
set lib [oa::LibFind basic]
oa::getAccess $lib write 1

# begin basic patch
set dmdata [oa::DMDataOpen [oa::CellFind $lib patch ] write ]
   db::createParamDef schPatchExpr \
   -data $dmdata \
   -prompt "Connection Expression" \
   -defValue "0=0" \
   -type string \
   -display "artParameterInToolDisplay('schPatchExpr)"
   
   db::setProplistProp paramLabelSet -data $dmdata \
      -value "-schPatchExpr" -type string
      
oa::save $dmdata
# end basic patch
