#
# Procedures for returning the symbol data
#

proc slDefinebasicSymbols { } {
return {
{ ipin symbol schematicSymbol
   { PinPoly ipin "signal" false "input" {-20 -9} {-20 9} {-9 9} {0 0} {-9 -9} }
   { InstRect {{-20 -10} {0 10}} }
   { TermAttrDisplay ipin 0 {-30 0} centerRight R0 stick 10 value 0 1 1 }
   { Prop String instNamePrefix PIN }
}
{ opin symbol schematicSymbol
   { PinPoly opin "signal" false "output" {11 -9} {20 0} {11 9} {0 9} {0 -9} }
   { InstRect {{0 -10} {20 10}} }
   { TermAttrDisplay opin 0 {25 0} centerLeft R0 stick 10 value 0 1 1 }
   { Prop String instNamePrefix PIN }
}
{ iopin symbol schematicSymbol
   { PinPoly iopin "signal" false "inputOutput" {14 0} {5 9}  {-5 9} {-14 0} {-5 -9} {5 -9} }
   { InstRect {{-14 -10} {14 10}} }
   { TermAttrDisplay iopin 0 {0 15} lowerCenter R0 stick 10 value 0 1 1 }
   { Prop String instNamePrefix PIN }
}
{ iopin symbolr schematicSymbol
   { PinPoly iopin "signal" false "inputOutput" {10 0} {1 -9} {-11 -9} {-20 0} {-11 9} {1 9} }
   { InstRect {{-20 -10} {10 10}} }
   { TermAttrDisplay iopin 0 {-25 0} centerRight R0 stick 10 value 0 1 1 }
   { Prop String instNamePrefix PIN }
}
{ noConn symbol schematicSymbol
   { PinRect noConn "signal" false "inputOutput" {{-4 -4} {4 4}} }
   { Rect {{-10 -50} {10 -30}} }
   { Line {0 0} {0 -30} }
   { Line {10 -30} {-10 -50} }
   { Line {-10 -30} {10 -50} }
   { InstRect {{-10 -50} {10 0}} }
   { EvalTextTerm cdsTerm("noConn") {-10 -10} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { Prop String nlAction ignore }
   { Prop String schType noConn }
   { Prop Boolean lvsIgnore true }
   { Prop Boolean ignore true }
}
{ noErc symbol schematicSymbol
   { PinRect noErc "signal" false "inputOutput" {{-4 -4} {4 4}} }
   { Ellipse { {10 -10} {30 10} } }
   { Line {0 0} {10 0} }
   { Line {13 -7} {27 7} }
   { Line {13 7} {27 -7} }
   { InstRect {{0 -10} {30 10}} }
   { EvalTextTerm cdsTerm("noErc") {-10 -10} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { Prop String nlAction ignore }
   { Prop String schType noConn }
   { Prop Boolean lvsIgnore true }
   { Prop Boolean ignore true }
}
{ patch symbol schematicSymbol
   { PinRect dst "signal" false "input" {{-4 -4} {4 4}} }
   { PinRect src "signal" false "input" {{-24 -4} {-16 4}} }
   { Line { -16 4 } { -14 6 } { -11 7 } { -9 7 } { -6 6 } { -4 4 } }
   { Line { -6 4 } { -4 4 } { -4 6 } { -6 4 } }
   { EvalTextTerm cdsTerm("dst") {10 10} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("src") {-30 10} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextParam cdsParam(1) {-10 20} lowerCenter R0 stick 8 cdsSkillEvalText 0 1 1 }
   { InstRect { {-20 0} {0 7} } }
   { Prop String nlAction ignore }
   { Prop String schType patchCord }
}

}
}
