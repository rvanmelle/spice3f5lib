#
# CDF Parameter generation for analogLib
# 
set lib [oa::LibFind analogLib]
oa::getAccess $lib write 1

# begin analogLib cap
set dmdata [oa::DMDataOpen [oa::CellFind $lib cap ] write ]
   db::setCDFProp fieldHeight -data $dmdata \
      -value 35 \
      -type integer
   db::setCDFProp formInitProc -data $dmdata -value {} \
      -type string
   db::createParamDef c \
   -data $dmdata \
   -prompt Capacitance \
   -defValue "1p" \
   -type string \
   -units capacitance \
   -display "artParameterInToolDisplay('c)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef model \
   -data $dmdata \
   -prompt "Model Name" \
   -type string \
   -display "artParameterInToolDisplay('model)" \
   -parseAsPEL false \
   -parseAsNumber false
   db::createParamDef ic \
   -data $dmdata \
   -prompt "Initial Condition" \
   -type string \
   -units voltage \
   -display "artParameterInToolDisplay('ic)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef w \
   -data $dmdata \
   -prompt "Width" \
   -type string \
   -units lengthMetric \
   -display "artParameterInToolDisplay('w)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef l \
   -data $dmdata \
   -prompt Length \
   -type string \
   -units lengthMetric \
   -display "artParameterInToolDisplay('l)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef m \
   -data $dmdata \
   -prompt "Multiplier" \
   -defValue "1" \
   -type string \
   -display "artParameterInToolDisplay('m)" \
   -parseAsPEL true \
   -parseAsNumber true
# NetlistInfo hspiceD
   db::setNetlistInfo componentName -cell $dmdata -netlister hspiceD -type symbol -value capacitor
   db::setNetlistInfo instParameters -cell $dmdata -netlister hspiceD -type list -value "(c ic m)"
   db::setNetlistInfo namePrefix -cell $dmdata -netlister hspiceD -type symbol -value C
   db::setNetlistInfo netlistProcedure -cell $dmdata -netlister hspiceD -type symbol -value nlPrintSpiceDevice
   db::setNetlistInfo otherParameters -cell $dmdata -netlister hspiceD -type list -value "(model)"
   db::setNetlistInfo termOrder -cell $dmdata -netlister hspiceD -type list -value {(PLUS MINUS)}
   
   db::setProplistProp paramLabelSet -data $dmdata \
      -value "c ic" -type string
   db::setProplistProp opPointLabelSet -data $dmdata \
      -value "i" -type string

oa::save $dmdata
# end analogLib cap

# begin analogLib diode
set dmdata [oa::DMDataOpen [oa::CellFind $lib diode ] write ]
   db::setCDFProp fieldHeight -data $dmdata \
      -value 35 \
      -type integer
   db::setCDFProp formInitProc -data $dmdata -value {} \
      -type string
   db::createParamDef model \
   -data $dmdata \
   -prompt "Model Name" \
   -type string \
   -display "artParameterInToolDisplay('model)" \
   -parseAsPEL false \
   -parseAsNumber false
   db::createParamDef area \
   -data $dmdata \
   -prompt "Device Area" \
   -type string \
   -display "artParameterInToolDisplay('area)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef ic \
   -data $dmdata \
   -prompt "Initial Diode Voltage" \
   -type string \
   -units voltage \
   -display "artParameterInToolDisplay('vd)" \
   -parseAsPEL false \
   -parseAsNumber false
   db::createParamDef  pj \
   -data $dmdata \
   -prompt "Perifery of Junction" \
   -type string \
   -units lengthMetric \
   -display "artParameterInToolDisplay('pj)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef wp \
   -data $dmdata \
   -prompt "Width of Polycap" \
   -type string \
   -units lengthMetric \
   -display "artParameterInToolDisplay('wp)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef lp \
   -data $dmdata \
   -prompt "Length of Polycap" \
   -type string \
   -units lengthMetric \
   -display "artParameterInToolDisplay('lp)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef wm \
   -data $dmdata \
   -prompt "Width of Metalcap" \
   -type string \
   -units lengthMetric \
   -display "artParameterInToolDisplay('wm)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef lm \
   -data $dmdata \
   -prompt "Length of Metalcap" \
   -type string \
   -units lengthMetric \
   -display "artParameterInToolDisplay('lm)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef w \
   -data $dmdata \
   -prompt "Width" \
   -type string \
   -units lengthMetric \
   -display "artParameterInToolDisplay('w)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef l \
   -data $dmdata \
   -prompt "Length" \
   -type string \
   -units lengthMetric \
   -display "artParameterInToolDisplay('l)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef off \
   -data $dmdata \
   -prompt "Initial Condition" \
   -defValue " " \
   -type cyclic \
   -display "artParameterInToolDisplay('off)" \
   -choices {(" " "off" "on")}
   db::createParamDef m \
   -data $dmdata \
   -prompt "Multiplier" \
   -defValue "1" \
   -type string \
   -display "artParameterInToolDisplay('m)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef dtemp \
   -data $dmdata \
   -prompt "Device Temperature" \
   -type string \
   -display "artParameterInToolDisplay('dtemp)" \
   -parseAsPEL true \
   -parseAsNumber true
# NetlistInfo hspiceD
   db::setNetlistInfo componentName -cell $dmdata -netlister hspiceD -type symbol -value diode
   db::setNetlistInfo instParameters -cell $dmdata -netlister hspiceD -type list -value "(area pj wp lp wm lm l w off vd m dtemp)"
   db::setNetlistInfo namePrefix -cell $dmdata -netlister hspiceD -type symbol -value D
   db::setNetlistInfo netlistProcedure -cell $dmdata -netlister hspiceD -type symbol -value nlPrintSpiceDiode
   db::setNetlistInfo otherParameters -cell $dmdata -netlister hspiceD -type list -value "(model)"
   db::setNetlistInfo termOrder -cell $dmdata -netlister hspiceD -type list -value {(PLUS MINUS)}
   
   db::setProplistProp paramLabelSet -data $dmdata \
      -value "-model vd area" -type string
   db::setProplistProp opPointLabelSet -data $dmdata \
      -value "id vd" -type string

oa::save $dmdata
# end analogLib diode

# begin analogLib idc
set dmdata [oa::DMDataOpen [oa::CellFind $lib idc ] write ]
   db::setCDFProp fieldHeight -data $dmdata \
      -value 35 \
      -type integer
   db::setCDFProp formInitProc -data $dmdata -value {} \
      -type string
   db::createParamDef idc \
   -data $dmdata \
   -prompt "DC Current" \
   -defValue "0" \
   -type string \
   -units current \
   -display "artParameterInToolDisplay('idc)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef acm \
   -data $dmdata \
   -prompt "AC Magnitude" \
   -type string \
   -units current \
   -display "artParameterInToolDisplay('acm)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef acp \
   -data $dmdata \
   -prompt "AC Phase" \
   -type string \
   -display "artParameterInToolDisplay('acp)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef m \
   -data $dmdata \
   -prompt "Multiplier" \
   -type string \
   -display "artParameterInToolDisplay('m)" \
   -parseAsPEL true \
   -parseAsNumber true
# NetlistInfo hspiceD
   db::setNetlistInfo componentName -cell $dmdata -netlister hspiceD -type symbol -value isrc
   db::setNetlistInfo instParameters -cell $dmdata -netlister hspiceD -type list -value "(idc acm acp m)"
   db::setNetlistInfo namePrefix -cell $dmdata -netlister hspiceD -type symbol -value I
   db::setNetlistInfo netlistProcedure -cell $dmdata -netlister hspiceD -type symbol -value nlPrintSpiceSource
   db::setNetlistInfo propMapping -cell $dmdata -netlister hspiceD -type list -value "(nil DC idc AC acm)"
   db::setNetlistInfo termOrder -cell $dmdata -netlister hspiceD -type list -value {(PLUS MINUS)}
   
   db::setProplistProp paramLabelSet -data $dmdata \
      -value "idc acm acp" -type string

oa::save $dmdata
# end analogLib idc

# begin analogLib iexp
set dmdata [oa::DMDataOpen [oa::CellFind $lib iexp ] write ]
   db::setCDFProp fieldHeight -data $dmdata \
      -value 35 \
      -type integer
   db::setCDFProp formInitProc -data $dmdata -value {} \
      -type string
   db::createParamDef idc \
   -data $dmdata \
   -prompt "DC Current" \
   -defValue "0" \
   -type string \
   -units current \
   -display "artParameterInToolDisplay('idc)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef type \
   -data $dmdata \
   -prompt "Type" \
   -defValue "EXP" \
   -type string \
   -display false \
   -editable false
   db::createParamDef i1 \
   -data $dmdata \
   -prompt "Current1" \
   -type string \
   -units current \
   -display "artParameterInToolDisplay('i1)" \
   -defValue "0" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef i2 \
   -data $dmdata \
   -prompt "Current2" \
   -type string \
   -units current \
   -display "artParameterInToolDisplay('i2)" \
   -defValue "0" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef td1 \
   -data $dmdata \
   -prompt "Rise Delay Time" \
   -type string \
   -units time \
   -display "artParameterInToolDisplay('td1)" \
   -defValue "0" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef td2 \
   -data $dmdata \
   -prompt "Fall Delay Time" \
   -type string \
   -units time \
   -display "artParameterInToolDisplay('td2)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef t1 \
   -data $dmdata \
   -prompt "Rise Time Constant" \
   -type string \
   -display "artParameterInToolDisplay('t1)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef t2 \
   -data $dmdata \
   -prompt "Fall Time Constant" \
   -type string \
   -display "artParameterInToolDisplay('t2)" \
   -defValue "0" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef acm \
   -data $dmdata \
   -prompt "AC Magnitude" \
   -defValue "0" \
   -type string \
   -units current \
   -display "artParameterInToolDisplay('acm)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef acp \
   -data $dmdata \
   -prompt "AC Phase" \
   -defValue "0" \
   -type string \
   -display "artParameterInToolDisplay('acp)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef m \
   -data $dmdata \
   -prompt "Multiplier" \
   -type string \
   -display "artParameterInToolDisplay('m)" \
   -parseAsPEL true \
   -parseAsNumber true
# NetlistInfo hspiceD
   db::setNetlistInfo componentName -cell $dmdata -netlister hspiceD -type symbol -value isrc
   db::setNetlistInfo instParameters -cell $dmdata -netlister hspiceD -type list -value "(idc i1 i2 td1 t1 td2 t2 acm acp m)"
   db::setNetlistInfo namePrefix -cell $dmdata -netlister hspiceD -type symbol -value I
   db::setNetlistInfo netlistProcedure -cell $dmdata -netlister hspiceD -type symbol -value nlPrintSpiceSource
   db::setNetlistInfo propMapping -cell $dmdata -netlister hspiceD -type list -value "(nil DC idc AC acm)"
   db::setNetlistInfo termOrder -cell $dmdata -netlister hspiceD -type list -value {(PLUS MINUS)}
   oa::save $dmdata     
# end analogLib iexp

# begin analogLib ind
set dmdata [oa::DMDataOpen [oa::CellFind $lib ind ] write ]
   db::setCDFProp fieldHeight -data $dmdata \
      -value 35 \
      -type integer
   db::setCDFProp formInitProc -data $dmdata -value {} \
      -type string
   db::createParamDef l \
   -data $dmdata \
   -prompt Inductance \
   -defValue "1p" \
   -type string \
   -units inductance \
   -display "artParameterInToolDisplay('l)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef model \
   -data $dmdata \
   -prompt "Model Name" \
   -type string \
   -display "artParameterInToolDisplay('model)" \
   -parseAsPEL false \
   -parseAsNumber false
   db::createParamDef ic \
   -data $dmdata \
   -prompt "Initial Condition" \
   -type string \
   -units voltage \
   -display "artParameterInToolDisplay('ic)" \
   -parseAsPEL false \
   -parseAsNumber false
   db::createParamDef tc1 \
   -data $dmdata \
   -prompt "Temperature Coefficient 1" \
   -type string \
   -display "artParameterInToolDisplay('tc1)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef tc2 \
   -data $dmdata \
   -prompt "Temperature Coefficient 2" \
   -type string \
   -display "artParameterInToolDisplay('tc2)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef r \
   -data $dmdata \
   -prompt Resistance \
   -defValue 1k \
   -type string \
   -units resistance \
   -display "artParameterInToolDisplay('r)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef m \
   -data $dmdata \
   -prompt "Multiplier" \
   -defValue "1" \
   -type string \
   -display "artParameterInToolDisplay('m)" \
   -parseAsPEL true \
   -parseAsNumber true
# NetlistInfo hspiceD
   db::setNetlistInfo componentName -cell $dmdata -netlister hspiceD -type symbol -value inductor
   db::setNetlistInfo instParameters -cell $dmdata -netlister hspiceD -type list -value "(l ic tc1 tc2 r m)"
   db::setNetlistInfo namePrefix -cell $dmdata -netlister hspiceD -type symbol -value L
   db::setNetlistInfo netlistProcedure -cell $dmdata -netlister hspiceD -type symbol -value nlPrintSpiceDevice
   db::setNetlistInfo otherParameters -cell $dmdata -netlister hspiceD -type list -value "(model)"
   db::setNetlistInfo termOrder -cell $dmdata -netlister hspiceD -type list -value {(PLUS MINUS)}

   db::setProplistProp paramLabelSet -data $dmdata \
      -value "l ic r" -type string

oa::save $dmdata
# end analogLib ind

# begin analogLib ipulse
set dmdata [oa::DMDataOpen [oa::CellFind $lib ipulse ] write ]
   db::setCDFProp fieldHeight -data $dmdata \
      -value 35 \
      -type integer
   db::setCDFProp formInitProc -data $dmdata -value {} \
      -type string
   db::createParamDef idc \
   -data $dmdata \
   -prompt "DC Current" \
   -defValue "0" \
   -type string \
   -units current \
   -display "artParameterInToolDisplay('idc)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef type \
   -data $dmdata \
   -prompt "Type" \
   -defValue "PULSE" \
   -type string \
   -display false \
   -editable false
   db::createParamDef i1 \
   -data $dmdata \
   -prompt "Current1" \
   -type string \
   -units current \
   -display "artParameterInToolDisplay('i1)" \
   -defValue "0" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef i2 \
   -data $dmdata \
   -prompt "Current2" \
   -type string \
   -units current \
   -display "artParameterInToolDisplay('i2)" \
   -defValue "0" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef td \
   -data $dmdata \
   -prompt "Delay Time" \
   -type string \
   -units time \
   -display "artParameterInToolDisplay('td)" \
   -defValue "0" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef tr \
   -data $dmdata \
   -prompt "Rise Time" \
   -type string \
   -units time \
   -display "artParameterInToolDisplay('tr)" \
   -defValue "0" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef tf \
   -data $dmdata \
   -prompt "Fall Time" \
   -type string \
   -units time \
   -display "artParameterInToolDisplay('tf)" \
   -defValue "0" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef pw \
   -data $dmdata \
   -prompt "Pulse Width" \
   -type string \
   -units time \
   -display "artParameterInToolDisplay('pw)" \
   -defValue "0" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef per \
   -data $dmdata \
   -prompt "Period" \
   -type string \
   -units time \
   -display "artParameterInToolDisplay('per)" \
   -defValue "0" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef acm \
   -data $dmdata \
   -prompt "AC Magnitude" \
   -defValue "0" \
   -type string \
   -units current \
   -display "artParameterInToolDisplay('acm)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef acp \
   -data $dmdata \
   -prompt "AC Phase" \
   -defValue "0" \
   -type string \
   -display "artParameterInToolDisplay('acp)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef m \
   -data $dmdata \
   -prompt "Multiplier" \
   -defValue "1" \
   -type string \
   -display "artParameterInToolDisplay('m)" \
   -parseAsPEL true \
   -parseAsNumber true
# NetlistInfo hspiceD
   db::setNetlistInfo componentName -cell $dmdata -netlister hspiceD -type symbol -value isrc
   db::setNetlistInfo instParameters -cell $dmdata -netlister hspiceD -type list -value "(idc i1 i2 td tr tf pw per acm acp m)"
   db::setNetlistInfo namePrefix -cell $dmdata -netlister hspiceD -type symbol -value I
   db::setNetlistInfo netlistProcedure -cell $dmdata -netlister hspiceD -type symbol -value nlPrintSpiceSource
   db::setNetlistInfo propMapping -cell $dmdata -netlister hspiceD -type list -value "(nil DC idc AC acm)"
   db::setNetlistInfo termOrder -cell $dmdata -netlister hspiceD -type list -value {(PLUS MINUS)}
   oa::save $dmdata   
# end analogLib ipulse

# begin analogLib ipwl
set dmdata [oa::DMDataOpen [oa::CellFind $lib ipwl ] write ]
   db::setCDFProp fieldHeight -data $dmdata \
      -value 35 \
      -type integer
   db::setCDFProp formInitProc -data $dmdata -value {} \
      -type string
   db::createParamDef idc \
   -data $dmdata \
   -prompt "DC Current" \
   -defValue "0" \
   -type string \
   -units current \
   -display "artParameterInToolDisplay('idc)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef type \
   -data $dmdata \
   -prompt "Type" \
   -defValue "PWL" \
   -type string \
   -display false \
   -editable false
   db::createParamDef tvpairs \
   -data $dmdata \
   -prompt "Time Value Pairs" \
   -defValue 10 \
   -type int \
   -display "artParameterInToolDisplay('tvpairs)"
   db::createParamDef t1 \
   -data $dmdata \
   -prompt "Time1" \
   -type string \
   -units time \
   -display "artParameterInToolDisplay('t1)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef i1 \
   -data $dmdata \
   -prompt "Current1" \
   -type string \
   -units current \
   -display "artParameterInToolDisplay('i1)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef t2 \
   -data $dmdata \
   -prompt "Time2" \
   -type string \
   -units time \
   -display "artParameterInToolDisplay('t2)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef i2 \
   -data $dmdata \
   -prompt "Current2" \
   -type string \
   -units current \
   -display "artParameterInToolDisplay('i2)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef t3 \
   -data $dmdata \
   -prompt "Time3" \
   -type string \
   -units time \
   -display "artParameterInToolDisplay('t3)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef i3 \
   -data $dmdata \
   -prompt "Current3" \
   -type string \
   -units current \
   -display "artParameterInToolDisplay('i3)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef t4 \
   -data $dmdata \
   -prompt "Time4" \
   -type string \
   -units time \
   -display "artParameterInToolDisplay('t4)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef i4 \
   -data $dmdata \
   -prompt "Current4" \
   -type string \
   -units current \
   -display "artParameterInToolDisplay('i4)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef t5 \
   -data $dmdata \
   -prompt "Time5" \
   -type string \
   -units time \
   -display "artParameterInToolDisplay('t5)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef i5 \
   -data $dmdata \
   -prompt "Current5" \
   -type string \
   -units current \
   -display "artParameterInToolDisplay('i5)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef t6 \
   -data $dmdata \
   -prompt "Time6" \
   -type string \
   -units time \
   -display "artParameterInToolDisplay('t6)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef i6 \
   -data $dmdata \
   -prompt "Current6" \
   -type string \
   -units current \
   -display "artParameterInToolDisplay('i6)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef t7 \
   -data $dmdata \
   -prompt "Time7" \
   -type string \
   -units time \
   -display "artParameterInToolDisplay('t7)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef i7 \
   -data $dmdata \
   -prompt "Current7" \
   -type string \
   -units current \
   -display "artParameterInToolDisplay('i7)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef t8 \
   -data $dmdata \
   -prompt "Time8" \
   -type string \
   -units time \
   -display "artParameterInToolDisplay('t8)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef i8 \
   -data $dmdata \
   -prompt "Current8" \
   -type string \
   -units current \
   -display "artParameterInToolDisplay('i8)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef t9 \
   -data $dmdata \
   -prompt "Time9" \
   -type string \
   -units time \
   -display "artParameterInToolDisplay('t9)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef i9 \
   -data $dmdata \
   -prompt "Current9" \
   -type string \
   -units current \
   -display "artParameterInToolDisplay('i9)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef t10 \
   -data $dmdata \
   -prompt "Time10" \
   -type string \
   -units time \
   -display "artParameterInToolDisplay('t10)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef i10 \
   -data $dmdata \
   -prompt "Current10" \
   -type string \
   -units current \
   -display "artParameterInToolDisplay('i10)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef r \
   -data $dmdata \
   -prompt "Repeat" \
   -type string \
   -units time \
   -display "artParameterInToolDisplay('r)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef td \
   -data $dmdata \
   -prompt "Delay Time" \
   -type string \
   -units time \
   -display "artParameterInToolDisplay('td)" \
   -defValue "0" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef acm \
   -data $dmdata \
   -prompt "AC Magnitude" \
   -defValue "0" \
   -type string \
   -units current \
   -display "artParameterInToolDisplay('acm)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef acp \
   -data $dmdata \
   -prompt "AC Phase" \
   -defValue "0" \
   -type string \
   -display "artParameterInToolDisplay('acp)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef m \
   -data $dmdata \
   -prompt "Multiplier" \
   -type string \
   -display "artParameterInToolDisplay('m)" \
   -parseAsPEL true \
   -parseAsNumber true
# NetlistInfo hspiceD
   db::setNetlistInfo componentName -cell $dmdata -netlister hspiceD -type symbol -value isrc
   db::setNetlistInfo instParameters -cell $dmdata -netlister hspiceD -type list -value "(idc t1 i1 t2 i2 t3 i3 t4 i4 t5 i5 t6 i6 t7 i7 t8 i8 t9 i9 t10 i10 r td acm acp m)"
   db::setNetlistInfo namePrefix -cell $dmdata -netlister hspiceD -type symbol -value I
   db::setNetlistInfo netlistProcedure -cell $dmdata -netlister hspiceD -type symbol -value nlPrintSpiceSource
   db::setNetlistInfo propMapping -cell $dmdata -netlister hspiceD -type list -value "(nil DC idc AC acm)"
   db::setNetlistInfo termOrder -cell $dmdata -netlister hspiceD -type list -value {(PLUS MINUS)}
   oa::save $dmdata
# end analogLib ipwl

# begin analogLib isin
set dmdata [oa::DMDataOpen [oa::CellFind $lib isin ] write ]
   db::setCDFProp fieldHeight -data $dmdata \
      -value 35 \
      -type integer
   db::setCDFProp formInitProc -data $dmdata -value {} \
      -type string
   db::createParamDef idc \
   -data $dmdata \
   -prompt "DC Current" \
   -defValue "0" \
   -type string \
   -units current \
   -display "artParameterInToolDisplay('idc)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef type \
   -data $dmdata \
   -prompt "Type" \
   -defValue "SIN" \
   -type string \
   -display false \
   -editable false
   db::createParamDef io \
   -data $dmdata \
   -prompt "Current Offset" \
   -type string \
   -units current \
   -display "artParameterInToolDisplay('io)" \
   -defValue "0" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef ia \
   -data $dmdata \
   -prompt "Current Amplitude" \
   -type string \
   -units current \
   -display "artParameterInToolDisplay('ia)" \
   -defValue "0" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef freq \
   -data $dmdata \
   -prompt "Frequency" \
   -type string \
   -units frequency \
   -display "artParameterInToolDisplay('freq)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef td \
   -data $dmdata \
   -prompt "Delay Time" \
   -type string \
   -units time \
   -display "artParameterInToolDisplay('td)" \
   -defValue "0" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef q \
   -data $dmdata \
   -prompt "Damping Factor" \
   -type string \
   -display "artParameterInToolDisplay('q)" \
   -defValue "0" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef j \
   -data $dmdata \
   -prompt "Phase Delay" \
   -type string \
   -display "artParameterInToolDisplay('j)" \
   -defValue "0" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef acm \
   -data $dmdata \
   -prompt "AC Magnitude" \
   -defValue "0" \
   -type string \
   -units current \
   -display "artParameterInToolDisplay('acm)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef acp \
   -data $dmdata \
   -prompt "AC Phase" \
   -defValue "0" \
   -type string \
   -display "artParameterInToolDisplay('acp)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef m \
   -data $dmdata \
   -prompt "Multiplier" \
   -type string \
   -display "artParameterInToolDisplay('m)" \
   -parseAsPEL true \
   -parseAsNumber true
# NetlistInfo hspiceD
   db::setNetlistInfo componentName -cell $dmdata -netlister hspiceD -type symbol -value isrc
   db::setNetlistInfo instParameters -cell $dmdata -netlister hspiceD -type list -value "(idc io ia freq td q j acm acp m)"
   db::setNetlistInfo namePrefix -cell $dmdata -netlister hspiceD -type symbol -value I
   db::setNetlistInfo netlistProcedure -cell $dmdata -netlister hspiceD -type symbol -value nlPrintSpiceSource
   db::setNetlistInfo propMapping -cell $dmdata -netlister hspiceD -type list -value "(nil DC idc AC acm)"
   db::setNetlistInfo termOrder -cell $dmdata -netlister hspiceD -type list -value {(PLUS MINUS)}
   oa::save $dmdata
# end analogLib isin

# begin analogLib mind
set dmdata [oa::DMDataOpen [oa::CellFind $lib mind ] write ]
   db::setCDFProp fieldHeight -data $dmdata \
      -value 35 \
      -type integer
   db::setCDFProp formInitProc -data $dmdata -value {} \
      -type string
   db::createParamDef ind1 \
   -data $dmdata \
   -prompt "First Coupled Inductor" \
   -type string \
   -display "artParameterInToolDisplay('ind1)" 
   db::createParamDef ind2 \
   -data $dmdata \
   -prompt "Second Coupled Inductor" \
   -type string \
   -display "artParameterInToolDisplay('ind2)" 
   db::createParamDef k \
   -data $dmdata \
   -prompt "Coupling Coefficient" \
   -defValue "1.0" \
   -type string \
   -display "artParameterInToolDisplay('k)" \
   -parseAsPEL true \
   -parseAsNumber true
# NetlistInfo hspiceD
   db::setNetlistInfo componentName -cell $dmdata -netlister hspiceD -type symbol -value mutual_inductor
   db::setNetlistInfo instParameters -cell $dmdata -netlister hspiceD -type list -value "(k)"
   db::setNetlistInfo namePrefix -cell $dmdata -netlister hspiceD -type symbol -value K
   db::setNetlistInfo netlistProcedure -cell $dmdata -netlister hspiceD -type symbol -value nlPrintSpiceMutualCore

   db::setProplistProp paramLabelSet -data $dmdata \
      -value "ind1 ind2 k" -type string

oa::save $dmdata
# end analogLib mind

# begin analogLib nmos
set dmdata [oa::DMDataOpen [oa::CellFind $lib nmos ] write ]
   db::setCDFProp fieldHeight -data $dmdata \
      -value 35 \
      -type integer
   db::setCDFProp formInitProc -data $dmdata -value {} \
      -type string
   db::createParamDef model \
   -data $dmdata \
   -prompt "Model Name" \
   -defValue "nmos" \
   -type string \
   -display "artParameterInToolDisplay('model)" \
   -parseAsPEL false \
   -parseAsNumber false
   db::createParamDef w \
   -data $dmdata \
   -prompt "Width" \
   -defValue "1u" \
   -type string \
   -units lengthMetric \
   -display "artParameterInToolDisplay('w)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef l \
   -data $dmdata \
   -prompt "Length" \
   -defValue "1u" \
   -type string \
   -units lengthMetric \
   -display "artParameterInToolDisplay('l)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef ad \
   -data $dmdata \
   -prompt "Drain Diffusion Area" \
   -type string \
   -display "artParameterInToolDisplay('ad)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef as \
   -data $dmdata \
   -prompt "Source Diffusion Area" \
   -type string \
   -display "artParameterInToolDisplay('as)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef pd \
   -data $dmdata \
   -prompt "Drain Diffusion Periphery" \
   -type string \
   -units lengthMetric \
   -display "artParameterInToolDisplay('pd)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef ps \
   -data $dmdata \
   -prompt "Source Diffusion Periphery" \
   -type string \
   -units lengthMetric \
   -display "artParameterInToolDisplay('ps)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef nrd \
   -data $dmdata \
   -prompt "Drain Diff Resistor Sq" \
   -type string \
   -display "artParameterInToolDisplay('nrd)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef nrs \
   -data $dmdata \
   -prompt "Source Diff Resistor Sq" \
   -type string \
   -display "artParameterInToolDisplay('nrs)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef rdc \
   -data $dmdata \
   -prompt "Additional Drain Resistance" \
   -type string \
   -units resistance \
   -display "artParameterInToolDisplay('rdc)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef rsc \
   -data $dmdata \
   -prompt "Additional Source Resistance" \
   -type string \
   -units resistance \
   -display "artParameterInToolDisplay('rsc)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef off \
   -data $dmdata \
   -prompt "Initially Off" \
   -type boolean \
   -display "artParameterInToolDisplay('off)"
   db::createParamDef Vds \
   -data $dmdata \
   -prompt "Drain Source Initial Voltage" \
   -type string \
   -units voltage \
   -display "artParameterInToolDisplay('Vds)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef Vgs \
   -data $dmdata \
   -prompt "Gate Source Initial Voltage" \
   -type string \
   -units voltage \
   -display "artParameterInToolDisplay('Vgs)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef Vbs \
   -data $dmdata \
   -prompt "Bulk Source Initial Voltage" \
   -type string \
   -units voltage \
   -display "artParameterInToolDisplay('Vbs)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef dtemp \
   -data $dmdata \
   -prompt "Temperature Difference" \
   -type string \
   -display "artParameterInToolDisplay('dtemp)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef geo \
   -data $dmdata \
   -prompt "Source/Drain Selector" \
   -type string \
   -display "artParameterInToolDisplay('geo)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef delvto \
   -data $dmdata \
   -prompt "Threshold Voltage Shift" \
   -type string \
   -units voltage \
   -display "artParameterInToolDisplay('delvto)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef m \
   -data $dmdata \
   -prompt "Multiplier" \
   -defValue "1" \
   -type string \
   -display "artParameterInToolDisplay('m)" \
   -parseAsPEL true \
   -parseAsNumber true 
# NetlistInfo hspiceD
   db::setNetlistInfo componentName -cell $dmdata -netlister hspiceD -type symbol -value mosfet
   db::setNetlistInfo instParameters -cell $dmdata -netlister hspiceD -type list -value "(w l ad as pd ps nrd nrs rdc rsc dtemp geo delvto m)"
   db::setNetlistInfo otherParameters -cell $dmdata -netlister hspiceD -type list -value "(model)"
   db::setNetlistInfo namePrefix -cell $dmdata -netlister hspiceD -type symbol -value M
   db::setNetlistInfo netlistProcedure -cell $dmdata -netlister hspiceD -type symbol -value nlPrintSpiceMOSFET
   db::setNetlistInfo termOrder -cell $dmdata -netlister hspiceD -type list -value {(D G S)}

   db::setProplistProp paramLabelSet -data $dmdata \
      -value "-model w l" -type string
   db::setProplistProp opPointLabelSet -data $dmdata \
      -value "id vgs vds" -type string

oa::save $dmdata
# end analogLib nmos

# begin analogLib nmos4
set dmdata [oa::DMDataOpen [oa::CellFind $lib nmos4 ] write ]
   db::setCDFProp fieldHeight -data $dmdata \
      -value 35 \
      -type integer
   db::setCDFProp formInitProc -data $dmdata -value {} \
      -type string
   db::createParamDef model \
   -data $dmdata \
   -prompt "Model Name" \
   -defValue "nmos4" \
   -type string \
   -display "artParameterInToolDisplay('model)" \
   -parseAsPEL false \
   -parseAsNumber false
   db::createParamDef w \
   -data $dmdata \
   -prompt "Width" \
   -defValue "1u" \
   -type string \
   -units lengthMetric \
   -display "artParameterInToolDisplay('w)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef l \
   -data $dmdata \
   -prompt "Length" \
   -defValue "1u" \
   -type string \
   -units lengthMetric \
   -display "artParameterInToolDisplay('l)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef ad \
   -data $dmdata \
   -prompt "Drain Diffusion Area" \
   -type string \
   -display "artParameterInToolDisplay('ad)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef as \
   -data $dmdata \
   -prompt "Source Diffusion Area" \
   -type string \
   -display "artParameterInToolDisplay('as)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef pd \
   -data $dmdata \
   -prompt "Drain Diffusion Periphery" \
   -type string \
   -units lengthMetric \
   -display "artParameterInToolDisplay('pd)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef ps \
   -data $dmdata \
   -prompt "Source Diffusion Periphery" \
   -type string \
   -units lengthMetric \
   -display "artParameterInToolDisplay('ps)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef nrd \
   -data $dmdata \
   -prompt "Drain Diff Resistor Sq" \
   -type string \
   -display "artParameterInToolDisplay('nrd)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef nrs \
   -data $dmdata \
   -prompt "Source Diff Resistor Sq" \
   -type string \
   -display "artParameterInToolDisplay('nrs)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef rdc \
   -data $dmdata \
   -prompt "Additional Drain Resistance" \
   -type string \
   -units resistance \
   -display "artParameterInToolDisplay('rdc)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef rsc \
   -data $dmdata \
   -prompt "Additional Source Resistance" \
   -type string \
   -units resistance \
   -display "artParameterInToolDisplay('rsc)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef off \
   -data $dmdata \
   -prompt "Initially Off" \
   -type boolean \
   -display "artParameterInToolDisplay('off)"
   db::createParamDef Vds \
   -data $dmdata \
   -prompt "Drain Source Initial Voltage" \
   -type string \
   -units voltage \
   -display "artParameterInToolDisplay('Vds)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef Vgs \
   -data $dmdata \
   -prompt "Gate Source Initial Voltage" \
   -type string \
   -units voltage \
   -display "artParameterInToolDisplay('Vgs)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef Vbs \
   -data $dmdata \
   -prompt "Bulk Source Initial Voltage" \
   -type string \
   -units voltage \
   -display "artParameterInToolDisplay('Vbs)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef dtemp \
   -data $dmdata \
   -prompt "Temperature Difference" \
   -type string \
   -display "artParameterInToolDisplay('dtemp)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef geo \
   -data $dmdata \
   -prompt "Source/Drain Selector" \
   -type string \
   -display "artParameterInToolDisplay('geo)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef delvto \
   -data $dmdata \
   -prompt "Threshold Voltage Shift" \
   -type string \
   -units voltage \
   -display "artParameterInToolDisplay('delvto)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef m \
   -data $dmdata \
   -prompt "Multiplier" \
   -defValue "1" \
   -type string \
   -display "artParameterInToolDisplay('m)" \
   -parseAsPEL true \
   -parseAsNumber true 
# NetlistInfo hspiceD
   db::setNetlistInfo componentName -cell $dmdata -netlister hspiceD -type symbol -value mosfet
   db::setNetlistInfo instParameters -cell $dmdata -netlister hspiceD -type list -value "(w l ad as pd ps nrd nrs rdc rsc dtemp geo delvto m)"
   db::setNetlistInfo otherParameters -cell $dmdata -netlister hspiceD -type list -value "(model)"
   db::setNetlistInfo namePrefix -cell $dmdata -netlister hspiceD -type symbol -value M
   db::setNetlistInfo netlistProcedure -cell $dmdata -netlister hspiceD -type symbol -value nlPrintSpiceMOSFET
   db::setNetlistInfo termOrder -cell $dmdata -netlister hspiceD -type list -value {(D G S B)}

   db::setProplistProp paramLabelSet -data $dmdata \
      -value "-model w l" -type string
   db::setProplistProp opPointLabelSet -data $dmdata \
      -value "id vgs vds" -type string

oa::save $dmdata
# end analogLib nmos4

# begin analogLib pcapacitor
set dmdata [oa::DMDataOpen [oa::CellFind $lib pcapacitor ] write ]
   db::setCDFProp fieldHeight -data $dmdata \
      -value 35 \
      -type integer
   db::setCDFProp formInitProc -data $dmdata -value {} \
      -type string
   db::createParamDef c \
   -data $dmdata \
   -prompt Capacitance \
   -defValue "1p" \
   -type string \
   -units capacitance \
   -display "artParameterInToolDisplay('c)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef model \
   -data $dmdata \
   -prompt "Model Name" \
   -type string \
   -display "artParameterInToolDisplay('model)" \
   -parseAsPEL false \
   -parseAsNumber false
   db::createParamDef ic \
   -data $dmdata \
   -prompt "Initial Condition" \
   -type string \
   -units voltage \
   -display "artParameterInToolDisplay('ic)" \
   -parseAsPEL false \
   -parseAsNumber false
   db::createParamDef w \
   -data $dmdata \
   -prompt "Width" \
   -type string \
   -units lengthMetric \
   -display "artParameterInToolDisplay('w)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef l \
   -data $dmdata \
   -prompt Length \
   -type string \
   -units lengthMetric \
   -display "artParameterInToolDisplay('l)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef m \
   -data $dmdata \
   -prompt "Multiplier" \
   -defValue "1" \
   -type string \
   -display "artParameterInToolDisplay('m)" \
   -parseAsPEL true \
   -parseAsNumber true
# NetlistInfo hspiceD
   db::setNetlistInfo componentName -cell $dmdata -netlister hspiceD -type symbol -value pcapacitor
   db::setNetlistInfo instParameters -cell $dmdata -netlister hspiceD -type list -value "(c ic m)"
   db::setNetlistInfo namePrefix -cell $dmdata -netlister hspiceD -type symbol -value C
   db::setNetlistInfo netlistProcedure -cell $dmdata -netlister hspiceD -type symbol -value nlPrintSpiceDevice
   db::setNetlistInfo otherParameters -cell $dmdata -netlister hspiceD -type list -value "(model)"
   db::setNetlistInfo termOrder -cell $dmdata -netlister hspiceD -type list -value {(PLUS MINUS)}
   
   db::setProplistProp paramLabelSet -data $dmdata \
      -value "c ic" -type string
   db::setProplistProp opPointLabelSet -data $dmdata \
      -value "i" -type string

oa::save $dmdata
# end analogLib pcapacitor

# begin analogLib pdiode
set dmdata [oa::DMDataOpen [oa::CellFind $lib pdiode ] write ]
   db::setCDFProp fieldHeight -data $dmdata \
      -value 35 \
      -type integer
   db::setCDFProp formInitProc -data $dmdata -value {} \
      -type string
   db::createParamDef model \
   -data $dmdata \
   -prompt "Model Name" \
   -type string \
   -display "artParameterInToolDisplay('model)" \
   -parseAsPEL false \
   -parseAsNumber false
   db::createParamDef area \
   -data $dmdata \
   -prompt "Device Area" \
   -type string \
   -display "artParameterInToolDisplay('area)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef ic \
   -data $dmdata \
   -prompt "Initial Diode Voltage" \
   -type string \
   -units voltage \
   -display "artParameterInToolDisplay('vd)" \
   -parseAsPEL false \
   -parseAsNumber false
   db::createParamDef  pj \
   -data $dmdata \
   -prompt "Perifery of Junction" \
   -type string \
   -units lengthMetric \
   -display "artParameterInToolDisplay('pj)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef wp \
   -data $dmdata \
   -prompt "Width of Polycap" \
   -type string \
   -units lengthMetric \
   -display "artParameterInToolDisplay('wp)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef lp \
   -data $dmdata \
   -prompt "Length of Polycap" \
   -type string \
   -units lengthMetric \
   -display "artParameterInToolDisplay('lp)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef wm \
   -data $dmdata \
   -prompt "Width of Metalcap" \
   -type string \
   -units lengthMetric \
   -display "artParameterInToolDisplay('wm)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef lm \
   -data $dmdata \
   -prompt "Length of Metalcap" \
   -type string \
   -units lengthMetric \
   -display "artParameterInToolDisplay('lm)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef w \
   -data $dmdata \
   -prompt "Width" \
   -type string \
   -units lengthMetric \
   -display "artParameterInToolDisplay('w)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef l \
   -data $dmdata \
   -prompt "Length" \
   -type string \
   -units lengthMetric \
   -display "artParameterInToolDisplay('l)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef off \
   -data $dmdata \
   -prompt "Initial Condition" \
   -defValue " " \
   -type cyclic \
   -display "artParameterInToolDisplay('off)" \
   -choices {(" " "off" "on")}
   db::createParamDef m \
   -data $dmdata \
   -prompt "Multiplier" \
   -defValue "1" \
   -type string \
   -display "artParameterInToolDisplay('m)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef dtemp \
   -data $dmdata \
   -prompt "Device Temperature" \
   -type string \
   -display "artParameterInToolDisplay('dtemp)" \
   -parseAsPEL true \
   -parseAsNumber true
# NetlistInfo hspiceD
   db::setNetlistInfo componentName -cell $dmdata -netlister hspiceD -type symbol -value diode
   db::setNetlistInfo instParameters -cell $dmdata -netlister hspiceD -type list -value "(area pj wp lp wm lm l w off vd m dtemp)"
   db::setNetlistInfo namePrefix -cell $dmdata -netlister hspiceD -type symbol -value D
   db::setNetlistInfo netlistProcedure -cell $dmdata -netlister hspiceD -type symbol -value nlPrintSpiceDiode
   db::setNetlistInfo otherParameters -cell $dmdata -netlister hspiceD -type list -value "(model)"
   db::setNetlistInfo termOrder -cell $dmdata -netlister hspiceD -type list -value {(PLUS MINUS)}
   
   db::setProplistProp paramLabelSet -data $dmdata \
      -value "-model vd area" -type string
   db::setProplistProp opPointLabelSet -data $dmdata \
      -value "id vd" -type string

oa::save $dmdata
# end analogLib pdiode

# begin analogLib pmos
set dmdata [oa::DMDataOpen [oa::CellFind $lib pmos ] write ]
   db::setCDFProp fieldHeight -data $dmdata \
      -value 35 \
      -type integer
   db::setCDFProp formInitProc -data $dmdata -value {} \
      -type string
   db::createParamDef model \
   -data $dmdata \
   -prompt "Model Name" \
   -defValue "pmos" \
   -type string \
   -display "artParameterInToolDisplay('model)" \
   -parseAsPEL false \
   -parseAsNumber false
   db::createParamDef w \
   -data $dmdata \
   -prompt "Width" \
   -defValue "1u" \
   -type string \
   -units lengthMetric \
   -display "artParameterInToolDisplay('w)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef l \
   -data $dmdata \
   -prompt "Length" \
   -defValue "1u" \
   -type string \
   -units lengthMetric \
   -display "artParameterInToolDisplay('l)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef ad \
   -data $dmdata \
   -prompt "Drain Diffusion Area" \
   -type string \
   -display "artParameterInToolDisplay('ad)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef as \
   -data $dmdata \
   -prompt "Source Diffusion Area" \
   -type string \
   -display "artParameterInToolDisplay('as)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef pd \
   -data $dmdata \
   -prompt "Drain Diffusion Periphery" \
   -type string \
   -display "artParameterInToolDisplay('pd)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef ps \
   -data $dmdata \
   -prompt "Source Diffusion Periphery" \
   -type string \
   -units lengthMetric \
   -display "artParameterInToolDisplay('ps)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef nrd \
   -data $dmdata \
   -prompt "Drain Diff Resistor Sq" \
   -type string \
   -units lengthMetric \
   -display "artParameterInToolDisplay('nrd)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef nrs \
   -data $dmdata \
   -prompt "Source Diff Resistor Sq" \
   -type string \
   -display "artParameterInToolDisplay('nrs)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef rdc \
   -data $dmdata \
   -prompt "Additional Drain Resistance" \
   -type string \
   -units resistance \
   -display "artParameterInToolDisplay('rdc)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef rsc \
   -data $dmdata \
   -prompt "Additional Source Resistance" \
   -type string \
   -units resistance \
   -display "artParameterInToolDisplay('rsc)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef off \
   -data $dmdata \
   -prompt "Initially Off" \
   -type boolean \
   -display "artParameterInToolDisplay('off)"
   db::createParamDef Vds \
   -data $dmdata \
   -prompt "Drain Source Initial Voltage" \
   -type string \
   -units voltage \
   -display "artParameterInToolDisplay('Vds)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef Vgs \
   -data $dmdata \
   -prompt "Gate Source Initial Voltage" \
   -type string \
   -display "artParameterInToolDisplay('Vgs)" \
   -units voltage \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef Vbs \
   -data $dmdata \
   -prompt "Bulk Source Initial Voltage" \
   -type string \
   -units voltage \
   -display "artParameterInToolDisplay('Vbs)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef dtemp \
   -data $dmdata \
   -prompt "Temperature Difference" \
   -type string \
   -display "artParameterInToolDisplay('dtemp)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef geo \
   -data $dmdata \
   -prompt "Source/Drain Selector" \
   -type string \
   -display "artParameterInToolDisplay('geo)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef delvto \
   -data $dmdata \
   -prompt "Threshold Voltage Shift" \
   -type string \
   -units voltage \
   -display "artParameterInToolDisplay('delvto)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef m \
   -data $dmdata \
   -prompt "Multiplier" \
   -defValue "1" \
   -type string \
   -display "artParameterInToolDisplay('m)" \
   -parseAsPEL true \
   -parseAsNumber true
# NetlistInfo hspiceD
   db::setNetlistInfo componentName -cell $dmdata -netlister hspiceD -type symbol -value mosfet
   db::setNetlistInfo instParameters -cell $dmdata -netlister hspiceD -type list -value "(w l ad as pd ps nrd nrs rdc rsc dtemp geo delvto m)"
   db::setNetlistInfo otherParameters -cell $dmdata -netlister hspiceD -type list -value "(model)"
   db::setNetlistInfo namePrefix -cell $dmdata -netlister hspiceD -type symbol -value M
   db::setNetlistInfo netlistProcedure -cell $dmdata -netlister hspiceD -type symbol -value nlPrintSpiceMOSFET
   db::setNetlistInfo termOrder -cell $dmdata -netlister hspiceD -type list -value {(D G S)}

   db::setProplistProp paramLabelSet -data $dmdata \
      -value "-model w l" -type string
   db::setProplistProp opPointLabelSet -data $dmdata \
      -value "id vgs vds" -type string

oa::save $dmdata
# end analogLib pmos

# begin analogLib pmos4
set dmdata [oa::DMDataOpen [oa::CellFind $lib pmos4 ] write ]
   db::setCDFProp fieldHeight -data $dmdata \
      -value 35 \
      -type integer
   db::setCDFProp formInitProc -data $dmdata -value {} \
      -type string
   db::createParamDef model \
   -data $dmdata \
   -prompt "Model Name" \
   -defValue "pmos4" \
   -type string \
   -display "artParameterInToolDisplay('model)" \
   -parseAsPEL false \
   -parseAsNumber false
   db::createParamDef w \
   -data $dmdata \
   -prompt "Width" \
   -defValue "1u" \
   -type string \
   -units lengthMetric \
   -display "artParameterInToolDisplay('w)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef l \
   -data $dmdata \
   -prompt "Length" \
   -defValue "1u" \
   -type string \
   -units lengthMetric \
   -display "artParameterInToolDisplay('l)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef ad \
   -data $dmdata \
   -prompt "Drain Diffusion Area" \
   -type string \
   -display "artParameterInToolDisplay('ad)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef as \
   -data $dmdata \
   -prompt "Source Diffusion Area" \
   -type string \
   -display "artParameterInToolDisplay('as)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef pd \
   -data $dmdata \
   -prompt "Drain Diffusion Periphery" \
   -type string \
   -display "artParameterInToolDisplay('pd)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef ps \
   -data $dmdata \
   -prompt "Source Diffusion Periphery" \
   -type string \
   -units lengthMetric \
   -display "artParameterInToolDisplay('ps)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef nrd \
   -data $dmdata \
   -prompt "Drain Diff Resistor Sq" \
   -type string \
   -units lengthMetric \
   -display "artParameterInToolDisplay('nrd)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef nrs \
   -data $dmdata \
   -prompt "Source Diff Resistor Sq" \
   -type string \
   -display "artParameterInToolDisplay('nrs)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef rdc \
   -data $dmdata \
   -prompt "Additional Drain Resistance" \
   -type string \
   -units resistance \
   -display "artParameterInToolDisplay('rdc)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef rsc \
   -data $dmdata \
   -prompt "Additional Source Resistance" \
   -type string \
   -units resistance \
   -display "artParameterInToolDisplay('rsc)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef off \
   -data $dmdata \
   -prompt "Initially Off" \
   -type boolean \
   -display "artParameterInToolDisplay('off)"
   db::createParamDef Vds \
   -data $dmdata \
   -prompt "Drain Source Initial Voltage" \
   -type string \
   -units voltage \
   -display "artParameterInToolDisplay('Vds)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef Vgs \
   -data $dmdata \
   -prompt "Gate Source Initial Voltage" \
   -type string \
   -display "artParameterInToolDisplay('Vgs)" \
   -units voltage \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef Vbs \
   -data $dmdata \
   -prompt "Bulk Source Initial Voltage" \
   -type string \
   -units voltage \
   -display "artParameterInToolDisplay('Vbs)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef dtemp \
   -data $dmdata \
   -prompt "Temperature Difference" \
   -type string \
   -display "artParameterInToolDisplay('dtemp)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef geo \
   -data $dmdata \
   -prompt "Source/Drain Selector" \
   -type string \
   -display "artParameterInToolDisplay('geo)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef delvto \
   -data $dmdata \
   -prompt "Threshold Voltage Shift" \
   -type string \
   -units voltage \
   -display "artParameterInToolDisplay('delvto)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef m \
   -data $dmdata \
   -prompt "Multiplier" \
   -defValue "1" \
   -type string \
   -display "artParameterInToolDisplay('m)" \
   -parseAsPEL true \
   -parseAsNumber true
# NetlistInfo hspiceD
   db::setNetlistInfo componentName -cell $dmdata -netlister hspiceD -type symbol -value mosfet
   db::setNetlistInfo instParameters -cell $dmdata -netlister hspiceD -type list -value "(w l ad as pd ps nrd nrs rdc rsc dtemp geo delvto m)"
   db::setNetlistInfo otherParameters -cell $dmdata -netlister hspiceD -type list -value "(model)"
   db::setNetlistInfo namePrefix -cell $dmdata -netlister hspiceD -type symbol -value M
   db::setNetlistInfo netlistProcedure -cell $dmdata -netlister hspiceD -type symbol -value nlPrintSpiceMOSFET
   db::setNetlistInfo termOrder -cell $dmdata -netlister hspiceD -type list -value {(D G S B)}

   db::setProplistProp paramLabelSet -data $dmdata \
      -value "-model w l" -type string
   db::setProplistProp opPointLabelSet -data $dmdata \
      -value "id vgs vds" -type string

oa::save $dmdata
# end analogLib pmos4

# begin analogLib presistor
set dmdata [oa::DMDataOpen [oa::CellFind $lib presistor ] write ]
   db::setCDFProp fieldHeight -data $dmdata \
      -value 35 \
      -type integer
   db::setCDFProp formInitProc -data $dmdata -value {} \
      -type string
   db::createParamDef r \
   -data $dmdata \
   -prompt Resistance \
   -defValue 1k \
   -type string \
   -units resistance \
   -display "artParameterInToolDisplay('r)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef tc1 \
   -data $dmdata \
   -prompt "Temperature Coefficient 1" \
   -type string \
   -display "artParameterInToolDisplay('tc1)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef tc2 \
   -data $dmdata \
   -prompt "Temperature Coefficient 2" \
   -type string \
   -display "artParameterInToolDisplay('tc2)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef m \
   -data $dmdata \
   -prompt "Multiplier" \
   -defValue "1" \
   -type string \
   -display "artParameterInToolDisplay('m)" \
   -parseAsPEL true \
   -parseAsNumber true 
# NetlistInfo hspice
   db::setNetlistInfo componentName -cell $dmdata -netlister hspiceD -type symbol -value presistor
   db::setNetlistInfo instParameters -cell $dmdata -netlister hspiceD -type list -value "(r tc1 tc2 m)"
   db::setNetlistInfo namePrefix -cell $dmdata -netlister hspiceD -type symbol -value R
   db::setNetlistInfo netlistProcedure -cell $dmdata -netlister hspiceD -type symbol -value nlPrintSpiceDevice
   db::setNetlistInfo termOrder -cell $dmdata -netlister hspiceD -type list -value {(PLUS MINUS)}

   db::setProplistProp paramLabelSet -data $dmdata \
      -value "r tc1 tc2" -type string
   db::setProplistProp opPointLabelSet -data $dmdata \
      -value "v i pwr" -type string
      
oa::save $dmdata
# end analogLib presistor

# begin analogLib res
set dmdata [oa::DMDataOpen [oa::CellFind $lib res ] write ]
   db::setCDFProp fieldHeight -data $dmdata \
      -value 35 \
      -type integer
   db::setCDFProp formInitProc -data $dmdata -value {} \
      -type string
   db::createParamDef r \
   -data $dmdata \
   -prompt Resistance \
   -defValue 1k \
   -type string \
   -units resistance \
   -display "artParameterInToolDisplay('r)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef tc1 \
   -data $dmdata \
   -prompt "Temperature Coefficient 1" \
   -type string \
   -display "artParameterInToolDisplay('tc1)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef tc2 \
   -data $dmdata \
   -prompt "Temperature Coefficient 2" \
   -type string \
   -display "artParameterInToolDisplay('tc2)" \
   -parseAsPEL true \
   -parseAsNumber true 
   db::createParamDef m \
   -data $dmdata \
   -prompt "Multiplier" \
   -defValue "1" \
   -type string \
   -display "artParameterInToolDisplay('m)" \
   -parseAsPEL true \
   -parseAsNumber true 
# NetlistInfo hspiceD
   db::setNetlistInfo componentName -cell $dmdata -netlister hspiceD -type symbol -value resistor
   db::setNetlistInfo instParameters -cell $dmdata -netlister hspiceD -type list -value "(r tc1 tc2 m)"
   db::setNetlistInfo namePrefix -cell $dmdata -netlister hspiceD -type symbol -value R
   db::setNetlistInfo netlistProcedure -cell $dmdata -netlister hspiceD -type symbol -value nlPrintSpiceDevice
   db::setNetlistInfo termOrder -cell $dmdata -netlister hspiceD -type list -value {(PLUS MINUS)}

   db::setProplistProp paramLabelSet -data $dmdata \
      -value "r tc1 tc2" -type string
   db::setProplistProp opPointLabelSet -data $dmdata \
      -value "v i pwr" -type string
      
oa::save $dmdata
# end analogLib res

# begin analogLib vdc
set dmdata [oa::DMDataOpen [oa::CellFind $lib vdc ] write ]
   db::setCDFProp fieldHeight -data $dmdata \
      -value 35 \
      -type integer
   db::setCDFProp formInitProc -data $dmdata -value {} \
      -type string
   db::createParamDef vdc \
   -data $dmdata \
   -prompt "DC Voltage" \
   -defValue "0" \
   -type string \
   -units voltage \
   -display "artParameterInToolDisplay('vdc)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef acm \
   -data $dmdata \
   -prompt "AC Magnitude" \
   -type string \
   -units voltage \
   -display "artParameterInToolDisplay('acm)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef acp \
   -data $dmdata \
   -prompt "AC Phase" \
   -type string \
   -display "artParameterInToolDisplay('acp)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef m \
   -data $dmdata \
   -prompt "Multiplier" \
   -type string \
   -display "artParameterInToolDisplay('m)" \
   -parseAsPEL true \
   -parseAsNumber true
# NetlistInfo hspiceD
   db::setNetlistInfo componentName -cell $dmdata -netlister hspiceD -type symbol -value vsrc
   db::setNetlistInfo instParameters -cell $dmdata -netlister hspiceD -type list -value "(vdc acm acp m)"
   db::setNetlistInfo namePrefix -cell $dmdata -netlister hspiceD -type symbol -value V
   db::setNetlistInfo netlistProcedure -cell $dmdata -netlister hspiceD -type symbol -value nlPrintSpiceSource
   db::setNetlistInfo propMapping -cell $dmdata -netlister hspiceD -type list -value "(nil DC vdc AC acm)"
   db::setNetlistInfo termOrder -cell $dmdata -netlister hspiceD -type list -value {(PLUS MINUS)}
   
   db::setProplistProp paramLabelSet -data $dmdata \
      -value "vdc acm acp" -type string

oa::save $dmdata
# end analogLib vdc

# begin analogLib vexp
set dmdata [oa::DMDataOpen [oa::CellFind $lib vexp ] write ]
   db::setCDFProp fieldHeight -data $dmdata \
      -value 35 \
      -type integer
   db::setCDFProp formInitProc -data $dmdata -value {} \
      -type string
   db::createParamDef vdc \
   -data $dmdata \
   -prompt "DC Voltage" \
   -defValue "0" \
   -type string \
   -units voltage \
   -display "artParameterInToolDisplay('vdc)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef type \
   -data $dmdata \
   -prompt "Type" \
   -defValue "EXP" \
   -type string \
   -display false \
   -editable false
   db::createParamDef v1 \
   -data $dmdata \
   -prompt "Voltage1" \
   -type string \
   -units voltage \
   -display "artParameterInToolDisplay('v1)" \
   -defValue "0" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef v2 \
   -data $dmdata \
   -prompt "Voltage2" \
   -type string \
   -units voltage \
   -display "artParameterInToolDisplay('v2)" \
   -defValue "0" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef td1 \
   -data $dmdata \
   -prompt "Rise Delay Time" \
   -type string \
   -units time \
   -display "artParameterInToolDisplay('td1)" \
   -defValue "0" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef td2 \
   -data $dmdata \
   -prompt "Fall Delay Time" \
   -type string \
   -units time \
   -display "artParameterInToolDisplay('td2)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef t1 \
   -data $dmdata \
   -prompt "Rise Time Constant" \
   -type string \
   -display "artParameterInToolDisplay('t1)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef t2 \
   -data $dmdata \
   -prompt "Fall Time Constant" \
   -type string \
   -display "artParameterInToolDisplay('t2)" \
   -defValue "0" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef acm \
   -data $dmdata \
   -prompt "AC Magnitude" \
   -defValue "0" \
   -type string \
   -units voltage \
   -display "artParameterInToolDisplay('acm)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef acp \
   -data $dmdata \
   -prompt "AC Phase" \
   -defValue "0" \
   -type string \
   -display "artParameterInToolDisplay('acp)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef m \
   -data $dmdata \
   -prompt "Multiplier" \
   -type string \
   -display "artParameterInToolDisplay('m)" \
   -parseAsPEL true \
   -parseAsNumber true
# NetlistInfo hspiceD
   db::setNetlistInfo componentName -cell $dmdata -netlister hspiceD -type symbol -value vsrc
   db::setNetlistInfo instParameters -cell $dmdata -netlister hspiceD -type list -value "(vdc v1 v2 td1 t1 td2 t2 acm acp m)"
   db::setNetlistInfo namePrefix -cell $dmdata -netlister hspiceD -type symbol -value V
   db::setNetlistInfo netlistProcedure -cell $dmdata -netlister hspiceD -type symbol -value nlPrintSpiceSource
   db::setNetlistInfo propMapping -cell $dmdata -netlister hspiceD -type list -value "(nil DC vdc AC acm)"
   db::setNetlistInfo termOrder -cell $dmdata -netlister hspiceD -type list -value {(PLUS MINUS)}
   oa::save $dmdata
# end analogLib vexp

# begin analogLib vpulse
set dmdata [oa::DMDataOpen [oa::CellFind $lib vpulse ] write ]
   db::setCDFProp fieldHeight -data $dmdata \
      -value 35 \
      -type integer
   db::setCDFProp formInitProc -data $dmdata -value {} \
      -type string
   db::createParamDef vdc \
   -data $dmdata \
   -prompt "DC Voltage" \
   -defValue "0" \
   -type string \
   -units voltage \
   -display "artParameterInToolDisplay('vdc)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef type \
   -data $dmdata \
   -prompt "Type" \
   -defValue "PULSE" \
   -type string \
   -display false \
   -editable false
   db::createParamDef v1 \
   -data $dmdata \
   -prompt "Voltage1" \
   -type string \
   -units voltage \
   -display "artParameterInToolDisplay('v1)" \
   -defValue "0" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef v2 \
   -data $dmdata \
   -prompt "Voltage2" \
   -type string \
   -units voltage \
   -display "artParameterInToolDisplay('v2)" \
   -defValue "0" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef td \
   -data $dmdata \
   -prompt "Delay Time" \
   -type string \
   -units time \
   -display "artParameterInToolDisplay('td)" \
   -defValue "0" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef tr \
   -data $dmdata \
   -prompt "Rise Time" \
   -type string \
   -units time \
   -display "artParameterInToolDisplay('tr)" \
   -defValue "0" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef tf \
   -data $dmdata \
   -prompt "Fall Time" \
   -type string \
   -units time \
   -display "artParameterInToolDisplay('tf)" \
   -defValue "0" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef pw \
   -data $dmdata \
   -prompt "Pulse Width" \
   -type string \
   -units time \
   -display "artParameterInToolDisplay('pw)" \
   -defValue "0" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef per \
   -data $dmdata \
   -prompt "Period" \
   -type string \
   -units time \
   -display "artParameterInToolDisplay('per)" \
   -defValue "0" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef acm \
   -data $dmdata \
   -prompt "AC Magnitude" \
   -defValue "0" \
   -type string \
   -units voltage \
   -display "artParameterInToolDisplay('acm)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef acp \
   -data $dmdata \
   -prompt "AC Phase" \
   -defValue "0" \
   -type string \
   -display "artParameterInToolDisplay('acp)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef m \
   -data $dmdata \
   -prompt "Multiplier" \
   -type string \
   -display "artParameterInToolDisplay('m)" \
   -parseAsPEL true \
   -parseAsNumber true
# NetlistInfo hspiceD
   db::setNetlistInfo componentName -cell $dmdata -netlister hspiceD -type symbol -value vsrc
   db::setNetlistInfo instParameters -cell $dmdata -netlister hspiceD -type list -value "(vdc v1 v2 td tr tf pw per acm acp m)"
   db::setNetlistInfo namePrefix -cell $dmdata -netlister hspiceD -type symbol -value V
   db::setNetlistInfo netlistProcedure -cell $dmdata -netlister hspiceD -type symbol -value nlPrintSpiceSource
   db::setNetlistInfo propMapping -cell $dmdata -netlister hspiceD -type list -value "(nil DC vdc AC acm)"
   db::setNetlistInfo termOrder -cell $dmdata -netlister hspiceD -type list -value {(PLUS MINUS)}
 oa::save $dmdata
     
# end analogLib vpulse

# begin analogLib vpwl
set dmdata [oa::DMDataOpen [oa::CellFind $lib vpwl ] write ]
   db::setCDFProp fieldHeight -data $dmdata \
      -value 35 \
      -type integer
   db::setCDFProp formInitProc -data $dmdata -value {} \
      -type string
   db::createParamDef vdc \
   -data $dmdata \
   -prompt "DC Voltage" \
   -defValue "0" \
   -type string \
   -units voltage \
   -display "artParameterInToolDisplay('vdc)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef type \
   -data $dmdata \
   -prompt "Type" \
   -defValue "PWL" \
   -type string \
   -display false \
   -editable false
   db::createParamDef tvpairs \
   -data $dmdata \
   -prompt "Time Value Pairs" \
   -defValue 10 \
   -type int \
   -display "artParameterInToolDisplay('tvpairs)"
   db::createParamDef t1 \
   -data $dmdata \
   -prompt "Time1" \
   -type string \
   -units time \
   -display "artParameterInToolDisplay('t1)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef v1 \
   -data $dmdata \
   -prompt "Voltage1" \
   -type string \
   -units voltage \
   -display "artParameterInToolDisplay('v1)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef t2 \
   -data $dmdata \
   -prompt "Time2" \
   -type string \
   -units time \
   -display "artParameterInToolDisplay('t2)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef v2 \
   -data $dmdata \
   -prompt "Voltage2" \
   -type string \
   -units voltage \
   -display "artParameterInToolDisplay('v2)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef t3 \
   -data $dmdata \
   -prompt "Time3" \
   -type string \
   -units time \
   -display "artParameterInToolDisplay('t3)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef v3 \
   -data $dmdata \
   -prompt "Voltage3" \
   -type string \
   -units voltage \
   -display "artParameterInToolDisplay('v3)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef t4 \
   -data $dmdata \
   -prompt "Time4" \
   -type string \
   -units time \
   -display "artParameterInToolDisplay('t4)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef v4 \
   -data $dmdata \
   -prompt "Voltage4" \
   -type string \
   -units voltage \
   -display "artParameterInToolDisplay('v4)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef t5 \
   -data $dmdata \
   -prompt "Time5" \
   -type string \
   -units time \
   -display "artParameterInToolDisplay('t5)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef v5 \
   -data $dmdata \
   -prompt "Voltage5" \
   -type string \
   -units voltage \
   -display "artParameterInToolDisplay('v5)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef t6 \
   -data $dmdata \
   -prompt "Time6" \
   -type string \
   -units time \
   -display "artParameterInToolDisplay('t6)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef v6 \
   -data $dmdata \
   -prompt "Voltage6" \
   -type string \
   -units voltage \
   -display "artParameterInToolDisplay('v6)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef t7 \
   -data $dmdata \
   -prompt "Time7" \
   -type string \
   -units time \
   -display "artParameterInToolDisplay('t7)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef v7 \
   -data $dmdata \
   -prompt "Voltage7" \
   -type string \
   -units voltage \
   -display "artParameterInToolDisplay('v7)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef t8 \
   -data $dmdata \
   -prompt "Time8" \
   -type string \
   -units time \
   -display "artParameterInToolDisplay('t8)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef v8 \
   -data $dmdata \
   -prompt "Voltage8" \
   -type string \
   -units voltage \
   -display "artParameterInToolDisplay('v8)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef t9 \
   -data $dmdata \
   -prompt "Time9" \
   -type string \
   -units time \
   -display "artParameterInToolDisplay('t9)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef v9 \
   -data $dmdata \
   -prompt "Voltage9" \
   -type string \
   -units voltage \
   -display "artParameterInToolDisplay('v9)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef t10 \
   -data $dmdata \
   -prompt "Time10" \
   -type string \
   -units time \
   -display "artParameterInToolDisplay('t10)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef v10 \
   -data $dmdata \
   -prompt "Voltage10" \
   -type string \
   -units voltage \
   -display "artParameterInToolDisplay('v10)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef r \
   -data $dmdata \
   -prompt "Repeat" \
   -type string \
   -units time \
   -display "artParameterInToolDisplay('r)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef td \
   -data $dmdata \
   -prompt "Delay Time" \
   -type string \
   -units time \
   -display "artParameterInToolDisplay('td)" \
   -defValue "0" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef acm \
   -data $dmdata \
   -prompt "AC Magnitude" \
   -defValue "0" \
   -type string \
   -units voltage \
   -display "artParameterInToolDisplay('acm)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef acp \
   -data $dmdata \
   -prompt "AC Phase" \
   -defValue "0" \
   -type string \
   -display "artParameterInToolDisplay('acp)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef m \
   -data $dmdata \
   -prompt "Multiplier" \
   -type string \
   -display "artParameterInToolDisplay('m)" \
   -parseAsPEL true \
   -parseAsNumber true
# NetlistInfo hspiceD
   db::setNetlistInfo componentName -cell $dmdata -netlister hspiceD -type symbol -value vsrc
   db::setNetlistInfo instParameters -cell $dmdata -netlister hspiceD -type list -value "(vdc t1 v1 t2 v2 t3 v3 t4 v4 t5 v5 t6 v6 t7 v7 t8 v8 t9 v9 t10 v10 r td acm acp m)"
   db::setNetlistInfo namePrefix -cell $dmdata -netlister hspiceD -type symbol -value V
   db::setNetlistInfo netlistProcedure -cell $dmdata -netlister hspiceD -type symbol -value nlPrintSpiceSource
   db::setNetlistInfo propMapping -cell $dmdata -netlister hspiceD -type list -value "(nil DC vdc AC acm)"
   db::setNetlistInfo termOrder -cell $dmdata -netlister hspiceD -type list -value {(PLUS MINUS)}

oa::save $dmdata
      
# end analogLib vpwl

# begin analogLib vsin
set dmdata [oa::DMDataOpen [oa::CellFind $lib vsin ] write ]
   db::setCDFProp fieldHeight -data $dmdata \
      -value 35 \
      -type integer
   db::setCDFProp formInitProc -data $dmdata -value {} \
      -type string
   db::createParamDef vdc \
   -data $dmdata \
   -prompt "DC Voltage" \
   -defValue "0" \
   -type string \
   -units voltage \
   -display "artParameterInToolDisplay('vdc)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef type \
   -data $dmdata \
   -prompt "Type" \
   -defValue "SIN" \
   -type string \
   -display false \
   -editable false
   db::createParamDef vo \
   -data $dmdata \
   -prompt "Voltage Offset" \
   -type string \
   -units voltage \
   -display "artParameterInToolDisplay('vo)" \
   -defValue "0" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef va \
   -data $dmdata \
   -prompt "Voltage Amplitude" \
   -type string \
   -units voltage \
   -display "artParameterInToolDisplay('va)" \
   -defValue "0" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef freq \
   -data $dmdata \
   -prompt "Frequency" \
   -type string \
   -units frequency \
   -display "artParameterInToolDisplay('freq)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef td \
   -data $dmdata \
   -prompt "Delay Time" \
   -type string \
   -units time \
   -display "artParameterInToolDisplay('td)" \
   -defValue "0" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef q \
   -data $dmdata \
   -prompt "Damping Factor" \
   -type string \
   -display "artParameterInToolDisplay('q)" \
   -defValue "0" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef j \
   -data $dmdata \
   -prompt "Phase Delay" \
   -type string \
   -display "artParameterInToolDisplay('j)" \
   -defValue "0" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef acm \
   -data $dmdata \
   -prompt "AC Magnitude" \
   -defValue "0" \
   -type string \
   -units voltage \
   -display "artParameterInToolDisplay('acm)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef acp \
   -data $dmdata \
   -prompt "AC Phase" \
   -defValue "0" \
   -type string \
   -display "artParameterInToolDisplay('acp)" \
   -parseAsPEL true \
   -parseAsNumber true
   db::createParamDef m \
   -data $dmdata \
   -prompt "Multiplier" \
   -type string \
   -display "artParameterInToolDisplay('m)" \
   -parseAsPEL true \
   -parseAsNumber true
# NetlistInfo hspiceD
   db::setNetlistInfo componentName -cell $dmdata -netlister hspiceD -type symbol -value vsrc
   db::setNetlistInfo instParameters -cell $dmdata -netlister hspiceD -type list -value "(vdc vo va freq td q j acm acp m)"
   db::setNetlistInfo namePrefix -cell $dmdata -netlister hspiceD -type symbol -value V
   db::setNetlistInfo netlistProcedure -cell $dmdata -netlister hspiceD -type symbol -value nlPrintSpiceSource
   db::setNetlistInfo propMapping -cell $dmdata -netlister hspiceD -type list -value "(nil DC vdc AC acm)"
   db::setNetlistInfo termOrder -cell $dmdata -netlister hspiceD -type list -value {(PLUS MINUS)}
   oa::save $dmdata
# end analogLib vsin
