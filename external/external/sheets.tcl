#
# Procedures for returning the symbol data
#

proc slDefinesheetsSymbols { } {
return {
{ Title symbol schematicSymbol   
   { InstRect { {0 0} {940 220} } }
   { RectBorder { {0 0} {940 220} } }
   { RectBorder { {10 10} {930 210} } }
   { LineBorder {10 130} {930 130} }
   { LineBorder {10 70} {930 70} }
   { LineBorder {380 130} {380 10} }
   { LineBorder {760 130} {760 210} }
   { LineBorder {760 170} {930 170} }
   { Text Title {20 195} lowerLeft R0 stick 12 0 1 1 }
   { Text Design {20 115} lowerLeft R0 stick 12 0 1 1 }
   { Text Owner {20 55} lowerLeft R0 stick 12 0 1 1 }
   { Text Updated {390 115} lowerLeft R0 stick 12 0 1 1 }
   { Text Company {390 55} lowerLeft R0 stick 12 0 1 1 }
   { Text DocNo {770 195} lowerLeft R0 stick 12 0 1 1 }
   { Text Rev {770 155} lowerLeft R0 stick 12 0 1 1 }
}
   
}
}
