#
# Procedures for returning the symbol data
#

proc slDefinesampleSymbols { } {
return {
{ an2 { symbol } schematicSymbol
   { PinRect a signal false input { {-4 36} {4 44} } }
   { PinRect b signal false input { {-4 -4} {4 4} } }
   { PinRect z signal false output { {196 16} {204 24} } }
   { InstRect { {0 -10} {200 50} } }
   { Arc  { {75 -10} {135 50}} -90 90 }
   { Line {0 0} {60 0}}
   { Line {0 40} {60 40}}
   { Line {105 -10} {60 -10} {60 50} {105 50}}
   { Line {135 20} {200 20}}
   { EvalTextTerm cdsTerm("a") {-5 45} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("b") {-5 5} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("z") {205 25} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {200 50} upperRight R0 stick 8 cdsSkillEvalText 0 1 1 }
}
{ an3 { symbol } schematicSymbol
   { PinRect a signal false input { {-4 36} {4 44} } }
   { PinRect b signal false input { {-4 16} {4 24} } }
   { PinRect c signal false input { {-4 -4} {4 4} } }
   { PinRect z signal false output { {196 16} {204 24} } }
   { InstRect { {0 -10} {200 50} } }
   { Arc  { {75 -10} {135 50}} -90 90 }
   { Line {0 0} {60 0}}
   { Line {0 20} {60 20}}
   { Line {0 40} {60 40}}
   { Line {105 -10} {60 -10} {60 50} {105 50}}
   { Line {135 20} {200 20}}
   { EvalTextTerm cdsTerm("a") {-5 45} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("b") {-5 25} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("c") {-5 5} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("z") {205 25} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {200 50} upperRight R0 stick 8 cdsSkillEvalText 0 1 1 }
}
{ an4 { symbol } schematicSymbol
   { PinRect a signal false input { {-4 36} {4 44} } }
   { PinRect b signal false input { {-4 16} {4 24} } }
   { PinRect c signal false input { {-4 -4} {4 4} } }
   { PinRect d signal false input { {-4 -24} {4 -16} } }
   { PinRect z signal false output { {196 16} {204 24} } }
   { InstRect { {0 -30} {200 50} } }
   { Arc  { {75 -10} {135 50}} -90 90 }
   { Line {0 -20} {60 -20}}
   { Line {0 0} {60 0}}
   { Line {0 20} {60 20}}
   { Line {0 40} {60 40}}
   { Line {60 -10} {60 -30}}
   { Line {105 -10} {60 -10} {60 50} {105 50}}
   { Line {135 20} {200 20}}
   { EvalTextTerm cdsTerm("a") {-5 45} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("b") {-5 25} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("c") {-5 5} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("d") {-5 -15} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("z") {205 25} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {200 50} upperRight R0 stick 8 cdsSkillEvalText 0 1 1 }
}
{ aoi21 { symbol } schematicSymbol
   { PinRect a1 signal false input { {-4 56} {4 64} } }
   { PinRect a2 signal false input { {-4 16} {4 25} } }
   { PinRect b signal false input { {-4 -4} {4 4} } }
   { PinRect z signal false output { {196 16} {204 24} } }
   { InstRect { {0 -10} {200 70} } }
   { Arc  { {35 10} {95 70}} -90 90 }
   { Arc { {4 -30} {104 70}} -37 37 }
   { Arc { {57 -70} {177 50}} 30 90 }
   { Arc { {57 -10} {177 110}} 270 330 }
   { Ellipse { {169 15} {179 25} } }
   { Line {0 0} {99 0}}
   { Line {0 20} {20 20}}
   { Line {0 60} {20 60}}
   { Line {65 10} {20 10} {20 70} {65 70}}
   { Line {179 20} {200 20}}
   { Line {94 50} {117 50}}
   { Line {94 -10} {117 -10}}
   { EvalTextTerm cdsTerm("a1") {-5 65} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("a2") {-5 25} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("b") {-5 5} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("z") {205 25} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {200 50} upperRight R0 stick 8 cdsSkillEvalText 0 1 1 }
}
{ aoi31 { symbol } schematicSymbol
   { PinRect a1 signal false input { {-4 56} {4 64} } }
   { PinRect a2 signal false input { {-4 36} {4 45} } }
   { PinRect a3 signal false input { {-4 16} {4 25} } }
   { PinRect b signal false input { {-4 -4} {4 4} } }
   { PinRect z signal false output { {196 16} {204 24} } }
   { InstRect { {0 -10} {200 70} } }
   { Arc  { {35 10} {95 70}} -90 90 }
   { Arc { {4 -30} {104 70}} -37 37 }
   { Arc { {57 -70} {177 50}} 30 90 }
   { Arc { {57 -10} {177 110}} 270 330 }
   { Ellipse { {169 15} {179 25} } }
   { Line {0 0} {99 0}}
   { Line {0 20} {20 20}}
   { Line {0 40} {20 40}}
   { Line {0 60} {20 60}}
   { Line {65 10} {20 10} {20 70} {65 70}}
   { Line {179 20} {200 20}}
   { Line {94 50} {117 50}}
   { Line {94 -10} {117 -10}}
   { EvalTextTerm cdsTerm("a1") {-5 65} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("a2") {-5 45} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("a3") {-5 25} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("b") {-5 5} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("z") {205 25} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {200 50} upperRight R0 stick 8 cdsSkillEvalText 0 1 1 }
}
{ aon21b { symbol } schematicSymbol
   { PinRect a1 signal false input { {-4 56} {4 64} } }
   { PinRect a2 signal false input { {-4 16} {4 25} } }
   { PinRect b signal false input { {-4 -4} {4 4} } }
   { PinRect z signal false output { {196 16} {204 24} } }
   { InstRect { {0 -10} {200 70} } }
   { Arc  { {35 10} {95 70}} -90 90 }
   { Arc { {4 -30} {104 70}} -37 37 }
   { Arc { {57 -70} {177 50}} 30 90 }
   { Arc { {57 -10} {177 110}} 270 330 }
   { Ellipse { {89 -5} {99 5} } }
   { Line {0 0} {89 0}}
   { Line {0 20} {20 20}}
   { Line {0 60} {20 60}}
   { Line {65 10} {20 10} {20 70} {65 70}}
   { Line {169 20} {200 20}}
   { Line {94 50} {117 50}}
   { Line {94 -10} {117 -10}}
   { EvalTextTerm cdsTerm("a1") {-5 65} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("a2") {-5 25} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("b") {-5 5} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("z") {205 25} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {200 50} upperRight R0 stick 8 cdsSkillEvalText 0 1 1 }
}
{ bf1 { symbol } schematicSymbol
   { PinRect a signal false input { {-4 -4} {4 4} } }
   { PinRect z signal false output { {116 -4} {124 4} } }
   { InstRect { {0 -25} {120 25} } }
   { Line {0 0} {34 0}}
   { Line {34 -24} {34 24} {75 0} {34 -24}}
   { Line {75 0} {120 0}}
   { EvalTextTerm cdsTerm("a") {-5 5} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("z") {125 5} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {120 30} upperRight R0 stick 8 cdsSkillEvalText 0 1 1 }
}
{ dly1 { symbol } schematicSymbol
   { PinRect a signal false input { {-4 -4} {4 4} } }
   { PinRect z signal false output { {116 -4} {124 4} } }
   { InstRect { {0 -25} {120 25} } }
   { Line {0 0} {34 0}}
   { Line {34 -24} {34 24} {75 0} {34 -24}}
   { Line {75 0} {120 0}}
   { EvalTextTerm cdsTerm("a") {-5 5} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("z") {125 5} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {120 30} upperRight R0 stick 8 cdsSkillEvalText 0 1 1 }
}
{ dly2 { symbol } schematicSymbol
   { PinRect a signal false input { {-4 -4} {4 4} } }
   { PinRect z signal false output { {116 -4} {124 4} } }
   { InstRect { {0 -25} {120 25} } }
   { Line {0 0} {34 0}}
   { Line {34 -24} {34 24} {75 0} {34 -24}}
   { Line {75 0} {120 0}}
   { EvalTextTerm cdsTerm("a") {-5 5} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("z") {125 5} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {120 30} upperRight R0 stick 8 cdsSkillEvalText 0 1 1 }
}
{ ha2 { symbol } schematicSymbol
   { PinRect a signal false input { {-4 36} {4 44} } }
   { PinRect b signal false input { {-4 -4} {4 4} } }
   { PinRect co signal false output { {116 36} {124 44} } }
   { PinRect so signal false output { {116 -4} {124 4} } }
   { InstRect { {0 -10} {120 50} } }
   { Line {0 0} {30 0}}
   { Line {0 40} {30 40}}
   { Line {90 0} {120 0}}
   { Line {90 40} {120 40}}
   { Rect {{30 -10} {90 50}} }
   { EvalTextTerm cdsTerm("a") {-5 45} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("b") {-5 5} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("co") {125 45} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("so") {125 45} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {115 50} upperRight R0 stick 8 cdsSkillEvalText 0 1 1 }
}
{ iv1 { symbol } schematicSymbol
   { PinRect a signal false input { {-4 -4} {4 4} } }
   { PinRect z signal false output { {116 -4} {124 4} } }
   { InstRect { {0 -25} {120 25} } }
   { Ellipse { {75 -5} {85 5} } }
   { Line {0 0} {34 0}}
   { Line {34 -24} {34 24} {75 0} {34 -24}}
   { Line {85 0} {120 0}}
   { EvalTextTerm cdsTerm("a") {-5 5} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("z") {125 5} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {120 30} upperRight R0 stick 8 cdsSkillEvalText 0 1 1 }
}
{ nd2 { symbol } schematicSymbol
   { PinRect a signal false input { {-4 36} {4 44} } }
   { PinRect b signal false input { {-4 -4} {4 4} } }
   { PinRect z signal false output { {196 16} {204 24} } }
   { InstRect { {0 -10} {200 50} } }
   { Arc  { {75 -10} {135 50}} -90 90 }
   { Ellipse { {135 15} {145 25} } }
   { Line {0 0} {60 0}}
   { Line {0 40} {60 40}}
   { Line {105 -10} {60 -10} {60 50} {105 50}}
   { Line {145 20} {200 20}}
   { EvalTextTerm cdsTerm("a") {-5 45} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("b") {-5 5} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("z") {205 25} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {200 50} upperRight R0 stick 8 cdsSkillEvalText 0 1 1 }
}
{ nd2a { symbol } schematicSymbol
   { PinRect a signal false input { {-4 36} {4 44} } }
   { PinRect b signal false input { {-4 -4} {4 4} } }
   { PinRect z signal false output { {196 16} {204 24} } }
   { InstRect { {0 -10} {200 50} } }
   { Arc  { {75 -10} {135 50}} -90 90 }
   { Ellipse { {135 15} {145 25} } }
   { Ellipse { {50 35} {60 45} } }
   { Line {0 0} {60 0}}
   { Line {0 40} {50 40}}
   { Line {105 -10} {60 -10} {60 50} {105 50}}
   { Line {145 20} {200 20}}
   { EvalTextTerm cdsTerm("a") {-5 45} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("b") {-5 5} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("z") {205 25} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {200 50} upperRight R0 stick 8 cdsSkillEvalText 0 1 1 }
}
{ nd2ab { symbol } schematicSymbol
   { PinRect a signal false input { {-4 36} {4 44} } }
   { PinRect b signal false input { {-4 -4} {4 4} } }
   { PinRect z signal false output { {196 16} {204 24} } }
   { InstRect { {0 -10} {200 50} } }
   { Arc  { {75 -10} {135 50}} -90 90 }
   { Ellipse { {135 15} {145 25} } }
   { Ellipse { {50 35} {60 45} } }
   { Ellipse { {50 -5} {60 5} } }
   { Line {0 0} {50 0}}
   { Line {0 40} {50 40}}
   { Line {105 -10} {60 -10} {60 50} {105 50}}
   { Line {145 20} {200 20}}
   { EvalTextTerm cdsTerm("a") {-5 45} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("b") {-5 5} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("z") {205 25} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {200 50} upperRight R0 stick 8 cdsSkillEvalText 0 1 1 }
}
{ nd3 { symbol } schematicSymbol
   { PinRect a signal false input { {-4 36} {4 44} } }
   { PinRect b signal false input { {-4 16} {4 24} } }
   { PinRect c signal false input { {-4 -4} {4 4} } }
   { PinRect z signal false output { {196 16} {204 24} } }
   { InstRect { {0 -10} {200 50} } }
   { Arc  { {75 -10} {135 50}} -90 90 }
   { Ellipse { {135 15} {145 25} } }
   { Line {0 0} {60 0}}
   { Line {0 20} {60 20}}
   { Line {0 40} {60 40}}
   { Line {105 -10} {60 -10} {60 50} {105 50}}
   { Line {145 20} {200 20}}
   { EvalTextTerm cdsTerm("a") {-5 45} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("b") {-5 25} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("c") {-5 5} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("z") {205 25} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {200 50} upperRight R0 stick 8 cdsSkillEvalText 0 1 1 }
}
{ nd4 { symbol } schematicSymbol
   { PinRect a signal false input { {-4 36} {4 44} } }
   { PinRect b signal false input { {-4 16} {4 24} } }
   { PinRect c signal false input { {-4 -4} {4 4} } }
   { PinRect d signal false input { {-4 -24} {4 -16} } }
   { PinRect z signal false output { {196 16} {204 24} } }
   { InstRect { {0 -30} {200 50} } }
   { Arc  { {75 -10} {135 50}} -90 90 }
   { Ellipse { {135 15} {145 25} } }
   { Line {0 -20} {60 -20}}
   { Line {0 0} {60 0}}
   { Line {0 20} {60 20}}
   { Line {0 40} {60 40}}
   { Line {60 -10} {60 -30}}
   { Line {105 -10} {60 -10} {60 50} {105 50}}
   { Line {145 20} {200 20}}
   { EvalTextTerm cdsTerm("a") {-5 45} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("b") {-5 25} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("c") {-5 5} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("d") {-5 -15} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("z") {205 25} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {200 50} upperRight R0 stick 8 cdsSkillEvalText 0 1 1 }
}
{ nr2 { symbol } schematicSymbol
   { PinRect a signal false input { {-4 36} {4 44} } }
   { PinRect b signal false input { {-4 -4} {4 4} } }
   { PinRect z signal false output { {196 16} {204 24} } }
   { InstRect { {0 -10} {200 50} } }
   { Arc { {-30 -30} {70 70}} -37 37 }
   { Arc { {23 -70} {143 50}} 30 90 }
   { Arc { {23 -10} {143 110}} 270 330 }
   { Ellipse { {135 15} {145 25} } }
   { Line {0 0} {65 0}}
   { Line {0 40} {65 40}}
   { Line {145 20} {200 20}}
   { Line {60 50} {83 50}}
   { Line {60 -10} {83 -10}}
   { EvalTextTerm cdsTerm("a") {-5 45} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("b") {-5 5} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("z") {205 25} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {200 50} upperRight R0 stick 8 cdsSkillEvalText 0 1 1 }
}
{ nr2a { symbol } schematicSymbol
   { PinRect a signal false input { {-4 36} {4 44} } }
   { PinRect b signal false input { {-4 -4} {4 4} } }
   { PinRect z signal false output { {196 16} {204 24} } }
   { InstRect { {0 -10} {200 50} } }
   { Arc { {-30 -30} {70 70}} -37 37 }
   { Arc { {23 -70} {143 50}} 30 90 }
   { Arc { {23 -10} {143 110}} 270 330 }
   { Ellipse { {135 15} {145 25} } }
   { Ellipse { {55 35} {65 45} } }
   { Line {0 0} {65 0}}
   { Line {0 40} {55 40}}
   { Line {145 20} {200 20}}
   { Line {60 50} {83 50}}
   { Line {60 -10} {83 -10}}
   { EvalTextTerm cdsTerm("a") {-5 45} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("b") {-5 5} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("z") {205 25} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {200 50} upperRight R0 stick 8 cdsSkillEvalText 0 1 1 }
}
{ nr3 { symbol } schematicSymbol
   { PinRect a signal false input { {-4 36} {4 44} } }
   { PinRect b signal false input { {-4 16} {4 24} } }
   { PinRect c signal false input { {-4 -4} {4 4} } }
   { PinRect z signal false output { {196 16} {204 24} } }
   { InstRect { {0 -10} {200 50} } }
   { Arc { {-30 -30} {70 70}} -37 37 }
   { Arc { {23 -70} {143 50}} 30 90 }
   { Arc { {23 -10} {143 110}} 270 330 }
   { Ellipse { {135 15} {145 25} } }
   { Line {0 0} {65 0}}
   { Line {0 20} {70 20}}
   { Line {0 40} {65 40}}
   { Line {145 20} {200 20}}
   { Line {60 50} {83 50}}
   { Line {60 -10} {83 -10}}
   { EvalTextTerm cdsTerm("a") {-5 45} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("b") {-5 25} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("c") {-5 5} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("z") {205 25} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {200 50} upperRight R0 stick 8 cdsSkillEvalText 0 1 1 }
}
{ nr4 { symbol } schematicSymbol
   { PinRect a signal false input { {-4 36} {4 44} } }
   { PinRect b signal false input { {-4 16} {4 24} } }
   { PinRect c signal false input { {-4 -4} {4 4} } }
   { PinRect d signal false input { {-4 -24} {4 -16} } }
   { PinRect z signal false output { {196 16} {204 24} } }
   { InstRect { {0 -30} {200 50} } }
   { Arc { {-30 -30} {70 70}} -37 37 }
   { Arc { {23 -70} {143 50}} 30 90 }
   { Arc { {23 -10} {143 110}} 270 330 }
   { Ellipse { {135 15} {145 25} } }
   { Line {0 -20} {60 -20}}
   { Line {0 0} {65 0}}
   { Line {0 20} {70 20}}
   { Line {0 40} {65 40}}
   { Line {145 20} {200 20}}
   { Line {60 50} {83 50}}
   { Line {60 -10} {83 -10}}
   { Line {60 -10} {60 -30}}
   { EvalTextTerm cdsTerm("a") {-5 45} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("b") {-5 25} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("c") {-5 5} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("d") {-5 -15} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("z") {205 25} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {200 50} upperRight R0 stick 8 cdsSkillEvalText 0 1 1 }
}
{ or2 { symbol } schematicSymbol
   { PinRect a signal false input { {-4 36} {4 44} } }
   { PinRect b signal false input { {-4 -4} {4 4} } }
   { PinRect z signal false output { {196 16} {204 24} } }
   { InstRect { {0 -10} {200 50} } }
   { Arc { {-30 -30} {70 70}} -37 37 }
   { Arc { {23 -70} {143 50}} 30 90 }
   { Arc { {23 -10} {143 110}} 270 330 }
   { Line {0 0} {65 0}}
   { Line {0 40} {65 40}}
   { Line {135 20} {200 20}}
   { Line {60 50} {83 50}}
   { Line {60 -10} {83 -10}}
   { EvalTextTerm cdsTerm("a") {-5 45} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("b") {-5 5} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("z") {205 25} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {200 50} upperRight R0 stick 8 cdsSkillEvalText 0 1 1 }
}
{ or3 { symbol } schematicSymbol
   { PinRect a signal false input { {-4 36} {4 44} } }
   { PinRect b signal false input { {-4 16} {4 24} } }
   { PinRect c signal false input { {-4 -4} {4 4} } }
   { PinRect z signal false output { {196 16} {204 24} } }
   { InstRect { {0 -10} {200 50} } }
   { Arc { {-30 -30} {70 70}} -37 37 }
   { Arc { {23 -70} {143 50}} 30 90 }
   { Arc { {23 -10} {143 110}} 270 330 }
   { Line {0 0} {65 0}}
   { Line {0 20} {70 20}}
   { Line {0 40} {65 40}}
   { Line {135 20} {200 20}}
   { Line {60 50} {83 50}}
   { Line {60 -10} {83 -10}}
   { EvalTextTerm cdsTerm("a") {-5 45} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("b") {-5 25} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("c") {-5 5} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("z") {205 25} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {200 50} upperRight R0 stick 8 cdsSkillEvalText 0 1 1 }
}
{ or4 { symbol } schematicSymbol
   { PinRect a signal false input { {-4 36} {4 44} } }
   { PinRect b signal false input { {-4 16} {4 24} } }
   { PinRect c signal false input { {-4 -4} {4 4} } }
   { PinRect d signal false input { {-4 -24} {4 -16} } }
   { PinRect z signal false output { {196 16} {204 24} } }
   { InstRect { {0 -30} {200 50} } }
   { Arc { {-30 -30} {70 70}} -37 37 }
   { Arc { {23 -70} {143 50}} 30 90 }
   { Arc { {23 -10} {143 110}} 270 330 }
   { Line {0 -20} {60 -20}}
   { Line {0 0} {65 0}}
   { Line {0 20} {70 20}}
   { Line {0 40} {65 40}}
   { Line {135 20} {200 20}}
   { Line {60 50} {83 50}}
   { Line {60 -10} {83 -10}}
   { Line {60 -10} {60 -30}}
   { EvalTextTerm cdsTerm("a") {-5 45} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("b") {-5 25} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("c") {-5 5} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("d") {-5 -15} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("z") {205 25} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {200 50} upperRight R0 stick 8 cdsSkillEvalText 0 1 1 }
}
{ xnr2 { symbol } schematicSymbol
   { PinRect a signal false input { {-4 36} {4 44} } }
   { PinRect b signal false input { {-4 -4} {4 4} } }
   { PinRect z signal false output { {196 16} {204 24} } }
   { InstRect { {0 -10} {200 50} } }
   { Arc { {-30 -30} {70 70}} -37 37 }
   { Arc { {-20 -30} {60 70}} -37 37 }
   { Arc { {23 -70} {143 50}} 30 90 }
   { Arc { {23 -10} {143 110}} 270 330 }
   { Ellipse { {135 15} {145 25} } }
   { Line {0 0} {56 0}}
   { Line {0 40} {56 40}}
   { Line {145 20} {200 20}}
   { Line {60 50} {83 50}}
   { Line {60 -10} {83 -10}}
   { EvalTextTerm cdsTerm("a") {-5 45} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("b") {-5 5} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("z") {205 25} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {200 50} upperRight R0 stick 8 cdsSkillEvalText 0 1 1 }
}
{ xnr3 { symbol } schematicSymbol
   { PinRect a signal false input { {-4 36} {4 44} } }
   { PinRect b signal false input { {-4 16} {4 24} } }
   { PinRect c signal false input { {-4 -4} {4 4} } }
   { PinRect z signal false output { {196 16} {204 24} } }
   { InstRect { {0 -10} {200 50} } }
   { Arc { {-30 -30} {70 70}} -37 37 }
   { Arc { {-20 -30} {60 70}} -37 37 }
   { Arc { {23 -70} {143 50}} 30 90 }
   { Arc { {23 -10} {143 110}} 270 330 }
   { Ellipse { {135 15} {145 25} } }
   { Line {0 0} {56 0}}
   { Line {0 20} {60 20}}
   { Line {0 40} {56 40}}
   { Line {145 20} {200 20}}
   { Line {60 50} {83 50}}
   { Line {60 -10} {83 -10}}
   { EvalTextTerm cdsTerm("a") {-5 45} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("b") {-5 25} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("c") {-5 5} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("z") {205 25} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {200 50} upperRight R0 stick 8 cdsSkillEvalText 0 1 1 }
}
{ xor2 { symbol } schematicSymbol
   { PinRect a signal false input { {-4 36} {4 44} } }
   { PinRect b signal false input { {-4 -4} {4 4} } }
   { PinRect z signal false output { {196 16} {204 24} } }
   { InstRect { {0 -10} {200 50} } }
   { Arc { {-30 -30} {70 70}} -37 37 }
   { Arc { {-20 -30} {60 70}} -37 37 }
   { Arc { {23 -70} {143 50}} 30 90 }
   { Arc { {23 -10} {143 110}} 270 330 }
   { Line {0 0} {56 0}}
   { Line {0 40} {56 40}}
   { Line {135 20} {200 20}}
   { Line {60 50} {83 50}}
   { Line {60 -10} {83 -10}}
   { EvalTextTerm cdsTerm("a") {-5 45} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("b") {-5 5} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("z") {205 25} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {200 50} upperRight R0 stick 8 cdsSkillEvalText 0 1 1 }
}
{ xor3 { symbol } schematicSymbol
   { PinRect a signal false input { {-4 36} {4 44} } }
   { PinRect b signal false input { {-4 16} {4 24} } }
   { PinRect c signal false input { {-4 -4} {4 4} } }
   { PinRect z signal false output { {196 16} {204 24} } }
   { InstRect { {0 -10} {200 50} } }
   { Arc { {-30 -30} {70 70}} -37 37 }
   { Arc { {-20 -30} {60 70}} -37 37 }
   { Arc { {23 -70} {143 50}} 30 90 }
   { Arc { {23 -10} {143 110}} 270 330 }
   { Line {0 0} {56 0}}
   { Line {0 20} {60 20}}
   { Line {0 40} {56 40}}
   { Line {135 20} {200 20}}
   { Line {60 50} {83 50}}
   { Line {60 -10} {83 -10}}
   { EvalTextTerm cdsTerm("a") {-5 45} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("b") {-5 25} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("c") {-5 5} lowerRight R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextTerm cdsTerm("z") {205 25} lowerLeft R0 stick 8 cdsSkillEvalText 0 1 1 }
   { EvalTextName cdsName() {200 50} upperRight R0 stick 8 cdsSkillEvalText 0 1 1 }
}

}
}
